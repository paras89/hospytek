<!DOCTYPE html>
<html lang="en">
<head>
<!-- Meta -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="description" content="<?php echo $meta_desc;?>">
<meta name="author" content="Hospytek">
<meta name="keywords" content="<?php echo $meta_key;?>">
<meta name="robots" content="all">
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" /> 
<title><?php echo $meta_title;?></title>
<meta content="<?php echo $meta_title;?>" property="og:title" />
<meta content="<?php echo $meta_desc;?>" property="og:description" />
<meta content="<?php echo $actual_link = (isset($_SERVER['HTTPS']) ? "https" : "http") . "://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";?>" property="og:url" />

<!-- Bootstrap Core CSS -->
<base href="<?php echo base_url();?>">
<link rel="stylesheet" href="assets/css/bootstrap.min.css">

<!-- Customizable CSS -->
<link rel="stylesheet" href="assets/css/main.css">
<link rel="stylesheet" href="assets/css/blue.css">
<link rel="stylesheet" href="assets/css/owl.carousel.css">
<link rel="stylesheet" href="assets/css/owl.transitions.css">
<link rel="stylesheet" href="assets/css/animate.min.css">
<link rel="stylesheet" href="assets/css/rateit.css">
<link rel="stylesheet" href="assets/css/bootstrap-select.min.css">
<link rel="stylesheet" href="assets/js/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.css">
<link rel="stylesheet" href="assets/js/sweetalert/sweet-alert.css">
<link href="assets/css/lity.min.css" rel="stylesheet">
<!-- Icons/Glyphs -->
<link rel="stylesheet" href="assets/css/font-awesome.css">

<!-- Fonts -->
<link href='https://fonts.googleapis.com/css?family=Roboto:300,400,500,700' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,400italic,600,600italic,700,700italic,800' rel='stylesheet' type='text/css'>
<link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="assets/css/custom.css">
<script src="assets/js/jquery-1.11.1.min.js"></script> 
<script type="text/javascript">var switchTo5x=true;</script>
<?php /* <script type="text/javascript" id="st_insights_js" src="http://w.sharethis.com/button/buttons.js?publisher=b64324f3-14d8-4cea-a6b6-9550367d02a0"></script>
<script type="text/javascript" src="http://s.sharethis.com/loader.js"></script>
<!-- Start Alexa Certify Javascript -->
<script type="text/javascript">
_atrk_opts = { atrk_acct:"E1Vno1IW1810O7", domain:"hospytek.com",dynamic: true};
(function() { var as = document.createElement('script'); as.type = 'text/javascript'; as.async = true; as.src = "https://d31qbv1cthcecs.cloudfront.net/atrk.js"; var s = document.getElementsByTagName('script')[0];s.parentNode.insertBefore(as, s); })();
</script>
<noscript><img src="https://d5nxst8fruw4z.cloudfront.net/atrk.gif?account=E1Vno1IW1810O7" style="display:none" height="1" width="1" alt="" /></noscript>
<!-- End Alexa Certify Javascript --> */?>
<script type='application/ld+json'>
{
 "@context": "http://www.schema.org",
 "@type": "Organization",
 "name": "Hospytek",
 "legalName" : "Hospytek",
 "logo": "http://hospytek.com/blog/wp-content/uploads/2017/02/hospytek1111.png",
 "url": "https://www.hospytek.com",
 "sameAs" : ["https://www.facebook.com/Hospytek/","https://twitter.com/hospytek","https://plus.google.com/+HospytekMedicalDevicesAggregator"],
 "address": {"@type": "PostalAddress","streetAddress": "Sco 10 - 11, First Floor above ICICI Bank","addressLocality": "Chandigarh - Ambala Road, Zirakpur, Punjab 140603","addressRegion": "India","addressCountry": "India"},
 "telephone" :"+91-1762-530-604",
 "email": "support(at)hospytek.com",
 "faxNumber": "+91-1762-530-604", 
 "contactPoint" : [
 { "@type" : "ContactPoint", "telephone" : "+91-1762-530-604", "contactType" : "customer support","areaServed" : "IN" }
 ]
 }
 </script>
</head>
<body class="cnt-home">
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.8";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<!-- ============================================== HEADER ============================================== -->
<header class="header-style-1"> 
  
  <!-- ============================================== TOP MENU ============================================== -->
  <div class="top-bar animate-dropdown">
    <div class="container">
      <div class="header-top-inner">
        <div class="cnt-account">
          <ul class="list-unstyled">
            <li><a href="page/aboutus"><i class="icon fa fa-shopping-cart"></i>About Hospytek</a></li>
            <li><a href="page/faqs"><i class="icon fa fa-check"></i>FAQs</a></li>
            <li><a href="http://support.hospytek.com"><i class="icon fa fa-heart"></i>Help & Support</a></li>
            <? if($urw->id>0){
			if($urw->user_type==1){ $link=base_url()."account/signin/".base64_encode("vendor")."/".base64_encode($urw->user_email);}else{ $link=base_url()."account/signin/".base64_encode("institution")."/".base64_encode($urw->user_email);}
			?><li><a href="<?=$link?>" rel="nofollow"><i class="icon fa fa-user"></i>Welcome <?=$urw->user_name?>!</a>  <a href="<?=base_url()."account/signout/"?>" rel="nofollow">(Logout)</a></strong></li><? }else{?><li><a data-toggle="modal" href="#modal-signup"><i class="icon fa fa-user"></i> Create Account</a></li> <li><a href="<?=base_url()."account/signin/"?>"><i class="icon fa fa-lock"></i>Signin</a></li><? }?>
          </ul>
        </div>
        <!-- /.cnt-account -->
        
        
        <!-- /.cnt-cart -->
        <div class="clearfix"></div>
      </div>
      <!-- /.header-top-inner --> 
    </div>
    <!-- /.container --> 
  </div>
  <!-- /.header-top --> 
  <!-- ============================================== TOP MENU : END ============================================== -->
  <div class="main-header">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-3 logo-holder"> 
          <!-- ============================================================= LOGO ============================================================= -->
          <div class="logo"> <a href="<?php echo base_url();?>"> <img src="assets/images/logo.png" alt="Hospytek - Medical Equipment Marketplace"> </a> </div>
          <!-- /.logo --> 
          <!-- ============================================================= LOGO : END ============================================================= --> 
		</div>
        <!-- /.logo-holder -->
        
        <div class="col-xs-12 col-sm-12 col-md-6 top-search-holder" style="padding-right:0; margin-right:0"> 
          <!-- /.contact-row --> 
          <!-- ============================================================= SEARCH AREA ============================================================= -->
          <div class="search-area">
            <form class="searchform" method="post" action="<?php echo base_url()."medical-equipments-search";?>">
              <div class="control-group">
                
                <input class="search-field searchbox-input typeahead" placeholder="Search medical equipments here..." value="<? echo $this->input->post('key');?>" name="key" id="key" data-provide="typeahead" autocomplete="off"/>
                <button type="submit" class="search-button" ></button>
			   </div>
            </form>
          </div>
          <!-- /.search-area --> 
          <!-- ============================================================= SEARCH AREA : END ============================================================= --> 
		  </div>
        <!-- /.top-search-holder -->
        
        <div class="col-xs-12 col-sm-12 col-md-3 animate-dropdown top-cart-row" style="padding-left:0; margin-left:0"> 
          <!-- ============================================================= SHOPPING CART DROPDOWN ============================================================= -->
          
          <div class="dropdown dropdown-cart"><a data-toggle="modal" href="#modal-callback" class="lnk-cart">
            <div class="items-cart-inner">
              <div class="basket" style="font-size:20px;"> <i class="fa fa-phone"></i> </div>
             <div class="total-price-basket" style="font-size:12px; font-weight:bold"> REQUEST CALL BACK </div>
            </div>
            </a>
          </div>
          <!-- /.dropdown-cart --> 
          
          <!-- ============================================================= SHOPPING CART DROPDOWN : END============================================================= --> </div>
        <!-- /.top-cart-row --> 
      </div>
      <!-- /.row --> 
      
    </div>
    <!-- /.container --> 
    
  </div>
  <!-- /.main-header --> 
  
  <!-- ============================================== NAVBAR ============================================== -->
  <div class="header-nav animate-dropdown">
    <div class="container" style="width:90%">
      <div class="yamm navbar navbar-default" role="navigation">
        <div class="navbar-header">
       <button data-target="#mc-horizontal-menu-collapse" data-toggle="collapse" class="navbar-toggle collapsed" type="button"> 
       <span class="sr-only">Toggle navigation</span> <span class="icon-bar"></span> <span class="icon-bar"></span> <span class="icon-bar"></span> </button>
        </div>
        <div class="nav-bg-class">
          <div class="navbar-collapse collapse" id="mc-horizontal-menu-collapse">
            <div class="nav-outer">
              <ul class="nav navbar-nav">
				<? 
				$i=0; foreach($this->config->item('topcats') as $key=>$val){ 
				?>
                <li class="dropdown yamm mega-menu"> <a href="javascript:;" data-hover="dropdown" class="dropdown-toggle" data-toggle="dropdown" title="<?php echo $key;?>"><?php echo $key; ?></a>
                  <ul class="dropdown-menu container scrolling">
                    <li>
                      <div class="yamm-content ">
                        <div class="row">
                              <?php $count = sizeof($topcats[$i]);
							  foreach($topcats[$i] as $srw=>$sval){ $count=$count+sizeof($sval['subcats']);}
							  $cols = floor($count/3);  $bal = $count%3; 
							  $k=0;foreach($topcats[$i] as $srw=>$sval){  if($k==0){ echo '<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-menu '.$count.'-'.$cols.'"><ul class="links">'; $col++; if($bal>0 && $col<=$bal){ $coll=$cols+1; }else{ $coll = $cols;}}  $k = $k+1;
							 ?>
							  <li><a href="<?php echo base_url().$sval['cat_slug'];?>" title="<?php echo ucwords(strtolower(trim($sval['cat_name'])))?>" class="mnlink"><i class="fa fa-angle-double-right"></i> <?php echo ucwords(strtolower(trim($sval['cat_name'])))?></a></li> <? if($k==$coll){ echo '</ul></div>'; $k=0;}?>
							  <? foreach($sval['subcats'] as $crw=>$scat){ if($k==0){ echo '<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-menu"><ul class="links">';  $col++;if($bal>0 && $col<=$bal){ $coll=$cols+1; }else{ $coll = $cols;} } $k=$k+1; echo '<li><a href="'.base_url().$sval['cat_slug'].'/'.$scat->cat_slug.'" class="sblink" title="'.ucwords(strtolower(trim($scat->cat_name))).'"><i class="fa fa-angle-right"></i> '.ucwords(strtolower(trim($scat->cat_name))).'</a></li>'; if($k==$cols){ echo '</ul></div>'; $k=0;} } 
							  ?>
							  <?php  if($k==$coll){ echo '</ul></div>'; $k=0;}}?>  
                          </div> 
                          <!-- /.yamm-content -->                        
                      </div>
                    </li>
                  </ul>
                </li>
				<? $i++;}?>

                <li class="dropdown yamm mega-menu"> <a href="javascript:;" data-hover="dropdown" class="dropdown-toggle" data-toggle="dropdown" title="All Medical Brands">Brands</a>
                  <ul class="dropdown-menu container scrolling">
                    <li>
                      <div class="yamm-content ">
                        <div class="row">
                              <?php $count = sizeof($topbrands);  $k=0;  
							  foreach($topbrands as $rw){ 
							  $cols = floor($count/3);  $bal = $count%3; 
							 if($k==0){ echo '<div class="col-xs-12 col-sm-6 col-md-4 col-lg-4 col-menu '.$count.'-'.$cols.'"><ul class="links">'; $col++; if($bal>0 && $col<=$bal){ $coll=$cols+1; }else{ $coll = $cols;}}  $k = $k+1;
							 ?>
							  <li><a href="<?php echo base_url().$rw->user_slug;?>" title="<?php echo ucwords(strtolower(trim($rw->user_company)))?>" class="sblink"><i class="fa fa-angle-double-right"></i> <?php echo ucwords(strtolower(trim($rw->user_company)))?></a></li> <? if($k==$coll){ echo '</ul></div>'; $k=0;}?>
							  
							  <?php  if($k==$coll){ echo '</ul></div>'; $k=0;}}?>  
                          </div> 
                          <!-- /.yamm-content -->                        
                      </div>
                    </li>
                  </ul>
				</li>		
				
              </ul>
              <!-- /.navbar-nav -->
              <div class="clearfix"></div>
            </div>
            <!-- /.nav-outer --> 
          </div>
          <!-- /.navbar-collapse --> 
          
        </div>
        <!-- /.nav-bg-class --> 
      </div>
      <!-- /.navbar-default --> 
    </div>
    <!-- /.container-class --> 
  </div>
  <!-- /.header-nav --> 
  	<!-- ============================================== NAVBAR : END ============================================== --> 
</header>
<div style="display:none">
<a href="http://www.mersinekspres.com/" title="mersin escort">mersin escort</a>
<a href="http://www.mersinpvc.com/" title="mersin escort">mersin escort</a>
<a href="http://www.mersinlitip.com/" title="mersin escort">mersin escort</a>
<a href="http://www.mersinkoleksiyon.com/" title="mersin escort">mersin escort</a>
<a href="http://www.mersinnaryapi.com/" title="mersin escort">mersin escort</a>
<a href="http://www.malatyaalsat.com/" title="malatya escort">malatya escort</a>
<a href="http://www.pornosnew.com/" title="porno izle">porno izle</a>
</div>
	<!-- ============================================== HEADER : END ============================================== -->