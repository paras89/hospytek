<!DOCTYPE html>
<html lang="en">
  
<head>
     <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<meta name="description" content="<?php echo $meta_desc;?>">
		<meta name="author" content="Hospytek">
		<meta name="keywords" content="<?php echo $meta_key;?>">
		<meta name="robots" content="all">
		<title><?php echo $meta_title;?></title>
		<!-- Bootstrap Core CSS -->
		<base href="<?php echo base_url();?>">
    <link href="https://www.hospytek.com/assets/sellers/tpl0007/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://www.hospytek.com/assets/sellers/tpl0007/css/font-awesome.min.css" rel="stylesheet">
        <script src="https://www.hospytek.com/assets/sellers/tpl0001/js/jquery.js"></script>
    <!--Main css-->
    <link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0007/css/style.css">
    <link href="https://fonts.googleapis.com/css?family=Caesar+Dressing|Montserrat:400,700|Open+Sans" rel="stylesheet">
    
    
  </head>
  <body>
      
     <section class="header">
      <div class="bg-color">
        <nav class="navbar navbar-inverse navbar navbar-fixed-top">
          <div class="container">
            <div class="navbar-header">
              <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>                        
              </button>
            <div class="logo" style="margin-top:5%;">
                                <a href="<?php echo $brand->user_slug;?>">  
                                    <?  if($brand->user_image=='') 
                                        { ?>
                                            <a href="<?php echo $brand->user_slug;?>"><?php echo $brand->user_company?></a><br/>
                                        <? } else if($brand->user_image=='no.gif') { ?>
                                    <a href="<?php echo $brand->user_slug;?>" style="text-decoration:none; color:#0b8aef;"><?php echo $brand->user_company;?></a><br/>
                                    <? }
                                        else
                                        { $imgurl= 'http://www.hospytek.com/newcrm/UserFiles/Image/'.$brand->user_image;?>
                                    <a href="<?php echo $brand->user_slug;?>"><img src="<?=$imgurl?>" alt="<?=$brand->user_company?>" style="height:70px;margin-top:5px;"/></a><br/>
                                    <? } 
                                ?>
                                   </a>
                                &nbsp;&nbsp;<span><?php echo $brand->user_city;?>, <?php echo $brand->user_state;?> <?php echo $brand->user_country;?></span></h2>
            </div>
            </div>
            <div class="collapse navbar-collapse" id="myNavbar">
              <ul class="nav navbar-nav navbar-right" style="margin-top:10px;margin-bottom:10px;">
                <li class="active"><a href="<?php echo $brand->user_slug;?>">Home</a></li>
                <li><a href="<?php echo $brand->user_slug;?>#about">About</a></li>
                <li><a href="<?php echo $brand->user_slug;?>#product">Products</a></li>
                <li><a href="<?php echo $brand->user_slug;?>#contact">Contact</a></li>
		<div style="display:inline;float:left;margin-top:15px;"><a data-toggle="modal" href="#modal-callback" class="cart-sellers"><button>Become a Dealer</button></a></div>
              </ul>
            </div>
          </div>
        </nav>
     
        <div class="container">
			<div class="row">
				<div class="col-sm-12" style="margin-top:150px;">
				
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#slider-carousel" data-slide-to="1"></li>
							<li data-target="#slider-carousel" data-slide-to="2"></li>
						</ol>
						
						<div class="carousel-inner">
						
							
						
						
						 <?php $i=0; foreach ($items as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;} ;
						  if($i>6){ break;}
						  ?>				
						  <?php if ($i==0) { $i++; ?>
        			  <div class="item active"> 
								<div class="col-sm-6" style="margin-top:150px">
									<h1><?php echo $rw->p_title." ".$rw->vr_name;?></h1>
									<h3 ><strong>Model:</strong> <?php echo $rw->p_model;?></h3>
									<p><?php echo $rw->p_sdesc;?></p>
									
									<button type="button" class="btn btn-default get"><a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online">View Detail</a></button>
									<button type="button" class="btn btn-default get"><a href="<?php echo '../pdf/'.$rw->p_slug.'.pdf'?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Brochure Manual Download">View Brochure</a></span></button>
								</div>
								<div class="col-sm-6">
									<img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="girl img-responsive" alt="" style="width:100%;height: 350px;"/>
									
								</div>
							</div>
							<?php } else  {  ?>
							
							<div class="item"> 
								<div class="col-sm-6" style="margin-top:150px">
									<h1><?php echo $rw->p_title." ".$rw->vr_name;?></h1>
									<h3><strong>Model:</strong> <?php echo $rw->p_model;?></h3>
									<p><?php echo $rw->p_sdesc;?></p>
									
									<button type="button" class="btn btn-default get"><a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online">View Detail</a></button>
									<button type="button" class="btn btn-default get"><a href="<?php echo '../pdf/'.$rw->p_slug.'.pdf'?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Brochure Manual Download">View Brochure</a></span></button>
								</div>
								<div class="col-sm-6">
									<img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="girl img-responsive" alt="" style="width:100%;height: 350px;" />
									
								</div>
							</div>
							<?php  } ?>
							
							
							
							<?php  } ?>
							
							
							
						</div>
						
						
                     <a href="#slider-carousel" class="left control-carousel pull-left" data-slide="prev">
                            <i class="fa fa-angle-left" style="font-size:50px;"></i>
                        </a>
                        <a href="#slider-carousel" class="right control-carousel pull-right" data-slide="next">
                            <i class="fa fa-angle-right" style="font-size:50px;"></i>
                        </a>


					</div>
					
				</div>
			</div>
		</div>
      </div>
    </section>
  
  
    <section id="about" class="section-padding" style="background:linear-gradient(to bottom right, #66ffcc 4%, #ccffcc 100%)">
      <div class="container">
        <div class="row">
		<div class="page-title text-center">
            <h1>WELCOME TO <?=$brand->user_company?></h1>
            <hr class="pg-titl-bdr-btm">
          </div>
          <div class="col-md-12">
           
			  <p><?=$brand->user_about?></p>
          
        </div>
      </div>
      </div>
    </section>
    <section id="product" class="section-padding">
      <div class="container">
        <div class="row">
          <div class="page-title text-center">
            <h1>Products</h1>
            <hr class="pg-titl-bdr-btm">
          </div>
          <div class="port-sec">
            <div id="Container">
			 <?php $i=0; foreach ($items as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;}  $i++;?>
				<?php if($i==1){echo '<div class="row">';}?>
                  <div class="filimg col-sm-4 col-xs-12" data-myorder="2" style="text-align:center;">
                    <img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" style="width:100%; height:350px;border:solid 2px rgba(128, 128, 128, 0.33);">
					<p><a href="#"><?php echo $rw->p_title." ".$rw->vr_name;?></a></p>	
					<h6><a href="#"><?php echo $rw->p_model;?></a></h6>
					<a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online"><button type="button" class="btn btn-primary">View Detail</button></a>
                  </div>
				  
				   <?php if($i==3){ $i=0; echo '</div><div class="clearfix">&nbsp; </div>';}?>
                    <? }?>
				  
             
            </div>
          </div>
        </div>
      </div>
    </section>
     <?php
        if(isset($_POST['submit'])){
            if($rw->user_email!=''){
            $to=$rw->user_email;    
            }else{
                $to=$rw->user_id;
            }
            //$to='msewmanan@gmail.com,tbcpl.shridhar@gmail.com';
            $from=$_POST['email'];
            $subject = 'Enquiry made at your Website';
            $name=strip_tags($_POST['cname']);
            $subject = 'Enquiry made at your Website';
            $headers = "From: " . strip_tags($_POST['email']) . "\r\n";
            $headers .= "Reply-To: ". strip_tags($_POST['email']) . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";
            $headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
            $message = '<html><body>';
            $message .= '<h3>Hello, '.$rw->user_name.' !</h3>';
            $message .= '<p style="tex-align:center;">'.strip_tags($_POST['msg'])."\r\n".'</p><br>';
            $message .= 'For further details, please feel free to contact at';
            $message .= '<br><h3>'.$name.'</h3><h4>'.$from.'</h4>';
            $message .= '</body></html>';
            mail($to,$subject,$message,$headers);
        }
    ?>
    <section id="contact" class="section-padding" style="background:linear-gradient(to right, #66ffff 0%, #ccffcc 100%)">
        <div class="container">
          <div class="row">
            <div class="page-title text-center">
            <h1>Contact</h1>
            <hr class="pg-titl-bdr-btm">
          </div>
          <div class="col-md-4 col-sm-6 contact-section">
            <h3>Contact Info</h3>
            <div class="space"></div>
            <i class="fa fa-map-marker fa-fw pull-left fa-2x"></i><p><?=$brand->user_address?> &nbsp;<?=$brand->user_city?>, &nbsp; <?=$brand->user_state?> &nbsp;
																	<?=$brand->user_country?> &nbsp;  <?=$brand->user_postcode?></p><br>
           
            <p><i class="fa fa-phone fa-fw pull-left fa-2x"></i></i>+91-<?=$brand->user_mobile?>     <?=$brand->user_phone?></p>
          </div>
          <div class="col-md-8 col-sm-6 contact-section">
            <h3>Leave us a message</h3>
            <form name="sentMessage" id="contactForm" novalidate="" action="" method="POST">
              <div class="row">
                <div class="col-md-6">
                  <div class="form-group">
                    <input id="name" name="cname" class="form-control" placeholder="Name" required="required" type="text">
                    <p class="help-block text-danger"></p>
                  </div>
                </div>
                <div class="col-md-6">
                  <div class="form-group">
                    <input id="email" name="email" class="form-control" placeholder="Email" required="required" type="email">
                    <p class="help-block text-danger"></p>
                  </div>
                </div>
              </div>
              <div class="form-group">
                <textarea id="message" name="msg" class="form-control" rows="4" placeholder="Message" required=""></textarea>
                <p class="help-block text-danger"></p>
              </div>
              <div id="success"></div>
              <button type="submit" class="btn btn-default" name="submit">Send Message</button>
            </form>
          </div>
        </div>
      </div>    
    </section>
  
    <div class="footer-bottom">
      <div class="container">
        <div style="visibility: visible; animation-name: zoomIn;" class="col-md-12 text-center wow zoomIn">
          <div class="footer_copyright">
            <p>powered by <a href="http://www.hospytek.com/" ><img src="<?php echo base_url();?>/assets/images/logo.png" alt="logo"></a> </p>
           
          </div>
        </div>
      </div>
    </div>
    
    
    <!-- Modal Area -->
   
    
  <div class="modal fade" id="modal-callback" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h2 class="modal-title">Please Fill the form & we will call you back</h2>
                          </div>
                          <div class="modal-body">
						  <form method="post" action="<?php echo base_url()."api";?>" class="form-ajax">
						  <input type="hidden" name="module" value="callback" />
						  <input type="hidden" name="type" value="0" />
						  <div class="row">
						  <div class="col-md-6">
						  	<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Name <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input" placeholder="" name="name" required>
							</div>

							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Phone <span>*</span></label>
		    <input type="phone" class="form-control unicase-form-control text-input"  placeholder=""  name="mobile" required>
							</div>
							
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Email <span>*</span></label>
		    <input type="email" class="form-control unicase-form-control text-input"  placeholder=""  name="email" required>
						   </div>	
							
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Best time to call you <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input"  placeholder="" name="timetocall" required>
						   </div>	


							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Message <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input"  placeholder=""  name="message" required>
						   </div>	


	<div class="form-group">
		<button type="submit" class="btn-upper btn btn-primary checkout-page-button">Submit Request</button>
	</div>
			
						  </div>

						  <div class="col-md-6" style="font-size:12px;">
						   <p><strong>Lets give you a call back to help you buy the most suitable Medical Equipment & Device as per your requirement, We would not only help you choose the most suitable medical device but also help you find the right localised vendor to make your after sale requirements more readily available.</strong></p>
						  <center><img src="assets/images/callback-icon.png" class="img-responsive" style="max-height:175px;" /></center>
						  <p>We at Hospytek hold immense value for every customer or prospective customer and consider it as our utmost duty to provide a solution to all the queries raised by the customers.</p>

<p>We thank you for filling the form and assure you that one of our sales executives would get back to you very soon to understand your problem and provide the most reasonable solution.</p>

<p><strong>We again thank you for your patience and hope you are your experience on our website.</strong></p>
						  </div>
						  
                          </div>
						  </form>
                      </div>
                  </div>
				  </div>
              </div>
    <!-- / End of modal area -->
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="../../../../../ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="https://www.hospytek.com/assets/sellers/tpl0007/js/bootstrap.min.js"></script>
    <script>
      $(document).ready(function(){
    
        $(".navbar a, footer a[href='#myPage'], .overlay-detail a").on('click', function(event) {

        event.preventDefault();

     
        var hash = this.hash;

        $('html, body').animate({
          scrollTop: $(hash).offset().top
        }, 900, function(){

        
          window.location.hash = hash;
          });
        });
      })
</script>

<script type="text/javascript">
$('.carousel').carousel({
  interval: 100;
});
</script>
  </body>
</html>
