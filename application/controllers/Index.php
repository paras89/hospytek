<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 	
 * 	@author : Hospytek
 * 	20th Sep, 2016
 * 	IT Team
 * 	www.hospytek.com
 * 	http://vendor.hospytek.com
 */

class Index extends CI_Controller {
    function __construct() {
        parent::__construct();
		
        $this->load->model('crud_model');
		$this->load->model('apicall');
        $this->load->database();
        $this->load->library('session');
        /* cache control */
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header("Expires: Mon, 26 Jul 2010 05:00:00 GMT");				
		if($this->session->userdata('user_uid')>0){
		$urw = $this->apicall->logged_user();
		}else{
		$uinfo = $this->input->cookie('uinfo');
		if($uinfo>0){	$urw = $this->apicall->get_user($uinfo); }
		}
		$menu = array(); $i=0;
		foreach($this->config->item('topcats') as $key=>$val){
			if($val==''){ $val=0; }
			$row = $this->apicall->master_categories($val);
			$row_array = array(); $return = array();						
			foreach($row as $rw){
			    $row_array['cat_id'] = $rw->cat_id;
				$row_array['cat_name'] = $rw->cat_name;			
				$row_array['cat_slug'] = $rw->cat_slug;			
				$row_array['subcats'] = $this->apicall->categories(1,$rw->cat_id,0,500);
                array_push($return,$row_array);
			}
			$menu[$i] = $return; $i++; 								
		}	
			$topbrands =$this->apicall->allbrands();
			$this->data = array('urw'=> $urw, 'topcats'=> $menu, 'topbrands'=> $topbrands);
		
    }

    //Default function, redirects to logged in user area
    public function index() {
        $data = $this->data;
		$data['meta_title'] = "Medical Equipments Marketplace for Vendors Suppliers Dealers Brands Manufacturers in India";
		$data['meta_desc'] ="Worldwide Marketplace of Medical Equipments, Devices & Products Industry for importers, vendors, suppliers & manufacturers. Buy Medical Equipments & Devices on a single click at Hospytek.";
		$newitems = array();
		foreach($this->config->item('newcats') as $key=>$val){ 
		$newitems[$key]=$this->apicall->homepage_products('new', $val, 8);
		}
		$data['newitems'] = $newitems;
		$data['new']=$this->apicall->homepage_products('new','32,18,15', 8);
		$data['featured'] = $this->apicall->homepage_products('featured', 0, 8);
		$data['onsale'] = $this->apicall->homepage_products('onsale', 0, 8);
		//$data['blogs'] = $this->wordpress->wp_posts(4);			
		$data['home'] = 1;
		$this->load->vars($data);
        $this->load->view('home');
    }


    public function products() {
          if(strpos($this->uri->segment(1), '_') !== false) { $uri=explode('_',$this->uri->segment(1)); $brnd=$uri[0]; $uri1 = $uri[1]; 
			$brand=$this->apicall->user_byslug($brnd); $data['brand'] = $brand; $_POST['brand']=$brand->id;
		  }else{ $uri1 =$this->uri->segment(1);  
		  $brand=$this->apicall->user_byslug($uri1); 
		  }
		if(strpos($this->uri->segment(2), '_') !== false) { $uri=explode('_',$this->uri->segment(2)); $brnd=$uri[0]; $uri2 = $uri[1];  }else{ $uri2 =$this->uri->segment(2);  }
		$uri3 = $this->uri->segment(3);
		$uri4 = $this->uri->segment(4);		
	    $data = $this->data;
		
		
		if($brand->id>0 && $this->uri->segment(2)==''){
		$title = $brand->user_company;
		$data['brand'] = $brand;
		$_POST['brand']=$brand->id; 
		$items = $this->apicall->products_bycat(0, 150);
		$data['items']= $items;
		$j=0;
		foreach($items as $rw){ 
			if($j<5){$keys .= $rw->p_title." ".$rw->vr_name." ";}else{ break;}  
			if($j==0){ $keyss=$rw->p_title." ".$rw->vr_name." ";}
			$j++;
		}
		
		
		$data['meta_title'] = ucwords(strtolower(trim($title." ". $brand->user_city." - ".$keyss." Manufacturer Supplier")));	
		$data['meta_desc'] = ucwords(strtolower(trim($title.", ".$brand->user_city ." Manufacturer Supplier of ".$keys."in ".$brand->user_city)));			
		
		$this->load->vars($data);
		$this->apicall->insert_views($brand->id, 0, 0, 0, 0);
		
		if($brand->user_tpl==''){
        $this->load->view('sellers/default');
		}else{
		$tpl= "sellers/".$brand->user_tpl;
		$this->load->view($tpl);
		}
		
		}else{
	
	if($uri4==''){
		$category=$this->apicall->category_byslug($uri1); $data['category'] = $category; $_POST['cat']=$category->cat_id; if($uri2==''){$title .= " ".$category->cat_name;}
		if($uri2!=''){ $scategory=$this->apicall->category_byslug($uri2); $data['scategory'] = $scategory;  $_POST['subcat']=$scategory->cat_id; $title .= " ".$brand->user_company." ".$scategory->cat_name;}
		if($uri3!=''){ $variation=$this->apicall->variation_byslug($uri3); $data['variation'] = $variation;  $_POST['vrid']=$variation->vr_id;  $title .= " ".$variation->vr_name;}
				
		$data['meta_title'] = ucwords(strtolower(trim($title ." Medical Equipment Vendors Suppliers Dealers Brands Manufacturers in India")));	
		$data['meta_desc'] = ucwords(strtolower(trim($title ." Medical Equipment Vendors Suppliers Dealers Brands Manufacturers in India")));			
		$items = $this->apicall->products_bycat(0, 15);
		$data['items']= $items;
		
		
		 if($scategory->cat_id==''){
			$row = $this->apicall->categories(1,$category->cat_id,0,500);
			$row_array = array(); $return = array();						
			foreach($row as $rw){
			    $row_array['cat_id'] = $rw->cat_id;
				$row_array['cat_name'] = $rw->cat_name;			
				$row_array['cat_slug'] = $rw->cat_slug;			
				$row_array['vars'] = $this->apicall->variations($rw->cat_id,0,500);
                array_push($return,$row_array);
			}
			$data['cats']=$return;
			$brands = $this->apicall->brands(0, $category->cat_id);
		}else{
		$data['vars']=$this->apicall->variations($scategory->cat_id,0,500);
		$brands = $this->apicall->brands(1, $scategory->cat_id);
		}
			$data['brands'] = $brands;	

		if($category->cat_id>0){		
		$this->load->vars($data);
		$this->apicall->insert_views($brand->id, $category->cat_id, $scategory->cat_id, $variation->vr_id, 0);
        $this->load->view('products');
		}else{
		$this->load->vars($data);
		$data['meta_title'] = "404 - Page not Found";	
		$data['meta_desc'] = "404 - Page not Found";			
		redirect(base_url().'page/404', 'refresh');
		}
		
}else{	
		$item = $this->apicall->product_byslug($uri4);
		$data['item']= $item;
		$data['meta_title'] = ucwords(strtolower(trim($item->user_company." - ". $item->p_model."  ". $item->p_title." ". $item->vr_name." ". $item->cat_name." Medical Equipment Vendors Suppliers Dealers")));	
		$data['meta_desc'] = ucwords(strtolower(trim($item->user_company." - ". $item->p_model."  ". $item->p_title." ". $item->vr_name." ". $item->cat_name." Medical Equipment Vendors Suppliers Dealers")));	


		$data['vendors_count'] = $this->apicall->vendors_count($id);
		$_POST['subcat'] = $item->cat_sid; $morecat = $this->apicall->products(0, 8);
		$_POST['brand'] = $item->p_brand; $morebrand = $this->apicall->products(0, 8);
		$data['morebrand'] = $morebrand;
		$data['morecat'] = $morecat;
		$this->load->vars($data);
		$this->apicall->insert_views($item->p_brand, $item->catid, $item->cat_id, $item->p_vrid, $item->p_id);
        $this->load->view('product');
}}
    }



    public function search() {
		$data = $this->data;
		$data['meta_title'] = ucwords(strtolower(trim($title ." dealers, suppliers, importers, manufacturers and vendors for hospitals clinics diagnostic labs in india Buy online")));	
		$data['meta_desc'] = ucwords(strtolower(trim($title ." dealers, suppliers, importers, manufacturers and vendors for hospitals clinics diagnostic labs in india Buy online")));	
		$data['items']=$this->apicall->products(0, 15);
		$key = $this->input->post('key');
		$row = $this->apicall->keyfinder('cats'); 

			$row_array = array(); $return = array();						
			foreach($row as $rw){
			    $row_array['cat_id'] = $rw->cat_id;
				$row_array['cat_name'] = $rw->cat_name;			
				$row_array['cat_slug'] = $rw->cat_slug;			
				$row_array['vars'] = $this->apicall->variations($rw->cat_id,0,500);
                array_push($return,$row_array);
			}
			$data['cats']=$return;
		$brands = $this->apicall->keyfinder('brands'); 
		$data['brands'] = $brands;			
		$this->load->vars($data);
        $this->load->view('products-search');
    }

/*******************CLIENT MANAGEMENT ******************/
//new/modify user page

//List clients

    function clients(){
	    $data = $this->data;
		$data['users'] = $this->apicall->clients();
		$this->load->vars($data);
		$this->load->view('main/clients');
    }

//new/modify user page
    function newclient(){
		$id = $this->uri->segment(2);
	    $data = $this->data;
		$data['user'] = $this->apicall->client($id);
		$this->load->vars($data);
		$this->load->view('main/newclient');
    }
// save user form action
    function save_client(){
            $this->form_validation->set_rules('name', 'Full Name', 'required|min_length[5]|max_length[25]'); 
            $this->form_validation->set_rules('company', 'Company Name', 'required|min_length[5]|max_length[100]'); 
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email'); 
            $this->form_validation->set_rules('mobile', 'Mobile No.', 'required|alpha_numeric|max_length[10]'); 
            $this->form_validation->set_rules('address', 'Address', 'required|max_length[100]'); 
            $this->form_validation->set_rules('city', 'City', 'required|max_length[50]'); 
            $this->form_validation->set_rules('state', 'State', 'required|max_length[50]'); 
            $this->form_validation->set_rules('country', 'Country', 'required|max_length[70]'); 
            $this->form_validation->set_rules('zip', 'Zip Code', 'required|max_length[6]'); 
			$this->form_validation->set_rules('taxno', 'TAX/VAT No.', 'alpha_numeric|min_length[6]|max_length[20]'); 

			if ($this->form_validation->run() == FALSE){
				echo "0|".validation_errors();
							
			}else{
			$this->apicall->save_client();
				echo "1";
			}	
    }

// Remove user
    function remove_client(){
			$this->apicall->remove_client();
			echo "1";
    }

}
