<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 	
 * 	@author : Hospytek
 * 	20th Sep, 2016
 * 	IT Team
 * 	www.hospytek.com
 * 	http://institution.hospytek.com
 */

class Login extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('crud_model');
        $this->load->database();
        $this->load->library('session');
        /* cache control */
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header("Expires: Mon, 26 Jul 2010 05:00:00 GMT");
    }

    //Default function, redirects to logged in user area
    public function index() {
		if($this->session->userdata('user_utype')==1){
        if ($this->session->userdata('user_type') == 1)
            redirect(base_url(), 'refresh');

        if ($this->session->userdata('user_type') == 2)
            redirect(base_url() . 'user', 'refresh');
		}else{
        $this->load->view('landing/index');
		}
    }

    //Ajax login function 
    function ajax_login(){
        $response = array();
        //Recieving post input of email, password from ajax request
        $email = $_POST["email"];
        //$password = sha1($_POST["password"]);
		$password = $_POST["password"];
        $response['submitted_data'] = $_POST;

        //Validating login
		$response['msg'] = "<span style='color:red;'>Login Failed, Try again.</span>";
        $login_status = $this->validate_login($email, $password);
        $response['status'] = $login_status;
        if ($login_status == 'success') {
            if($this->session->userdata('user_type')==1){$response['redirect_url'] = '';}
			if($this->session->userdata('user_type')==2){$response['redirect_url'] = 'indent';}
			$response['msg'] = "<span style='color:green;'>Login successful...redirecting.</span>";
        }

        //Replying ajax request with validation response
        echo json_encode($response);
    }

    //Validating login from ajax request
    function validate_login($email = '', $password = '') {
        $credential = array('user_id' => $email, 'user_pass' => $password, 'user_status' => 'Active');
        // Checking login credential for users
         $query = $this->db->query("select * from users where user_id='$email' and user_pass='$password' and user_status='Active' and (user_type=1 or id in (select id from users where user_type=6 and user_owner in (select id from users where user_type=1 and user_status='Active')))");
        if ($query->num_rows() > 0) {
            $row = $query->row();
			if($row->user_type==1 || $row->user_type==6){	
            $this->session->set_userdata('user_login', '1');
			$this->session->set_userdata('user_utype', '1'); // fixed for Vendors
            $this->session->set_userdata('user_uid', $row->id);
            $this->session->set_userdata('user_id', $row->user_id);
			$this->session->set_userdata('user_owner', $row->user_owner);
            $this->session->set_userdata('user_company', $row->user_company);
            $this->session->set_userdata('user_name', $row->user_name);
            if($row->user_type==1){$this->session->set_userdata('user_type', '1'); }else{ $this->session->set_userdata('user_type', '2');}
            return 'success';
			}else{
			return 'invalid';
			}	
        }


        return 'invalid';
    }

    /*     * *DEFAULT NOR FOUND PAGE**** */

    function four_zero_four() {
        $this->load->view('four_zero_four');
    }

    // PASSWORD RESET BY EMAIL
    function forgot_password()
    {
        $this->load->view('backend/forgot_password');
    }

    function ajax_forgot_password()
    {
        $resp                   = array();
        $resp['status']         = 'false';
        $email                  = $_POST["email"];
        $reset_account_type     = '';
        //resetting user password here
        $new_password           =   substr( md5( rand(100000000,20000000000) ) , 0,7);

        // Checking credential for admin
        $query = $this->db->get_where('admin' , array('email' => $email));
        if ($query->num_rows() > 0) 
        {
            $reset_account_type     =   'admin';
            $this->db->where('email' , $email);
            $this->db->update('admin' , array('password' => sha1($new_password)));
            $resp['status']         = 'true';
        }
        // Checking credential for student
        $query = $this->db->get_where('student' , array('email' => $email));
        if ($query->num_rows() > 0) 
        {
            $reset_account_type     =   'student';
            $this->db->where('email' , $email);
            $this->db->update('student' , array('password' => sha1($new_password)));
            $resp['status']         = 'true';
        }
        // Checking credential for teacher
        $query = $this->db->get_where('teacher' , array('email' => $email));
        if ($query->num_rows() > 0) 
        {
            $reset_account_type     =   'teacher';
            $this->db->where('email' , $email);
            $this->db->update('teacher' , array('password' => sha1($new_password)));
            $resp['status']         = 'true';
        }
        // Checking credential for parent
        $query = $this->db->get_where('parent' , array('email' => $email));
        if ($query->num_rows() > 0) 
        {
            $reset_account_type     =   'parent';
            $this->db->where('email' , $email);
            $this->db->update('parent' , array('password' => sha1($new_password)));
            $resp['status']         = 'true';
        }

        // send new password to user email  
        $this->email_model->password_reset_email($new_password , $reset_account_type , $email);

        $resp['submitted_data'] = $_POST;

        echo json_encode($resp);
    }

    /*     * *****LOGOUT FUNCTION ****** */

    function logout() {
		$this->input->delete_cookie('uinfo'); 	
        $this->session->sess_destroy();
        $this->session->set_flashdata('logout_notification', 'logged_out');
        redirect(base_url().'signin', 'refresh');
    }

}
