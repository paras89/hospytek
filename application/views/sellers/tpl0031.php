<!doctype html>
<html class="no-js" lang="">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
        <meta name="description" content="<?php echo $meta_desc;?>">
        <meta name="author" content="Hospytek">
        <meta name="keywords" content="<?php echo $meta_key;?>">
        <meta name="robots" content="all">
        <title><?php echo $meta_title;?></title>
        <!-- Bootstrap Core CSS -->
        <base href="<?php echo base_url();?>">
        <link rel="apple-touch-icon" href="apple-touch-icon.html">
        <script src="https://www.hospytek.com/assets/sellers/tpl0001/js/jquery.js"></script>
		
        <!-- Place favicon.ico in the root directory -->
        <!-- Font -->
        <link href='https://fonts.googleapis.com/css?family=Open+Sans:400,300,300italic,400italic,600,700,600italic,700italic,800,800italic' rel='stylesheet' type='text/css'>
        <link href='https://fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
        <!-- Font -->
        <link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0031/css/normalize.css">
        <link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0031/css/main.css">
        <link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0031/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0031/css/animate.css">
        <link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0031/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0031/css/style.css">
        <link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0031/css/responsive.css">
     
		
        <script src="https://www.hospytek.com/assets/sellers/tpl0031/js/vendor/modernizr-2.8.3.min.js"></script>
    </head>
    <body>
        <!-- Header Start -->
        <header id="home">
            <!-- Main Menu Start -->
            <div class="main-menu">
                <div class="navbar-wrapper">
                    <div class="navbar navbar-inverse navbar-fixed-top" role="navigation" style="background-color:#03a9f4;">
                        <div class="container">
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                <span class="sr-only">Toggle Navigation</span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                <span class="icon-bar"></span>
                                </button>
                                <div class="logo">
                                <a href="<?php echo $brand->user_slug;?>">  
                                    <?  if($brand->user_image=='') 
                                        { ?>
                                            <a href="<?php echo $brand->user_slug;?>"><?php echo $brand->user_company?></a><br/>
                                        <? } else if($brand->user_image=='no.gif') { ?>
                                    <a href="<?php echo $brand->user_slug;?>" style="text-decoration:none; color:#ffffff"><?php echo $brand->user_company;?></a><br/>
                                    <? }
                                        else
                                        { $imgurl= 'http://www.hospytek.com/newcrm/UserFiles/Image/'.$brand->user_image;?>
                                    <a href="<?php echo $brand->user_slug;?>"><img src="<?=$imgurl?>" alt="<?=$brand->user_company?>" style="height:70px;margin-top:5px;"/></a><br/>
                                    <? } 
                                ?>
                                   </a>
                                &nbsp;&nbsp;<span><?php echo $brand->user_city;?>, <?php echo $brand->user_state;?> <?php echo $brand->user_country;?></span></h2>
                        </div>
                            </div>
                            <div class="navbar-collapse collapse">
                                <ul class="nav navbar-nav navbar-right">
                                    <li><a href="<?php echo $brand->user_slug;?>">Home</a></li>
                                    <li><a href="<?php echo $brand->user_slug;?>#about">About</a></li>
                                    <li><a href="<?php echo $brand->user_slug;?>#product">Products</a></li>
                                    <li><a href="<?php echo $brand->user_slug;?>#contact-us">Contact</a></li>
                                    <div style="display:inline;float:left;margin-top:10px;padding-left:20px"><a data-toggle="modal" href="#modal-callback" class="cart-sellers"><button style="background:#00d4d4"
                                        >Become a Dealer</button></a></div>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Main Menu End -->
            <!-- Sider Start -->
            <div id="home" class="home-section-wrap center">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-12">
                            <div id="slider-carousel" class="carousel slide" data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
                                    <li data-target="#slider-carousel" data-slide-to="1"></li>
                                    <li data-target="#slider-carousel" data-slide-to="2"></li>
                                </ol>
                                <div class="carousel-inner">
                                    <?php $i=0; foreach ($items as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;} ;
                                        if($i>6){ break;}
                                        ?>				
                                    <?php if ($i==0) { $i++; ?>
                                    <div class="item active">
                                        <div class="col-sm-6" style="margin-top:200px">
                                            <h1><?php echo $rw->p_title." ".$rw->vr_name;?></h1>
                                            <h3 ><strong>Model:</strong> <?php echo $rw->p_model;?></h3>
                                            <p><?php echo $rw->p_sdesc;?></p>
                                            <button type="button" class="btn btn-default get"><a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online">View Detail</a></button>
                                            <button type="button" class="btn btn-default get"><a href="<?php echo '../pdf/'.$rw->p_slug.'.pdf'?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Brochure Manual Download">View Brochure</a></span></button>
                                        </div>
                                        <div class="col-sm-6">
                                            <img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="girl img-responsive" alt=""  />
                                        </div>
                                    </div>
                                    <?php } else  {  ?>
                                    <div class="item">
                                        <div class="col-sm-6" style="margin-top:200px">
                                            <h1><?php echo $rw->p_title." ".$rw->vr_name;?></h1>
                                            <h3><strong>Model:</strong> <?php echo $rw->p_model;?></h3>
                                            <p><?php echo $rw->p_sdesc;?></p>
                                            <button type="button" class="btn btn-default get"><a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online">View Detail</a></button>
                                            <button type="button" class="btn btn-default get"><a href="<?php echo '../pdf/'.$rw->p_slug.'.pdf'?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Brochure Manual Download">View Brochure</a></span></button>
                                        </div>
                                        <div class="col-sm-6">
                                            <img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="girl img-responsive" alt="" />
                                        </div>
                                    </div>
                                    <?php  } ?>
                                    <?php  } ?>
                                </div>
                                <a href="#slider-carousel" class="left control-carousel pull-left" data-slide="prev">
                                <i class="fa fa-angle-left" style="color:grey;font-size:50px"></i>
                                </a>
                                <a href="#slider-carousel" class="right control-carousel pull-right" data-slide="next">
                                <i class="fa fa-angle-right" style="color:grey;font-size:50px"></i>
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Sider End -->
        </header>
        <!-- Header End -->
        <!-- About Section -->
        <section id="about" class="site-padding">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <h3 align="center">WELCOME TO <?=$brand->user_company?></h3>
                        <p><?=$brand->user_about?></p>
                    </div>
                </div>
            </div>
        </section>
        <!-- About Section -->
        <!-- product  -->
        <section id="product" class="protfolio-padding">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="title">
                            <h3>Our Products</span></h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="product-list">
                <div id="grid" class="clearfix" style="text-align:center">
                    <?php $i=0; foreach ($items as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;}  $i++;?>
                    <?php if($i==1){echo '<div class="row" style="margin-left:38px;margin-right:38px">';}?>
                    <div class="thumb col-md-4">
                        <img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>">					
                        <h5><a href="#"><?php echo $rw->p_title." ".$rw->vr_name;?></a></h5>
                        <h6><a href="#"><?php echo $rw->p_model;?></a></h6>
                        <a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online"><button type="button" class="btn btn-primary">View Detail</button></a>
                    </div>
                    <?php if($i==3){ $i=0; echo '</div>';}?>
                    <? }?>
                </div>
            </div>
        </section>
        <!-- product -->
        <!-- Contact -->
        <section id="contact-us">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="title">
                            <h3>Contact <span>Us</span></h3>
                        </div>
                    </div>
                </div>
            </div>
            <div class="contact">
                <div class="container">

                        <div class="col-md-8" style="float:left">
                            <h4>Please Contact With Us For Any Kind Of Information</strong></h4>
                            <form id="contactform" action="#" method="post" class="validateform" name="send-contact">
                                <div class="row">
                                    <div class="col-lg-4 field">
                                        <input type="text" name="name" placeholder="* Your Name" data-rule="maxlen:4" data-msg="Please enter at least 4 chars" />
                                        <div class="validation">
                                        </div>
                                    </div>
                                    <div class="col-lg-4 field">
                                        <input type="text" name="email" placeholder="* Your Email" data-rule="email" data-msg="Please enter a valid email" />
                                        <div class="validation">
                                        </div>
                                    </div>
                                    <div class="col-lg-4 field">
                                        <input type="text" name="subject" placeholder="Subject" data-rule="maxlen:4" data-msg="Please enter at least 4 chars" />
                                        <div class="validation">
                                        </div>
                                    </div>
                                    <div class="col-lg-12 margintop10 field">
                                        <textarea rows="12" name="message" class="input-block-level" placeholder="* Your message here..." data-rule="required" data-msg="Please write something"></textarea>
                                        <div class="validation">
                                        </div>
                                        <p>
                                            <button class="btn btn-theme margintop10 pull-left" type="submit">Submit message</button>
                                        </p>
                                    </div>
                                </div>
                            </form>
                        </div>
                        <div clas="col-md-4">
                            <h3><strong>Address:</strong></h3>
                            <p><i class="fa fa-pencil"></i><?=$brand->user_address?>,<br/><?=$brand->user_city?>, <?=$brand->user_state?></p>                        
                            <p><?=$brand->user_country?>  <?=$brand->user_postcode?></p>                            
                            <p><i class="fa fa-phone"></i>+91-<?=$brand->user_mobile?> <?=$brand->user_phone?></p>
                        </div>
                   
                </div>
            </div>
        </section>
        <!-- Contact -->
        <!-- Copyright -->
        <div class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="copy-text">
                            <p>powered by <a href="https://www.hospytek.com/" ><img src="<?php echo base_url();?>/assets/images/logo.png" alt="logo"></a> </p>
                        </div>
                    </div>
                  
                </div>
            </div>
        </div>
        
        
         <div class="modal fade" id="modal-callback" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h2 class="modal-title">Please Fill the form & we will call you back</h2>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="<?php echo base_url()."api";?>" class="form-ajax">
                            <input type="hidden" name="module" value="callback" />
                            <input type="hidden" name="type" value="0" />
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="info-title" for="exampleInputName">Your Name <span>*</span></label>
                                        <input type="text" class="form-control unicase-form-control text-input" placeholder="" name="name" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="info-title" for="exampleInputName">Your Phone <span>*</span></label>
                                        <input type="phone" class="form-control unicase-form-control text-input"  placeholder=""  name="mobile" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="info-title" for="exampleInputName">Your Email <span>*</span></label>
                                        <input type="email" class="form-control unicase-form-control text-input"  placeholder=""  name="email" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="info-title" for="exampleInputName">Best time to call you <span>*</span></label>
                                        <input type="text" class="form-control unicase-form-control text-input"  placeholder="" name="timetocall" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="info-title" for="exampleInputName">Your Message <span>*</span></label>
                                        <input type="text" class="form-control unicase-form-control text-input"  placeholder=""  name="message" required>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn-upper btn btn-primary checkout-page-button">Submit Request</button>
                                    </div>
                                </div>
                                <div class="col-md-6" style="font-size:12px;">
                                    <p><strong>Lets give you a call back to help you buy the most suitable Medical Equipment & Device as per your requirement, We would not only help you choose the most suitable medical device but also help you find the right localised vendor to make your after sale requirements more readily available.</strong></p>
                                    <center><img src="assets/images/callback-icon.png" class="img-responsive" style="max-height:175px;" /></center>
                                    <p>We at Hospytek hold immense value for every customer or prospective customer and consider it as our utmost duty to provide a solution to all the queries raised by the customers.</p>
                                    <p>We thank you for filling the form and assure you that one of our sales executives would get back to you very soon to understand your problem and provide the most reasonable solution.</p>
                                    <p><strong>We again thank you for your patience and hope you are your experience on our website.</strong></p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        
        <script src="../../../../code.jquery.com/jquery-1.12.0.min.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.12.0.min.js"><\/script>')</script>
        <script src="https://www.hospytek.com/assets/sellers/tpl0031/js/plugins.js"></script>
        <script src="https://www.hospytek.com/assets/sellers/tpl0031/js/bootstrap.min.js"></script>
        <script src="https://www.hospytek.com/assets/sellers/tpl0031/js/jquery.mousewheel-3.0.6.pack.js"></script>
        <script src="https://www.hospytek.com/assets/sellers/tpl0031/js/paralax.js"></script>
        <script src="https://www.hospytek.com/assets/sellers/tpl0031/js/jquery.smooth-scroll.js"></script>
        <script src="https://www.hospytek.com/assets/sellers/tpl0031/js/jquery.sticky.js"></script>
        <script src="https://www.hospytek.com/assets/sellers/tpl0031/js/wow.min.js"></script>
        <script src="https://www.hospytek.com/assets/sellers/tpl0031/js/main.js"></script>
        <script type="text/javascript">
            $(document).ready(function(){
            	$('a[href^="#"]').on('click',function (e) {
            		e.preventDefault();
            
            		var target = this.hash;
            		var $target = $(target);
            
            		$('html, body').stop().animate({
            			 'scrollTop': $target.offset().top
            		}, 900, 'swing');
            		});
            });
        </script>
        <script src="https://www.hospytek.com/assets/sellers/tpl0031/js/custom.js"></script>
        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='../../../../www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
       
    </body>
</html>
