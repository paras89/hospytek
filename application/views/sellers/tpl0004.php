<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="<?php echo $meta_desc;?>">
    <meta name="author" content="Hospytek">
    <meta name="keywords" content="<?php echo $meta_key;?>">
    <meta name="robots" content="all">
    <title>
        <?php echo $meta_title;?>
    </title>
    <!-- Bootstrap Core CSS -->
    <base href="<?php echo base_url();?>">
    <script type="application/x-javascript">
        addEventListener("load", function() {
            setTimeout(hideURLbar, 0);
        }, false);

        function hideURLbar() {
            window.scrollTo(0, 1);
        }
    </script>
    <!-- //for-mobile-apps -->
    <link href="https://www.hospytek.com/assets/sellers/tpl0004/css/bootstrap.css" rel='stylesheet' type='text/css' />
    <!-- Custom Theme files -->
    <link href="https://www.hospytek.com/assets/sellers/tpl0004/css/style.css" rel='stylesheet' type='text/css' />
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- js -->
    <script src="https://www.hospytek.com/assets/sellers/tpl0004/js/jquery-1.11.1.min.js"></script>
    <!-- //js -->
    <!-- start-smoth-scrolling -->
    <script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0004/js/move-top.js"></script>
    <script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0004/js/easing.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
    <script type="text/javascript">
        jQuery(document).ready(function($) {
            $(".scroll").click(function(event) {
                event.preventDefault();
                $('html,body').animate({
                    scrollTop: $(this.hash).offset().top
                }, 1000);
            });
        });
    </script>
    <style type="text/css">
        .btn-sm.btn-success.hyper::hover {
            color: red;
        }
    </style>
    <!-- start-smoth-scrolling -->
    <link href="assets/sellers/tpl0004/css/font-awesome.css" rel="stylesheet">
    <link href='//fonts.googleapis.com/css?family=Montserrat:400,700' rel='stylesheet' type='text/css'>
    <link href='//fonts.googleapis.com/css?family=Noto+Sans:400,700' rel='stylesheet' type='text/css'>
    <!--- start-rate---->
    <script src="assets/sellers/tpl0004/js/jstarbox.js"></script>
    <link rel="stylesheet" href="assets/sellers/tpl0004/css/jstarbox.css" type="text/css" media="screen" charset="utf-8" />
    <script type="text/javascript">
        jQuery(function() {
            jQuery('.starbox').each(function() {
                var starbox = jQuery(this);
                starbox.starbox({
                    average: starbox.attr('data-start-value'),
                    changeable: starbox.hasClass('unchangeable') ? false : starbox.hasClass('clickonce') ? 'once' : true,
                    ghosting: starbox.hasClass('ghosting'),
                    autoUpdateAverage: starbox.hasClass('autoupdate'),
                    buttons: starbox.hasClass('smooth') ? false : starbox.attr('data-button-count') || 5,
                    stars: starbox.attr('data-star-count') || 5
                }).bind('starbox-value-changed', function(event, value) {
                    if (starbox.hasClass('random')) {
                        var val = Math.random();
                        starbox.next().text(' ' + val);
                        return val;
                    }
                })
            });
        });
    </script>
    <!---//End-rate---->
</head>

<body>
    <div class="header">
        <div class="nav-top">
            <nav class="navbar navbar-inverse navbar-fixed-top" style="background-color: #FCFFF1;width:100%;">
                <div class="navbar-header nav_2">
                    <button type="button" class="navbar-toggle collapsed navbar-toggle1" data-toggle="collapse" data-target="#bs-megadropdown-tabs">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>
                <div class="collapse navbar-collapse" id="bs-megadropdown-tabs">
                    <ul class="nav navbar-nav pull-right col-md-12">
                        <li class="dropdown col-md-6">
                           <div class="logo">
                                <a href="<?php echo $brand->user_slug;?>">  
                                    <?  if($brand->user_image=='') 
                                        { ?>
                                            <a href="<?php echo $brand->user_slug;?>"><?php echo $brand->user_company?></a><br/>
                                        <? } else if($brand->user_image=='no.gif') { ?>
                                    <a href="<?php echo $brand->user_slug;?>" style="text-decoration:none; color:#0b8aef;"><?php echo $brand->user_company;?></a><br/>
                                    <? }
                                        else
                                        { $imgurl= 'http://www.hospytek.com/newcrm/UserFiles/Image/'.$brand->user_image;?>
                                    <a href="<?php echo $brand->user_slug;?>"><img src="<?=$imgurl?>" alt="<?=$brand->user_company?>" style="height:70px;margin-top:5px;"/></a><br/>
                                    <? } 
                                ?>
                                   </a>
                                &nbsp;&nbsp;<span><?php echo $brand->user_city;?>, <?php echo $brand->user_state;?> <?php echo $brand->user_country;?></span></h2>
                            </div>
                        </li>
                        <li><a href="<?php echo $brand->user_slug;?>" class="hyper "><span>Home</span></a></li>
                        <li class="dropdown ">
                            <a href="<?php echo $brand->user_slug;?>#about" class="hyper"><span>About Us</span></a>
                        </li>
                        <li><a href="<?php echo $brand->user_slug;?>#products" class="hyper"><span>Products</span></a></li>
                        <li><a href="<?php echo $brand->user_slug;?>#contact" class="hyper"><span>Contact Us</span></a></li>
                        <li>
                            <a data-toggle="modal" href="#modal-callback" class="cart-sellers">
                                <button class="btn btn-default">Become a Dealer</button>
                            </a>
                        </li>

                    </ul>
                </div>
            </nav>
            <div class="clearfix"></div>
        </div>
    </div>

    <!---->
    <section id="slider">
        <!--slider-->
        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <div id="slider-carousel" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
                            <li data-target="#slider-carousel" data-slide-to="1"></li>
                            <li data-target="#slider-carousel" data-slide-to="2"></li>
                        </ol>

                        <div class="carousel-inner">

                            <?php $i=0; foreach ($items as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;} ;
                          if($i>6){ break;}
                          ?>
                                <?php if ($i==0) { $i++; ?>
                                    <div class="item active">
                                        <div class="col-sm-6" style="margin-top:160px;">
                                            <h1><?php echo $rw->p_title." ".$rw->vr_name;?></h1>
                                            <h3><strong>Model:</strong> <?php echo $rw->p_model;?></h3>
                                            <p>
                                                <?php echo $rw->p_sdesc;?>
                                            </p>

                                            <button type="button" class="btn btn-default get"><a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online">View Detail</a></button>
                                            <button type="button" class="btn btn-default get"><a href="<?php echo '../pdf/'.$rw->p_slug.'.pdf'?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Brochure Manual Download">View Brochure</a></span>
                                            </button>
                                        </div>
                                        <div class="col-sm-6">
                                            <img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="girl img-responsive" alt="" />

                                        </div>
                                    </div>
                                    <?php } else  {  ?>

                                        <div class="item">
                                            <div class="col-sm-6" style="margin-top:160px;">
                                                <h1><?php echo $rw->p_title." ".$rw->vr_name;?></h1>
                                                <h3><strong>Model:</strong> <?php echo $rw->p_model;?></h3>
                                                <p>
                                                    <?php echo $rw->p_sdesc;?>
                                                </p>

                                                <a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online"><button type="button" class="btn btn-default get">View Detail</button></a>
                                                <a href="<?php echo '../pdf/'.$rw->p_slug.'.pdf'?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Brochure Manual Download"><button type="button" class="btn btn-default get">View Brochure </button></a>
                                               
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="girl img-responsive" alt="" />

                                            </div>
                                        </div>
                                        <?php  } ?>

                                            <?php  } ?>

                        </div>

                        <a href="#slider-carousel" class="left control-carousel hidden-xs pull-left" data-slide="prev">
                            <i class="fa fa-angle-left"></i>
                        </a>
                        <a href="#slider-carousel" class="right control-carousel hidden-xs pull-right" data-slide="next">
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!--/slider-->

    <!-- Area for about us -->
    <div class="content-top" id="about">
        <div class="container ">
            <div class="spec ">
                <h3>WELCOME TO <?=$brand->user_company?></h3>
                <div class="ser-t">
                    <b></b>
                    <span><i></i></span>
                    <b class="line"></b>
                </div>
            </div>
            <div class="information-blocks">
                <div class="row">
                    <div class="col-md-12 information-entry">
                        <div class="information-blocks">
                            <div class="row">
                                <div class="col-md-12 information-entry">
                                    <div class="article-container">
                                        <?=$brand->user_about?>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="product" id="products">
        <div class="container">
            <div class="spec ">
                <h3>Our Products</h3>
                <div class="ser-t">
                    <b></b>
                    <span><i></i></span>
                    <b class="line"></b>
                </div>
            </div>
            <div class=" con-w3l">
                <?php $i=0; foreach ($items as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;}  $i++;?>
                    <?php if($i==1){echo '<div class="row">';}?>
                        <div class="col-md-4 pro-1">
                            <div class="col-m">
                                <a href="#" data-toggle="modal" data-target="#myModal17" class="offer-img">
                            <img  src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="img-responsive" alt="">
                            </a>
                                <div class="mid-1" style="height:125px">
                                    <div class="women" >
                                        <h6 style="text-align:center"><a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>"><?php echo $rw->p_title." ".$rw->vr_name;?></h6>
                                        <p style="text-align:center"><a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>">Model <span><?php echo $rw->p_model;?></span></p>
                                    </div>
                                    </div>
                                    <div class="mid-2">
                                        <p></p>
                                        <div class="block">

                                        </div>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="add add-2">
                                        <a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>"><button class="btn-default btn-lg">View Details</button></a>
                                    </div>

                                </div>
                            </div>

                            <?php if($i==3){ $i=0; echo '</div><div class="clearfix">&nbsp; </div>';}?>
                                <? }?>

                                    <div class="clearfix"></div>
                        </div>
            </div>
        </div>
        <!-- Area for Contact us -->
        <div>
            <div class="container" id="contact">
                <h2 style="text-align: center;font-size: 30px;">Connect With <?=$brand->user_company?></h2>
                <br>
                <div class="ser-t">
                    <b></b>
                    <span><i></i></span>
                    <b class="line"></b>
                </div>
                <br>
                <form action="#">
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                <label>Full Name</label>
                                <input type="text" class="form-control" name="name" placeholder="Full Name">
                            </div>
                            <div class="form-group">
                                <label>Email address</label>
                                <input type="email" class="form-control" name="email" placeholder="Email">
                            </div>
                            <div class="form-group">
                                <label>Message</label>
                                <textarea class="form-control" name="message" placeholder="Your message here"></textarea>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <p>
                                <?=$brand->user_address?>
                            </p>
                            <p>
                                <?=$brand->user_city?>,
                                    <?=$brand->user_state?>
                            </p>
                            <p>
                                <?=$brand->user_country?>
                                    <?=$brand->user_postcode?>
                            </p>
                            <p>+91-
                                <?=$brand->user_mobile?>
                                    <?=$brand->user_phone?>
                            </p>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-sm-6">
                            <button type="submit" class="btn btn-primary hvr-sweep-to-right" style="width:100%;">Submit</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
        <br>
        <!--footer-->

        <div class="footer">
            <div class="container">
                <div class="copy-right" style="margin-bottom: 2%">
                    <div class="row" style="margin-top:none;">
                        <div class="col-md-8">
                            <div class="copyright">

                                <p>Powered by
                                    <br>
                                    <a href="http://hospytek.com/" target="blank"><img src="assets/sellers/tpl0003/img/logo.png" /></a>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-4" style="margin-top: 3%;">
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- //footer-->
        <!-- Modal Area -->

        <div class="modal fade" id="modal-callback" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h2 class="modal-title">Please Fill the form & we will call you back</h2>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="<?php echo base_url()." api ";?>" class="form-ajax">
                            <input type="hidden" name="module" value="callback" />
                            <input type="hidden" name="type" value="0" />
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="info-title" for="exampleInputName">Your Name <span>*</span></label>
                                        <input type="text" class="form-control unicase-form-control text-input" placeholder="" name="name" required>
                                    </div>

                                    <div class="form-group">
                                        <label class="info-title" for="exampleInputName">Your Phone <span>*</span></label>
                                        <input type="phone" class="form-control unicase-form-control text-input" placeholder="" name="mobile" required>
                                    </div>

                                    <div class="form-group">
                                        <label class="info-title" for="exampleInputName">Your Email <span>*</span></label>
                                        <input type="email" class="form-control unicase-form-control text-input" placeholder="" name="email" required>
                                    </div>

                                    <div class="form-group">
                                        <label class="info-title" for="exampleInputName">Best time to call you <span>*</span></label>
                                        <input type="text" class="form-control unicase-form-control text-input" placeholder="" name="timetocall" required>
                                    </div>

                                    <div class="form-group">
                                        <label class="info-title" for="exampleInputName">Your Message <span>*</span></label>
                                        <input type="text" class="form-control unicase-form-control text-input" placeholder="" name="message" required>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn-upper btn btn-primary checkout-page-button">Submit Request</button>
                                    </div>

                                </div>

                                <div class="col-md-6" style="font-size:12px;">
                                    <p><strong>Lets give you a call back to help you buy the most suitable Medical Equipment & Device as per your requirement, We would not only help you choose the most suitable medical device but also help you find the right localised vendor to make your after sale requirements more readily available.</strong></p>
                                    <center><img src="assets/images/callback-icon.png" class="img-responsive" style="max-height:175px;" /></center>
                                    <p>We at Hospytek hold immense value for every customer or prospective customer and consider it as our utmost duty to provide a solution to all the queries raised by the customers.</p>

                                    <p>We thank you for filling the form and assure you that one of our sales executives would get back to you very soon to understand your problem and provide the most reasonable solution.</p>

                                    <p><strong>We again thank you for your patience and hope you are your experience on our website.</strong></p>
                                </div>

                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
        <!-- End of modal -->
        <!-- smooth scrolling -->
        <script type="text/javascript">
            $(document).ready(function() {
                /*
                    var defaults = {
                    containerID: 'toTop', // fading element id
                    containerHoverID: 'toTopHover', // fading element hover id
                    scrollSpeed: 1200,
                    easingType: 'linear' 
                    };
                */
                $().UItoTop({
                    easingType: 'easeOutQuart'
                });
            });
        </script>
        <a href="#" id="toTop" style="display: block;"> <span id="toTopHover" style="opacity: 1;"> </span></a>
        <!-- //smooth scrolling -->
        <!-- for bootstrap working -->
        <script src="https://www.hospytek.com/assets/sellers/tpl0004/js/bootstrap.js"></script>
        <!-- //for bootstrap working -->
        <script type='text/javascript' src="https://www.hospytek.com/assets/sellers/tpl0004/js/jquery.mycart.js"></script>
        <script type="text/javascript">
            $(function() {

                var goToCartIcon = function($addTocartBtn) {
                    var $cartIcon = $(".my-cart-icon");
                    var $image = $('<img width="30px" height="30px" src="' + $addTocartBtn.data("image") + '"/>').css({
                        "position": "fixed",
                        "z-index": "999"
                    });
                    $addTocartBtn.prepend($image);
                    var position = $cartIcon.position();
                    $image.animate({
                        top: position.top,
                        left: position.left
                    }, 500, "linear", function() {
                        $image.remove();
                    });
                }

                $('.my-cart-btn').myCart({
                    classCartIcon: 'my-cart-icon',
                    classCartBadge: 'my-cart-badge',
                    affixCartIcon: true,
                    checkoutCart: function(products) {
                        $.each(products, function() {
                            console.log(this);
                        });
                    },
                    clickOnAddToCart: function($addTocart) {
                        goToCartIcon($addTocart);
                    },
                    getDiscountPrice: function(products) {
                        var total = 0;
                        $.each(products, function() {
                            total += this.quantity * this.price;
                        });
                        return total * 1;
                    }
                });

            });
        </script>

</body>

</html>