<!DOCTYPE HTML>
<html lang="en-US">

<head>	
	 <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<meta name="description" content="<?php echo $meta_desc;?>">
		<meta name="author" content="Hospytek">
		<meta name="keywords" content="<?php echo $meta_key;?>">
		<meta name="robots" content="all">
		<title><?php echo $meta_title;?></title>
		<!-- Bootstrap Core CSS -->
		<base href="<?php echo base_url();?>">
	<link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0010/css/bootstrap.css" />
	<link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0010/css/font-awesome.min.css" />
	<link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0010/css/linea-icon.css" />
	<link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0010/css/fancy-buttons.css" />
	
	<!--=== Google Fonts ===-->
	<link href='http://fonts.googleapis.com/css?family=Bangers' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Roboto+Slab:300,700,400' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Raleway:600,400,300' rel='stylesheet' type='text/css'>
	<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700' rel='stylesheet' type='text/css'>
	<!--=== Other CSS files ===-->
	<link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0010/css/animate.css" />
	<link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0010/css/jquery.vegas.css" />
	<link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0010/css/baraja.css" />
	<link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0010/css/jquery.bxslider.css" />
	
	<!--=== Main Stylesheets ===-->
	<link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0010/css/style.css" />
	<link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0010/css/responsive.css" />
	
	<!--=== Color Scheme Stylesheets ===-->

	<link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0010/css/schemes/red.css" disabled />
	<link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0010/css/schemes/orange.css" disabled />
	<link rel="stylesheet" id="scheme-source" href="https://www.hospytek.com/assets/sellers/tpl0010/css/schemes/gray.css" />
	
	
	<link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0010/css/color-switcher.css" />
	
</head>
<body>
	
<!-- Fb COde -->
<div id="fb-root"></div>
<script>
(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "../../../../connect.facebook.net/en_US/sdk.js#xfbml=1&appId=321345521337473&version=v2.0";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
</script>


	
	<!--=== Header section Starts ===-->
	<div id="home">
		<!-- sticky-bar Starts-->
		<div class="sticky-bar-wrap">
			<div class="sticky-section">
				
					<nav class="navbar" role="navigation" style="background-color:rgb(97, 90, 90);">
						<div class="navbar-header col-md-6">
							<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-responsive-collapse">
									<span class="sr-only">Toggle navigation</span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
									<span class="icon-bar"></span>
							</button>
							<!--=== Site Name ===-->
						  <div class="logo">
                                <a href="<?php echo $brand->user_slug;?>">  
                                    <?  if($brand->user_image=='') 
                                        { ?>
                                            <a href="<?php echo $brand->user_slug;?>"><?php echo $brand->user_company?></a><br/>
                                        <? } else if($brand->user_image=='no.gif') { ?>
                                    <a href="<?php echo $brand->user_slug;?>" style="text-decoration:none; color:#40caa3;"><?php echo $brand->user_company;?></a><br/>
                                    <? }
                                        else
                                        { $imgurl= 'http://www.hospytek.com/newcrm/UserFiles/Image/'.$brand->user_image;?>
                                    <a href="<?php echo $brand->user_slug;?>"><img src="<?=$imgurl?>" alt="<?=$brand->user_company?>" style="height:70px;margin-top:5px;"/></a><br/>
                                    <? } 
                                ?>
                                   </a>
                                &nbsp;&nbsp;<span style="color:#40caa3;"><?php echo $brand->user_city;?>, <?php echo $brand->user_state;?> <?php echo $brand->user_country;?></span></h2>
                        </div>
						</div>
						
						<!-- Main Navigation menu Starts -->
						<div class="collapse navbar-collapse navbar-responsive-collapse col-md-6">
							<ul class="nav navbar-nav navbar-right">
								<li class="current"><a href="<?php echo $brand->user_slug;?>">Home</a></li>						
								<li><a href="<?php echo $brand->user_slug;?>#about">About</a></li>
								<li><a href="<?php echo $brand->user_slug;?>#product">Products</a></li>
								<li><a href="<?php echo $brand->user_slug;?>#contact">Contact</a></li>
							
							<div style="display:inline;float:left;margin-top:10px;padding-left:20px"><a data-toggle="modal" href="#modal-callback" class="cart-sellers"><button style="background-color:#2196f3;">Become a Dealer</button></a></div>
							</ul>
						</div>
						<!-- Main Navigation menu ends-->
					</nav>
				
			</div>
		</div>
	
		<div id="home" class="home-section-wrap center">
		
					 	<div class="container">
			<div class="row">
				<div class="col-sm-12">
				
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#slider-carousel" data-slide-to="1"></li>
							<li data-target="#slider-carousel" data-slide-to="2"></li>
						</ol>
						
						<div class="carousel-inner">
						
							
						
						
						 <?php $i=0; foreach ($items as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;} ;
						  if($i>6){ break;}
						  ?>				
						  <?php if ($i==0) { $i++; ?>
        			  <div class="item active"> 
								<div class="col-sm-6" style="margin-top:150px">
									<h3><?php echo $rw->p_title." ".$rw->vr_name;?></h3>
									<h5><strong>Model:</strong> <?php echo $rw->p_model;?></h5>
									<p><?php echo $rw->p_sdesc;?></p>
									
									<button type="button" class="btn btn-default get"><a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online">View Detail</a></button>
									<button type="button" class="btn btn-default get"><a href="<?php echo '../pdf/'.$rw->p_slug.'.pdf'?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Brochure Manual Download">View Brochure</a></span></button>
								</div>
								<div class="col-sm-6">
									<img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="girl img-responsive" alt="" />
									
								</div>
							</div>
							<?php } else  {  ?>
							
							<div class="item"> 
								<div class="col-sm-6" style="margin-top:150px">
									<h3><?php echo $rw->p_title." ".$rw->vr_name;?></h3>
									<h5><strong>Model:</strong> <?php echo $rw->p_model;?></h5>
									<p><?php echo $rw->p_sdesc;?></p>
									
									<button type="button" class="btn btn-default get"><a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online">View Detail</a></button>
									<button type="button" class="btn btn-default get"><a href="<?php echo '../pdf/'.$rw->p_slug.'.pdf'?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Brochure Manual Download">View Brochure</a></span></button>
								</div>
								<div class="col-sm-6">
									<img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="girl img-responsive" alt="" />
									
								</div>
							</div>
							<?php  } ?>
							
							
							
							<?php  } ?>
							
							
							
						</div>
						
						
                        <a href="#slider-carousel" class="left control-carousel pull-left" data-slide="prev">
                            <i class="fa fa-angle-left" style="color:grey;font-size:50px;"></i>
                        </a>
                        <a href="#slider-carousel" class="right control-carousel pull-right" data-slide="next">
                            <i class="fa fa-angle-right" style="color:grey;font-size:50px;"></i>
                        </a>

					</div>
					
				</div>
			</div>
		</div>
		
		</div>
		<!--=== Home Section Ends ===-->
	</div>

	
	<!--=== Services section Starts ===-->
	<section id="about">
		<div class="container services">
			<div class="row">
			
				<div class="col-md-10 col-md-offset-1 center section-title">
					<h3>WELCOME TO <?=$brand->user_company?></h3>
					<p><?=$brand->user_about?></p>
				</div>
			
				
			</div>
		</div>
	</section>
	<!--=== Services section Ends ===-->
	
	
	
	<!--=== ScreenShots section Starts ===-->
	<section id="product">
		<div class="container screenshots animated" data-animation="fadeInUp" data-animation-delay="1000">
			<div class="row porfolio-container">
				<div class="col-md-10 col-md-offset-1 center section-title">
					<h3>Our Products</h3>
				</div>

				  <?php $i=0; foreach ($items as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;}  $i++;?>
				<?php if($i==1){echo '<div class="row">';}?>
				
				<div class="col-md-4 col-sm-4 col-xs-6" style="text-align:center;">			
							<img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> " style="border:solid 4px;">
							<p><a href="#"><?php echo $rw->p_title." ".$rw->vr_name;?></a></p>	
							<h6><a href="#"><?php echo $rw->p_model;?></a></h6>
							<a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online"><button type="button" class="btn btn-primary">View Detail</button></a>
				</div>
			  <?php if($i==3){ $i=0; echo '</div><div class="clearfix">&nbsp; </div>';}?>
                    <? }?>
			
				
			</div>
			
		
		</div>
	</section>
	<!--=== ScreenShots section Ends ===-->

	
	<!--=== Contact section Starts ===-->
	<section id="contact">
	<div class="section-overlay"></div>
		<div class="container contact center animated" data-animation="flipInY" data-animation-delay="1000">
			<div class="row">
				<div class="col-md-8">
					<div class="col-md-10 col-md-offset-1 center section-title">
						<h3>Leave a message</h3>
					</div>
					
					<div class="confirmation">
						<p><span class="fa fa-check"></span></p>
					</div>
					
					<form class="contact-form support-form">
						
						<div class="col-lg-12">
							<input id="name" class="input-field form-item field-name" type="text" required="required" name="contact-name" placeholder="Name" />

							<input id="email" class="input-field form-item field-email" type="email" required="required" name="contact-email" placeholder="Email" />

							<input id="subject" class="input-field form-item field-subject" type="text" required="required" name="contact-subject" placeholder="Subject" />
							<textarea id="message" class="textarea-field form-item field-message" rows="10" name="contact-message" placeholder="Message"></textarea>
						</div>
						<button type="submit" class="fancy-button button-line button-white large zoom subform">
							Send message
							<span class="icon">
								<i class="fa fa-paper-plane-o"></i>
							</span>
						</button>
					</form>
				</div>
				<div class="col-md-4" style="color:white;">
				<h3><strong><u>ADDRESS</u></strong></h3>
				  <p><i class="fa fa-pencil"></i><p><?=$brand->user_city?>, <?=$brand->user_state?></p>
					<p><?=$brand->user_country?>  <?=$brand->user_postcode?></p><br>
					<p><i class="fa fa-phone"></i>+91-<?=$brand->user_mobile?>     <?=$brand->user_phone?></p>
				</div>
			</div>
		</div>

	<!--=== Contact section Ends ===-->
	
	<!--=== Footer section Starts ===-->
	<div id="section-footer" class="footer-wrap">
		<div class="container footer center">
			<div class="row">
				<div class="col-lg-12">
					
					<!-- Social Links -->
					<div class="social-icons">
						<ul>
							<li><a href="#"><i class="fa fa-facebook-square"></i></a></li>
							<li><a href="#"><i class="fa fa-twitter-square"></i></a></li>
							<li><a href="#"><i class="fa fa-google-plus-square"></i></a></li>
							<li><a href="#"><i class="fa fa-pinterest-square"></i></a></li>
						    <li><a href="#"><i class="fa fa-linkedin-square"></i></a></li>
						</ul>
					</div>
					
					 <p>powered by <a href="https://www.hospytek.com/" ><img src="<?php echo base_url();?>/assets/images/logo.png" alt="logo"></a> </p>
				</div>
			</div>
		</div>
	</div>
	<!--=== Footer section Ends ===-->
	</section>
	

	
<!--==== Js files ====-->
<!--==== Essential files ====-->
<script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0010/js/jquery-1.11.1.min.js"></script>
<script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0010/js/bootstrap.min.js"></script>
<script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0010/js/bootstrapValidator.min.js"></script>
<script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0010/js/modernizr.js"></script>
<script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0010/js/jquery.easing.1.3.js"></script>

<!--==== Slider and Card style plugin ====-->
<script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0010/js/jquery.baraja.js"></script>
<script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0010/js/jquery.vegas.min.js"></script>
<script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0010/js/jquery.bxslider.min.js"></script>

<!--==== MailChimp Widget plugin ====-->
<script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0010/js/jquery.ajaxchimp.min.js"></script>

<!--==== Scroll and navigation plugins ====-->
<script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0010/js/jquery.nicescroll.min.js"></script>
<script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0010/js/jquery.nav.js"></script>
<script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0010/js/jquery.appear.js"></script>
<script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0010/js/jquery.fitvids.js"></script>

<!--==== Custom Script files ====-->
<script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0010/js/custom.js"></script>

<script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0010/js/color-switcher.js"></script>
<script>
	$(window).load(function() {
		'use strict';
		$('.fb-popup').delay(40000).fadeIn();
		$('.fb-close').click(function() {
			$('.fb-popup').fadeOut();
		})
	});
</script>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','../../../../www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-58786679-1', 'auto');
  ga('send', 'pageview');
</script>

  <div class="modal fade" id="modal-callback" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h5 class="modal-title">Please Fill the form & we will call you back</h5>
                          </div>
                          <div class="modal-body">
						  <form method="post" action="<?php echo base_url()."api";?>" class="form-ajax">
						  <input type="hidden" name="module" value="callback" />
						  <input type="hidden" name="type" value="0" />
						  <div class="row">
						  <div class="col-md-6">
						  	<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Name <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input" placeholder="" name="name" required>
							</div>

							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Phone <span>*</span></label>
		    <input type="phone" class="form-control unicase-form-control text-input"  placeholder=""  name="mobile" required>
							</div>
							
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Email <span>*</span></label>
		    <input type="email" class="form-control unicase-form-control text-input"  placeholder=""  name="email" required>
						   </div>	
							
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Best time to call you <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input"  placeholder="" name="timetocall" required>
						   </div>	


							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Message <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input"  placeholder=""  name="message" required>
						   </div>	


	<div class="form-group">
		<button type="submit" class="btn-upper btn btn-primary checkout-page-button">Submit Request</button>
	</div>
			
						  </div>

						  <div class="col-md-6" style="font-size:12px;">
						   <p><strong>Lets give you a call back to help you buy the most suitable Medical Equipment & Device as per your requirement, We would not only help you choose the most suitable medical device but also help you find the right localised vendor to make your after sale requirements more readily available.</strong></p>
						  <center><img src="assets/images/callback-icon.png" class="img-responsive" style="max-height:175px;" /></center>
						  <p>We at Hospytek hold immense value for every customer or prospective customer and consider it as our utmost duty to provide a solution to all the queries raised by the customers.</p>

<p>We thank you for filling the form and assure you that one of our sales executives would get back to you very soon to understand your problem and provide the most reasonable solution.</p>

<p><strong>We again thank you for your patience and hope you are your experience on our website.</strong></p>
						  </div>
						  
                          </div>
						  </form>
                      </div>
                  </div>
				  </div>
              </div>


</body>

</html>