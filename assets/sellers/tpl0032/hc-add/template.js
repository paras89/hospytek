(function($){
	$(document).ready(function(){

		//SIDE PANEL 
		//--------------------------------------------------------
		//Scroll totop
		//-----------------------------------------------
		$(window).scroll(function() {
			if($(this).scrollTop() > 1700) {
				$(".hc-add").fadeIn();	
			} else {
				$(".hc-add").fadeOut();
			}
		});

		// Remove Button
		//-----------------------------------------------
		$(".hc-remove").click(function() {
			$(this).closest(".hc-data").remove();
			return false;
		});

	}); // End document ready
})(this.jQuery);