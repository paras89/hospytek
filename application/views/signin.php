<?php $this->view('common/header'); ?>
<!-- ============================================== HEADER : END ============================================== -->
<!-- ============================================== HEADER : END ============================================== -->
<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
				<li><a href="">Home</a></li>
				<li class='active'>Login</li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->

<div class="body-content">
	<div class="container">
		<div class="sign-in-page">
			<div class="row">
				<!-- Sign-in -->			
<div class="col-md-6 col-sm-6 sign-in">
	<h4 class="">Already a member? Sign in</h4>
	
	
	<form class="register-form outer-top-xs" role="form" action="account/ajax_login" id="signin" method="post">
		<div class="loading"></div>
		
		<div class="form-group maildiv">
		<p class="">Welcome, Please enter the registered email address to generate OTP.</p>
		    <label class="info-title" for="exampleInputEmail1">Email Address <span>*</span></label>
		    <input type="text" id="email" name='email' class="form-control unicase-form-control text-input" value="<?=$email?>" > 
			<br />
			<button type="button" class="btn-upper btn btn-success checkout-page-button otpbutton">Generate OTP</button>
		</div>
		
		

		<div class="otpdiv">
		<p class="">Please enter the OTP recieved on your registered mobile number.</p>
	  	<div class="form-group">
		    <label class="info-title" for="exampleInputPassword1">Enter OTP <span>*</span></label>
		    <input type="text" name="otp" id="otp" class="form-control unicase-form-control text-input">
		</div>
		
	  	<button type="submit" class="btn-upper btn btn-primary checkout-page-button">Signin</button>
		
		</div>
	</form>					
</div>
<!-- Sign-in -->

<!-- create a new account -->
<div class="col-md-6 col-sm-6 create-new-account">
	<h4 class="checkout-subtitle">Not a Member yet?</h4>
	<p class="text title-tag-line">Join Hospytek today & stay amazed with quick & hassle free services like:</p>
	<ul>
	<li style="line-height:24px;"><i class="fa fa-check"></i> Get access to largest library of medical equipment and providers</li>
	<li style="line-height:24px;"><i class="fa fa-check"></i> Get instant automated generated quotes at the click of a button.</li>	
	<li style="line-height:24px;"><i class="fa fa-check"></i> Regulate your medical device business.</li>	
	<li style="line-height:24px;"><i class="fa fa-check"></i> Get hassle free logistics support & tracking.</li>		
	<li style="line-height:24px;"><i class="fa fa-check"></i> Get medical device service support anywhere in the country.</li>			
	</ul>
	<br /><br />
	<a data-toggle="modal" href="#modal-signup" class="btn-upper btn btn-primary checkout-page-button">Register Now</a>
	
</div>	
<!-- create a new account -->			</div><!-- /.row -->
		</div><!-- /.sigin-in-->
	</div>
</div>		
		<div style="clear:both">&nbsp;</div>
		
<script>
$(function() { $('.otpdiv').hide(); });
$(document).on('click', '.otpbutton', function (e) {
		$.ajax({
				type : "POST",
				url : '../api',
				dataType : "html",
				data : {email : $('#email').val(), 'module': 'otp'},
				beforeSend : function() {
					$('.loading').html("<img src='assets/images/loading.gif'><br>Please wait! connecting to server.");
				},
				success : function(response) {
					var data = $.parseJSON(response);
					if(data.status=='success'){ $('.otpdiv').show();  $('.maildiv').hide(); }
					$('.loading').html(data.msg);
				}
		});
});


$(document).on('submit', '#signin', function (e) {
		var elem = $(this);
		var urlTarget = $(this).attr("action");
		$.ajax({
				type : "POST",
				url : urlTarget,
				dataType : "html",
				data : $(this).serialize(),
				beforeSend : function() {
					$('.loading').html("<div class='alert'><img src='assets/images/loading.gif'><br>Please wait! connecting to server.</div>");
				},
				success : function(response) {
					//var res=response.split("|");
					var data = $.parseJSON(response);
					$('.loading').html("<div class='alert'>" + data.msg + "</div>");
					if(data.status=='success'){
						var url = data.redirect_url; window.location = decodeURIComponent(url);
						elem.find("input[type='text'],input[type='email'],textarea").val("");	
					}
				}
		});
		return false;
});
</script>		
<?php $this->view('common/footer'); ?>