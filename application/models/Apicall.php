<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class apicall extends CI_Model {
    
    public function __construct() {
        parent::__construct();
    }

	function logged_user(){
			$credential = array('id' => $this->session->userdata('user_uid'));
			$query = $this->db->get_where('users', $credential);
			return $query->row();
	
	}

	function get_user($id){
			$credential = array('id' => $id);
			$query = $this->db->get_where('users', $credential);
			return $query->row();
	}

	function user_byemail($email){
			$credential = array('user_email' => $email);
			$query = $this->db->get_where('users', $credential);
			return $query->row();
	}
	
	function search_user(){
					 $customer = $this->input->post('key');
					 $query = $this->db->query("select * from users where user_type<=4 and (user_email='$customer' or user_mobile='$customer') order by user_type");
					 return $query->row();
	}
	function count_order_byuser($tp, $id)
    {
		 if ($this->session->userdata('user_type') == 1){
		 $urw = $this->apicall->logged_user();
		 $query = $this->db->query("select * from orders o, users u, odetails d, vendors v where o.o_id=d.o_id and o.o_buyer=u.id and ( (v.v_pid=d.d_pid and v.v_vendor='$id') or ($id='434' or $id='430')) and o.o_type='$tp' group by o.o_id order by o.o_id desc");
		 if($query->num_rows()==0){
		 $query = $this->db->query("select * from orders o, users u, odetails d, product p where o.o_id=d.o_id and o.o_buyer=u.id and p.p_id=d.d_pid and(p.p_brand in($urw->user_brands) or ($id='434' or $id='430')) and o.o_type='$tp'  group by o.o_id order by o.o_id desc");
		 
		 }
		 
		 }else{
		 $query = $this->db->query("select o_id from orders where o_createdby='$id' and o_invtype='$tp' and o_type=9");
		 }
		 return $query->num_rows();
	}




    function key_search($type)
    {
	$key = $this->input->post('key');
	$key = $this->db->escape_like_str($key);
	if($type==1){
	$query = $this->db->query("select c.cat_name, c.cat_slug, ct.cat_slug as catslug from category c, category ct  where c.cat_parent=ct.cat_id and c.cat_name like '%$key%' group by c.cat_name Limit 0,50",false);
	}
	
	if($type==2){
	$query = $this->db->query("select v.vr_name, v.vr_slug, c.cat_name, c.cat_slug, ct.cat_slug as catslug from variations v, category c, category ct where c.cat_parent=ct.cat_id and c.cat_id=v.vr_catid and v.vr_name like '%$key%' group by v.vr_name Limit 0,50",false);
	}
	
	if($type==3){
	$query = $this->db->query("select * from users where user_type=3 and user_company like '%$key%' Limit 0,50",false);
	}

	if($type==4){
	$query = $this->db->query("select p.p_title, p.p_model, p.p_slug, u.user_company, v.vr_name, v.vr_slug, c.cat_slug, ct.cat_slug as catslug from product p, users u, variations v, category c, category ct where p.cat_id=ct.cat_id and c.cat_id=p.cat_sid and p.p_vrid=v.vr_id and p.p_brand=u.id and (v.vr_name like '%$key%' or p.p_title like '%$key%' or p.p_model like '%$key%' or u.user_company like '%$key%' or CONCAT(p.p_title, ' ', v.vr_name) like '%$key%') Limit 0,50",false);
	}
	
	return $query->result();
    }

    function cat_search($type)
    {
	$key = $this->input->post('query');
	$key = $this->db->escape_like_str($key);
	$parent = $this->input->post('parent');
	if($type==1){
	$query = $this->db->query("select * from category  where cat_parent='$parent' order by cat_name",false);
	}
	
	if($type==2){
	$query = $this->db->query("select * from variations  where vr_catid='$parent' order by vr_name",false);
	}
	return $query->result();
    }


    function categories($type, $parent, $start, $limit)
    {
	$query = $this->db->query("select * from category  where cat_parent='$parent' and cat_type='$type' order by cat_name limit $start, $limit",false);
	return $query->result();
    }


    function variations($parent, $start, $limit)
    {
	$query = $this->db->query("select v.*, c.*, ct.cat_slug as catslug from variations v, category c, category ct where v.vr_catid=c.cat_id and c.cat_parent=ct.cat_id and  v.vr_catid='$parent' order by v.vr_name limit $start, $limit",false);
	return $query->result();
    }

    function allbrands()
    {
	$query = $this->db->query("select id, user_slug, user_company from users where user_isbrand=1 and user_company!='' order by user_company",false);
	return $query->result();
    }


    function brands($tp, $parent)
    {
	if($tp==0){
	$query = $this->db->query("select * from users u, product p  where u.user_type=3 and p.p_brand=u.id and p.cat_id='$parent' group by u.id order by u.user_company",false);
	}elseif($tp==1){
	$query = $this->db->query("select * from users u, product p  where u.user_type=3 and p.p_brand=u.id and p.cat_sid='$parent' group by u.id order by u.user_company",false);
	}elseif($tp==2){
	$query = $this->db->query("select * from users u, product p  where u.user_type=3 and p.p_brand=u.id and p.p_vrid='$parent' group by u.id order by u.user_company",false);
	}else{
	$query = $this->db->query("select * from users where user_type=3 order by user_company",false);
	}
	return $query->result();
    }


    function master_categories($in)
    {
	$query = $this->db->query("select * from category  where cat_id in ($in)",false);
	return $query->result();
    }

	function homepage_products($type, $cat, $limit)
    {
	if (strpos($cat, ',') !== false) { $sq .=" and p.cat_id in($cat)"; }elseif($cat>0){ $sq .=" and p.cat_id=$cat";}
	switch($type){
	case "new":
	$sq .=" order by p.p_id desc";
	break;

	case "featured":
	$sq .=" order by rand() ";
	break;

	case "onsale":
	$sq .=" order by rand() asc ";
	break;
	}	
	$query = $this->db->query("select  p.*, u.*, c.*, v.*, ct.cat_slug as catslug from product p, users u, category c, variations v, category ct where p.p_brand=u.id and p.p_vrid=v.vr_id and p.cat_sid=c.cat_id and p.cat_id=ct.cat_id and p.p_image!=''   $sq Limit 0,$limit",false);
	return $query->result();
    }
    
	
	function products_bycat($start, $limit)
    {
	$key = $this->input->post('query'); $key = $this->db->escape_like_str($key);
	$brand = $this->input->post('brand');
	$vrid = $this->input->post('vrid');
	$subcat = $this->input->post('subcat');
	$cat = $this->input->post('cat');
	if($key!=''){ $sq .= " and (v.vr_name like '%$key%' OR c.cat_name like '%$key%' OR p.p_title like '%$key%' OR p.p_model like '%$key%' OR u.user_company like '%$key%')";}
	if($brand>0){ $sq .= " and p.p_brand='$brand'";}
	if($vrid>0){ $sq .= " and p.p_vrid in ('$vrid')";}
	if($subcat>0){ $sq .= " and p.cat_sid='$subcat'";}
	if($cat>0){ $sq .= " and p.cat_id='$cat'";}
	
	$query = $this->db->query("select p.*, u.*, c.*, v.*, ct.cat_slug as catslug from product p, users u, category c, variations v, category ct where p.p_brand=u.id and p.p_vrid=v.vr_id and p.cat_sid=c.cat_id and p.cat_id=ct.cat_id  $sq order by p.p_title desc Limit $start,$limit",false);
	return $query->result();
    }

    function user_byslug($slug)
    {
	$query = $this->db->query("select * from users where user_slug='$slug'",false);
	return $query->row();
    }

    function category_byslug($slug)
    {
	$query = $this->db->query("select * from category  where cat_slug='$slug'",false);
	return $query->row();
    }

    function variation_byslug($slug)
    {
	$query = $this->db->query("select v.*, c.*, ct.cat_slug as catslug from variations v, category c, category ct where v.vr_catid=c.cat_id and c.cat_parent=ct.cat_id and  v.vr_slug='$slug'",false);
	return $query->row();
    }

    function page_byslug($slug)
    {
	$query = $this->db->query("select * from pages where p_slug='$slug'",false);
	return $query->row();
    }


    function product_byslug($slug)
    {
	$query = $this->db->query("select p.*, v.*, c.*, u.*, pd.*, ct.cat_slug as catslug, ct.cat_name as catname, ct.cat_id as catid from variations v, category c, category ct, product p, product_details pd, users u where pd.p_id=p.p_id and p.p_vrid=v.vr_id and v.vr_catid=c.cat_id and c.cat_parent=ct.cat_id and u.id=p.p_brand and p.p_slug='$slug'",false);
	return $query->row();
    }


    function product_bypid($slug)
    {
	$query = $this->db->query("select p.*, v.*, c.*, u.*, pd.*, ct.cat_slug as catslug, ct.cat_name as catname, ct.cat_id as catid from variations v, category c, category ct, product p, product_details pd, users u where pd.p_id=p.p_id and p.p_vrid=v.vr_id and v.vr_catid=c.cat_id and c.cat_parent=ct.cat_id and u.id=p.p_brand and p.p_id='$slug'",false);
	return $query->row();
    }


	function products($start, $limit)
    {
	$key = $this->input->post('key'); $key = $this->db->escape_like_str($key);
	$brand = $this->input->post('brand');
	if ( (array) $brand !== $brand ) {  }else{if(sizeof($brand)>0){$brand = implode(",", $brand);}else{ $brand =''; }}
	$vrid = $this->input->post('vrid');
	if ( (array) $vrid !== $vrid ) {  }else{if(sizeof($vrid)>0){$vrid = implode(",", $vrid);}else{ $vrid =''; }}
	$subcat = $this->input->post('subcat');
	$cat = $this->input->post('cat');
	if($key!=''){ $sq .= " and (v.vr_name like '%$key%' OR c.cat_name like '%$key%' OR p.p_title like '%$key%' OR p.p_model like '%$key%' OR u.user_company like '%$key%')";}
	if($brand!='' && $brand!=0){ $sq .= " and p.p_brand in($brand)";}
	if($vrid!='' && $vrid!=0){ $sq .= " and p.p_vrid in($vrid)";}
	if($subcat>0){ $sq .= " and p.cat_sid='$subcat'";}
	if($cat>0){ $sq .= " and p.cat_id='$cat'";}
	$query = $this->db->query("select p.*, c.*, v.*, u.*, ct.cat_slug as catslug from product p, users u, category c, variations v, category ct where p.p_brand=u.id and p.p_vrid=v.vr_id and p.cat_sid=c.cat_id and ct.cat_id=p.cat_id  $sq order by p.p_title desc Limit $start,$limit",false);
	return $query->result();
    }


	function inventory($start, $limit)
    {
	 if($this->session->userdata('user_type')==1){$uid =$this->session->userdata('user_uid');}else{ $uid =$this->session->userdata('user_owner');}
	$key = $this->input->post('query'); $key = $this->db->escape_like_str($key);
	$brand = $this->input->post('brand');
	$vrid = $this->input->post('vrid');
	$subcat = $this->input->post('subcat');
	$cat = $this->input->post('cat');
	if($key!=''){ $sq .= " and (v.vr_name like '%$key%' OR c.cat_name like '%$key%' OR p.p_title like '%$key%' OR p.p_model like '%$key%' OR u.user_company like '%$key%')";}
	if($brand>0){ $sq .= " and p.p_brand='$brand'";}
	if($vrid>0){ $sq .= " and p.p_vrid='$vrid'";}
	if($subcat>0){ $sq .= " and p.cat_sid='$subcat'";}
	if($cat>0){ $sq .= " and p.cat_id='$cat'";}
	
	$query = $this->db->query("select * from product p, users u, category c, variations v, vendors vnd where vnd.v_pid=p.p_id and vnd.v_vendor='$uid' and p.p_brand=u.id and p.p_vrid=v.vr_id and p.cat_sid=c.cat_id $sq order by p.p_title desc Limit $start,$limit",false);
	return $query->result();
    }

	function removelib()
    {
		 $id = $this->input->post('query');
		 if($this->session->userdata('user_type')==1){$uid =$this->session->userdata('user_uid');}else{ $uid =$this->session->userdata('user_owner');}
		 if($id>0){
		 $query = $this->db->query("delete from vendors where v_id='$id' and v_vendor='$uid'");
		 }
	}



	function keyfinder($tp)
    {
	$key = $this->input->post('key'); $key = $this->db->escape_like_str($key);
/*// exact match
	$query = $this->db->query("select * from category where cat_name='$key' and cat_type=1");
	if($query->num_rows()>0){ $row = $query->row(); return '1|'.$row->cat_parent.'|'.$row->cat_id;}
	
	$query = $this->db->query("select * from variations vr, category c where c.cat_id=vr.vr_catid and vr.vr_name='$key'");
	if($query->num_rows()>0){ $row = $query->row(); return '2|'.$row->cat_parent.'|'.$row->cat_id;}

	$query = $this->db->query("select * from product where p_model='$key' or p_title='$key'");
	if($query->num_rows()>0){ $row = $query->row(); return '3|'.$row->cat_id.'|'.$row->cat_sid;}

	$query = $this->db->query("select * from users where user_type=3 and user_company='$key'");
	if($query->num_rows()>0){ $row = $query->row(); return '4|'.$row->id;}
// like search
	$query = $this->db->query("select * from category where cat_name like '%$key%' and cat_type=1");
	if($query->num_rows()>0){ $row = $query->row(); return '1|'.$row->cat_parent.'|'.$row->cat_id;}
	
	$query = $this->db->query("select * from variations vr, category c where c.cat_id=vr.vr_catid and vr.vr_name like '%$key%'");
	if($query->num_rows()>0){ $row = $query->row(); return '2|'.$row->cat_parent.'|'.$row->cat_id;}

	$query = $this->db->query("select * from product where p_model='$key' or p_title like '%$key%'");
	if($query->num_rows()>0){ $row = $query->row(); return '3|'.$row->cat_id.'|'.$row->cat_sid;}

	$query = $this->db->query("select * from users where user_type=3 and user_company like '%$key%'");
	if($query->num_rows()>0){ $row = $query->row(); return '4|'.$row->id;}
*/
/*$sql="select *,
MATCH(c.cat_name) AGAINST ('$words' IN BOOLEAN MODE) * 5 as rel1,
MATCH(vr.vr_name) AGAINST ('$words' IN BOOLEAN MODE) * 10 as rel2 from category c, variations vr where vr.vr_catid=c.cat_id and MATCH (c.cat_name, vr.vr_name) 
      AGAINST ('$words' IN BOOLEAN MODE)
ORDER BY (rel1)+(rel2) DESC";
*/

	$sq .= " MATCH(c.cat_name) AGAINST ('$key' IN BOOLEAN MODE) * 10 as rel1, MATCH(v.vr_name) AGAINST ('$key' IN BOOLEAN MODE) * 10 as rel2, MATCH(p.p_title) AGAINST ('$key' IN BOOLEAN MODE) * 10 as rel3, MATCH(p.p_model) AGAINST ('$key' IN BOOLEAN MODE) * 10 as rel4, MATCH(u.user_company) AGAINST ('$key' IN BOOLEAN MODE) * 10 as rel5";	
if($tp=='cats'){
if(trim($key)!=''){
$query = $this->db->query("select c.cat_id, c.cat_name, $sq from product p, users u, category c, variations v where p.p_brand=u.id and p.p_vrid=v.vr_id and p.cat_sid=c.cat_id and MATCH (c.cat_name, v.vr_name, p.p_title, p.p_model, u.user_company) AGAINST ('$key' IN BOOLEAN MODE) group by c.cat_id ORDER BY (rel1)+(rel2)+(rel3)+(rel4)+(rel5) DESC",false);	
}else{
$query = $this->db->query("select cat_id, cat_name from category where cat_type=1 order by cat_name",false);	
}
}	

if($tp=='vars'){
if(trim($key)!='' && $cat=''){
$query = $this->db->query("select v.vr_id, v.vr_name, $sq from product p, users u, category c, variations v where p.p_brand=u.id and p.p_vrid=v.vr_id and p.cat_sid=c.cat_id and MATCH (c.cat_name, v.vr_name, p.p_title, p.p_model, u.user_company) AGAINST ('$key' IN BOOLEAN MODE) group by v.vr_id ORDER BY (rel1)+(rel2)+(rel3)+(rel4)+(rel5) DESC",false);
}else{
$query = $this->db->query("select vr_id, vr_name from variations where vr_catid='$cat' order by vr_name",false);	
}
}	


if($tp=='brands'){
if(trim($key)!=''){
	$query = $this->db->query("select u.id, u.user_company, $sq from product p, users u, category c, variations v where p.p_brand=u.id and p.p_vrid=v.vr_id and p.cat_sid=c.cat_id and MATCH (c.cat_name, v.vr_name, p.p_title, p.p_model, u.user_company) AGAINST ('$key' IN BOOLEAN MODE) group by u.id ORDER BY (rel1)+(rel2)+(rel3)+(rel4)+(rel5) DESC",false);
}else{
$query = $this->db->query("select id, user_company from users where user_type=3 order by user_company",false);	
}	
}	

	return $query->result();
    }

function mykeyfinder($tp)
    {
	$key = $this->input->post('key'); $key = $this->db->escape_like_str($key);
	if($this->session->userdata('user_type')==1){$uid =$this->session->userdata('user_uid');}else{ $uid =$this->session->userdata('user_owner');}
	$sq .= " MATCH(c.cat_name) AGAINST ('$key' IN BOOLEAN MODE) * 10 as rel1, MATCH(v.vr_name) AGAINST ('$key' IN BOOLEAN MODE) * 10 as rel2, MATCH(p.p_title) AGAINST ('$key' IN BOOLEAN MODE) * 10 as rel3, MATCH(p.p_model) AGAINST ('$key' IN BOOLEAN MODE) * 10 as rel4, MATCH(u.user_company) AGAINST ('$key' IN BOOLEAN MODE) * 10 as rel5";	
if($tp=='cats'){
if(trim($key)!=''){
$query = $this->db->query("select c.cat_id, c.cat_name, $sq from product p, users u, category c, variations v, vendors vr where vr.v_pid=p.p_id and vr.v_vendor='$uid' and p.p_brand=u.id and p.p_vrid=v.vr_id and p.cat_sid=c.cat_id and MATCH (c.cat_name, v.vr_name, p.p_title, p.p_model, u.user_company) AGAINST ('$key' IN BOOLEAN MODE) group by c.cat_id ORDER BY (rel1)+(rel2)+(rel3)+(rel4)+(rel5) DESC",false);	
}else{
$query = $this->db->query("select c.cat_id, c.cat_name, $sq from product p, users u, category c, variations v, vendors vr where vr.v_pid=p.p_id and vr.v_vendor='$uid' and p.p_brand=u.id and p.p_vrid=v.vr_id and p.cat_sid=c.cat_id group by c.cat_id order by c.cat_name",false);	

}
}	

if($tp=='vars'){
if(trim($key)!=''){
$query = $this->db->query("select v.vr_id, v.vr_name, $sq from product p, users u, category c, variations v, vendors vr where vr.v_pid=p.p_id and vr.v_vendor='$uid' and p.p_brand=u.id and p.p_vrid=v.vr_id and p.cat_sid=c.cat_id and MATCH (c.cat_name, v.vr_name, p.p_title, p.p_model, u.user_company) AGAINST ('$key' IN BOOLEAN MODE) group by v.vr_id ORDER BY (rel1)+(rel2)+(rel3)+(rel4)+(rel5) DESC",false);
}else{
$query = $this->db->query("select v.vr_id, v.vr_name, $sq from product p, users u, category c, variations v, vendors vr where vr.v_pid=p.p_id and vr.v_vendor='$uid' and p.p_brand=u.id and p.p_vrid=v.vr_id and p.cat_sid=c.cat_id group by v.vr_id order by v.vr_name",false);
}
}	


if($tp=='brands'){
if(trim($key)!=''){
	$query = $this->db->query("select u.id, u.user_company, $sq from product p, users u, category c, variations v, vendors vr where vr.v_pid=p.p_id and vr.v_vendor='$uid' and p.p_brand=u.id and p.p_vrid=v.vr_id and p.cat_sid=c.cat_id and MATCH (c.cat_name, v.vr_name, p.p_title, p.p_model, u.user_company) AGAINST ('$key' IN BOOLEAN MODE) group by u.id ORDER BY (rel1)+(rel2)+(rel3)+(rel4)+(rel5) DESC",false);
}else{
	$query = $this->db->query("select u.id, u.user_company, $sq from product p, users u, category c, variations v, vendors vr where vr.v_pid=p.p_id and vr.v_vendor='$uid' and p.p_brand=u.id and p.p_vrid=v.vr_id and p.cat_sid=c.cat_id group by u.id order by u.user_company",false);
}
}	

	return $query->result();
    }

/**************************** DEPARTMENTS ****************/
/// request sent to other vendor

/**************************** USERS ****************/
	function insert_mail($email, $subject, $msg)
    {
		 $today= date("Y-m-d H:i:s");
		 $query = $this->db->query("insert into campaign_recievers(recv_mobemail, recv_subject, recv_msg, recv_date) values('$email', '$subject', '$msg', '$today')");
	}



	function insert_signup()
    {

		 $name = $this->input->post('name');
		 $company = $this->input->post('company');
		 $email = $this->input->post('email');
		 $mobile = $this->input->post('mobile');
		 $address = $this->input->post('address');
		 $city = $this->input->post('city');
		 $country = $this->input->post('country');
		 $state = $this->input->post('state');
		 $zip = $this->input->post('zip');
		 $type = $this->input->post('type');
		 $today= date("Y-m-d H:i:s");
		 $query = $this->db->query("insert into users(user_id, user_pass, user_cat, user_name, user_company, user_fax, user_phone, user_mobile, user_web, user_email, user_date, user_address,  user_city,  user_state, user_country, user_postcode, user_status, user_type) values('$email', '$password',  '1', '$name', '$company', '$fax', '$phone', '$mobile', '$web', '$email', '$today', '$address', '$city', '$state', '$country', '$zip',  'Active', '$type')");
		 
	}



//////
	function insert_quote()
    {
		 $name = $this->input->post('name');
		 $company = $this->input->post('company');
		 $email = $this->input->post('email');
		 $mobile = $this->input->post('mobile');
		 $location = $this->input->post('location');
		  $timetocall = $this->input->post('timetocall');
		 $qty = $this->input->post('qty');
		 $type = $this->input->post('type'); if($type>0){}else{ $type=1;}
		  $pid = $this->input->post('pid');

if($pid>0){
  $prod = $this->apicall->product_view($pid);
  $catid = $prod->cat_parent;
  $catsid = $prod->cat_sid;
  $vrid = $prod->p_vrid;		  
}else{		  
		  $catid = $this->input->post('catid');
		  $catsid = $this->input->post('catsid');
		  $vrid = $this->input->post('vrid');		  
}		  
		 $pname = $this->input->post('pname');
		 $message = $this->input->post('message');
		 $today= date("Y-m-d H:i:s"); $ip= $_SERVER['REMOTE_ADDR']; $url=$_SERVER['HTTP_REFERER'];

		 $query = $this->db->query("select id from users where user_email='$email' or user_mobile='$mobile'");
		 if($query->num_rows()==0){ 
		 	$password=substr($email,0,4);
			$loc=explode(",", $location);
			if(count($loc)==1){ $city=$loc[0];}
			if(count($loc)==2){ $state=$loc[0]; $country=$loc[1];}
			if(count($loc)==3){ $city=$loc[0]; $state=$loc[1]; $country=$loc[2];}
			if(count($loc)==4){ $address=$loc[0]; $city=$loc[1]; $state=$loc[2]; $country=$loc[3];}
		 	$query = $this->db->query("insert into users(user_id, user_pass, user_cat, user_name, user_company, user_fax, user_phone, user_mobile, user_web, user_email, user_date, user_address,  user_city,  user_state, user_country, user_postcode, user_status, user_type) values('$email', '$password',  '1', '$name', '$company', '$fax', '$phone', '$mobile', '$web', '$email', '$today', '$address', '$city', '$state', '$country', '$zip',  'Active', '2')");
			$buyer = $this->db->insert_id();
		 }else{
		 $buyr = $query->row();
		 $buyer=$buyr->id;
		 }
		 
		 
			$y=date("Y"); $ny=date("y")+1;
			if($type==1){$pre='QR'.$y; $tp=7; $invtp=7;}else{ $pre='PR'.$y; $tp=5; $invtp=5;}
			$query = $this->db->query("select * from orders where o_type='$tp' and o_date like '%$y%'");
			$num=$query->num_rows();
			$num=$num+1;
			$invoice =$pre.'/'.$num;
		 	$query = $this->db->query("insert into orders(o_desc,  o_invoice, o_date, o_buyer, o_createdby, o_type, o_invtype, o_ind) values('$message', '$invoice', '$today', '$buyer', '$buyer', '$tp', '$invtp', '$ind')");
			$oid = $this->db->insert_id();
			if(is_array($vrid)){
			foreach($vrid as $vr){
		 $query = $this->db->query("insert into leads(ld_name, ld_company, ld_timetocall, ld_mobile, ld_email, ld_location, ld_pname, ld_pid, ld_catid, ld_catsid, ld_vrid, ld_qty, ld_message, ld_ip, ld_pageurl, ld_date, ld_type) values('$name', '$company', '$timetocall', '$mobile', '$email', '$location', '$pname', '$pid', '$catid', '$catsid', '$vr',  '$qty', '$message', '$ip', '$url', '$today', '$type')");			
			$this->db->query("insert into odetails(o_id, d_pid, d_catid, d_catsid, d_vrid, d_qty, d_uid, d_date) values('$oid', '$pid', '$catid', '$catsid', '$vr', '$qty', '$buyer', '$today')");				
			}
			}else{
		 	$this->db->query("insert into leads(ld_name, ld_company, ld_timetocall, ld_mobile, ld_email, ld_location, ld_pname, ld_pid, ld_catid, ld_catsid, ld_vrid, ld_qty, ld_message, ld_ip, ld_pageurl, ld_date, ld_type) values('$name', '$company', '$timetocall', '$mobile', '$email', '$location', '$pname', '$pid', '$catid', '$catsid', '$vrid',  '$qty', '$message', '$ip', '$url', '$today', '$type')");			
			$this->db->query("insert into odetails(o_id, d_pid, d_catid, d_catsid, d_vrid, d_qty, d_uid, d_date) values('$oid', '$pid', '$catid', '$catsid', '$vrid', '$qty', '$buyer', '$today')");	
			}
			$this->input->set_cookie('uinfo', $buyer, 315360000);
			 	
			return $oid;
		 
		}



	function insert_callback()
    {
		 $name = $this->input->post('name');
		 $company = $this->input->post('company');
		 $email = $this->input->post('email');
		 $mobile = $this->input->post('mobile');
		 $location = $this->input->post('location');
		  $timetocall = $this->input->post('timetocall');
		 $qty = $this->input->post('qty');
		 $type = $this->input->post('type');
		 $message = $this->input->post('message');
		 $today= date("Y-m-d H:i:s"); $ip= $_SERVER['REMOTE_ADDR']; $url=$_SERVER['HTTP_REFERER'];		 

		 $query = $this->db->query("select id from users where user_email='$email' or user_mobile='$mobile'");
		 if($query->num_rows()==0){ 
		 	$password=substr($email,0,4);
			$loc=explode(",", $location);
			if(count($loc)==1){ $city=$loc[0];}
			if(count($loc)==2){ $state=$loc[0]; $country=$loc[1];}
			if(count($loc)==3){ $city=$loc[0]; $state=$loc[1]; $country=$loc[2];}
			if(count($loc)==4){ $address=$loc[0]; $city=$loc[1]; $state=$loc[2]; $country=$loc[3];}
		 	$query = $this->db->query("insert into users(user_id, user_pass, user_cat, user_name, user_company, user_fax, user_phone, user_mobile, user_web, user_email, user_date, user_address,  user_city,  user_state, user_country, user_postcode, user_status, user_type) values('$email', '$password',  '1', '$name', '$company', '$fax', '$phone', '$mobile', '$web', '$email', '$today', '$address', '$city', '$state', '$country', '$zip',  'Active', '2')");
			$buyer = $this->db->insert_id();
		 }else{
		 $buyr = $query->row();
		 $buyer=$buyr->id;
		 }
		 
$this->db->query("insert into leads(ld_name, ld_company, ld_timetocall, ld_mobile, ld_email, ld_location, ld_pname, ld_pid, ld_catid, ld_catsid, ld_vrid, ld_qty, ld_message, ld_ip, ld_pageurl, ld_date, ld_type) values('$name', '$company', '$timetocall', '$mobile', '$email', '$location', '$pname', '$pid', '$catid', '$catsid', '$vrid',  '$qty', '$message', '$ip', '$url', '$today', '$type')");			
		 
		$this->input->set_cookie('uinfo', $buyer, 315360000); 			 
		}



function insert_views($brand, $catid, $scatid, $vrid, $pid){
			 $today= date("Y-m-d H:i:s"); $ip= $_SERVER['REMOTE_ADDR']; $refurl=$_SERVER['HTTP_REFERER'];
			 $url = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";

					if($this->session->userdata('user_uid')>0){
					$uid = $this->session->userdata('user_uid');
					}else{
					$uid = $this->input->cookie('uinfo');
					} 
		 			$this->db->query("insert into views(vw_uid, vw_catid, vw_catsid, vw_vrid, vw_pid, vw_brand,  vw_ip, vw_date, vw_refurl, vw_pageurl) values('$uid', '$catid', '$scatid', '$vrid', '$pid', '$brand', '$ip', '$today', '$refurl',  '$url')");
}

/******************PRODUCTS ***************/ 

	function product_view($id)
    {
		 $query = $this->db->query("select * from product p, users u, category c, variations v where p.p_brand=u.id and p.p_vrid=v.vr_id and p.cat_sid=c.cat_id and p.p_id='$id'");
		 return $query->row();
	}

	function product_details($id)
    {
		 $query = $this->db->query("select * from product_details where p_id='$id'");
		 return $row = $query->row();
	}

	function vendors_count($id)
    {
		 $query = $this->db->query("select v_id from vendors where v_pid='$id'");
		 return $query->num_rows();
	}


    
}