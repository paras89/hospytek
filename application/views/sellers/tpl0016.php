<!doctype html>
<html ng-app="storeApp">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
        <meta name="description" content="<?php echo $meta_desc;?>">
        <meta name="author" content="Hospytek">
        <meta name="keywords" content="<?php echo $meta_key;?>">
        <meta name="robots" content="all">
        <title><?php echo $meta_title;?></title>
        <link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0016/plugins/bootstrap/bootstrap.min.css">
        <link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0016/plugins/anima/anima.css">
        <link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0016/plugins/owlcarousel/owl.carousel.css">
        <link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0016/css/style.min.css">
        <link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0016/css/switcher.css">
        <link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0016/css/green.css" class="colors">
        <link rel="shortcut icon" href="https://www.hospytek.com/assets/sellers/tpl0016/img/ico/32.png" sizes="32x32" type="image/png"/>
        <link rel="apple-touch-icon-precomposed" href="https://www.hospytek.com/assets/sellers/tpl0016/img/ico/60.png" type="image/png"/>
        <link rel="apple-touch-icon-precomposed" sizes="72x72" href="https://www.hospytek.com/assets/sellers/tpl0016/img/ico/72.png" type="image/png"/>
        <link rel="apple-touch-icon-precomposed" sizes="120x120" href="https://www.hospytek.com/assets/sellers/tpl0016/img/ico/120.png" type="image/png"/>
        <link rel="apple-touch-icon-precomposed" sizes="152x152" href="https://www.hospytek.com/assets/sellers/tpl0016/img/ico/152.png" type="image/png"/>
    </head>
    <body id="home">
        <div id="main-nav" class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header navbar:fixed;">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                    </button>
                    <div class="logo">
                                <a href="<?php echo $brand->user_slug;?>">  
                                    <?  if($brand->user_image=='') 
                                        { ?>
                                            <a href="<?php echo $brand->user_slug;?>"><?php echo $brand->user_company?></a><br/>
                                        <? } else if($brand->user_image=='no.gif') { ?>
                                    <a href="<?php echo $brand->user_slug;?>" style="text-decoration:none; color:white;"><b></b><?php echo $brand->user_company;?></b></a><br/>
                                    <? }
                                        else
                                        { $imgurl= 'http://www.hospytek.com/newcrm/UserFiles/Image/'.$brand->user_image;?>
                                    <a href="<?php echo $brand->user_slug;?>"><img src="<?=$imgurl?>" alt="<?=$brand->user_company?>" style="height:70px;margin-top:5px;"/></a><br/>
                                    <? } 
                                ?>
                                   </a>
                                &nbsp;&nbsp;<span style="color:white;"><?php echo $brand->user_city;?>, <?php echo $brand->user_state;?> <?php echo $brand->user_country;?></span></h2>
                        </div>
                </div>
                <div class="collapse navbar-collapse">
                    <ul id="navigation" class="nav navbar-nav navbar-right text-center">
                        <li><a href="<?php echo $brand->user_slug;?>">Home</a></li>
                        <li><a href="<?php echo $brand->user_slug;?>#about">About</a></li>
                        <li><a href="<?php echo $brand->user_slug;?>#products">Products</a></li>
                        <li><a href="<?php echo $brand->user_slug;?>#contact">Contact</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <section id="slider" style="background-color:wheat">
            <!--slider-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div id="slider-carousel" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
                                <li data-target="#slider-carousel" data-slide-to="1"></li>
                                <li data-target="#slider-carousel" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                <?php $i=0; foreach ($items as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;} ;
                                    if($i>6){ break;}
                                    ?>
                                <?php if ($i==0) { $i++; ?>
                                <div class="item active">
                                    <div class="col-sm-6" style="margin-top:160px">
                                        <h1><?php echo $rw->p_title." ".$rw->vr_name;?></h1>
                                        <h3><strong>Model:</strong> <?php echo $rw->p_model;?></h3>
                                        <p>
                                            <?php echo $rw->p_sdesc;?>
                                        </p>
                                        <a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online"><button type="button" class="btn btn-default get">View Detail</button></a>
                                        <a href="<?php echo '../pdf/'.$rw->p_slug.'.pdf'?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Brochure Manual Download"><button type="button" class="btn btn-default get">View Brochure</button></a>
                                    </div>
                                    <div class="col-sm-6" style="margin-top:120px">
                                        <img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="girl img-responsive" alt="" />
                                    </div>
                                </div>
                                <?php } else  {  ?>
                                <div class="item">
                                    <div class="col-sm-6" style="margin-top:160px">
                                        <h1><?php echo $rw->p_title." ".$rw->vr_name;?></h1>
                                        <h3><strong>Model:</strong> <?php echo $rw->p_model;?></h3>
                                        <p>
                                            <?php echo $rw->p_sdesc;?>
                                        </p>
                                        <a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online"><button type="button" class="btn btn-default get">View Detail</button></a>
                                        <a href="<?php echo '../pdf/'.$rw->p_slug.'.pdf'?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Brochure Manual Download"><button type="button" class="btn btn-default get">View Brochure</button></a>
                                    </div>
                                    <div class="col-sm-6" style="margin-top:120px">
                                        <img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="girl img-responsive" alt="" />
                                    </div>
                                </div>
                                <?php  } ?>
                                <?php  } ?>
                            </div>
                            <a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
                            <i class="fa fa-angle-left"></i>
                            </a>
                            <a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
                            <i class="fa fa-angle-right"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/slider-->
        <section id="about" class="padding-top-bottom ">
            <div class="container">
                <div class="row">
                    <div class="col-md-8 cta-message anima fade-up">
                        <h2>WELCOME TO <?=$brand->user_company?></h2>
                        <p><?=$brand->user_about?></p>
                    </div>
                </div>
            </div>
        </section>
        <section id="products" class=" padding-top-bottom" ng-controller="productsController">
            <div class="container">
                <header class="section-header text-center anima fade-up">
                    <h2>Products</h2>
                </header>
            </div>
            <div class="container ">
                <div id="projects-container" class="row">
                    <?php $i=0; foreach ($items as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;}  $i++;?>
                    <?php if($i==1){echo '<div class="row">';}?>	
                    <article class="design product col-xs-12 col-sm-6 col-md-4 anima fade-up">
                        <div class="img-box">
                            <img class="img-responsive project-image" src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>">
                        </div>
                        <div class="product-info col-md-12" style="text-align:center">
                            <div>
                                <h4 class="project-title" style="text-align:center"><?php echo $rw->p_title." ".$rw->vr_name;?></h4>
                                <br>
                                <p  style="text-align:center">Model : <span><?php echo $rw->p_model;?></span></p>
                            </div>
                        </div>
                              <a style="text-align:center;margin-left:38%" href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online"><button type="button" class="btn btn-primary btn-sm" style="text-align:center" >View Details </button></a>
                    
                    </article>
                  
                    <?php if($i==3){ $i=0; echo '</div><div class="clearfix">&nbsp; </div>';}?>
                    <? }?>
                    <!--	<article class="mock-up product col-xs-12 col-sm-6 col-md-4 anima fade-up d1">
                        <div class="img-box">
                        	
                        	<img class="img-responsive project-image" src="img/product2.jpg" alt="">
                        </div>
                        <div class="product-info col-md-12">
                        	<p class="project-price">model</p>
                        	<div>
                        		<h4 class="project-title">product name</h4>
                        	<div>
                        	</div>
                        		<button type="button" >view details </button> &emsp;<button type="button" >get quote </button>							
                        </div>
                        </div>
                        <div class="sr-only project-description" data-images="img/06.jpg,img/07.jpg,img/08.jpg,img/09.jpg">
                        	
                        </div>
                        </article>
                        
                        <article class=" icons product col-xs-12 col-sm-6 col-md-4 anima fade-up d2">
                        <div class="img-box">
                        	
                        	<img class="img-responsive project-image" src="img/product3.jpg" alt="">
                        </div>
                        <div class="product-info col-md-12">
                        	<p class="project-price">model</p>
                        	<div>
                        		<h4 class="project-title">Product name</h4>
                        	<div>
                        	</div>
                        		<button type="button" >view details </button> &emsp;<button type="button" >get quote </button>							
                        </div>
                        </div>
                        
                        </article>-->
                </div>
            </div>
        </section>
        <section id="contact" class="gray-bg padding-top-bottom">
            <div class="container">
                <header class="section-header text-center">
                    <h1><strong>Contact</strong></h1>
                </header>
                <form action="http://demo.angelostudio.net/microstore/index.html" method="post" novalidate id="contact-form">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-4 contact-info cta-message anima fade-right">
                            <address>
                                <p><?=$brand->user_address?></p>
                                <p><?=$brand->user_city?>, <?=$brand->user_state?></p>
                                <p><?=$brand->user_country?>  <?=$brand->user_postcode?></p>
                                <p>+91-<?=$brand->user_mobile?>     <?=$brand->user_phone?></p>
                            </address>
                        </div>
                        <div class="col-xs-12 col-sm-12 col-md-8 anima fade-up d1" >
                            <div class="form-group">
                                <label class="control-label" for="contact-name">Name</label>
                                <div class="controls">
                                    <input id="contact-name" name="contactName" placeholder="Your name" class="form-control input-lg requiredField" type="text">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="contact-mail">Email</label>
                                <div class=" controls">
                                    <input id="contact-mail" name="email" placeholder="Your email" class="form-control input-lg requiredField" type="email">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label" for="contact-message">Message</label>
                                <div class="controls">
                                    <textarea id="contact-message" name="comments" placeholder="Your message" class="form-control input-lg requiredField" rows="5"></textarea>
                                </div>
                            </div>
                            <p>
                                <button name="submit" type="submit" class="btn btn-store btn-block">Send Message</button>
                            </p>
                            <input type="hidden" name="submitted" id="submitted3" value="true">
                        </div>
                    </div>
                </form>
            </div>
        </section>
        <footer id="footer" style="background-color:black;">
            <div class="container">
                <div class="row">
                    <div class="col-md-12">
                        <div class="copyright small">
                            <a href="#">
                                <p style="text-align:center">Copyright 2017 All rights reserved</p>
                            </a>
                        </div>
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container -->
        </footer>
        <!-- End footer -->
        <!-- Scroll to top button -->
        <a href="#body-content" class="scrolltotop sm-scroll bg-main"><i class="fa fa-chevron-up"></i></a>
        </div>
        <script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0016/plugins/plugins.js"></script>
        <script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0016/js/custom2.min.js"></script>
        <script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0016/js/ga.js"></script>
        

    <script src="https://www.hospytek.com/assets/sellers/tpl0001/js/jquery.js"></script>
    
    
   
    </body>
  
</html>

