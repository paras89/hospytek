<?php $this->view('common/header'); ?>
<div class="body-content" id="top-banner-and-menu">

        <!-- ========================================== SECTION – HERO ========================================= -->
        
        <div id="hero">
          <div id="owl-main" class="owl-carousel owl-inner-nav owl-ui-sm">
            <div class="item" style="background-image: url(assets/images/sliders/01.jpg);">
              <div class="container-fluid" align="center">
                <div class="caption bg-color vertical-center" style="width:100%">
                  <div class="slider-header fadeInDown-1" style="color:#d3004d">Worldwide Marketplace for importers, vendors, suppliers & manufacturers in </div>
                  <div class="big-text fadeInDown-1" style="color:#0f6cb2">Medical Equipment Industry</div>
                  <div class="excerpt fadeInDown-2 hidden-xs" style="font-family:Arial, Helvetica, sans-serif"> <span>The Perfect Marketplace of Medical Device & Equipments where Buyers Meets Sellers! <br /> &nbsp; Top Medical Equipment & Devices accross the globe on a single click at Hospytek.</span> </div>
                  <div class="button-holder fadeInDown-3"> <a data-toggle="modal" href="#modal-signup" class="btn-lg btn btn-uppercase btn-primary shop-now-button">Join Hospytek Today</a> </div>
                </div>
                <!-- /.caption --> 
              </div>
              <!-- /.container-fluid --> 
            </div>
            <!-- /.item -->
            
            <div class="item" style="background-image: url(assets/images/sliders/slide-inst.jpg);">
              <div class="container-fluid">
                <div class="caption bg-color vertical-center text-left">
                  <div class="slider-header fadeInDown-1">Intelligent solution for</div>
                  <div class="big-text fadeInDown-1"> Medical <span class="highlight">Institutions</span> </div>
                  <div class="excerpt fadeInDown-2 hidden-xs"> <span>One stop solution to manage your medical device & equipment requirements</span> </div>
                  <div class="button-holder fadeInDown-3"> <a data-toggle="modal"  href="#modal-signup" class="btn-lg btn btn-uppercase btn-primary shop-now-button">Join Now</a> </div>
                </div>
                <!-- /.caption --> 
              </div>
              <!-- /.container-fluid --> 
            </div>
            <!-- /.item --> 



            <div class="item" style="background-image: url(assets/images/sliders/slide-vendor.jpg);">
              <div class="container-fluid">
                <div class="caption bg-color vertical-center text-right" style="width:94%">
                  <div class="slider-header fadeInDown-1">Robust Technology to boost the performance of </div>
                  <div class="big-text fadeInDown-1"> Vendors & <span class="highlight">Brands</span> </div>
                  <div class="excerpt fadeInDown-2 hidden-xs"> <span>One stop marketplace to sale your medical devices worldwide.</span> </div>
                  <div class="button-holder fadeInDown-3"> <a data-toggle="modal" href="#modal-signup" class="btn-lg btn btn-uppercase btn-primary shop-now-button">Join Now</a> </div>
                </div>
                <!-- /.caption --> 
              </div>
              <!-- /.container-fluid --> 
            </div>
            <!-- /.item --> 

            
          </div>
          <!-- /.owl-carousel --> 
        </div>
        
        <!-- ========================================= SECTION – HERO : END ========================================= --> 
        
        <!-- ============================================== INFO BOXES ============================================== -->
        <div class="info-boxes wow fadeInUp">
          <div class="info-boxes-inner" style="background-color:#d3004d">
            <div class="row">
              <div class="col-md-3 col-sm-4 col-lg-3">
                <a href="landing/hospitals-clinics" title="Medical Equipments Management for Hospitals & Clinics">   
                <div class="info-box">
                  <div class="row">
                    <div class="col-xs-12">
					  <h4 class="icon"><i class="fa fa-stethoscope"></i></h4>
                      <h4 class="info-box-heading green">hospitals/clinics</h4>
                    </div>
                  </div>
                  <h6 class="text">Hassle free solutions for Hospitals & Clinics, One stop for your medical equipment requirements.</h6>
                </div>
                </a>
              </div>
              <!-- .col -->
              
              <div class="hidden-md col-sm-4 col-lg-3">
                 <a href="landing/vendors" title="Medical Equipments Management for Vendors">
                <div class="info-box">
                  <div class="row">
                    <div class="col-xs-12">
					  <h4 class="icon"><i class="fa fa-users"></i></h4>
                      <h4 class="info-box-heading green">Vendors/Dealers</h4>
                    </div>
                  </div>
                  <h6 class="text">Increase your sale with Hospytek Marketplace, Boost your presence in Medical Industry.</h6>
                </div>
                </a>
              </div>
              <!-- .col -->
              
              <div class="col-md-3 col-sm-4 col-lg-3">
                  <a href="landing/brands" title="Medical Equipments Management for Brands">
                <div class="info-box">
                  <div class="row">
                    <div class="col-xs-12">
                      <h4 class="icon"><i class="fa fa-university"></i></h4>
					  <h4 class="info-box-heading green">Brands/Manufacturers</h4>
					 
                    </div>
                  </div>
                  <h6 class="text">Expand your dealers/vendors network across the world, Increase your sale performance by 200%.</h6>
                </div>
                </a>
              </div>


			  <div class="col-md-3 col-sm-4 col-lg-3">
                <a href="https://www.youtube.com/embed/5_KoxI8sie0" data-lity><div class="info-box">
                  <div class="row">
                    <div class="col-xs-12">
						<h4 class="icon"><i class="fa fa-video-camera"></i></h4>
                      <h4 class="info-box-heading green">Watch Demo</h4>
                    </div>
                  </div>
                  <h6 class="text">Watch our quick video presentation to know how hospytek can boost you in medical industry across the globe.</h6>
                </div></a>
              </div>			  
              <!-- .col --> 
            </div>
            <!-- /.row --> 
          </div>
          <!-- /.info-boxes-inner --> 
          
        </div>
        <!-- /.info-boxes --> 
        <!-- ============================================== INFO BOXES : END ============================================== --> 

  <div class="container">
    <div class="row"> 
     
      <!-- ============================================== CONTENT ============================================== -->
      <div class="col-xs-12 col-sm-12 col-md-12 homebanner-holder"> 
        <!-- ============================================== SCROLL TABS ============================================== -->
       
        <div id="product-tabs-slider" class="scroll-tabs outer-top-vs wow fadeInUp">
            
          <div class="more-info-tab clearfix ">
            <h2 class="new-product-title pull-center">New Medical Equipment & Devices</h2>
            <ul class="nav nav-tabs nav-tab-line pull-center" id="new-products-1">
              <li class="active"><a data-transition-type="backSlide" href="#all" data-toggle="tab">All</a></li>
			  <?php foreach($this->config->item('newcats') as $key=>$val){?>
              <li><a data-transition-type="backSlide" href="#<?php echo slugify($key);?>" data-toggle="tab"><?php echo $key;?></a></li>
			  <?php }?>
            </ul>
            <!-- /.nav-tabs --> 
          </div>
          <div class="tab-content outer-top-xs">
            <div class="tab-pane in active" id="all">
              <div class="product-slider">
                <div class="owl-carousel home-owl-carousel custom-carousel owl-theme" data-item="4">
				  <?php foreach ($new as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;}?>
                  <div class="item item-carousel">
                    <div class="products">
                      <div class="product">
                        <div class="product-image">
                          <div class="image"> <a href="<?php echo base_url().$rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>"><img  src="<?php echo $this->config->item('img_url').$img;?>&w=250" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>"></a> </div>
                          <!-- /.image -->
                          
                          <div class="tag new"><span>new</span></div>
                        </div>
                        <!-- /.product-image -->
                        
                        <div class="product-info text-center" >
							  <h3 class="name"><a href="<?php echo base_url().$rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online"><?php echo $rw->p_title." ".$rw->vr_name;?></a></h3>
                          <div style="text-align:center"><? if($rw->p_price>0){ echo '<span style="text-decoration:line-through; color:#999999"><i class="fa fa-inr"></i>'.$rw->p_mrp.'</span> &nbsp; &nbsp; <i class="fa fa-inr"></i>'.$rw->p_price; }else{ echo 'Price: <b>On Request</b>';}?></div>
                          <div class="description"><?php echo $rw->p_model;?></div>
                          <div class="product-price"><a href="<?php echo $rw->user_slug;?>" title="<?php echo $rw->user_company;?> - <?php echo $rw->user_city;?> <?php echo $rw->user_state;?> Medical Equipment  Device Manufacturer"><?php echo $rw->user_company;?></a></div>
                          <!-- /.product-price --> 
                     		
                        </div>
                        <!-- /.product-info -->
                        
					
<div class="row btnrow">
	<div class="col-md-12 text-center">                          
	 <a href="<?php echo base_url().$rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="view details of <?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="btn btn-sm btn-details" >View Details</a>				   <a href="javascript:;" class="btn btn-quote" pid="<?=$rw->p_id?>" pname="<?php echo $rw->p_title." ".$rw->vr_name;?>">Inquire Now</a>						  
    </div>
</div> 

                    
					
                        <!-- /.cart --> 
                      </div>
                      <!-- /.product --> 
                      
                    </div>
                    <!-- /.products --> 
                  </div>
                  <!-- /.item -->
				  <?php }?>
                </div>
                <!-- /.home-owl-carousel --> 
              </div>
              <!-- /.product-slider --> 
            </div>
            <!-- /.tab-pane -->
			 <?php foreach($this->config->item('newcats') as $key=>$val){  $tabprods=$newitems[$key];?>            
            <div class="tab-pane" id="<?php echo slugify($key)?>">
              <div class="product-slider">
                <div class="owl-carousel home-owl-carousel custom-carousel owl-theme">

				  <?php foreach ($tabprods as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;}?>
                  <div class="item item-carousel">
                    <div class="products">
                      <div class="product">
                        <div class="product-image">
                          <div class="image"> <a href="<?php echo base_url().$rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>"><img  src="<?php echo $this->config->item('img_url').$img;?>&w=250" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>"></a> </div>
                          <!-- /.image -->
                          
                          <div class="tag new"><span>new</span></div>
                        </div>
                        <!-- /.product-image -->
                        
                        <div class="product-info text-center">
                          <h3 class="name"><a href="<?php echo base_url().$rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online"><?php echo $rw->p_title." ".$rw->vr_name;?></a></h3>
                          <div style="text-align:center"><? if($rw->p_price>0){ echo '<span style="text-decoration:line-through; color:#999999"><i class="fa fa-inr"></i>'.$rw->p_mrp.'</span> &nbsp; &nbsp; <i class="fa fa-inr"></i>'.$rw->p_price; }else{ echo 'Price: <b>On Request</b>';}?></div>
                          <div class="description"><?php echo $rw->p_model;?></div>
                          <div class="product-price"><a href="<?php echo $rw->user_slug;?>" title="<?php echo $rw->user_company;?> - <?php echo $rw->user_city;?> <?php echo $rw->user_state;?> Medical Equipment  Device Manufacturer"><?php echo $rw->user_company;?></a></div>
                          <!-- /.product-price --> 
                          
                        </div>
                        <!-- /.product-info -->

<div class="row btnrow">
	<div class="col-md-12 text-center">                          
	 <a href="<?php echo base_url().$rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="view details of <?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="btn btn-sm btn-details" >View Details</a>				   <a href="javascript:;" class="btn btn-quote" pid="<?=$rw->p_id?>" pname="<?php echo $rw->p_title." ".$rw->vr_name;?>">Inquire Now</a>						  
    </div>
</div> 
                        
                        <!-- /.cart --> 
                      </div>
                      <!-- /.product --> 
                      
                    </div>
                    <!-- /.products --> 
                  </div>
                  <!-- /.item -->
				  <?php }?>

              
                </div>
                <!-- /.home-owl-carousel --> 
              </div>
              <!-- /.product-slider --> 
            </div>
            <!-- /.tab-pane -->
            <?php } ?>
            
          
            
          </div>
          <!-- /.tab-content --> 
        </div>
        <!-- /.scroll-tabs --> 
        <!-- ============================================== SCROLL TABS : END ============================================== --> 
        <!-- ============================================== WIDE PRODUCTS ============================================== -->

<div class="wide-banners wow fadeInUp outer-bottom-xs">
          <div class="row">
            <div class="col-md-12">
              <div class="wide-banner cnt-strip">
                <div class="image"><a href="<?php echo base_url();?>./paramount-surgimed-ltd"> <img class="img-responsive" src="assets/images/banners/home-banner11.gif" alt="Medical Equipment Devices"></a> </div>
                <div class="strip strip-text" style="width:100%; display:none">
                  <div class="strip-inner">
                    <h2 class="text-left">Huge discount<br>
                      <span class="shopping-needs">on medical devices</span></h2>
                  </div>
                </div>
                <div class="new-label" style="display:none">
                  <div class="text">NEW</div>
                </div>
                <!-- /.new-label --> 
              </div>
              <!-- /.wide-banner --> 
            </div>
            <!-- /.col --> 
            
          </div>
          <!-- /.row --> 
        </div>
				
        <div class="wide-banners wow fadeInUp outer-bottom-xs" style="display:none">
          <div class="row">
            <div class="col-md-7 col-sm-7">
              <div class="wide-banner cnt-strip">
                <div class="image"> <img class="img-responsive" src="assets/images/banners/home-banner1.jpg" alt="Medical Equipment Devices"></div>
              </div>
              <!-- /.wide-banner --> 
            </div>
            <!-- /.col -->
            <div class="col-md-5 col-sm-5">
              <div class="wide-banner cnt-strip">
                <div class="image"> <img class="img-responsive" src="assets/images/banners/home-banner2.jpg" alt="Medical Equipment Devices"> </div>
              </div>
              <!-- /.wide-banner --> 
            </div>
            <!-- /.col --> 
          </div>
          <!-- /.row --> 
        </div>
        <!-- /.wide-banners --> 
        
        <!-- ============================================== WIDE PRODUCTS : END ============================================== --> 
        <!-- ============================================== FEATURED PRODUCTS ============================================== -->
        <section class="section featured-product wow fadeInUp">
          <h2 class="section-title pull-center">Featured Medical Equipment & Devices</h2>
          <div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs">
				  <?php foreach ($featured as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;}?>
                  <div class="item item-carousel">
                    <div class="products">
                      <div class="product">
                        <div class="product-image">
                          <div class="image"> <a href="<?php echo base_url().$rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>"><img  src="<?php echo $this->config->item('img_url').$img;?>&w=250" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>"></a> </div>
                          <!-- /.image -->
                          
                          <div class="tag hot"><span>hot</span></div>
                        </div>
                        <!-- /.product-image -->
                        
                        <div class="product-info text-center">
                          <h3 class="name"><a href="<?php echo base_url().$rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online"><?php echo $rw->p_title." ".$rw->vr_name;?></a></h3>
                          <div style="text-align:center"><? if($rw->p_price>0){ echo '<span style="text-decoration:line-through; color:#999999"><i class="fa fa-inr"></i>'.$rw->p_mrp.'</span> &nbsp; &nbsp; <i class="fa fa-inr"></i>'.$rw->p_price; }else{ echo 'Price: <b>On Request</b>';}?></div>
                          <div class="description"><?php echo $rw->p_model;?></div>
                          <div class="product-price"><a href="<?php echo $rw->user_slug;?>" title="<?php echo $rw->user_company;?> - <?php echo $rw->user_city;?> <?php echo $rw->user_state;?> Medical Equipment  Device Manufacturer"><?php echo $rw->user_company;?></a></div>
                          <!-- /.product-price --> 
                          
                        </div>
                        <!-- /.product-info -->

<div class="row btnrow">
	<div class="col-md-12 text-center">                          
	 <a href="<?php echo base_url().$rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="view details of <?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="btn btn-sm btn-details" >View Details</a>				   <a href="javascript:;" class="btn btn-quote" pid="<?=$rw->p_id?>" pname="<?php echo $rw->p_title." ".$rw->vr_name;?>">Inquire Now</a>						  
    </div>
</div> 
						
                        <!-- /.cart --> 
                      </div>
                      <!-- /.product --> 
                      
                    </div>
                    <!-- /.products --> 
                  </div>
                  <!-- /.item -->
				  <?php }?>
          </div>
          <!-- /.home-owl-carousel --> 
        </section>
        <!-- /.section --> 
        <!-- ============================================== FEATURED PRODUCTS : END ============================================== --> 
        <!-- ============================================== WIDE PRODUCTS ============================================== -->
        <div class="wide-banners wow fadeInUp outer-bottom-xs">
          <div class="row">
            <div class="col-md-12">
              <div class="wide-banner cnt-strip">
                <div class="image"><a href="<?php echo base_url();?>./bellcross-industries"><img class="img-responsive" src="assets/images/banners/home-banner.gif" alt="Medical Equipment Devices"></a>  </div>
                <div class="strip strip-text" style="width:100%; display:none">
                  <div class="strip-inner">
                    <h2 class="text-left">Huge discount<br>
                      <span class="shopping-needs">on medical devices</span></h2>
                  </div>
                </div>
                <div class="new-label">
                  <div class="text">NEW</div>
                </div>
                <!-- /.new-label --> 
              </div>
              <!-- /.wide-banner --> 
            </div>
            <!-- /.col --> 
            
          </div>
          <!-- /.row --> 
        </div>
        <!-- /.wide-banners --> 
        <!-- ============================================== WIDE PRODUCTS : END ============================================== --> 
        
        
        <!-- ============================================== FEATURED PRODUCTS ============================================== -->
        <section class="section wow fadeInUp new-arriavls">
          <h2 class="section-title">New Arrivals in Medical Equipment & Devices Industry</h2>
          <div class="owl-carousel home-owl-carousel custom-carousel owl-theme outer-top-xs">
				  <?php foreach ($onsale as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;}?>
                  <div class="item item-carousel">
                    <div class="products">
                      <div class="product">
                        <div class="product-image">
                          <div class="image"> <a href="<?php echo base_url().$rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>"><img  src="<?php echo $this->config->item('img_url').$img;?>&w=250" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>"></a> </div>
                          <!-- /.image -->
                          
                          <div class="tag sale"><span>sale</span></div>
                        </div>
                        <!-- /.product-image -->
                        
                        <div class="product-info text-center">
                          <h3 class="name"><a href="<?php echo base_url().$rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online"><?php echo $rw->p_title." ".$rw->vr_name;?></a></h3>
                          <div style="text-align:center"><? if($rw->p_price>0){ echo '<span style="text-decoration:line-through; color:#999999"><i class="fa fa-inr"></i>'.$rw->p_mrp.'</span> &nbsp; &nbsp; <i class="fa fa-inr"></i>'.$rw->p_price; }else{ echo 'Price: <b>On Request</b>';}?></div>
                          <div class="description"><?php echo $rw->p_model;?></div>
                          <div class="product-price"><?php echo $rw->user_company;?></div>
                          <!-- /.product-price --> 
                          
                        </div>
                        <!-- /.product-info -->

<div class="row btnrow">
	<div class="col-md-12 text-center">                          
	 <a href="<?php echo base_url().$rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="view details of <?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="btn btn-sm btn-details" >View Details</a>				   <a href="javascript:;" class="btn btn-quote" pid="<?=$rw->p_id?>" pname="<?php echo $rw->p_title." ".$rw->vr_name;?>">Inquire Now</a>						  
    </div>
</div> 
						
                        <!-- /.cart --> 
                      </div>
                      <!-- /.product --> 
                      
                    </div>
                    <!-- /.products --> 
                  </div>
                  <!-- /.item -->
				  <?php }?>
         
          </div>
          <!-- /.home-owl-carousel --> 
        </section>
        <!-- /.section --> 
        <!-- ============================================== FEATURED PRODUCTS : END ============================================== --> 


        <!-- ============================================== BLOG SLIDER ============================================== -->
        <section class="section latest-blog outer-bottom-vs wow fadeInUp">
          <h2 class="section-title" style="text-align:center;">latest from our blog</h2>
          <div class="blog-slider-container outer-top-xs">
            <div class="owl-carousel blog-slider custom-carousel">
              <div class="item">
                <div class="blog-post">
                  <div class="blog-post-image">
                    <div class="image"> <a href="https://www.hospytek.com/blog/anxiety-depression/"><img src="assets/images/blog-post/post1.jpg" alt="Medical Equipment's Pros & Cons"></a> </div>
                  </div>
                  <!-- /.blog-post-image -->
                  
                  <div class="blog-post-info text-left">
                    <h3 class="name"><a href="https://www.hospytek.com/blog/anxiety-depression/">Anxiety & Depression</a></h3>
                    <span class="info">By Hospytek &nbsp;|&nbsp; 24th April 2017 </span>
                    <p class="text">Sadness and grief are human emotions that cling to you in this journey of ups and downs. But it usually goes away within a few days. </p>
                    <a href="https://www.hospytek.com/blog/anxiety-depression/" class="lnk btn btn-primary">Read more</a> </div>
                  <!-- /.blog-post-info --> 
                  
                </div>
                <!-- /.blog-post --> 
              </div>
              <!-- /.item -->
              
              <div class="item">
                <div class="blog-post">
                  <div class="blog-post-image">
                    <div class="image"> <a href="https://www.hospytek.com/blog/pan-india-a-concern-today/"><img src="assets/images/blog-post/post2.jpg" alt="Why Online Presence is Important for Medical Device Procurement."></a> </div>
                  </div>
                  <!-- /.blog-post-image -->
                  
                  <div class="blog-post-info text-left">
                    <h3 class="name"><a href="https://www.hospytek.com/blog/pan-india-a-concern-today/">PAN INDIA – A Concern Today!</a></h3>
                    <span class="info">By Hospytek &nbsp;|&nbsp; 28th January 2017 </span>
                    <p class="text">‘India’ as known in other countries as – Oh! The one which is quite infamous for its sanitation and cleanliness? Why is it so? </p>
                    <a href="https://www.hospytek.com/blog/pan-india-a-concern-today/" class="lnk btn btn-primary">Read more</a> </div>
                  <!-- /.blog-post-info --> 
                  
                </div>
                <!-- /.blog-post --> 
              </div>
              <!-- /.item --> 
              
              <!-- /.item -->
              
              <div class="item">
                <div class="blog-post">
                  <div class="blog-post-image">
                    <div class="image"> <a href="https://www.hospytek.com/blog/sudden-cardiac-arrest-a-disaster-waiting-to-happen/"><img src="assets/images/blog-post/post3.jpg" alt="Choosing Right Medical Equipments For Hospital"></a> </div>
                  </div>
                  <!-- /.blog-post-image -->
                  
                  <div class="blog-post-info text-left">
                    <h3 class="name"><a href="https://www.hospytek.com/blog/sudden-cardiac-arrest-a-disaster-waiting-to-happen/">Sudden Cardiac Arrest</a></h3>
                    <span class="info">By Hospytek &nbsp;|&nbsp; 1st February 2017 </span>
                    <p class="text">Do you hold your life precious? But are oblivious to the fragile life-lines that each of us hold on to? Cardiac Arrest is one of the ...</p>
                    <a href="https://www.hospytek.com/blog/sudden-cardiac-arrest-a-disaster-waiting-to-happen/" class="lnk btn btn-primary">Read more</a> </div>
                  <!-- /.blog-post-info --> 
                  
                </div>
                <!-- /.blog-post --> 
              </div>
              <!-- /.item -->
              
              <div class="item">
                <div class="blog-post">
                  <div class="blog-post-image">
                    <div class="image"> <a href="https://www.hospytek.com/blog/heart-disease-a-wake-up-call/"><img src="assets/images/blog-post/post4.jpg" alt="Best ECG Machines for Hospitals"></a> </div>
                  </div>
                  <!-- /.blog-post-image -->
                  
                  <div class="blog-post-info text-left">
                    <h3 class="name"><a href="#">HEART ATTACK : A Wake Up Call!</a></h3>
                    <span class="info">By Hospytek &nbsp;|&nbsp; 28th January 2017 </span>
                    <p class="text">In our day to day life we don’t really try to comprehend the possible reasons of heart diseases.  Several health conditions, our lifestyle .. </p>
                    <a href="https://www.hospytek.com/blog/heart-disease-a-wake-up-call/" class="lnk btn btn-primary">Read more</a> </div>
                  <!-- /.blog-post-info --> 
                  
                </div>
                <!-- /.blog-post --> 
              </div>
              <!-- /.item -->
              
              
              <!-- /.item --> 
              
            </div>
            <!-- /.owl-carousel --> 
          </div>
          <!-- /.blog-slider-container --> 
        </section>
        <!-- /.section --> 
        <!-- ============================================== BLOG SLIDER : END ============================================== --> 

        
      </div>
      <!-- /.homebanner-holder --> 
      <!-- ============================================== CONTENT : END ============================================== --> 
    </div>
    <!-- /.row --> 
    <!-- ============================================== BRANDS CAROUSEL ============================================== -->
    <div id="brands-carousel" class="logo-slider wow fadeInUp">
      <div class="logo-slider-inner">
        <div id="brand-slider" class="owl-carousel brand-slider custom-carousel owl-theme">
          <div class="item m-t-15"> <a href="<?php echo base_url();?>./hygia" class="image"> <img data-echo="assets/images/brands/brand4.png" src="assets/images/blank.gif" alt="Hygia Medical Equipments"> </a> </div>
          <!--/.item-->
          
          <div class="item m-t-10"> <a href="<?php echo base_url();?>./philips" class="image"> <img data-echo="assets/images/brands/brand2.png" src="assets/images/blank.gif" alt="Philips Medical Equipments"> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="<?php echo base_url();?>./bpl" class="image"> <img data-echo="assets/images/brands/brand3.png" src="assets/images/blank.gif" alt="BPL Medical Equipments"> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="<?php echo base_url();?>./blue-chemp" class="image"> <img data-echo="http://bluechemp.com/images/bluechamp.png" src="assets/images/blank.gif" alt="Blue Chemp"> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="<?php echo base_url();?>./technocare-medisystems" class="image"> <img data-echo="http://2.imimg.com/data2/VJ/HS/MY-375026/techno-care-medi-systems-logo-120x120.gif" src="assets/images/blank.gif" alt="Technocare Medisystems"> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="<?php echo base_url();?>./siemens" class="image"> <img data-echo="assets/images/brands/brand6.png" src="assets/images/blank.gif" alt="Siemens Medical Equipments"> </a> </div>
          <!--/.item-->
          <div class="item"> <a href="<?php echo base_url();?>./surgitech" class="image"> <img data-echo="http://3.imimg.com/data3/IX/EH/MY-1596901/surgitech-logo-120x120.jpg" src="assets/images/blank.gif" alt="Surgitech"> </a> </div>

          <div class="item"> <a href="<?php echo base_url();?>./flamingo" class="image"> <img data-echo="assets/images/brands/brand8.png" src="assets/images/blank.gif" alt="Flamingo Medical Equipments"> </a> </div>
          
        </div>
        <!-- /.owl-carousel #logo-slider --> 
      </div>
      <!-- /.logo-slider-inner --> 
      
    </div>
    <!-- /.logo-slider --> 
    <!-- ============================================== BRANDS CAROUSEL : END ============================================== --> 
  </div>
  <!-- /.container --> 
</div>
<!-- /#top-banner-and-menu -->
<? if($urw->id==''){?>
<script>
    $(window).load(function(){
		setTimeout(function(){	
       // $('.mpname').removeClass('hidden');
	   // $('.quote-title').html('Are you looking for some <strong>Medical Devices?</strong>');
		$('#modal-home').modal('show');
		}, 1000);
    });	
</script> 
<? }?>
<?php $this->view('common/footer'); ?>