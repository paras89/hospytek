<?php $this->view('common/header-sellers'); ?>
	<!--header-->
	<div class="header">
		<div class="container">
		<!---->
				<div class="logo">
				<!--<a href="<?php echo $brand->user_slug;?>"><img src="assets/sellers/images/logo.png" alt="" ></a>-->
				<h2><?php echo $brand->user_company;?></h2>
				<span><?php echo $brand->user_city;?>, <?php echo $brand->user_state;?> <?php echo $brand->user_country;?></span>
				</div>
				<div class="header-right">
			<div class="header-bottom">
				<div class="top-nav">
					<span class="menu"> </span>
					<ul>
						<li class="active"><a href="<?php echo $brand->user_slug;?>">HOME</a> </li>
						<li><a href="<?php echo $brand->user_slug;?>#about" > ABOUT</a></li>
						<li><a href="<?php echo $brand->user_slug;?>#products" > PRODUCTS</a></li>
						<li><a href="<?php echo $brand->user_slug;?>#contact">CONTACT </a></li>
					</ul>
					<!--script-->
				<script>
					$("span.menu").click(function(){
						$(".top-nav ul").slideToggle(500, function(){
						});
					});
			</script>				
		</div>
		<a  data-toggle="modal" href="#modal-callback" class="cart-sellers">Become a Dealer</a>
		<div class="clearfix"> </div>
		</div>
		</div>
		<div class="clearfix"> </div>
	</div>
</div> 
<!---->
	<div class="container">
	<!--banner-->
		<div class="banner ">	
		  <div class="wmuSlider example1">
			   <div class="wmuSliderWrapper">
			  <?php $i=0; foreach ($items as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;} $i++;
			  if($i>5){ break;}
			  ?>							
			 <article style="position: absolute; width: 100%; opacity: 0;"> 
				   	<div class="banner-wrap">	
						<div class="short">
							<img class="img-responsive" src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" style="box-shadow: 0 2px 4px 0 rgba(0,0,0,.3);">
						 </div>
						   <div class="month">
							<h4><?php echo $rw->p_title." ".$rw->vr_name;?></h4>
							<div class="month-grid">
								<p><strong>Model:</strong> <?php echo $rw->p_model;?></p>
								<p><?php echo $rw->p_sdesc;?></p>
								<div class="banner-btns">
									<span class="detail"><a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online">View Detail</a></span>
									<span class="buy"><a href="<?php echo '../pdf/'.$rw->p_slug.'.pdf'?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Brochure Manual Download">View Brochure</a></span>
								</div>
							</div>
						   </div>
				   		 <div class="clearfix"> </div>
				   	 </div>
			</article>
			  <?php }?>
			</div>
		</div>
		<!---->
		  <script src="assets/sellers/js/jquery.wmuSlider.js"></script> 
			  <script>
       			$('.example1').wmuSlider({
					 pagination : false,
				});         
   		     </script> 	
		
		</div>  
	</div>
	</div>
	<!---->
	<div class="content">	
		<div class="container">
			<div class="content-top">
				<h5 class="welcome">WELCOME TO <?=$brand->user_company?></h5> <a name="about"></a>
				<div class="clearfix"> </div>
			</div>	
			<!---->
			<div class="content-middle">
				<div class="middle-content">
					<div class="middle">
						<?=$brand->user_about?>
					</div>
				</div>
					<div class="clearfix"> </div>
			</div>
				<!---->
				<div class="content-bottom"><a name="products"></a>
				<?php $i=0; foreach ($items as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;}  $i++;?>
				<?php if($i==1){echo '<div class="row">';}?>							
					<div class="col-md-4 bottom-content" style="background-color:#FFFFFF; text-align:center; float:left"   >
						<a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online"><img class="img-responsive" style="max-height:274px; margin:0 auto" src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>">
							<p class="tun"><?php echo $rw->p_title." ".$rw->vr_name;?></p>
							<p class="best">Model <span><?php echo $rw->p_model;?></span></p>
						</a>			
					</div>
					<?php if($i==3){ $i=0; echo '</div><div class="clearfix">&nbsp; </div>';}?>
					<? }?>
					
					<div class="clearfix"> </div>
					
				</div>
				
				<!---->
				<div class="grid-top-in">
				<div class="grid-top">
					<div class="col-md-4 top-grid">
						<h5><?=$brand->user_company?></h5> <a name="contact"></a>
							<div class="house">
								<i class="in-house"> </i>
								<div class="add">
								<p><?=$brand->user_address?></p>
									<p><?=$brand->user_city?>, <?=$brand->user_state?></p>
									<p><?=$brand->user_country?>  <?=$brand->user_postcode?></p>
								</div>
							<div class="clearfix"> </div>
							</div>
							<div class="house">
								<i class="in-house in-on"> </i>
								<div class="add">
									<p>+91-<?=$brand->user_mobile?></p>
									<p><?php if ($brand->user_std==0){ echo $brand->user_phone; } else { ?> <?=$brand->user_std?> - <?=$brand->user_phone?><? } ?></p>
								</div>
							<div class="clearfix"> </div>
							</div>
					</div>
					<div class="col-md-2 top-grid">
						<h5>GET SOCIAL WITH US!</h5>
							<ul class="social-in">
								<li><a href="#"><i> </i></a> <a href="#"><i class="twitter"> </i></a></li>				
							</ul>
							<div class="clearfix"> </div>
				</div>
				
					<div class="col-md-6 top-grid">
						<h5>CONTACT <?=$brand->user_company?></h5>
							<div class="clearfix"> </div>
				</div>
					<div class="clearfix"> </div>
				</div>
			</div>
		</div>
	</div>
	<!---->

<?php $this->view('common/footer-sellers'); ?>