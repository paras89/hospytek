 <!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="<?php echo $meta_desc;?>">
    <meta name="author" content="Hospytek">
    <meta name="keywords" content="<?php echo $meta_key;?>">
    <meta name="robots" content="all">
    <title>
        <?php echo $meta_title;?>
    </title>
    <!-- Bootstrap Core CSS -->
    <base href="<?php echo base_url();?>">

    <link href="https://www.hospytek.com/assets/sellers/tpl0001/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://www.hospytek.com/assets/sellers/tpl0001/css/font-awesome.min.css" rel="stylesheet">
    <link href="https://www.hospytek.com/assets/sellers/tpl0001/css/prettyPhoto.css" rel="stylesheet">
    <link href="https://www.hospytek.com/assets/sellers/tpl0001/css/price-range.css" rel="stylesheet">
    <link href="https://www.hospytek.com/assets/sellers/tpl0001/css/animate.css" rel="stylesheet">
    <link href="https://www.hospytek.com/assets/sellers/tpl0001/css/main.css" rel="stylesheet">
    <link href="https://www.hospytek.com/assets/sellers/tpl0001/css/responsive.css" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="js/html5shiv.js"></script>
    <script src="js/respond.min.js"></script>
    <![endif]-->
    <link rel="shortcut icon" href="images/ico/favicon.html">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="images/ico/apple-touch-icon-144-precomposed.html">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="images/ico/apple-touch-icon-114-precomposed.html">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="images/ico/apple-touch-icon-72-precomposed.html">
    <link rel="apple-touch-icon-precomposed" href="images/ico/apple-touch-icon-57-precomposed.html">
    <style type="text/css">
      
        #about.title::before {
            content: " ";
            position: absolute;
            background: #fff;
            bottom: -6px;
            width: 327px;
            height: 30px;
            z-index: -1;
            left: 45%;
            margin-left: -110px;
        }
    </style>
</head>
<!--/head-->

<body>
    <header id="header">
        

        <div class="header navbar navbar-inverse navbar-fixed-top">
            <!--header-bottom-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div class="col-sm-5">
                           <div class="logo" style="margin-top:15px">
                                <a href="<?php echo $brand->user_slug;?>">  
                                    <?  if($brand->user_image=='') 
                                        { ?>
                                            <a href="<?php echo $brand->user_slug;?>"><?php echo $brand->user_company?></a><br/>
                                        <? } else if($brand->user_image=='no.gif') { ?>
                                    <a href="<?php echo $brand->user_slug;?>" style="text-decoration:none; color:#fafdff;"><?php echo $brand->user_company;?></a><br/>
                                    <? }
                                        else
                                        { $imgurl= 'http://www.hospytek.com/newcrm/UserFiles/Image/'.$brand->user_image;?>
                                    <a href="<?php echo $brand->user_slug;?>"><img src="<?=$imgurl?>" alt="<?=$brand->user_company?>" style="height:70px;margin-top:5px;"/></a><br/>
                                    <? } 
                                ?>
                                   </a>
                                &nbsp;&nbsp;<span><?php echo $brand->user_city;?>, <?php echo $brand->user_state;?> <?php echo $brand->user_country;?></span></h2>
                            </div>
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                    <span class="icon-bar"></span>
                                </button>
                            </div>
                        </div>
                        <div class="col-sm-7">
                            <div class="mainmenu pull-right">
                                <ul class="nav navbar-nav collapse navbar-collapse">
                                    <li><a href="<?php echo $brand->user_slug;?>" class="active">Home</a></li>
                                    <li><a href="<?php echo $brand->user_slug;?>#about">About</a></li>
                                    <li><a href="<?php echo $brand->user_slug;?>#products">Products</a></li>
                                    <li><a href="<?php echo $brand->user_slug;?>#contact">Contact</a></li>
                                     <a data-toggle="modal" href="#modal-callback" class="cart-sellers">
                                    <button class="btn btn-default" style="margin-top:15px">Become a Dealer</button>
                                </a>

                                </ul>
                              
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <!--/header-bottom-->
    </header>
    <!--/header-->
    <br>
    <br>
    <br>
    <br>

    <section id="slider">
        <!--slider-->
        <div class="container">
            <div class="row">
                <div class="col-sm-12">

                    <div id="slider-carousel" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
                            <li data-target="#slider-carousel" data-slide-to="1"></li>
                            <li data-target="#slider-carousel" data-slide-to="2"></li>
                        </ol>

                        <div class="carousel-inner">

                            <?php $i=0; foreach ($items as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;} ;
              if($i>6){ break;}
              ?>
                                <?php if ($i==0) { $i++; ?>
                                    <div class="item active">
                                        <div class="col-sm-6">
                                            <h1><?php echo $rw->p_title." ".$rw->vr_name;?></h1>
                                            <h3><strong>Model:</strong> <?php echo $rw->p_model;?></h3>
                                            <p>
                                                <?php echo $rw->p_sdesc;?>
                                            </p>

                                            <a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online"><button type="button" class="btn btn-default get">View Detail</button></a>
                                                <a href="<?php echo '../pdf/'.$rw->p_slug.'.pdf'?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Brochure Manual Download"><button type="button" class="btn btn-default get">View Brochure</button></a>
                                        </div>
                                        <div class="col-sm-6">
                                            <img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="girl img-responsive" alt="" />

                                        </div>
                                    </div>
                                    <?php } else  {  ?>

                                        <div class="item">
                                            <div class="col-sm-6">
                                                <h1><?php echo $rw->p_title." ".$rw->vr_name;?></h1>
                                                <h3><strong>Model:</strong> <?php echo $rw->p_model;?></h3>
                                                <p>
                                                    <?php echo $rw->p_sdesc;?>
                                                </p>

                                                <a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online"><button type="button" class="btn btn-default get">View Detail</button></a>
                                                <a href="<?php echo '../pdf/'.$rw->p_slug.'.pdf'?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Brochure Manual Download"><button type="button" class="btn btn-default get">View Brochure</button></a>
                                                
                                            </div>
                                            <div class="col-sm-6">
                                                <img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="girl img-responsive" alt="" />

                                            </div>
                                        </div>
                                        <?php  } ?>

                                            <?php  } ?>


                        </div>

                        <a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
                            <i class="fa fa-angle-left"></i>
                        </a>
                        <a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>

                </div>
            </div>
        </div>
    </section>
    <!--/slider-->

    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-12 padding-right">
                    <h2 class="title text-center" id="about">WELCOME TO <?=$brand->user_company?></h2>
                </div>
                <p class="col-sm-12">
                    <?=$brand->user_about?>
                </p>

            </div>
        </div>
    </section>
    <section>
        <div class="container">
            <div class="row">

                <div class="col-sm-12 padding-right">
                    <div class="features_items">
                        <!--features_items-->
                        <h2 class="title text-center" id="products"><br><br><br>Our Products</h2>

                        <?php $i=0; foreach ($items as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;}  $i++;?>
                            <?php if($i==1){echo '<div class="row">';}?>
                                <div class="col-sm-4">
                                    <div class="product-image-wrapper">
                                        <div class="single-products">
                                            <div class="productinfo text-center">
                                                <img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" />
                                                <h5><?php echo $rw->p_title." ".$rw->vr_name;?></h5>
                                                <p>
                                                    Model <span><?php echo $rw->p_model;?></span>
                                                </p>
                                                <a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" class="btn-sm btn-success">View Details</a>

                                                <br>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <?php if($i==3){ $i=0; echo '</div><div class="clearfix">&nbsp; </div>';}?>
                                    <? }?>
                                        
                    </div>
                    <!--features_items-->
                    <hr/>

                    <div id="contact" class="container">
                        <div class="bg">
                            <div class="row">
                                <div class="col-sm-12">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-8">
                                    <div class="contact-form">
                                        <br>
                                        <br>
                                        <h2 class="title text-center"><br>Get In Touch</h2>
                                        <div class="status alert alert-success" style="display: none"></div>
                                        <form id="main-contact-form" class="contact-form row" name="contact-form" method="post">
                                            <div class="form-group col-md-6">
                                                <input type="text" name="name" class="form-control" required="required" placeholder="Name">
                                            </div>
                                            <div class="form-group col-md-6">
                                                <input type="email" name="email" class="form-control" required="required" placeholder="Email">
                                            </div>
                                            <div class="form-group col-md-12">
                                                <input type="text" name="subject" class="form-control" required="required" placeholder="Subject">
                                            </div>
                                            <div class="form-group col-md-12">
                                                <textarea name="message" id="message" required="required" class="form-control" rows="8" placeholder="Your Message Here"></textarea>
                                            </div>
                                            <div class="form-group col-md-12">
                                                <input type="submit" name="submit" class="btn btn-primary pull-right" value="Submit">
                                            </div>
                                        </form>
                                    </div>
                                </div>
                                <div class="col-sm-4">
                                    <br>
                                    <br>
                                    <div class="contact-info">
                                        <h2 class="title text-center"><br>Contact Info</h2>
                                        <address>
                            <p><?=$brand->user_address?></p>
                                    <p><?=$brand->user_city?>, <?=$brand->user_state?></p>
                                    <p><?=$brand->user_country?>  <?=$brand->user_postcode?></p>
                                    <p>+91-<?=$brand->user_mobile?><br>(0)<?=$brand->user_std?>-<?=$brand->user_phone?></p>
                        </address>
                                        <div class="social-networks">
                                            <h2 class="title text-center">Social Networking</h2>
                                            <ul>
                                                <li>
                                                    <a href="#"><i class="fa fa-facebook"></i></a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-twitter"></i></a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-google-plus"></i></a>
                                                </li>
                                                <li>
                                                    <a href="#"><i class="fa fa-youtube"></i></a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!--/recommended_items-->

                </div>
            </div>
        </div>
    </section>

    <footer id="footer">
        <!--Footer-->

        <div class="footer-widget">

        </div>

        <div class="footer-bottom">
            <div class="container">
                <div class="row">
                    <p style="color:white;text-align:center">Powered by <a href="http://www.hospytek.com"> <img src="<?php echo base_url();?>/assets/images/logo.png" alt="logo" /></a></p>
                    
                </div>
            </div>
        </div>

    </footer>
    <!--/Footer-->

    <script src="https://www.hospytek.com/assets/sellers/tpl0001/js/jquery.js"></script>
    <script src="https://www.hospytek.com/assets/sellers/tpl0001/js/bootstrap.min.js"></script>
    <script src="https://www.hospytek.com/assets/sellers/tpl0001/js/jquery.scrollUp.min.js"></script>
    <script src="https://www.hospytek.com/assets/sellers/tpl0001/js/price-range.js"></script>
    <script src="https://www.hospytek.com/assets/sellers/tpl0001/js/jquery.prettyPhoto.js"></script>
    <script src="https://www.hospytek.com/assets/sellers/tpl0001/js/main.js"></script>

    <!-- Modal Area -->

    <div class="modal fade" id="modal-callback" tabindex="-1" role="dialog" aria-hidden="true">
        <div class="modal-dialog modal-lg">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h3 class="modal-title">Please Fill the form & we will call you back</h3>
                </div>
                <div class="modal-body">
                    <form method="post" action="<?php echo base_url()." api ";?>" class="form-ajax">
                        <input type="hidden" name="module" value="callback" />
                        <input type="hidden" name="type" value="0" />
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="info-title" for="exampleInputName">Your Name <span>*</span></label>
                                    <input type="text" class="form-control unicase-form-control text-input" placeholder="" name="name" required>
                                </div>

                                <div class="form-group">
                                    <label class="info-title" for="exampleInputName">Your Phone <span>*</span></label>
                                    <input type="phone" class="form-control unicase-form-control text-input" placeholder="" name="mobile" required>
                                </div>

                                <div class="form-group">
                                    <label class="info-title" for="exampleInputName">Your Email <span>*</span></label>
                                    <input type="email" class="form-control unicase-form-control text-input" placeholder="" name="email" required>
                                </div>

                                <div class="form-group">
                                    <label class="info-title" for="exampleInputName">Best time to call you <span>*</span></label>
                                    <input type="text" class="form-control unicase-form-control text-input" placeholder="" name="timetocall" required>
                                </div>

                                <div class="form-group">
                                    <label class="info-title" for="exampleInputName">Your Message <span>*</span></label>
                                    <input type="text" class="form-control unicase-form-control text-input" placeholder="" name="message" required>
                                </div>

                                <div class="form-group">
                                    <button type="submit" class="btn-upper btn btn-primary checkout-page-button">Submit Request</button>
                                </div>

                            </div>

                            <div class="col-md-6" style="font-size:12px;">
                                <p><strong>Lets give you a call back to help you buy the most suitable Medical Equipment & Device as per your requirement, We would not only help you choose the most suitable medical device but also help you find the right localised vendor to make your after sale requirements more readily available.</strong></p>
                                <center><img src="assets/images/callback-icon.png" class="img-responsive" style="max-height:175px;" /></center>
                                <p>We at Hospytek hold immense value for every customer or prospective customer and consider it as our utmost duty to provide a solution to all the queries raised by the customers.</p>

                                <p>We thank you for filling the form and assure you that one of our sales executives would get back to you very soon to understand your problem and provide the most reasonable solution.</p>

                                <p><strong>We again thank you for your patience and hope you are your experience on our website.</strong></p>
                            </div>

                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- End of modal -->

    <script type="text/javascript">
        $('.typeahead').typeahead({
            items: 100,
            matcher: function(item) {
                return true;
            },
            source: function(query, process) {
                objects = [];
                map = {};
                $.ajax({
                    url: '<?php echo base_url()."api";?>',
                    type: 'POST',
                    data: {
                        key: query,
                        module: 'key_search'
                    },
                    dataType: 'JSON',
                    success: function(data) {
                        objects = [];
                        $.each(data, function(i, object) {
                            map[object.label] = object;
                            objects.push(object.label);
                        });
                        process(objects);
                    },
                    beforeSend: function() {
                        $('.searchbox-icon').html('<img src="<?php echo base_url();?>assets/images/loader.gif">');
                    },
                    complete: function() {
                        $('.searchbox-icon').html('<span class="zmdi zmdi-search search-icon"></span>');
                    }
                });
            },
            updater: function(item) {
                $('#key').val(map[item].name);
                if (map[item].slug != '') {
                    window.location = "<?php echo base_url();?>" + map[item].slug;
                } else {
                    $('.searchform').submit();
                }
            }
        });

        $(function() {
            $(".scrolling").mCustomScrollbar({
                theme: "dark",
                scrollInertia: 0,
                mouseWheel: {
                    preventDefault: true
                }
            });
        });

        $(document).on('submit', '.form-ajax', function(e) {
            var elem = $(this);
            $.ajax({
                type: "POST",
                url: elem.attr("action"),
                dataType: "html",
                data: elem.serialize(),
                beforeSend: function() {
                    pleasewait('');
                },
                success: function(response) {
                    if (Number(response) > 0) {
                        swal("Message Sent!", "Your message has been sent to concerned department. You will get response within next couple hrs.", "success");
                    } else {
                        var res = response.split("|");
                        swal("Error", res[1], "error");
                    }
                }
            });
            return false;
        });

        function pleasewait(msg) {
            swal({
                title: '<img src="assets/images/loading.gif">',
                text: "Please wait while we process the operation.",
                showConfirmButton: false
            });
        }
    </script>
    
  
    <script>
        (function(i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function() {
                (i[r].q = i[r].q || []).push(arguments)
            }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', 'https://www.google-analytics.com/analytics.js', 'ga');

        ga('create', 'UA-88935475-1', 'auto');
        ga('send', 'pageview');
    </script>
    <script type="text/javascript">
        stLight.options({
            publisher: "b64324f3-14d8-4cea-a6b6-9550367d02a0",
            doNotHash: false,
            doNotCopy: false,
            hashAddressBar: false
        });
    </script>
    <script>
        var options = {
            "publisher": "b64324f3-14d8-4cea-a6b6-9550367d02a0",
            "position": "left",
            "ad": {
                "visible": false,
                "openDelay": 5,
                "closeDelay": 0
            },
            "chicklets": {
                "items": ["facebook", "twitter", "googleplus", "whatsapp", "linkedin", "pinterest", "email", "sharethis"]
            }
        };
        var st_hover_widget = new sharethis.widgets.hoverbuttons(options);
    </script>
</body>

</html>