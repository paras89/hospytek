<?php $this->view('common/header'); if($item->p_image=='' || is_null($item->p_image)){$pimg='no-img.jpg';}else{ $pimg=$item->p_image;} ?>
<!-- ============================================== HEADER : END ============================================== -->
<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
  <li><a href="<?php echo base_url();?>">Home</a>  
  		<i class="fa fa-angle-double-right"></i> <a href="<?=$item->catslug?>"><?=ucwords(strtolower(trim($item->catname)))?></a>
		<i class="fa fa-angle-double-right"></i> <a href="<?=$item->catslug.'/'.$item->cat_slug?>"><?=ucwords(strtolower(trim($item->cat_name)))?></a>
		<i class="fa fa-angle-double-right"></i> <a href="<?=$item->catslug.'/'.$item->cat_slug.'/'.$item->vr_slug.'/'.$item->p_slug?>"><?=ucwords(strtolower(trim($item->user_company)))?> - <?=ucwords(strtolower(trim($item->p_model)))?> <?=ucwords(strtolower(trim($item->p_title)))?> <?=ucwords(strtolower(trim($item->vr_name)))?></a>
</li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->
<div class="body-content outer-top-xs">
	<div class='container'>
		<div class='row single-product'>
			<div class='col-md-12'>
            <div class="detail-block">
				<div class="row  wow fadeInUp">
                
					     <div class="col-xs-12 col-sm-6 col-md-5 gallery-holder">
    <div class="product-item-holder size-big single-product-gallery small-gallery">

        <div id="owl-single-product">
            <div class="single-product-gallery-item" id="slide1">
                <a data-lity data-lity-desc="<?=ucwords(strtolower(trim($item->user_company)))?> - <?=trim($item->p_model)?> <?=ucwords(strtolower(trim($item->p_title)))?> <?=ucwords(strtolower(trim($item->vr_name)))?>" data-title="<?=ucwords(strtolower(trim($item->user_company)))?> - <?=trim($item->p_model)?> <?=ucwords(strtolower(trim($item->p_title)))?> <?=ucwords(strtolower(trim($item->vr_name)))?>" href="<?php echo $this->config->item('img_url').$pimg;?>">
                    <img class="img-responsive" alt="" src="assets/images/blank.gif" data-echo="<?php echo $this->config->item('img_url').$pimg;?>" />
                </a>
            </div><!-- /.single-product-gallery-item -->

        </div><!-- /.single-product-slider -->


        

    </div><!-- /.single-product-gallery -->
</div><!-- /.gallery-holder -->        			
					<div class='col-sm-6 col-md-7 product-info-block'>
						<div class="product-info">
							<h2 class="name"><?php echo $item->p_title." ".$item->vr_name;?></h2>

							<div class="stock-container info-container m-t-10">
								<div class="row">
									<div class="col-sm-2">
										<div class="stock-box">
											<span class="label"><strong>Model:</strong></span>
										</div>	
									</div>
									<div class="col-sm-9">
										<div class="stock-box">
											<span class="value"><?php echo $item->p_model;?></span>
										</div>	
									</div>
								</div><!-- /.row -->	
								<div class="row">
									<div class="col-sm-2">
										<div class="stock-box">
											<span class="label"><strong>Brand:</strong> </span>
										</div>	
									</div>
									<div class="col-sm-9">
										<div class="stock-box">
											<span class="value"><a href="<?php echo $item->user_slug;?>"><?php echo $item->user_company;?></a></span>
										</div>	
									</div>
								</div><!-- /.row -->	
							</div><!-- /.stock-container -->

							<div class="description-container m-t-20">
								<?php echo remove_bs($item->p_sdesc);?>
							</div><!-- /.description-container -->

							<div class="quantity-container info-container">
								<div class="row">
									<div class="col-sm-8">
										<a href="http://FreeHTMLtoPDF.com/?convert=<?=base_url().$item->catslug.'/'.$item->cat_slug.'/'.$item->vr_slug.'/'.$item->p_slug?>&size=US_Letter&orientation=landscape&framesize=1160&language=de" class="btn btn-primary"><i class="fa fa-file-pdf-o inner-right-vs"></i> DOWNLOAD AS PDF</a>
										
										<a data-toggle="modal" href="#modal-quote" class="btn btn-danger"><i class="fa fa-shopping-cart inner-right-vs"></i> GET FREE QUOTE</a>
										
										<a data-toggle="modal" href="#modal-service" class="btn btn-success pull-right"><i class="fa fa-tools inner-right-vs"></i> REQUEST SERVICE</a>
									</div>

								</div><!-- /.row -->
							</div><!-- /.quantity-container -->

							

							

							
						</div><!-- /.product-info -->
					</div><!-- /.col-sm-7 -->
				</div><!-- /.row -->
                </div>
				
				<div class="product-tabs inner-bottom-xs  wow fadeInUp">
					<div class="row">
						<div class="col-sm-3">
							<ul id="product-tabs" class="nav nav-tabs nav-tab-cell">
								<li class="active"><a data-toggle="tab" href="#description">DESCRIPTION</a></li>
                    <? if(trim(strip_tags($item->p_techspecs))!=''){?><li><a href="#techspecs" data-toggle="tab"><i class="icon-stack"></i>Technical  Specification</a></li><? }?>
                     <? if(trim(strip_tags($item->p_features))!=''){?><li><a href="#features" data-toggle="tab"><i class="icon-key"></i> Key Features</a></li><? }?>
				     <? if(trim(strip_tags($item->p_otherinfo))!=''){?><li><a href="#otherinfo" data-toggle="tab"><i class="icon-list"></i> Other Information</a></li><? }?>
				     <? if(trim(strip_tags($item->p_video))!=''){?><li><a href="#media" data-toggle="tab"><i class="icon-youtube"></i> Media</a></li><? }?>
					  
							</ul><!-- /.nav-tabs #product-tabs -->
						</div>
						<div class="col-sm-9">

							<div class="tab-content">
								
                    <div class="tab-pane active fade in" id="description">
						<div class="product-tab">	
						 <?=remove_bs($item->p_desc)?>
						 </div>
                    </div>
					<? if(trim(strip_tags($item->p_techspecs))!=''){?>
                    <div class="tab-pane fade in" id="techspecs">
						<div class="product-tab">
						 <?=remove_bs($item->p_techspecs)?>					
						 </div>
                    </div>
					 <? } if(trim(strip_tags($item->p_features))!=''){?>	
                    <div class="tab-pane fade in" id="features">
						<div class="product-tab">
						 <?=remove_bs($item->p_features)?>						
						 </div>
                    </div>
					<? } if(trim(strip_tags($item->p_otherinfo))!=''){?>
                    <div class="tab-pane fade in" id="otherinfo">
						<div class="product-tab">
						 <?=remove_bs($item->p_otherinfo)?>					
						 </div>
                    </div>
					<? } if(trim(strip_tags($item->p_video))!=''){?>
                    <div class="tab-pane fade in" id="media">
						<div class="product-tab">
						<center><iframe width="760" height="485" src="https://www.youtube.com/embed/<?=$item->p_video?>" frameborder="0" allowfullscreen></iframe></center>
						</center>
                    </div>
					<? }?>

							</div><!-- /.tab-content -->
						</div><!-- /.col -->
					</div><!-- /.row -->
				</div><!-- /.product-tabs -->


<section class="section featured-product wow fadeInUp">
	<h3 class="section-title">Post your comments on <span style="color:#006699"><?php echo $item->p_title." ".$item->vr_name;?></span></h3>
	<div class="owl-theme outer-top-xs" style="padding:20px;">
		<div class="fb-comments" data-href="<?php echo base_url().$item->catslug.'/'.$item->cat_slug.'/'.$item->vr_slug.'/'.$item->p_slug?>" data-numposts="5" data-width="100%"></div>
	</div>
</section>	


				<!-- ============================================== UPSELL PRODUCTS ============================================== -->
<section class="section featured-product wow fadeInUp">
	<h3 class="section-title">More Products from <span style="color:#006699"><? echo $item->user_company?></span></h3>
	<div class="owl-carousel home-owl-carousel upsell-product custom-carousel owl-theme outer-top-xs">

				  <?php foreach ($morebrand as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;}?>				
               		<div class="item item-carousel">
                    <div class="products">
                      <div class="product">
                        <div class="product-image">
                          <div class="image"> <a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>"><img  src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>"></a> </div>
                          <!-- /.image -->
                          
                          <div class="tag hot"><span>hot</span></div>
                        </div>
                        <!-- /.product-image -->
                        
                        <div class="product-info text-center">
                          <h3 class="name"><a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online"><?php echo $rw->p_title." ".$rw->vr_name;?></a></h3>
                          <div class="rating rateit-small"></div>
                          <div class="description"><?php echo $rw->p_model;?></div>
                          <div class="product-price"><a href="<?php echo $rw->user_slug;?>" title="<?php echo $rw->user_company;?> - <?php echo $rw->user_city;?> <?php echo $rw->user_state;?> Medical Equipment  Device Manufacturer"><?php echo $rw->user_company;?></a></div>
                          <!-- /.product-price --> 
                          
                        </div>
                        <!-- /.product-info -->
                        <!-- /.cart --> 
                      </div>
                      <!-- /.product --> 
                      
                    </div>
                    <!-- /.products --> 
				 
                    
                    <!-- /.products --> 
                  </div>
                  <!-- /.item -->
                   <?php }?>

			</div><!-- /.home-owl-carousel -->
</section><!-- /.section -->
<!-- ============================================== UPSELL PRODUCTS : END ============================================== -->

<section class="section featured-product wow fadeInUp">
	<h3 class="section-title">More Products in <span style="color:#006699"><? echo $item->cat_name?></span></h3>
	<div class="owl-carousel home-owl-carousel upsell-product custom-carousel owl-theme outer-top-xs">

				  <?php foreach ($morecat as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;}?>				
               		<div class="item item-carousel">
                    <div class="products">
                      <div class="product">
                        <div class="product-image">
                          <div class="image"> <a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>"><img  src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>"></a> </div>
                          <!-- /.image -->
                          
                          <div class="tag hot"><span>hot</span></div>
                        </div>
                        <!-- /.product-image -->
                        
                        <div class="product-info text-center">
                          <h3 class="name"><a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online"><?php echo $rw->p_title." ".$rw->vr_name;?></a></h3>
                          <div class="rating rateit-small"></div>
                          <div class="description"><?php echo $rw->p_model;?></div>
                          <div class="product-price"><a href="<?php echo $rw->user_slug;?>" title="<?php echo $rw->user_company;?> - <?php echo $rw->user_city;?> <?php echo $rw->user_state;?> Medical Equipment  Device Manufacturer"><?php echo $rw->user_company;?></a></div>
                          <!-- /.product-price --> 
                          
                        </div>
                        <!-- /.product-info -->
                        <!-- /.cart --> 
                      </div>
                      <!-- /.product --> 
                      
                    </div>
                    <!-- /.products --> 
				 
                    
                    <!-- /.products --> 
                  </div>
                  <!-- /.item -->
                   <?php }?>

			</div><!-- /.home-owl-carousel -->
</section><!-- /.section -->
<!-- ============================================== UPSELL PRODUCTS : END ============================================== -->
			
			</div><!-- /.col -->
			<div class="clearfix"></div>
		</div><!-- /.row -->
		<!-- ============================================== BRANDS CAROUSEL ============================================== -->
<div id="brands-carousel" class="logo-slider wow fadeInUp">

		<div class="logo-slider-inner">	
			<div id="brand-slider" class="owl-carousel brand-slider custom-carousel owl-theme">
				<div class="item m-t-15">
					<a href="#" class="image">
						<img data-echo="assets/images/brands/brand1.png" src="assets/images/blank.gif" alt="">
					</a>	
				</div><!--/.item-->

				<div class="item m-t-10">
					<a href="#" class="image">
						<img data-echo="assets/images/brands/brand2.png" src="assets/images/blank.gif" alt="">
					</a>	
				</div><!--/.item-->

				<div class="item">
					<a href="#" class="image">
						<img data-echo="assets/images/brands/brand3.png" src="assets/images/blank.gif" alt="">
					</a>	
				</div><!--/.item-->

				<div class="item">
					<a href="#" class="image">
						<img data-echo="assets/images/brands/brand4.png" src="assets/images/blank.gif" alt="">
					</a>	
				</div><!--/.item-->

				<div class="item">
					<a href="#" class="image">
						<img data-echo="assets/images/brands/brand5.png" src="assets/images/blank.gif" alt="">
					</a>	
				</div><!--/.item-->

				<div class="item">
					<a href="#" class="image">
						<img data-echo="assets/images/brands/brand6.png" src="assets/images/blank.gif" alt="">
					</a>	
				</div><!--/.item-->

				<div class="item">
					<a href="#" class="image">
						<img data-echo="assets/images/brands/brand2.png" src="assets/images/blank.gif" alt="">
					</a>	
				</div><!--/.item-->

				<div class="item">
					<a href="#" class="image">
						<img data-echo="assets/images/brands/brand4.png" src="assets/images/blank.gif" alt="">
					</a>	
				</div><!--/.item-->

				<div class="item">
					<a href="#" class="image">
						<img data-echo="assets/images/brands/brand1.png" src="assets/images/blank.gif" alt="">
					</a>	
				</div><!--/.item-->

				<div class="item">
					<a href="#" class="image">
						<img data-echo="assets/images/brands/brand5.png" src="assets/images/blank.gif" alt="">
					</a>	
				</div><!--/.item-->
		    </div><!-- /.owl-carousel #logo-slider -->
		</div><!-- /.logo-slider-inner -->
	
</div><!-- /.logo-slider -->
<!-- ============================================== BRANDS CAROUSEL : END ============================================== -->	</div><!-- /.container -->
</div><!-- /.body-content -->
<script>
    $(window).load(function(){
        setTimeout(function(){	
        $('.quote-title').html('Are you looking for <b><?=trim($item->p_title)?> <?=trim($item->vr_name)?>?</b>');
		$('#modal-quote').modal('show');
		}, 10000);
    });	
</script>
<?php $this->view('common/footer'); ?>