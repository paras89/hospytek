<!DOCTYPE html>
<html lang="en">


<meta http-equiv="content-type" content="text/html;charset=utf-8" />
<head>

    <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<meta name="description" content="<?php echo $meta_desc;?>">
		<meta name="author" content="Hospytek">
		<meta name="keywords" content="<?php echo $meta_key;?>">
		<meta name="robots" content="all">
		<title><?php echo $meta_title;?></title>
		<!-- Bootstrap Core CSS -->
		<base href="<?php echo base_url();?>">
    <!-- Bootstrap Core CSS -->
    <link href="https://www.hospytek.com/assets/sellers/tpl0009/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="https://www.hospytek.com/assets/sellers/tpl0009/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css">
    <link href='https://fonts.googleapis.com/css?family=Kaushan+Script' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Droid+Serif:400,700,400italic,700italic' rel='stylesheet' type='text/css'>
    <link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700' rel='stylesheet' type='text/css'>

    <!-- Theme CSS -->
    <link href="https://www.hospytek.com/assets/sellers/tpl0009/css/agency.min.css" rel="stylesheet">

</head>

<body id="page-top" class="index">

    <!-- Navigation -->
    <nav id="mainNav" class="navbar navbar-default navbar-custom navbar-fixed-top">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header page-scroll col-md-6">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span> Menu <i class="fa fa-bars"></i>
                </button>
                  <div class="logo">
                                <a href="<?php echo $brand->user_slug;?>">  
                                    <?  if($brand->user_image=='') 
                                        { ?>
                                            <a href="<?php echo $brand->user_slug;?>"><?php echo $brand->user_company?></a><br/>
                                        <? } else if($brand->user_image=='no.gif') { ?>
                                    <a href="<?php echo $brand->user_slug;?>" style="text-decoration:none; color:#fed136;"><?php echo $brand->user_company;?></a><br/>
                                    <? }
                                        else
                                        { $imgurl= 'http://www.hospytek.com/newcrm/UserFiles/Image/'.$brand->user_image;?>
                                    <a href="<?php echo $brand->user_slug;?>"><img src="<?=$imgurl?>" alt="<?=$brand->user_company?>" style="height:70px;margin-top:5px;"/></a><br/>
                                    <? } 
                                ?>
                                   </a>
                                &nbsp;&nbsp;<span style="color:#fed136;"><?php echo $brand->user_city;?>, <?php echo $brand->user_state;?> <?php echo $brand->user_country;?></span></h2>
                        </div>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse col-md-6" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav navbar-right">
                    <li class="active">
                        <a class="page-scroll" href="<?php echo $brand->user_slug;?>">Home</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="<?php echo $brand->user_slug;?>#about">About</a>
                    </li>
                    <li>
                        <a class="page-scroll" href="<?php echo $brand->user_slug;?>#product">Products</a>
                    </li>
                    
                    <li>
                        <a class="page-scroll" href="<?php echo $brand->user_slug;?>#contact">Contact</a>
                    </li>
                </ul>
		<div style="display:inline;float:left;margin-top:10px;padding-left:20px"><a data-toggle="modal" href="#modal-callback" class="cart-sellers"><button class="btn btn-primary">Become a Dealer</button></a></div>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container-fluid -->
    </nav>

    <!-- Header -->
    <header>
      	<section id="main-slider"><!--slider-->
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
				
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#slider-carousel" data-slide-to="1"></li>
							<li data-target="#slider-carousel" data-slide-to="2"></li>
						</ol>
						
						<div class="carousel-inner">
						
							
						
						
						 <?php $i=0; foreach ($items as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;} ;
						  if($i>6){ break;}
						  ?>				
						  <?php if ($i==0) { $i++; ?>
        			  <div class="item active"> 
								<div class="col-sm-6" style="margin-top:150px">
									<h1><?php echo $rw->p_title." ".$rw->vr_name;?></h1>
									<h3 ><strong>Model:</strong> <?php echo $rw->p_model;?></h3>
									<p><?php echo $rw->p_sdesc;?></p>
									
									<button type="button" class="btn btn-default get"><a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online">View Detail</a></button>
									<button type="button" class="btn btn-default get"><a href="<?php echo '../pdf/'.$rw->p_slug.'.pdf'?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Brochure Manual Download">View Brochure</a></span></button>
								</div>
								<div class="col-sm-6">
									<img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="girl img-responsive" alt="" />
									
								</div>
							</div>
							<?php } else  {  ?>
							
							<div class="item"> 
								<div class="col-sm-6" style="margin-top:150px">
									<h1><?php echo $rw->p_title." ".$rw->vr_name;?></h1>
									<h3><strong>Model:</strong> <?php echo $rw->p_model;?></h3>
									<p><?php echo $rw->p_sdesc;?></p>
									
									<button type="button" class="btn btn-default get"><a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online">View Detail</a></button>
									<button type="button" class="btn btn-default get"><a href="<?php echo '../pdf/'.$rw->p_slug.'.pdf'?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Brochure Manual Download">View Brochure</a></span></button>
								</div>
								<div class="col-sm-6">
									<img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="girl img-responsive" alt="" />
									
								</div>
							</div>
							<?php  } ?>
							
							
							
							<?php  } ?>
							
							
							
						</div>
						
						
                        <a href="#slider-carousel" class="left control-carousel pull-left" data-slide="prev">
                            <i class="fa fa-angle-left" style="color:grey;font-size:50px;"></i>
                        </a>
                        <a href="#slider-carousel" class="right control-carousel pull-right" data-slide="next">
                            <i class="fa fa-angle-right" style="color:grey;font-size:50px;"></i>
                        </a>

					</div>
					
				</div>
			</div>
		</div>
	</section><!--/slider-->
    </header>

    <!-- About Section -->
    <section id="about" style="background-color:hsla(46, 99%, 60%, 0.16);">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading"><u><i>WELCOME TO <?=$brand->user_company?></i></u></h2>
                    <h3 class="section-subheading text-muted"><?=$brand->user_about?></h3>
                </div>
            </div>
           
        </div>
    </section>

    <!-- Portfolio Grid Section -->
    <section id="product" class="bg-light-gray">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading"><u><i>Products</i></u></h2>                    
                </div>
            </div>
            <div class="row" style="text-align:center;">
			  <?php $i=0; foreach ($items as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;}  $i++;?>
				<?php if($i==1){echo '<div class="row">';}?>
                <div class="col-md-4 col-sm-6 product-item" >                    
                        <img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" style="width:100%;">                 
                        <h4><a href="#"><?php echo $rw->p_title." ".$rw->vr_name;?></a></h4> 
						<h6><a href="#"><span><?php echo $rw->p_model;?></span></a></h6>
						<div>
						    <a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online"><button type="button" class="btn btn-primary">View Detail</button></a>
						</div>
                </div>
              <?php if($i==3){ $i=0; echo '</div><div class="clearfix">&nbsp; </div>';}?>
                    <? }?>
            </div>
        </div>
    </section>

   

    <!-- Contact Section -->
    <section id="contact">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2 class="section-heading"><u><i>Contact Us</i></u></h2>
                   
                </div>
            </div>
            <div class="row" style="color:white;">
                <div class="col-lg-8">
                    <form name="sentMessage" id="contactForm" novalidate>
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <input type="text" class="form-control" placeholder="Your Name *" id="name" required data-validation-required-message="Please enter your name.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="email" class="form-control" placeholder="Your Email *" id="email" required data-validation-required-message="Please enter your email address.">
                                    <p class="help-block text-danger"></p>
                                </div>
                                <div class="form-group">
                                    <input type="tel" class="form-control" placeholder="Your Phone *" id="phone" required data-validation-required-message="Please enter your phone number.">
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea class="form-control" placeholder="Your Message *" id="message" required data-validation-required-message="Please enter a message."></textarea>
                                    <p class="help-block text-danger"></p>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                            <div class="col-lg-12 text-center">
                                <div id="success"></div>
                                <button type="submit" class="btn btn-xl">Send Message</button>
                            </div>
                        </div>
                    </form>
                </div>
				<div class="col-lg-4">
				<h3><strong>Address</strong></h3>
				    <p><i class="fa fa-pencil"></i><?=$brand->user_city?>, <?=$brand->user_state?></p>
					<p><?=$brand->user_country?>  <?=$brand->user_postcode?></p><br>
					<p><i class="fa fa-phone"></i> +91-<?=$brand->user_mobile?>     <?=$brand->user_phone?></p>
				
				</div>
            </div>
        </div>
    </section>

    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                     <p>powered by <a href="https://www.hospytek.com/" ><img src="<?php echo base_url();?>/assets/images/logo.png" alt="logo"></a> </p>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline social-buttons">
                        <li><a href="#"><i class="fa fa-twitter"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-facebook"></i></a>
                        </li>
                        <li><a href="#"><i class="fa fa-linkedin"></i></a>
                        </li>
                    </ul>
                </div>
                <div class="col-md-4">
                    <ul class="list-inline quicklinks">
                        <li><a href="<?php echo $brand->user_slug;?>">Home</a>
                        </li>
                        <li><a href="<?php echo $brand->user_slug;?>#about">About</a>
                        </li>
						 <li><a href="<?php echo $brand->user_slug;?>#product">Products</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </footer>
    <!-- jQuery -->
    <script src="https://www.hospytek.com/assets/sellers/tpl0009/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="https://www.hospytek.com/assets/sellers/tpl0009/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="../../cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.3/jquery.easing.min.js" integrity="sha384-mE6eXfrb8jxl0rzJDBRanYqgBxtJ6Unn4/1F7q4xRRyIw7Vdg9jP4ycT7x1iVsgb" crossorigin="anonymous"></script>

    <!-- Contact Form JavaScript -->
    <script src="https://www.hospytek.com/assets/sellers/tpl0009/js/jqBootstrapValidation.js"></script>
    <script src="https://www.hospytek.com/assets/sellers/tpl0009/js/contact_me.js"></script>

    <!-- Theme JavaScript -->
    <script src="https://www.hospytek.com/assets/sellers/tpl0009/js/agency.min.js"></script>

	
	
	 		  <div class="modal fade" id="modal-callback" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h2 class="modal-title">Please Fill the form & we will call you back</h2>
                          </div>
                          <div class="modal-body">
						  <form method="post" action="<?php echo base_url()."api";?>" class="form-ajax">
						  <input type="hidden" name="module" value="callback" />
						  <input type="hidden" name="type" value="0" />
						  <div class="row">
						  <div class="col-md-6">
						  	<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Name <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input" placeholder="" name="name" required>
							</div>

							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Phone <span>*</span></label>
		    <input type="phone" class="form-control unicase-form-control text-input"  placeholder=""  name="mobile" required>
							</div>
							
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Email <span>*</span></label>
		    <input type="email" class="form-control unicase-form-control text-input"  placeholder=""  name="email" required>
						   </div>	
							
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Best time to call you <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input"  placeholder="" name="timetocall" required>
						   </div>	


							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Message <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input"  placeholder=""  name="message" required>
						   </div>	


	<div class="form-group">
		<button type="submit" class="btn-upper btn btn-primary checkout-page-button">Submit Request</button>
	</div>
			
						  </div>

						  <div class="col-md-6" style="font-size:12px;">
						   <p><strong>Lets give you a call back to help you buy the most suitable Medical Equipment & Device as per your requirement, We would not only help you choose the most suitable medical device but also help you find the right localised vendor to make your after sale requirements more readily available.</strong></p>
						  <center><img src="assets/images/callback-icon.png" class="img-responsive" style="max-height:175px;" /></center>
						  <p>We at Hospytek hold immense value for every customer or prospective customer and consider it as our utmost duty to provide a solution to all the queries raised by the customers.</p>

<p>We thank you for filling the form and assure you that one of our sales executives would get back to you very soon to understand your problem and provide the most reasonable solution.</p>

<p><strong>We again thank you for your patience and hope you are your experience on our website.</strong></p>
						  </div>
						  
                          </div>
						  </form>
                      </div>
                  </div>
				  </div>
              </div>

	
	
	
</body>

</html>
