<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
        <meta name="description" content="<?php echo $meta_desc;?>">
        <meta name="author" content="Hospytek">
        <meta name="keywords" content="<?php echo $meta_key;?>">
        <meta name="robots" content="all">
        <title><?php echo $meta_title;?></title>
        <!-- Bootstrap Core CSS -->
        <base href="<?php echo base_url();?>">
        <!-- Favicon -->
        <link rel="shortcut icon" href="assets/sellers/tpl0032/images/favicon.html">
        <!-- Web Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,700italic,400,700,300&amp;subset=latin,latin-ext' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:700,400,300' rel='stylesheet' type='text/css'>
        <!-- Bootstrap core CSS -->
        <link href="https://www.hospytek.com/assets/sellers/tpl0032/bootstrap/css/bootstrap.css" rel="stylesheet">
        <!-- Font Awesome CSS -->
        <link href="https://www.hospytek.com/assets/sellers/tpl0032/fonts/font-awesome/css/font-awesome.css" rel="stylesheet">
        <!-- Plugins -->
        <link href="https://www.hospytek.com/assets/sellers/tpl0032/css/animations.css" rel="stylesheet">
        <!-- Worthy core CSS file -->
        <link href="https://www.hospytek.com/assets/sellers/tpl0032/css/style.css" rel="stylesheet">
        <!-- Custom css --> 
        <link href="https://www.hospytek.com/https://www.hospytek.com/assets/sellers/tpl0032/css/custom.css" rel="stylesheet">
        <!-- Html Coder Add -->
        <link href="https://www.hospytek.com/assets/sellers/tpl0032/hc-add/style.css" rel="stylesheet">
    </head>
    <body class="no-trans">
        <div id="fb-root"></div>
        <script>(function(d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0];
            if (d.getElementById(id)) return;
            js = d.createElement(s); js.id = id;
            js.src = "../../../../connect.facebook.net/el_GR/sdk.js#xfbml=1&version=v2.0";
            fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));
        </script>
        <!-- scrollToTop -->
        <!-- ================ -->
        <div class="scrollToTop"><i class="icon-up-open-big"></i></div>
        <!-- header start -->
        <!-- ================ --> 
        <header class="header fixed clearfix navbar navbar-fixed-top">
            <div class="container">
                <div class="row">
                    <div class="col-md-4">
                        <!-- header-left start -->
                        <!-- ================ -->
                        <div class="header-left clearfix">
                            <!-- logo -->
                            <!-- name-and-slogan -->
                            <div class="site-name-and-slogan smooth-scroll">
                                <div class="logo">
                                <a href="<?php echo $brand->user_slug;?>">  
                                    <?  if($brand->user_image=='') 
                                        { ?>
                                            <a href="<?php echo $brand->user_slug;?>"><?php echo $brand->user_company?></a><br/>
                                        <? } else if($brand->user_image=='no.gif') { ?>
                                    <a href="<?php echo $brand->user_slug;?>" style="text-decoration:none; color:#0b8aef;"><?php echo $brand->user_company;?></a><br/>
                                    <? }
                                        else
                                        { $imgurl= 'http://www.hospytek.com/newcrm/UserFiles/Image/'.$brand->user_image;?>
                                    <a href="<?php echo $brand->user_slug;?>"><img src="<?=$imgurl?>" alt="<?=$brand->user_company?>" style="height:70px;margin-top:5px;"/></a><br/>
                                    <? } 
                                ?>
                                   </a>
                                &nbsp;&nbsp;<span><?php echo $brand->user_city;?>, <?php echo $brand->user_state;?> <?php echo $brand->user_country;?></span></h2>
                        </div>
                            </div>
                        </div>
                        <!-- header-left end -->
                    </div>
                    <div class="col-md-8">
                        <!-- header-right start -->
                        <!-- ================ -->
                        <div class="header-right clearfix">
                            <!-- main-navigation start -->
                            <!-- ================ -->
                            <div class="main-navigation animated">
                                <!-- navbar start -->
                                <!-- ================ -->
                                <nav class="navbar navbar-default" role="navigation">
                                    <div class="container-fluid">
                                        <!-- Toggle get grouped for better mobile display -->
                                        <div class="navbar-header">
                                            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse-1">
                                            <span class="sr-only">Toggle navigation</span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            <span class="icon-bar"></span>
                                            </button>
                                        </div>
                                        <!-- Collect the nav links, forms, and other content for toggling -->
                                        <div class="collapse navbar-collapse scrollspy smooth-scroll" id="navbar-collapse-1">
                                            <ul class="nav navbar-nav navbar-right">
                                                <li class="active"><a href="<?php echo $brand->user_slug;?>">Home</a></li>
                                                <li><a href="<?php echo $brand->user_slug;?>#about">About</a></li>
                                                <li><a href="<?php echo $brand->user_slug;?>#product">Products</a></li>
                                                <li><a href="<?php echo $brand->user_slug;?>#contact">Contact</a></li>
                                                <div style="display:inline;float:left;margin-top:15px;padding-left:20px"><a data-toggle="modal" href="#modal-callback" class="cart-sellers"><button class="btn btn-primary">Become a Dealer</button></a></div>
                                            </ul>
                                        </div>
                                    </div>
                                </nav>
                                <!-- navbar end -->
                            </div>
                            <!-- main-navigation end -->
                        </div>
                        <!-- header-right end -->
                    </div>
                </div>
            </div>
        </header>
        <!-- header end -->
        <!-- banner start -->
        <!-- ================ -->
        <div id="home">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div id="slider-carousel" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
                                <li data-target="#slider-carousel" data-slide-to="1"></li>
                                <li data-target="#slider-carousel" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                <?php $i=0; foreach ($items as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;} ;
                                    if($i>6){ break;}
                                    ?>				
                                <?php if ($i==0) { $i++; ?>
                                <div class="item active">
                                    <div class="col-sm-6" style="margin-top:150px">
                                        <h1><?php echo $rw->p_title." ".$rw->vr_name;?></h1>
                                        <h3 ><strong>Model:</strong> <?php echo $rw->p_model;?></h3>
                                        <p><?php echo $rw->p_sdesc;?></p>
                                        <button type="button" class="btn btn-default get"><a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online">View Detail</a></button>
                                        <button type="button" class="btn btn-default get"><a href="<?php echo '../pdf/'.$rw->p_slug.'.pdf'?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Brochure Manual Download">View Brochure</a></span></button>
                                    </div>
                                    <div class="col-sm-6">
                                        <img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="girl img-responsive" alt="" />
                                    </div>
                                </div>
                                <?php } else  {  ?>
                                <div class="item">
                                    <div class="col-sm-6" style="margin-top:150px">
                                        <h1><?php echo $rw->p_title." ".$rw->vr_name;?></h1>
                                        <h3><strong>Model:</strong> <?php echo $rw->p_model;?></h3>
                                        <p><?php echo $rw->p_sdesc;?></p>
                                        <button type="button" class="btn btn-default get"><a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online">View Detail</a></button>
                                        <button type="button" class="btn btn-default get"><a href="<?php echo '../pdf/'.$rw->p_slug.'.pdf'?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Brochure Manual Download">View Brochure</a></span></button>
                                    </div>
                                    <div class="col-sm-6">
                                        <img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="girl img-responsive" alt="" />
                                    </div>
                                </div>
                                <?php  } ?>
                                <?php  } ?>
                            </div>
                            <a href="#slider-carousel" class="left control-carousel pull-left" data-slide="prev">
                            <i class="fa fa-angle-left" style="color:grey;font-size:50px"></i>
                            </a>
                            <a href="#slider-carousel" class="right control-carousel pull-right" data-slide="next">
                            <i class="fa fa-angle-right" style="color:grey;font-size:50px"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- banner end -->
        <!-- section start -->
        <!-- ================ -->
        <div class="section clearfix object-non-visible" data-animation-effect="fadeIn">
            <div class="container" style="margin:50px">
                <div class="row">
                    <div class="col-md-12">
                        <h1 id="about" class="title text-center">WELCOME TO <?=$brand->user_company?></h1>
                        <p class="lead text-center"><?=$brand->user_about?></p>
                    </div>
                </div>
            </div>
        </div>
        <!-- section end -->
        <!-- section start -->
        <!-- ================ -->
        <div class="section">
            <div class="container" style="padding-top:50px;padding-bottom:50px">
                <h1 class="text-center title" id="product">Products</h1>
                <div class="separator"></div>
                <br>			
                <div class="row object-non-visible" data-animation-effect="fadeIn">
                    <div class="col-md-12">
                        <!-- product items start -->
                        <div class="isotope-container row grid-space-20" style="text-align:center;">
                            <?php $i=0; foreach ($items as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;}  $i++;?>
                            <?php if($i==1){echo '<div class="row">';}?>
                            <div class="col-md-4 isotope-item app-development">
                                <img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" style="border:solid 2px #9E9E9E">					
                                <p><a href="#"><?php echo $rw->p_title." ".$rw->vr_name;?></a></p>
                                <h6><a href="#"><?php echo $rw->p_model;?></a></h6>
                                <a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online"><button type="button" class="btn btn-primary">View Detail</button></a>
                            </div>
                            <?php if($i==3){ $i=0; echo '</div><div class="clearfix">&nbsp; </div>';}?>
                            <? }?>
                        </div>
                        <!-- product items end -->
                    </div>
                </div>
            </div>
        </div>
        <!-- section end -->
        <!-- footer start -->
        <!-- ================ -->
        <footer id="footer">
            <!-- .footer start -->
            <!-- ================ -->
            <div class="footer section">
                <div class="container" style="padding-top:50px;padding-bottom:50px">
                    <h1 class="title text-center" id="contact">Contact Us</h1>
                    <div class="space"></div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="footer-content">
                                <ul class="list-icons">
                                    <li>
                                        <i class="fa fa-map-marker pr-10"></i><?=$brand->user_address?>,<br/> <?=$brand->user_city?>, <?=$brand->user_state?></p><br>
                                        <p><?=$brand->user_country?>  <?=$brand->user_postcode?></p>
                                    </li>
                                    <li><i class="fa fa-phone pr-10"></i>+91-<?=$brand->user_mobile?></li>
                                    <li><i class="fa fa-fax pr-10"></i>  <?=$brand->user_phone?> </li>
                                </ul>
                                <ul class="social-links">
                                    <li class="facebook"><a target="_blank" href="https://www.facebook.com/pages/HtmlCoder/714570988650168"><i class="fa fa-facebook"></i></a></li>
                                    <li class="twitter"><a target="_blank" href="https://twitter.com/HtmlcoderMe"><i class="fa fa-twitter"></i></a></li>
                                    <li class="googleplus"><a target="_blank" href="http://plus.google.com/"><i class="fa fa-google-plus"></i></a></li>
                                    <li class="skype"><a target="_blank" href="http://www.skype.com/"><i class="fa fa-skype"></i></a></li>
                                    <li class="linkedin"><a target="_blank" href="http://www.linkedin.com/"><i class="fa fa-linkedin"></i></a></li>
                                    <li class="pinterest"><a target="_blank" href="http://www.pinterest.com/"><i class="fa fa-pinterest"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="footer-content">
                                <form role="form" id="footer-form">
                                    <div class="form-group has-feedback">
                                        <label class="sr-only" for="name2">Name</label>
                                        <input type="text" class="form-control" id="name2" placeholder="Name" name="name2" required>
                                        <i class="fa fa-user form-control-feedback"></i>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label class="sr-only" for="email2">Email address</label>
                                        <input type="email" class="form-control" id="email2" placeholder="Enter email" name="email2" required>
                                        <i class="fa fa-envelope form-control-feedback"></i>
                                    </div>
                                    <div class="form-group has-feedback">
                                        <label class="sr-only" for="message2">Message</label>
                                        <textarea class="form-control" rows="8" id="message2" placeholder="Message" name="message2" required></textarea>
                                        <i class="fa fa-pencil form-control-feedback"></i>
                                    </div>
                                    <input type="submit" value="Send" class="btn btn-default">
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- .footer end -->
            <!-- .subfooter start -->
            <!-- ================ -->
            <div class="subfooter" style="text-align:center">
                <div class="container">
                    <div class="row" style="margin-bottom: 15px;margin-top: 15px;">
                        <div class="col-md-12">
                            <p>powered by <a href="https://www.hospytek.com/" ><img src="<?php echo base_url();?>/assets/images/logo.png" alt="logo"></a> </p>
                        </div>
                    </div>
                </div>
            </div>
            <!-- .subfooter end -->
        </footer>
        <!-- footer end -->
        <!-- JavaScript files placed at the end of the document so the pages load faster
            ================================================== -->
        <!-- Jquery and Bootstap core js files -->
        <script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0032/plugins/jquery.min.js"></script>
        <script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0032/bootstrap/js/bootstrap.min.js"></script>
        <!-- Modernizr javascript -->
        <script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0032/plugins/modernizr.js"></script>
        <!-- Isotope javascript -->
        <script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0032/plugins/isotope/isotope.pkgd.min.js"></script>
        <!-- Backstretch javascript -->
        <script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0032/plugins/jquery.backstretch.min.js"></script>
        <!-- Appear javascript -->
        <script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0032/plugins/jquery.appear.js"></script>
        <!-- Initialization of Plugins -->
        <script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0032/js/template.js"></script>
        <!-- Custom Scripts -->
        <script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0032/js/custom.js"></script>
        <!-- Html Coder Add -->
        <script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0032/hc-add/template.js"></script>
        <div class="modal fade" id="modal-callback" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h2 class="modal-title">Please Fill the form & we will call you back</h2>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="<?php echo base_url()."api";?>" class="form-ajax">
                            <input type="hidden" name="module" value="callback" />
                            <input type="hidden" name="type" value="0" />
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="info-title" for="exampleInputName">Your Name <span>*</span></label>
                                        <input type="text" class="form-control unicase-form-control text-input" placeholder="" name="name" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="info-title" for="exampleInputName">Your Phone <span>*</span></label>
                                        <input type="phone" class="form-control unicase-form-control text-input"  placeholder=""  name="mobile" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="info-title" for="exampleInputName">Your Email <span>*</span></label>
                                        <input type="email" class="form-control unicase-form-control text-input"  placeholder=""  name="email" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="info-title" for="exampleInputName">Best time to call you <span>*</span></label>
                                        <input type="text" class="form-control unicase-form-control text-input"  placeholder="" name="timetocall" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="info-title" for="exampleInputName">Your Message <span>*</span></label>
                                        <input type="text" class="form-control unicase-form-control text-input"  placeholder=""  name="message" required>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn-upper btn btn-primary checkout-page-button">Submit Request</button>
                                    </div>
                                </div>
                                <div class="col-md-6" style="font-size:12px;">
                                    <p><strong>Lets give you a call back to help you buy the most suitable Medical Equipment & Device as per your requirement, We would not only help you choose the most suitable medical device but also help you find the right localised vendor to make your after sale requirements more readily available.</strong></p>
                                    <center><img src="assets/images/callback-icon.png" class="img-responsive" style="max-height:175px;" /></center>
                                    <p>We at Hospytek hold immense value for every customer or prospective customer and consider it as our utmost duty to provide a solution to all the queries raised by the customers.</p>
                                    <p>We thank you for filling the form and assure you that one of our sales executives would get back to you very soon to understand your problem and provide the most reasonable solution.</p>
                                    <p><strong>We again thank you for your patience and hope you are your experience on our website.</strong></p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>