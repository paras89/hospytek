<!-- ============================================================= FOOTER ============================================================= -->
<footer id="footer" class="footer color-bg">
  <div class="footer-bottom">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-6 col-md-3">
          <div class="module-heading">
            <h4 class="module-title">Contact Us</h4>
          </div>
          <!-- /.module-heading -->
          
          <div class="module-body">
            <ul class="toggle-footer" style="">
              <li class="media">
                <div class="pull-left"> <span class="icon fa-stack fa-lg"> <i class="fa fa-map-marker fa-stack-1x fa-inverse"></i> </span> </div>
                <div class="media-body">
                  <p>Sigma Enterprises<br/>SCO-13, Above Easy Day,First Floor,Royal Estate, Ambala-Chd. Highway, Zirakpur, Punjab, India</p>
                </div>
              </li>
              <li class="media">
                <div class="pull-left"> <span class="icon fa-stack fa-lg"> <i class="fa fa-mobile fa-stack-1x fa-inverse"></i> </span> </div>
                <div class="media-body">
                  <p>+91 (1762) 530604<br>
                    +91 (1762) 530605</p>
                </div>
              </li>
              <li class="media">
                <div class="pull-left"> <span class="icon fa-stack fa-lg"> <i class="fa fa-envelope fa-stack-1x fa-inverse"></i> </span> </div>
                <div class="media-body"> <span><a href="mailto:info@hospytek.com">info@hospytek.com</a></span> </div>
              </li>
            </ul>
          </div>
          <!-- /.module-body --> 
        </div>
        <!-- /.col -->
        
        <div class="col-xs-12 col-sm-6 col-md-3">
          <div class="module-heading">
            <h4 class="module-title">Information</h4>
          </div>
          <!-- /.module-heading -->
          
          <div class="module-body">
            <ul class='list-unstyled'>
             <li class="first"><a title="About Hospytek" href="page/aboutus">About us</a></li>
			 <li><a href="page/investors-relations" title="Invest in Medical Equipments Industry with Hospytek">Investors Relations</a></li>
              <li><a href="blog" title="Medical Equipment Blog">Blog</a></li>
			  <li><a href="page/privacy" title="privacy policy">Privacy Policy</a></li>
			  
              <li class="last"><a href="page/social-responsibility" title="Contact Medical Equipment Suppliers">Social Responsibility</a></li>
            </ul>
          </div>
          <!-- /.module-body --> 
        </div>
        <!-- /.col -->
        
        <div class="col-xs-12 col-sm-6 col-md-3">
          <div class="module-heading">
            <h4 class="module-title">Customer Service</h4>
          </div>
          <!-- /.module-heading -->
          
          <div class="module-body">
            <ul class='list-unstyled'>
              <li  class="first"><a href="page/faqs" title="faq">FAQs</a></li>
  			  <li><a href="page/terms" title="faq">Terms & Conditions</a></li>
			  <li><a href="http://support.hospytek.com" title="Where is my order?">Help Center</a></li>
              <li class="last"><a title="Addresses" href="page/contact">Contact Us</a></li>
            </ul>
          </div>
          <!-- /.module-body --> 
        </div>
        <!-- /.col -->
        
        <div class="col-xs-12 col-sm-6 col-md-3">
          <div class="module-heading">
            <h4 class="module-title">Why Choose Us</h4>
          </div>
          <!-- /.module-heading -->
          
          <div class="module-body">
            <ul class='list-unstyled'>
			  <li class=" first"><a href="landing/vendors" title="Medical Equipments Management for Vendors">Solutions for Vendors</a></li>
  			  <li><a href="landing/brands" title="Medical Equipments Management for Brands">Solutions for Brands</a></li>
			  <li><a href="landing/hospitals-clinics" title="Medical Equipments Management for Hospitals & Clinics">Solutions for Hospitals & Clinics</a></li>			  
			  <li class="last"><a href="landing/events" title="Medical Equipment Events">Events & Promotions</a></li>
            </ul>
          </div>
          <!-- /.module-body --> 
        </div>
      </div>
    </div>
  </div>
  <div class="copyright-bar">
    <div class="container">
      <div class="col-xs-12 col-sm-5 no-padding social">
        <ul class="link">
          <li class="fb pull-left"><a target="_blank"  href="https://www.facebook.com/Hospytek/" title="Facebook"></a></li>
          <li class="tw pull-left"><a target="_blank"  href="https://twitter.com/hospytek" title="Twitter"></a></li>
          <li class="googleplus pull-left"><a target="_blank"  href="https://plus.google.com/113015133774959781756" title="GooglePlus"></a></li>
          <li class="linkedin pull-left"><a target="_blank"  href="https://www.linkedin.com/company/hospytek" title="Linkedin"></a></li>
          <li class="youtube pull-left"><a target="_blank" href="https://www.youtube.com/channel/UCFBX1N-VcfJTQKINqTKC5jg" title="Youtube"></a></li>
        </ul>
      </div>
      <div class="col-xs-12 col-sm-7 no-padding text-right" style="color:#FFFFFF"> 
	  Copyrights &copy; <?=date("Y")?>. Hospytek (A Unit of Sigma Enterprises). All rights reserved
	  <br /><span style="font-size:10px;"><strong>Disclaimer:</strong> All product's images & logos used in website are properties of respective brands or manufacturers.</span>  
        <!-- /.payment-methods --> 
      </div>
    </div>
  </div>
</footer>
<!-- ============================================================= FOOTER : END============================================================= --> 
	 		  <div class="modal fade" id="modal-signup" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h2 class="modal-title">Create Account</h2>
                          </div>
                          <div class="modal-body">
						  <form method="post" action="<?php echo base_url()."api";?>" class="form-ajax" popid="modal-signup">
						  <input type="hidden" name="module" value="signup" />
						  <div class="row">
						  <div class="col-md-12">

						<div class="row">
						  <div class="col-md-12">							
							<div class="form-group">
						   <label class="info-title" for="exampleInputName"><strong>I am </strong></label> (Please select the best profile match for you)<br />
						   <input type="radio" name="type" value="2" checked="checked" /> Buyer/Clinic/Hospital/Lab   &nbsp; &nbsp; &nbsp; <input type="radio" name="type" value="1" /> Vendor/Dealer/Distributor/Supplier &nbsp; &nbsp; &nbsp;  <input type="radio" name="type" value="3" /> Brand/Manufacturer 
						   </div>	
						</div>
						</div>   
						  
						 <div class="row">
						  <div class="col-md-6">
						  	<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Name <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input" placeholder="" value="<?=$urw->user_name?>" name="name" required>
							</div>
							</div>
							
							<div class="col-md-6">
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Company Name<span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input" value="<?=$urw->user_company?>"   name="company" required>
						   </div>	
						   </div>
						   </div>
						   
							
						<div class="row">
						  <div class="col-md-6">
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Mobile <span>*</span></label>
		    <input type="phone" class="form-control unicase-form-control text-input"  placeholder="" value="<?=$urw->user_mobile?>"  name="mobile" required>
							</div>
						  </div>
						
						  <div class="col-md-6">
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Email <span>*</span></label>
		    <input type="email" class="form-control unicase-form-control text-input"  placeholder="" value="<?=$urw->user_email?>" name="email" required>
						   </div>	
						   </div>
						   </div>	


						<div class="row">
						  <div class="col-md-12">							
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Address <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input"  placeholder=""  name="address">
						   </div>	
						</div>
						</div>   



						<div class="row">
						  <div class="col-md-4">							
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">City<span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input geocomplete" name="city" required>
						   </div>
						   </div>	

						 <div class="col-md-4">
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">State<span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input geocomplete" name="state" required>
						   </div>	
						   </div>

						 <div class="col-md-4">
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Country<span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input geocomplete" name="country" required>
						   </div>	
						   </div>						   
						   </div>


					<div class="form-group">
						<button type="submit" class="btn-upper btn btn-primary checkout-page-button">Register Now</button>
					</div>
			
						  </div>
                          </div>
						  </form>
                      </div>
                  </div>
				  </div>
              </div>




	 		  <div class="modal fade" id="modal-callback" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h2 class="modal-title">Please Fill the form & we will call you back</h2>
                          </div>
                          <div class="modal-body">
						  <form method="post" action="<?php echo base_url()."api";?>" class="form-ajax" popid="modal-callback">
						  <input type="hidden" name="module" value="callback" />
						  <input type="hidden" name="type" value="0" />
						  <div class="row">
						  <div class="col-md-6">
						  	<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Name <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input" placeholder="" value="<?=$urw->user_name?>" name="name" required>
							</div>

							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Phone <span>*</span></label>
		    <input type="phone" class="form-control unicase-form-control text-input"  placeholder="" value="<?=$urw->user_mobile?>"  name="mobile" required>
							</div>
							
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Email <span>*</span></label>
		    <input type="email" class="form-control unicase-form-control text-input"  placeholder="" value="<?=$urw->user_email?>"  name="email" required>
						   </div>	
							
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Best time to call you <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input"  placeholder="" name="timetocall" required>
						   </div>	


							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Message <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input"  placeholder=""  name="message" required>
						   </div>	


	<div class="form-group">
		<button type="submit" class="btn-upper btn btn-primary checkout-page-button">Submit Request</button>
	</div>
			
						  </div>

						  <div class="col-md-6" style="font-size:12px;">
						   <p><strong>Lets give you a call back to help you buy the most suitable Medical Equipment & Device as per your requirement, We would not only help you choose the most suitable medical device but also help you find the right localised vendor to make your after sale requirements more readily available.</strong></p>
						  <center><img src="assets/images/callback-icon.png" class="img-responsive" style="max-height:175px;" /></center>
						  <p>We at Hospytek hold immense value for every customer or prospective customer and consider it as our utmost duty to provide a solution to all the queries raised by the customers.</p>

<p>We thank you for filling the form and assure you that one of our sales executives would get back to you very soon to understand your problem and provide the most reasonable solution.</p>

<p><strong>We again thank you for your patience and hope you are your experience on our website.</strong></p>
						  </div>
						  
                          </div>
						  </form>
                      </div>
                  </div>
				  </div>
              </div>
			<? if($urw->id==''){?>
	 		  <div class="modal fade" id="modal-intro" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                      <div class="modal-content">
					  
					  </div>
				  </div>
			 </div>	  
			<? }?> 

	 		  <div class="modal fade" id="modal-quote" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h2 class="modal-title quote-title" style="font-size:18px;">Request Quotation for <strong><?php echo ucwords(strtolower($item->p_title." ".$item->vr_name));?></strong></h2>
                          </div>
                          <div class="modal-body">
						 <? if($item->p_title!=''){?>
						  <p>Lets make your job easier, We at Hospytek believe that You being a Busy Guy should not go through the trivial process of finding a vendor, lets send a shout out to <?=rand(20,100)?> vendors selling <?php echo ucwords(strtolower($item->p_title." ".$item->vr_name));?> and let them propose a deal for you. </p>
						 <? }else{?>
						  <p>Please fill the form below and matching vendors will contact you shortly!</p>
						  <? }?>
						  <form method="post" action="<?php echo base_url()."api";?>" class="form-ajax" popid="modal-quote">
						   <input type="hidden" name="module" value="quote" />
						   <input type="hidden" name="pid" id="pid" value="<?php echo $item->p_id;?>" />
						   <input type="hidden" name="catid" value="<?php if($item->catid>0){echo $item->catid;}else{ echo $category->cat_id;}?>" />
						   <? if($item->cat_sid>0 || $scategory->cat_id>0){?><input type="hidden" name="catsid" value="<?php if($item->cat_sid>0){echo $item->cat_sid;}else{ echo $scategory->cat_id; }?>" /><?php }?>
						   <? if($item->vr_id>0 || $variation->vr_id>0){?><input type="hidden" name="vrid" value="<?php if($item->vr_id>0){ echo $item->vr_id;}else{ echo $variation->vr_id; }?>" /><?php }?>
						   <?php if($item->p_id==''){?><input type="hidden" name="type" value="1" /><? }?>
						   
						   
						  <div class="row">
						  <div class="col-md-12">
						  
						 <div class="row">
						  <div class="col-md-6">
						  	<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Name <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input" placeholder="" value="<?=$urw->user_name?>" name="name" required>
							</div>
							</div>
							
							<div class="col-md-6">
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Company Name<span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input" value="<?=$urw->user_company?>"   name="company" required>
						   </div>	
						   </div>
						   </div>
						   
							
						<div class="row">
						  <div class="col-md-6">
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Phone <span>*</span></label>
		    <input type="phone" class="form-control unicase-form-control text-input"  placeholder="" value="<?=$urw->user_mobile?>"  name="mobile" required>
							</div>
						  </div>
						
						  <div class="col-md-6">
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Email <span>*</span></label>
		    <input type="email" class="form-control unicase-form-control text-input"  placeholder="" value="<?=$urw->user_email?>" name="email" required>
						   </div>	
						   </div>
						   </div>	

						<? if($item->vr_id=='' && $variation->vr_id==''){ ?>
						<div class="row vpname">
						  <div class="col-md-6">							
							<div class="form-group">
						   <label class="info-title vpname-title" for="exampleInputName">Select Product<span>*</span></label>
						  <? if($item->cat_sid=='' && $scategory->cat_id==''){?>
						  <select class="form-control unicase-form-control selectpicker catsid" data-live-search="true" title="Select a Prooduct.." name="catsid">
						  <option value="" selected="selected">Select a Product</option>
						   <?php 
						   for($i=1; $i<=8; $i++){
						   foreach($topcats[$i] as $srw=>$sval){
						   echo '<optgroup label="'.trim($sval['cat_name']).'">';
						   foreach($sval['subcats'] as $crw=>$scat){
						   ?>	
						  <option value="<?php echo $scat->cat_id?>"><?php echo trim($scat->cat_name)?></option>
						  <? }
						  echo '</optgroup>';
						  }}?>
						  </select>
						  <? }else{?> <span class="form-control unicase-form-control"><? if($item->cat_sid>0){echo $item->cat_name;}else{ echo $scategory->cat_name; }?></span> <? }?>
						   </div>	
						</div>

						  <div class="col-md-6">							
							<div class="form-group">
						   <label class="info-title vpname-title" for="exampleInputName">Select Variations <span>*</span></label>
						  <select class="form-control unicase-form-control selectpicker" data-live-search="true"  title="Select Variations.." multiple  name="vrid[]" id="vrid" data-actions-box="true">
						   <?php foreach($vars as $rw){?>	
						  <option value="<?php echo $rw->vr_id?>"><?php echo $rw->vr_name?></option>
						  <? }?>
						  </select>
						   </div>	
						</div>
						</div>   
						<?php }?>




						<div class="row mtype">
						  <div class="col-md-12">							
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Are you flexible to choose any brands/model of <span class="mpname"><?php echo ucwords(strtolower($item->p_title." ".$item->vr_name));?></span>?</label><br />
						   <input type="radio" name="type" value="1" checked="checked" /> Yes! i am flexible  &nbsp; &nbsp; &nbsp;  <input type="radio" name="type" value="2" /> No! i need this exact brand/model
						   </div>	
						</div>
						</div>   


						<div class="row">
						  <div class="col-md-3">							
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Quantity<span>*</span></label>
		    <input type="number" class="form-control unicase-form-control text-input" value="1" name="qty" required>
						   </div>
						   </div>	


						  <div class="col-md-3">							
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Budget<span>*</span></label>
		    <input type="number" class="form-control unicase-form-control text-input" value="0" name="budget" required>
						   </div>
						   </div>	


						 <div class="col-md-6">
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Location<span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input geocomplete" name="location" required>
						   </div>	
						   </div>
						   </div>


						<div class="row">
						  <div class="col-md-12">							
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Message <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input"  placeholder=""  name="message">
						   </div>	
						</div>
						</div>   




	<div class="form-group">
		<button type="submit" class="btn-upper btn btn-primary checkout-page-button">Submit Request</button>
	</div>
			
						  </div>
                          </div>
						  </form>
                      </div>
                  </div>
				  </div>
              </div>




	 		  <div class="modal fade" id="modal-service" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h2 class="modal-title" style="font-size:18px;">Request Service for <strong><?php echo ucwords(strtolower($item->p_title." ".$item->vr_name));?></strong></h2>
                          </div>
                          <div class="modal-body">
						  <p>Please fill the form below and service engineer will contact you shortly!</p>
						  <form method="post" action="<?php echo base_url()."api";?>" class="form-ajax" popid="modal-service">
						   <input type="hidden" name="module" value="quote" />
						   <input type="hidden" name="pid" value="<?php echo $item->p_id;?>" />
						   <input type="hidden" name="catid" value="<?php if($item->catid>0){echo $item->catid;}else{ echo $category->cat_id;}?>" />
						   <input type="hidden" name="catsid" value="<?php if($item->cat_sid>0){echo $item->cat_sid;}else{ echo $scategory->cat_id; }?>" />
						   <input type="hidden" name="vrid" value="<?php if($item->vr_id>0){ echo $mvrid=$item->vr_id;}else{ echo $mvrid=$variation->vr_id; }?>" />
						   <input type="hidden" name="type" value="1" />
						  <div class="row">
						  <div class="col-md-12">
						  
						 <div class="row">
						  <div class="col-md-6">
						  	<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Name <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input" placeholder="" value="<?=$urw->user_name?>" name="name" required>
							</div>
							</div>
							
							<div class="col-md-6">
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Company Name<span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input"   name="company" value="<?=$urw->user_company?>" required>
						   </div>	
						   </div>
						   </div>
						   
							
						<div class="row">
						  <div class="col-md-6">
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Phone <span>*</span></label>
		    <input type="phone" class="form-control unicase-form-control text-input"  placeholder="" value="<?=$urw->user_mobile?>"  name="mobile" required>
							</div>
						  </div>
						
						  <div class="col-md-6">
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Email <span>*</span></label>
		    <input type="email" class="form-control unicase-form-control text-input"  placeholder="" value="<?=$urw->user_email?>" name="email" required>
						   </div>	
						   </div>
						   </div>


						<div class="row">
						  <div class="col-md-6">							
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Quantity<span>*</span></label>
		    <input type="number" class="form-control unicase-form-control text-input" value="1" name="qty" required>
						   </div>
						   </div>	

						 <div class="col-md-6">
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Location<span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input geocomplete" name="location" required>
						   </div>	
						   </div>
						   </div>


						<div class="row">
						  <div class="col-md-12">							
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Message <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input"  placeholder=""  name="message">
						   </div>	
						</div>
						</div>   


	<div class="form-group">
		<button type="submit" class="btn-upper btn btn-primary checkout-page-button">Submit Request</button>
	</div>
			
						  </div>
                          </div>
						  </form>
                      </div>
                  </div>
				  </div>
              </div>

<!-- JavaScripts placed at the end of the document so the pages load faster --> 
<script src="assets/js/bootstrap.min.js"></script> 
<script src="assets/js/bootstrap-hover-dropdown.min.js"></script> 
<script src="assets/js/owl.carousel.min.js"></script> 
<script src="assets/js/echo.min.js"></script> 
<script src="assets/js/jquery.easing-1.3.min.js"></script> 
<script src="assets/js/bootstrap-slider.min.js"></script> 
<script src="assets/js/jquery.rateit.min.js"></script> 
<script type="text/javascript" src="assets/js/lightbox.min.js"></script> 
<script src="assets/js/bootstrap-select.min.js"></script> 
<script type="text/javascript" src="assets/js/lity.min.js"></script>
<script src="assets/js/wow.min.js"></script> 
<script src="assets/js/sweetalert/sweet-alert.js"></script>
<script src="assets/js/scripts.js"></script>
<script src="assets/js/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.js"></script>
<script src="assets/js/bootstrap3-typeahead.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAYFPW1ze1fdTUWek5hvK0SryYHG5CCqoY&amp;libraries=places"></script>
<script src="https://ubilabs.github.io/geocomplete/jquery.geocomplete.js"></script>	
<script type="text/javascript">
	 $('.typeahead').typeahead({
	 	  items:100,	
		   matcher: function(item) {
		      return true;
  			},
          source: function (query, process) {
		 	objects = [];
        	map = {};
            $.ajax({
              url: '<?php echo base_url()."api";?>',
              type: 'POST',
			  data: {key : query, module: 'key_search'},
              dataType: 'JSON',
              success: function(data) {
				objects = [];
        		$.each(data, function(i, object) {
	            map[object.label] = object;
    	        objects.push(object.label);
        		});
		        process(objects);
              },
			   beforeSend: function(){ $('.searchbox-icon').html('<img src="<?php echo base_url();?>assets/images/loader.gif">'); },
               complete: function(){   $('.searchbox-icon').html('<span class="zmdi zmdi-search search-icon"></span>'); }
            });
          },
		updater: function(item) {
			$('#key').val(map[item].name);
			if(map[item].slug!=''){ window.location ="<?php echo base_url();?>" + map[item].slug; }else{
			$('.searchform').submit();		
			}
	    }		  
        });


$(function() {
  $(".scrolling").mCustomScrollbar({
    theme: "dark",
    scrollInertia: 0,
    mouseWheel:{
      preventDefault: true
    }
  });
});		

$(document).on('click', '.btn-quote, .btnquote', function (e) {
$('#pid').val($(this).attr('pid'));
$('.quote-title').html('Get Quote for <b>'+ $(this).attr('pname') +'</b>');
$('.mpname').html($(this).attr('pname'));
$('#pname').val($(this).attr('pname'));
 $('.mtype').show();
$('.vpname').hide();
$('#modal-quote').modal('show'); 
});    

$(document).on('submit', '.form-ajax', function (e) {
	var elem = $(this); var popup = $(this).attr('popid');
				$.ajax({
						type : "POST",
						url : elem.attr("action"),
						dataType : "html",
						data : elem.serialize(),
						beforeSend : function() {
							pleasewait('');
						},
						success : function(response) {	
if(popup=='modal-signup'){
if(Number(response)>0){swal("Congratulations!", "Your account has been created successfully!", "success"); $('#' + popup).modal('hide');}else{ var res=response.split("|");  swal("Error", res[1], "error"); }
}else{
if(Number(response)>0){ swal("Message Sent!", "Your message has been sent to concerned department. You will get response within next couple hrs.", "success"); $('#' + popup).modal('hide'); }else{ var res=response.split("|");  swal("Error", res[1], "error");}
}
						}
				});			  
	return false;		  
});    

function pleasewait(msg){
			swal({
                title: '<img src="assets/images/loading.gif">',
                text: "Please wait while we process the operation.",
                showConfirmButton: false
            });
}



$(document).on('change', '.catsid', function (e) {
load_vars();
});
function load_vars(){
var  cat = $('.catsid').val();
		$.ajax({
				type : "POST",
				url : '<?php echo base_url()."api";?>',
				dataType : "html",
				data : {parent : cat, module : 'get_vars'},
				success : function(response) {
					$('#vrid').html('');
					var data = $.parseJSON(response);
					$.each(data, function(i, field){
            		$("#vrid").append('<option value="'+ field.id +'">'+ field.name +'</option>');
        			});
					$('#vrid').selectpicker('refresh');
				}
		});
}		



      $(function(){
        
        $(".geocomplete").geocomplete()
          .bind("geocode:result", function(event, result){
            console.log("Result: " + result.formatted_address);
			$(this).val(result.formatted_address);
          })
          .bind("geocode:error", function(event, status){
            console.log("ERROR: " + status);
          })
          .bind("geocode:multiple", function(event, results){
            console.log("Multiple: " + results.length + " results found");
          });
      });

</script>	
<!--End of Tawk.to Script-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-88935475-1', 'auto');
  ga('send', 'pageview');

</script> 
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/58532e52fccdfa3ec85f19f2/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>

<script type="text/javascript">stLight.options({publisher: "b64324f3-14d8-4cea-a6b6-9550367d02a0", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
<script>
var options={ "publisher": "b64324f3-14d8-4cea-a6b6-9550367d02a0", "position": "left", "ad": { "visible": false, "openDelay": 5, "closeDelay": 0}, "chicklets": { "items": ["facebook", "twitter", "googleplus", "whatsapp", "linkedin", "pinterest", "email", "sharethis"]}};
var st_hover_widget = new sharethis.widgets.hoverbuttons(options);
</script>
</body>
<div style="display: none;">
<a rel="dofollow" href="http://www.hatayturk.com/" title="hatay escort">hatay escort</a>
<a rel="dofollow" href="http://www.mersincicikiz.com/" title="mersin escort">mersin escort</a>
<a rel="dofollow" href="http://www.kibrisaltin.com/" title="kibris escort">kibris escort</a>
<a rel="dofollow" href="http://www.mekaneskisehir.com/" title="eskisehir escort">eskisehir escort</a>
<a rel="dofollow" href="http://www.akadana.com/" title="adana escort">adana escort</a>
<a rel="dofollow" href="http://www.bizimizmit.com/" title="izmit escort">izmit escort</a>
<a rel="dofollow" href="http://www.antalyaanbar.com/" title="antalya escort">antalya escort</a>
<a rel="dofollow" href="http://www.bodrumkitapkulubu.com/" title="bodrum escort">bodrum escort</a>
<a rel="dofollow" href="http://www.kayseribazar.com/" title="kayseri escort">kayseri escort</a>
<a rel="dofollow" href="http://www.konyaekol.com/" title="konya escort">konya escort</a>
</div>
</html>