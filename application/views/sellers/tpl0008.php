<!DOCTYPE html>

 <html class="no-js"> 
    

<head>
       <meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<meta name="description" content="<?php echo $meta_desc;?>">
		<meta name="author" content="Hospytek">
		<meta name="keywords" content="<?php echo $meta_key;?>">
		<meta name="robots" content="all">
		<title><?php echo $meta_title;?></title>
		<!-- Bootstrap Core CSS -->
		<base href="<?php echo base_url();?>">
        <link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0008/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0008/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0008/css/flexslider.css">
        <link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0008/css/style.css">

        <script src="https://www.hospytek.com/assets/sellers/tpl0008/js/vendor/modernizr-2.6.2-respond-1.1.0.min.js"></script>
</head>

    <body>

   

    <div id="st-trigger-effects" class="trigger-fixed visible-xs visible-sm">
      <a href="#" data-effect="st-effect-2">Menu<i class="fa fa-bars"></i></a>
    </div>

    <div id="st-container" class="st-container">

      <nav class="st-menu st-effect-2" id="menu-4">
                       <div class="logo">
                                <a href="<?php echo $brand->user_slug;?>">  
                                    <?  if($brand->user_image=='') 
                                        { ?>
                                            <a href="<?php echo $brand->user_slug;?>"><?php echo $brand->user_company?></a><br/>
                                        <? } else if($brand->user_image=='no.gif') { ?>
                                    <a href="<?php echo $brand->user_slug;?>" style="text-decoration:none; color:#0b8aef;"><?php echo $brand->user_company;?></a><br/>
                                    <? }
                                        else
                                        { $imgurl= 'http://www.hospytek.com/newcrm/UserFiles/Image/'.$brand->user_image;?>
                                    <a href="<?php echo $brand->user_slug;?>"><img src="<?=$imgurl?>" alt="<?=$brand->user_company?>" style="height:70px;margin-top:5px;"/></a><br/>
                                    <? } 
                                ?>
                                   </a>
                                &nbsp;&nbsp;<span><?php echo $brand->user_city;?>, <?php echo $brand->user_state;?> <?php echo $brand->user_country;?></span></h2>
            </div>
         <ul clas="col-md-6">
                      <li><a href="<?php echo $brand->user_slug;?>">Home</a>                       
                      </li>
                      <li><a href="<?php echo $brand->user_slug;?>#aboutus">About us</a></li>
                    
                      <li><a href="<?php echo $brand->user_slug;?>#product">Products</a></li>
                      
                      <li><a href="<?php echo $brand->user_slug;?>#contact">Contact</a></li>
                     
          </ul>
         	<div style="display:inline;float:left;margin-top:35px;padding-left:20px"><a data-toggle="modal" href="#modal-callback" class="cart-sellers" style="background-color:red;"><button class="btn btn-primary">Become a Dealer</button></a></div>
      </nav>

      <!-- content push wrapper -->
      <div class="st-pusher">  

        <div class="st-content"><!-- this is the wrapper for the content -->
          <div class="st-content-inner"><!-- extra div for emulating position:fixed of the menu -->

        
              

              <!-- NAVBAR -->
              <div id="navbar" class="nav-fixed hidden-xs hidden-sm">
                <div class="container" style="margin-top: -5px;">
                  <div class="nav-logo col-md-6"><a href="#"><strong><?php echo $brand->user_company;?></strong></a></div><br>
				  	<span><?php echo $brand->user_city;?>, <?php echo $brand->user_state;?> <?php echo $brand->user_country;?></span></h6></a>
                  <nav class="main-nav col-md-6" style="margin-top: -25px;">
                       
                    <ul>
                      <li><a href="<?php echo $brand->user_slug;?>">Home</a>                       
                      </li>
                      <li><a href="<?php echo $brand->user_slug;?>#aboutus">About us</a></li>
                    
                      <li><a href="<?php echo $brand->user_slug;?>#product">Products</a></li>
                      
                      <li><a href="<?php echo $brand->user_slug;?>#contact">Contact</a></li>
                     
                    </ul>
				<div style="display:inline;padding-left:20px"><a data-toggle="modal" href="#modal-callback" class="cart-sellers" style="background-color:red;"><button class="btn btn-danger" >Become a Dealer</button></a></div>
                  </nav>
                </div>
              </div>
              <!-- NAVBAR End -->
			  <!-- HOME FULLSCREEN -->
            	<section id="main-slider"><!--slider-->
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
				
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#slider-carousel" data-slide-to="1"></li>
							<li data-target="#slider-carousel" data-slide-to="2"></li>
						</ol>
						
						<div class="carousel-inner">
						
							
						
						
						 <?php $i=0; foreach ($items as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;} ;
						  if($i>6){ break;}
						  ?>				
						  <?php if ($i==0) { $i++; ?>
        			  <div class="item active"> 
								<div class="col-sm-6" style="margin-top:150px">
									<h3><?php echo $rw->p_title." ".$rw->vr_name;?></h3>
									<h5><strong>Model:</strong> <?php echo $rw->p_model;?></h5>
									<p><?php echo $rw->p_sdesc;?></p>
									
									<button type="button" class="btn btn-default get"><a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online">View Detail</a></button>
									<button type="button" class="btn btn-default get"><a href="<?php echo '../pdf/'.$rw->p_slug.'.pdf'?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Brochure Manual Download">View Brochure</a></span></button>
								</div>
								<div class="col-sm-6">
									<img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="girl img-responsive" alt="" />
									
								</div>
							</div>
							<?php } else  {  ?>
							
							<div class="item"> 
								<div class="col-sm-6" style="margin-top:150px">
									<h3><?php echo $rw->p_title." ".$rw->vr_name;?></h3>
									<h5><strong>Model:</strong> <?php echo $rw->p_model;?></h5>
									<p><?php echo $rw->p_sdesc;?></p>
									
									<button type="button" class="btn btn-default get"><a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online">View Detail</a></button>
									<button type="button" class="btn btn-default get"><a href="<?php echo '../pdf/'.$rw->p_slug.'.pdf'?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Brochure Manual Download">View Brochure</a></span></button>
								</div>
								<div class="col-sm-6">
									<img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="girl img-responsive" alt="" />
									
								</div>
							</div>
							<?php  } ?>
							
							
							
							<?php  } ?>
							
							
							
						</div>
						
						
                        <a href="#slider-carousel" class="left control-carousel pull-left" data-slide="prev">
                            <i class="fa fa-angle-left" style="color:grey;font-size:50px;"></i>
                        </a>
                        <a href="#slider-carousel" class="right control-carousel pull-right" data-slide="next">
                            <i class="fa fa-angle-right" style="color:grey;font-size:50px;"></i>
                        </a>

					</div>
					
				</div>
			</div>
		</div>
	</section><!--/slider-->
              <!-- HOME FULLSCREEN End -->
     

            <!-- ABOUT US SECTION CONTAINER -->
            <section id="aboutus">
              <section class="section about-text">
                <div class="container first-section">

                  <!-- SECTION HEADER -->
                  <div class="section-header">
                    <h2>
                      <span>WELCOME TO <?=$brand->user_company?></span>
                    </h2>
                   <p><?=$brand->user_about?></p>
                  </div>
               
                </div>
              </section>
         
            </section>          
  
              <!-- PORTFOLIO SECTION -->
              <section id="product" class="section">
                <div class="container">

                  <!-- SECTION HEADER -->
                  <div class="section-header">
                    <h1>
                      <span>Products</span>
                    </h1>
                  </div>
                  <!-- SECTION HEADER End -->

                </div>
           <div class="product-items-container" style="text-align: center;">
                	  <?php $i=0; foreach ($items as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;}  $i++;?>
				<?php if($i==1){echo '<div class="row">';}?>
               
                  <div class="col-md-4">
			
                  
                   <img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" style="border:solid .1px #dddddd;">

					<p><a href="#"><?php echo $rw->p_title." ".$rw->vr_name;?></a></p>
					<h6><a href="#"><span><?php echo $rw->p_model;?></span></a></h6>
					<div>
					<a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online"><button type="button" class="btn btn-primary">View Detail</button></a>
					</div>
                  </div>
                 
               <?php if($i==3){ $i=0; echo '</div><div class="clearfix">&nbsp; </div>';}?>
                    <? }?>
                 
                </div>

              </section>
          
         
		 
            <section data-scroll-index="5">

              <!-- CONTACT SECTION -->
              <section id="contact" class="section">
                <div class="container">

                  <!-- SECTION HEADER -->
                  <div class="section-header">
                    <h1>
                      <span>Contact</span>
                    </h1>
                   
                  </div>
                  <!-- SECTION HEADER End -->

                  <div class="row">
                    <div class="col-sm-7 fadeLeft">
                      <h5 class="small-header">
                        <span>Contact us</span>
                      </h5>
              
                      <form id="contact-form" class="form-horizontal" method="post" action="http://loprd.pl/templates/ante/php/send.php">
                   
                          <div id="message-input">
                            <input type="text" name="name" id="name" value="" placeholder="Name" size="22" tabindex="1" aria-required="true" class="requiredField name input-name label-better">
                            <input type="text" name="email" id="email" value="" placeholder="Email" size="22" tabindex="2" aria-required="true" class="requiredField email input-email label-better">
                          </div>
                          <div id="message-textarea">
                              <textarea name="message" id="message" cols="39" rows="6" tabindex="4" class="requiredField label-better" placeholder="Message..."></textarea>
                          </div>
                          <div id="message-submit"><p></p>
                              <div>
                                  <input name="Send" type="submit" id="Send" tabindex="5" value="Send message" class="btn-submit btn">
                              </div>
                          </div>
                      </form>


                    </div>

                    <div class="col-sm-5 fadeRight resp-no-gap">
                      <h5 class="small-header">
                        <span>Our office</span>
                      </h5>

                      <div class="row bottom-gap">
                        <div class="col-sm-12">
                          <h6 class="accent-color"><strong>Address</strong></h6>
						  <p><i class="fa fa-pencil"></i><?=$brand->user_address?>,<br/><?=$brand->user_city?>, <?=$brand->user_state?>
									                     <p><?=$brand->user_country?>  <?=$brand->user_postcode?></p></p>
						                              	<p><i class="fa fa-phone"></i>+91-<?=$brand->user_mobile?>     <?=$brand->user_phone?></p>
                        </div>
                          
                      </div>

                      <h5 class="small-header">
                        <span>Get connected</span>
                      </h5>

                      <div class="social-icons">
                          <a href="#" class="fa fa-facebook"></a>
                          <a href="#" class="fa fa-twitter"></a>
                          <a href="#" class="fa fa-pinterest"></a>                        
                          <a href="#" class="fa fa-linkedin"></a>                         
                          <a href="#" class="fa fa-google-plus"></a>
                          <a href="#" class="fa fa-youtube"></a>                         
                          <a href="#" class="fa fa-instagram"></a>
                         
                      </div>

                    </div>
                  </div>

                </div>
              </section>
              <!-- CONTACT SECTION End -->

           

            </section>
            <!-- CONTACT SECTION End -->

            <footer>
            
              <p>powered by <a href="https://www.hospytek.com/" ><img src="<?php echo base_url();?>/assets/images/logo.png" alt="logo"></a> </p>
            </footer>

          </div><!-- /st-content-inner -->
        </div><!-- /st-content -->
      </div><!-- /st-pusher -->
    </div><!-- /st-container -->
    
    
      
    <script src="https://www.hospytek.com/assets/sellers/tpl0008/js/vendor/jquery-1.10.1.min.js"></script>
    <script>window.jQuery || document.write('<script src="https://www.hospytek.com/assets/sellers/tpl0008/js/vendor/jquery-1.10.1.min.js"><\/script>')</script>
    <script src="https://www.hospytek.com/assets/sellers/tpl0008/js/vendor/bootstrap.min.js"></script>

    <script src="https://www.hospytek.com/assets/sellers/tpl0008/js/classie.js"></script>
    <script src="https://www.hospytek.com/assets/sellers/tpl0008/js/sidebarEffects.js"></script>
    <script src="https://www.hospytek.com/assets/sellers/tpl0008/js/jquery.countTo.js"></script>
    <script src="https://www.hospytek.com/assets/sellers/tpl0008/js/waypoints.min.js"></script>
    <script src="https://www.hospytek.com/assets/sellers/tpl0008/js/jquery-scrolltofixed-min.js"></script>
    <script src="https://www.hospytek.com/assets/sellers/tpl0008/js/scrollIt.min.js"></script>
    <script src="https://www.hospytek.com/assets/sellers/tpl0008/js/jquery.flexslider.js"></script>
    <script src="https://www.hospytek.com/assets/sellers/tpl0008/js/jquery.label_better.min.js"></script>
    <script src="../../../maps.googleapis.com/maps/api/js0863?sensor=false&amp;extension=.js" type="text/javascript"></script>
    <script src="https://www.hospytek.com/assets/sellers/tpl0008/js/map.js"></script>
    <!--<script src="assets/sellers/tpl0008/js/jquery.isotope.min.js"></script>-->
    <script src="https://www.hospytek.com/assets/sellers/tpl0008/js/isotope.pkgd.min.js"></script>
    <script src="https://www.hospytek.com/assets/sellers/tpl0008/js/skrollr.min.js"></script>
    <script src="https://www.hospytek.com/assets/sellers/tpl0008/js/socialcount.min.js"></script>
    <script src="https://www.hospytek.com/assets/sellers/tpl0008/js/lightbox-2.6.min.js"></script>
    <script src="https://www.hospytek.com/assets/sellers/tpl0008/js/retina-1.1.0.min.js"></script>
    <script src="https://www.hospytek.com/assets/sellers/tpl0008/js/jquery.fitvids.js"></script>
    <script src="https://www.hospytek.com/assets/sellers/tpl0008/js/twitter/jquery.tweet.js"></script>
    <script src="https://www.hospytek.com/assets/sellers/tpl0008/js/jquery.mb.YTPlayer.js"></script>

    <script src="https://www.hospytek.com/assets/sellers/tpl0008/js/main.js"></script>



    <!-- STYLE SWITCHER -->
    <script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0008/style-switcher/style-switcher.js"></script>
    <link rel="stylesheet" type="text/css" href="https://www.hospytek.com/assets/sellers/tpl0008/style-switcher/style-switcher.css">



	 		  <div class="modal fade" id="modal-callback" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h2 class="modal-title">Please Fill the form & we will call you back</h2>
                          </div>
                          <div class="modal-body">
						  <form method="post" action="<?php echo base_url()."api";?>" class="form-ajax">
						  <input type="hidden" name="module" value="callback" />
						  <input type="hidden" name="type" value="0" />
						  <div class="row">
						  <div class="col-md-6">
						  	<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Name <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input" placeholder="" name="name" required>
							</div>

							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Phone <span>*</span></label>
		    <input type="phone" class="form-control unicase-form-control text-input"  placeholder=""  name="mobile" required>
							</div>
							
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Email <span>*</span></label>
		    <input type="email" class="form-control unicase-form-control text-input"  placeholder=""  name="email" required>
						   </div>	
							
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Best time to call you <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input"  placeholder="" name="timetocall" required>
						   </div>	


							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Message <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input"  placeholder=""  name="message" required>
						   </div>	


	<div class="form-group">
		<button type="submit" class="btn-upper btn btn-primary checkout-page-button">Submit Request</button>
	</div>
			
						  </div>

						  <div class="col-md-6" style="font-size:12px;">
						   <p><strong>Lets give you a call back to help you buy the most suitable Medical Equipment & Device as per your requirement, We would not only help you choose the most suitable medical device but also help you find the right localised vendor to make your after sale requirements more readily available.</strong></p>
						  <center><img src="assets/images/callback-icon.png" class="img-responsive" style="max-height:175px;" /></center>
						  <p>We at Hospytek hold immense value for every customer or prospective customer and consider it as our utmost duty to provide a solution to all the queries raised by the customers.</p>

<p>We thank you for filling the form and assure you that one of our sales executives would get back to you very soon to understand your problem and provide the most reasonable solution.</p>

<p><strong>We again thank you for your patience and hope you are your experience on our website.</strong></p>
						  </div>
						  
                          </div>
						  </form>
                      </div>
                  </div>
				  </div>
              </div>


    </body>

</html>
