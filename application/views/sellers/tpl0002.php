<!DOCTYPE html>
<html lang="en">
    <head>
       <meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
<meta name="description" content="<?php echo $meta_desc;?>">
<meta name="author" content="Hospytek">
<meta name="keywords" content="<?php echo $meta_key;?>">
<meta name="robots" content="all">
<title><?php echo $meta_title;?></title>
<!-- Bootstrap Core CSS -->
<base href="<?php echo base_url();?>">
        <!-- Google Fonts -->
        <link href='http://fonts.googleapis.com/css?family=Titillium+Web:400,200,300,700,600' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Roboto+Condensed:400,700,300' rel='stylesheet' type='text/css'>
        <link href='http://fonts.googleapis.com/css?family=Raleway:400,100' rel='stylesheet' type='text/css'>
        <!-- Bootstrap -->
        <link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0002/css/bootstrap.min.css">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0002/css/font-awesome.min.css">
        <!-- Custom CSS -->
        <link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0002/css/owl.carousel.css">
        <link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0002/css/style.css">
        <link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0002/css/responsive.css">
        <link href="https://www.hospytek.com/assets/sellers/tpl0002/css/full-slider.css" rel="stylesheet">
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
       
        <div class="mainmenu-area" style="background:#dbe9bb">
            <div class="container" >
                <div class="row">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="navbar-collapse collapse">
                        <div class="col-md-6 pull-left" style="padding-top:1%;">
                         <div class="logo">
                                <a href="<?php echo $brand->user_slug;?>">  
                                    <?  if($brand->user_image=='') 
                                        { ?>
                                            <a href="<?php echo $brand->user_slug;?>"><?php echo $brand->user_company?></a><br/>
                                        <? } else if($brand->user_image=='no.gif') { ?>
                                    <a href="<?php echo $brand->user_slug;?>" style="text-decoration:none; color:#0b8aef;"><?php echo $brand->user_company;?></a><br/>
                                    <? }
                                        else
                                        { $imgurl= 'http://www.hospytek.com/newcrm/UserFiles/Image/'.$brand->user_image;?>
                                    <a href="<?php echo $brand->user_slug;?>"><img src="<?=$imgurl?>" alt="<?=$brand->user_company?>" style="height:70px;margin-top:5px;"/></a><br/>
                                    <? } 
                                ?>
                                   </a>
                                &nbsp;&nbsp;<span><?php echo $brand->user_city;?>, <?php echo $brand->user_state;?> <?php echo $brand->user_country;?></span></h2>
                            </div>
                        </div>
                        <div class="col-md-6 pull-right" style="margin-top:10px;margin-bottom:5px;">
                            <ul class="nav navbar-nav ">
                                <li class="active"><a href="<?php echo $brand->user_slug;?>">Home</a></li>
                                <li><a href="<?php echo $brand->user_slug;?>#about">About</a></li>
                                <li><a href="<?php echo $brand->user_slug;?>#products">Product</a></li>
                                <li><a href="<?php echo $brand->user_slug;?>#contact">Contact</a></li>
                           
                             </ul>
                            <a data-toggle="modal" href="#modal-callback" class="cart-sellers"> <button type="button" class="btn btn-info" style="background:#469f53;margin-top: 2.7%; margin-left: 2%">Become a Dealer</button></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- End mainmenu area -->
  
    <section id="slider"><!--slider-->
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                
                    <div id="slider-carousel" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
                            <li data-target="#slider-carousel" data-slide-to="1"></li>
                            <li data-target="#slider-carousel" data-slide-to="2"></li>
                        </ol>
                        
                        <div class="carousel-inner">
                        <?php $i=0; foreach ($items as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;} ;
                          if($i>6){ break;}
                          ?>                
                          <?php if ($i==0) { $i++; ?>
                      <div class="item active"> 
                                <div class="col-sm-6" style="margin-top:150px;">
                                    <h1><?php echo $rw->p_title." ".$rw->vr_name;?></h1>
                                    <h3 ><strong>Model:</strong> <?php echo $rw->p_model;?></h3>
                                    <p><?php echo $rw->p_sdesc;?></p>
                                    
                                    <a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online"><button type="button" class="btn btn-default get">View Detail</button></a>
                                    <a href="<?php echo '../pdf/'.$rw->p_slug.'.pdf'?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Brochure Manual Download"><button type="button" class="btn btn-default get">View Brochure</span></button></a>
                                </div>
                                <div class="col-sm-6">
                                    <img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="girl img-responsive" alt="" />
                                    
                                </div>
                            </div>
                            <?php } else  {  ?>
                            
                            <div class="item"> 
                                 <div class="col-sm-6" style="margin-top:150px;">
                                    <h1><?php echo $rw->p_title." ".$rw->vr_name;?></h1>
                                    <h3><strong>Model:</strong> <?php echo $rw->p_model;?></h3>
                                    <p><?php echo $rw->p_sdesc;?></p>
                                    
                                    <a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online"><button type="button" class="btn btn-default get">View Detail</button></a>
                                    <a href="<?php echo '../pdf/'.$rw->p_slug.'.pdf'?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Brochure Manual Download"><button type="button" class="btn btn-default get">View Brochure</span></button></a>
                                </div>
                                <div class="col-sm-6">
                                    <img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="girl img-responsive" alt="" />
                                    
                                </div>
                            </div>
                            <?php  } ?>
                            
                            
                            
                            <?php  } ?>
                            
                            
                            
                        </div>
                        
                        <a href="#slider-carousel" class="left control-carousel hidden-xs pull-left" data-slide="prev">
                            <i class="fa fa-angle-left"></i>
                        </a>
                        <a href="#slider-carousel" class="right control-carousel hidden-xs pull-right" data-slide="next">
                            <i class="fa fa-angle-right"></i>
                        </a>
                    </div>
                    
                </div>
            </div>
        </div>
    </section><!--/slider-->
    
    

        <!-- End slider area -->
        <div class="container" id="about">
            <div class="col-md-12">
                <h2><br><br><br>WELCOME TO <?=$brand->user_company?></h2>
                <div class="col-sm-12">
                    <p><?=$brand->user_about?></p>
                </div>
            </div>
        </div><br><hr>
       
        <div class="maincontent-area" >
            <div class="zigzag-bottom"></div>
            <div class="container" id="products">
                <div class="row">
                    <div class="col-md-12">
                        <div class="latest-product">
                            <h4 class="section-title" style="z-index:9999"><br>Our Products</h4>
                        </div>
                        <!-- Multi product layout -->
                        <div class="single-product-area">
                            <div class="zigzag-bottom"></div>
                            <div class="container">
                                <div class="row">
                                <?php $i=0; foreach ($items as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;}  $i++;?>
                <?php if($i==1){echo '<div class="row">';}?>    
                                    <div class="col-md-4 col-sm-8">
                                        <div class="single-shop-product" style="border: solid .1px #cddbe9;">
                                            <div class="product-upper">
                                                <a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online"><img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" style="position: relative;padding-left: 11%;">
                                            </div>
                                            <br>
                                            <span><h5 style="text-align:center;"><a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online"><?php echo $rw->p_title." ".$rw->vr_name;?></a></h5></span>
                                            
                                            <span><h6 style="text-align:center;">Model <?php echo $rw->p_model;?></a></h6></span>
                                            
                                            <div class="item product-option-shop">
                                                <a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" class="add_to_cart_button" >View Details</a>
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <?php if($i==3){ $i=0; echo '</div><div class="clearfix">&nbsp; </div>';}?>
                    <? }?>

                                </div>
                            </div>
                        </div>
                        <!--  -->
                    </div>
                </div>
               
            </div>
        </div>
         <div class="container" id="contact">
                    <h2 class="sidebar-title">Connect with <?=$brand->user_company?></h2>
                    <form action="#">
                        <div class="row">
                            <div class="col-sm-6">
                                <div class="form-group">
                                    <label>Full Name</label>
                                    <input type="text" class="form-control" name="name" placeholder="Full Name">
                                </div>
                                <div class="form-group">
                                    <label>Email address</label>
                                    <input type="email" class="form-control" name="email" placeholder="Email">
                                </div>
                                <div class="form-group">
                                    <label>Message</label>
                                    <textarea class="form-control" name="message" placeholder="Your message here"></textarea>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <p><?=$brand->user_address?></p>
                                    <p><?=$brand->user_city?>, <?=$brand->user_state?></p>
                                    <p><?=$brand->user_country?>  <?=$brand->user_postcode?></p>
                                    <p>+91-<?=$brand->user_mobile?>     <?=$brand->user_phone?></p>
                                
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-6" style="padding-left: 20%;">
                                <button type="submit" class="btn btn-primary hvr-sweep-to-right">Submit</button>
                            </div>
                        </div>
                    </form>
                </div>
        <div class="footer-bottom-area">
            <div class="container">
                <div class="row" style="padding-left:45%">
                    <p style="color:white">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Powered by<br></p><p><a href="http://www.hospytek.com"><img src="<?php echo base_url();?>/assets/images/logo.png" alt="logo" /></a></p>
                    
                </div>
            </div>
        </div>
        <!-- End footer bottom area -->
        <!-- Latest jQuery form server -->
        <script src="https://code.jquery.com/jquery.min.js"></script>
        <!-- Bootstrap JS form CDN -->
        <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
        <!-- jQuery sticky menu -->
        <script src="https://www.hospytek.com/assets/sellers/tpl0002/js/owl.carousel.min.js"></script>
        <script src="https://www.hospytek.com/assets/sellers/tpl0002/js/jquery.sticky.js"></script>
        <!-- jQuery easing -->
        <script src="https://www.hospytek.com/assets/sellers/tpl0002/js/jquery.easing.1.3.min.js"></script>
        <!-- Main Script -->
        <script src="https://www.hospytek.com/assets/sellers/tpl0002/js/main.js"></script>
        <!-- Slider -->
        <script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0002/js/bxslider.min.js"></script>
        <script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0002/js/script.slider.js"></script>
        <script>
    $('.carousel').carousel({
        interval: 5000 //changes the speed
    })
    </script>
    </body>
    <div class="modal fade" id="modal-callback" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h2 class="modal-title">Please Fill the form & we will call you back</h2>
                          </div>
                          <div class="modal-body">
						  <form method="post" action="<?php echo base_url()."api";?>" class="form-ajax">
						  <input type="hidden" name="module" value="callback" />
						  <input type="hidden" name="type" value="0" />
						  <div class="row">
						  <div class="col-md-6">
						  	<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Name <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input" placeholder="" name="name" required>
							</div>

							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Phone <span>*</span></label>
		    <input type="phone" class="form-control unicase-form-control text-input"  placeholder=""  name="mobile" required>
							</div>
							
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Email <span>*</span></label>
		    <input type="email" class="form-control unicase-form-control text-input"  placeholder=""  name="email" required>
						   </div>	
							
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Best time to call you <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input"  placeholder="" name="timetocall" required>
						   </div>	


							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Message <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input"  placeholder=""  name="message" required>
						   </div>	


	<div class="form-group">
		<button type="submit" class="btn-upper btn btn-primary checkout-page-button">Submit Request</button>
	</div>
			
						  </div>

						  <div class="col-md-6" style="font-size:12px;">
						   <p><strong>Lets give you a call back to help you buy the most suitable Medical Equipment & Device as per your requirement, We would not only help you choose the most suitable medical device but also help you find the right localised vendor to make your after sale requirements more readily available.</strong></p>
						  <center><img src="assets/images/callback-icon.png" class="img-responsive" style="max-height:175px;" /></center>
						  <p>We at Hospytek hold immense value for every customer or prospective customer and consider it as our utmost duty to provide a solution to all the queries raised by the customers.</p>

<p>We thank you for filling the form and assure you that one of our sales executives would get back to you very soon to understand your problem and provide the most reasonable solution.</p>

<p><strong>We again thank you for your patience and hope you are your experience on our website.</strong></p>
						  </div>
						  
                          </div>
						  </form>
                      </div>
                  </div>
				  </div>
              </div>



	 		  <div class="modal fade" id="modal-quote" tabindex="-1" role="dialog" aria-hidden="true">
</html>

