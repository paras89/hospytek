<!DOCTYPE html>
<html lang="en">
  <head>
		
	<!-- Meta -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
	<meta name="description" content="<?php echo $meta_desc;?>">
	<meta name="author" content="Hospytek">
	<meta name="keywords" content="<?php echo $meta_key;?>">
	<meta name="robots" content="all">
	<title><?php echo $meta_title;?></title>
	<!-- Bootstrap Core CSS -->
	<base href="<?php echo base_url();?>">
	
    <!-- Font awesome -->
    <link href="https://www.hospytek.com/assets/sellers/tpl0024/css/font-awesome.css" rel="stylesheet">
    <!-- Bootstrap -->
    <link href="https://www.hospytek.com/assets/sellers/tpl0024/css/bootstrap.css" rel="stylesheet">   
    <!-- slick slider -->
    <link rel="stylesheet" type="text/css" href="https://www.hospytek.com/assets/sellers/tpl0024/css/slick.css">
    <!-- price picker slider -->
    <link rel="stylesheet" type="text/css" href="https://www.hospytek.com/assets/sellers/tpl0024/css/nouislider.css">
    <!-- Fancybox slider -->
    <link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0024/css/jquery.fancybox.css" type="text/css" media="screen" /> 
    <!-- Theme color -->
    <link id="switcher" href="https://www.hospytek.com/assets/sellers/tpl0024/css/theme-color/default-theme.css" rel="stylesheet">     

    <!-- Main style sheet -->
    <link href="https://www.hospytek.com/assets/sellers/tpl0024/css/style.css" rel="stylesheet">  
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">  

   
    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Vollkorn' rel='stylesheet' type='text/css'>    
    <link href='https://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
    

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
   
  </head>
  <body class="aa-price-range" style="background-color:#8a7c7c">  
  <!-- Pre Loader -->
  <!--<div id="aa-preloader-area">
    <div class="pulse"></div>
  </div>-->
  <!-- SCROLL TOP BUTTON -->
    <a class="scrollToTop" href="#"><i class="fa fa-angle-double-up"></i></a>
  <!-- END SCROLL TOP BUTTON -->
  <!-- Start menu section -->
  <section id="aa-menu-area">
    <nav class="navbar  navbar-fixed-top" role="navigation" style="background-color: #50f2ad;" >  
      <div class="container">
        <div class="navbar-header">
          <!-- FOR MOBILE VIEW COLLAPSED BUTTON -->
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <!-- LOGO -->                                               
          <!-- Text based logo -->
          <div class="logo">
                                <a href="<?php echo $brand->user_slug;?>">  
                                    <?  if($brand->user_image=='') 
                                        { ?>
                                            <a href="<?php echo $brand->user_slug;?>"><?php echo $brand->user_company?></a><br/>
                                        <? } else if($brand->user_image=='no.gif') { ?>
                                    <a href="<?php echo $brand->user_slug;?>" style="text-decoration:none; color:#0b8aef;"><?php echo $brand->user_company;?></a><br/>
                                    <? }
                                        else
                                        { $imgurl= 'http://www.hospytek.com/newcrm/UserFiles/Image/'.$brand->user_image;?>
                                    <a href="<?php echo $brand->user_slug;?>"><img src="<?=$imgurl?>" alt="<?=$brand->user_company?>" style="height:70px;margin-top:5px;"/></a><br/>
                                    <? } 
                                ?>
                                   </a>
                                &nbsp;&nbsp;<span><?php echo $brand->user_city;?>, <?php echo $brand->user_state;?> <?php echo $brand->user_country;?></span></h2>
                        </div>
           <!-- Image based logo -->
           <!-- <a class="navbar-brand aa-logo-img" href="index.html"><img src="img/logo.png" alt="logo"></a> -->
        </div>
        <div id="navbar" class="navbar-collapse collapse ">
          <ul id="top-menu" class="nav navbar-nav navbar-right aa-main-nav">
            <li class="active"><a href="<?php echo $brand->user_slug;?>">HOME</a></li>
            <li><a href="<?php echo $brand->user_slug;?>#about">ABOUT US</a></li>                                         
            <li class="dropdown">
              <a href="<?php echo $brand->user_slug;?>#product">PRODUCTS</a> 
            </li>
            <li><a href="<?php echo $brand->user_slug;?>#contact">CONTACT</a></li>
			<li><a data-toggle="modal" href="#modal-callback" class="btn-warning" style="background-color:#fef5b5">Become a Dealer</a></li>
          </ul>                            
        </div><!--/.nav-collapse -->       
      </div>          
    </nav> 
  </section>
  <!-- End menu section -->

  <!-- Start slider  -->
  	<section id="slider" style="margin-top:2%"><!--slider-->
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
				
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#slider-carousel" data-slide-to="1"></li>
							<li data-target="#slider-carousel" data-slide-to="2"></li>
						</ol>
						
						<div class="carousel-inner">
						
							
						
						
						 <?php $i=0; foreach ($items as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;} ;
						  if($i>6){ break;}
						  ?>				
						  <?php if ($i==0) { $i++; ?>
        			  <div class="item active"> 
								<div class="col-sm-6" style="margin-top:180px!important;">
									<h1><?php echo $rw->p_title." ".$rw->vr_name;?></h1>
									<h3 ><strong>Model:</strong> <?php echo $rw->p_model;?></h3>
									<p><?php echo $rw->p_sdesc;?></p>
									
									<a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online"><button type="button" class="btn btn-default get">View Detail</button></a>
									<a href="<?php echo '../pdf/'.$rw->p_slug.'.pdf'?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Brochure Manual Download"><button type="button" class="btn btn-default get">View Brochure</button></a>
								</div>
								<div class="col-sm-6" style="margin-top:60px!important;">
									<img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="girl img-responsive" alt="" />
									
								</div>
							</div>
							<?php } else  {  ?>
							
							<div class="item"> 
								<div class="col-sm-6" style="margin-top:180px!important;">
									<h1><?php echo $rw->p_title." ".$rw->vr_name;?></h1>
									<h3><strong>Model:</strong> <?php echo $rw->p_model;?></h3>
									<p><?php echo $rw->p_sdesc;?></p>
									
									<a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online"><button type="button" class="btn btn-default get">View Detail</button></a>
									<a href="<?php echo '../pdf/'.$rw->p_slug.'.pdf'?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Brochure Manual Download"><button type="button" class="btn btn-default get">View Brochure</button></a>
								</div>
								<div class="col-sm-6" style="margin-top:60px!important;">
									<img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="girl img-responsive" alt="" />
									
								</div>
							</div>
							<?php  } ?>
							
							
							
							<?php  } ?>
							
							
							
						</div>
						
						<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev" >
							<i class="fa fa-angle-left" style="font-size:40px;"></i>
						</a>
						<a href="#slider-carousel" class="right control-carousel hidden-xs pull-right" data-slide="next">
							<i class="fa fa-angle-right" style="font-size:40px;"></i>
						</a>
					</div>
					
				</div>
			</div>
		</div>
	</section><!--/slider-->
  <!-- End slider  -->

 

  <!-- About us -->
  <section id="aa-about-us" style="background:linear-gradient(to top right, #ffffff 0%, #3399ff 100%)">
    <div class="container" id="about">
      <div class="row">
        <div class="col-md-12">
          <div class="aa-about-us-area">
            <div class="row">
                <div class="col-md-12">
                <div class="aa-about-us-right">
                  <div class="aa-title">
                    <h2>WELCOME TO <?=$brand->user_company?></h2>
                    <span></span>
                  </div>
                  <p><?=$brand->user_about?></p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / About us -->

  <!-- Latest property -->
  <section id="aa-latest-property" style="background-color:#c8b9cb">
    <div class="container" id="product">
      <div class="aa-latest-property-area">
        <div class="aa-title">
          <h2>Our Products</h2>
          <span></span>        
        </div>
        <div class="aa-latest-properties-content">
          <div class="row">
		  <?php $i=0; foreach ($items as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;}  $i++;?>
				<?php if($i==1){echo '<div class="row">';}?>	
            <div class="col-md-4" >
             
                  <div class="aa-single-agents">
                    <div class="aa-agents-img">
                      <img  src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" style="width:100%">
                    </div>
                    <div class="aa-agetns-info" style="text-align:center">
                      <h4><a href="#"><?php echo $rw->p_title." ".$rw->vr_name;?></a></h4>
                      <span>Model : <span><?php echo $rw->p_model;?></span></span>
                      <div class="aa-agent-social">
                        <a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online"><button class="btn-sm btn-danger">View Details</button></a>
                      </div>
                    </div>
                  </div>               
            </div>
			<?php if($i==3){ $i=0; echo '</div><div class="clearfix">&nbsp; </div>';}?>
					<? }?>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- / Latest property -->
  <!-- Contact Form -->
  <section id="aa-contact">
 	<div id="contact" class="container">
    	<div class="bg">
	    	<div class="row">    		
	    		<div class="col-sm-12">
	    		<div class="aa-title">
                    <h2>Connect with Us</h2>
                    <span></span>
                  </div>    			   			
					</div>			 		
			</div>    	
    		<div class="row">  	
	    		<div class="col-sm-8">
	    			<div class="contact-form"><br><br>
	    				<h2 class="title text-center"><br>Get In Touch</h2>
	    				<div class="status alert alert-success" style="display: none"></div>
				    	<form id="main-contact-form" class="contact-form row" name="contact-form" method="post">
				            <div class="form-group col-md-6">
				                <input type="text" name="name" class="form-control" required="required" placeholder="Name">
				            </div>
				            <div class="form-group col-md-6">
				                <input type="email" name="email" class="form-control" required="required" placeholder="Email">
				            </div>
				            <div class="form-group col-md-12">
				                <input type="text" name="subject" class="form-control" required="required" placeholder="Subject">
				            </div>
				            <div class="form-group col-md-12">
				                <textarea name="message" id="message" required="required" class="form-control" rows="8" placeholder="Your Message Here"></textarea>
				            </div>                        
				            <div class="form-group col-md-12">
				                <input type="submit" name="submit" class="btn btn-primary pull-right" value="Submit">
				            </div>
				        </form>
	    			</div>
	    		</div>
	    		<div class="col-sm-4"><br><br>
	    			<div class="contact-info">
	    				<h2 class="title text-center"><br>Contact Info</h2>
	    				<address>
	    					<p><?=$brand->user_address?></p>
									<p><?=$brand->user_city?>, <?=$brand->user_state?></p>
									<p><?=$brand->user_country?>  <?=$brand->user_postcode?></p>
									<p>+91-<?=$brand->user_mobile?>     <?=$brand->user_phone?></p>
	    				</address>
	    				<div class="social-networks">
	    					<iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d15557.327727373915!2d77.596648!3d12.886365!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0xc2135b2707164863!2sBPL+Medical+Technologies+Private+Limited!5e0!3m2!1sen!2sin!4v1489995217619" width="100%" height="100%" frameborder="0" style="border:0" allowfullscreen></iframe>
	    				</div>
	    			</div>
    			</div>    			
	    	</div>  
    	</div>	
    </div>
    </section>
  <!-- / Contact Form Close -->
  
  <!-- Footer -->
  <footer id="aa-footer">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
        <div class="aa-footer-area">
          <div class="row">
            <div class="col-md-3 col-sm-6 col-xs-12">
             
            </div>
            <div class="col-md-3 col-sm-6 col-xs-12">
              <div class="aa-footer-middle">
                <p style="color:white">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Powered by<a href="https://www.hospytek.com/" target="blank"><img src="assets/sellers/tpl0024/img/logo.png" /></a></p>
              </div>
            </div>
                    
          </div>
        </div>
      </div>
      </div>
    </div>
  </footer>
  <!-- / Footer -->
  
  <div class="modal fade" id="modal-callback" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h2 class="modal-title">Please Fill the form & we will call you back</h2>
                          </div>
                          <div class="modal-body">
						  <form method="post" action="<?php echo base_url()."api";?>" class="form-ajax">
						  <input type="hidden" name="module" value="callback" />
						  <input type="hidden" name="type" value="0" />
						  <div class="row">
						  <div class="col-md-6">
						  	<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Name <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input" placeholder="" name="name" required>
							</div>

							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Phone <span>*</span></label>
		    <input type="phone" class="form-control unicase-form-control text-input"  placeholder=""  name="mobile" required>
							</div>
							
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Email <span>*</span></label>
		    <input type="email" class="form-control unicase-form-control text-input"  placeholder=""  name="email" required>
						   </div>	
							
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Best time to call you <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input"  placeholder="" name="timetocall" required>
						   </div>	


							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Message <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input"  placeholder=""  name="message" required>
						   </div>	


	<div class="form-group">
		<button type="submit" class="btn-upper btn btn-primary checkout-page-button">Submit Request</button>
	</div>
			
						  </div>

						  <div class="col-md-6" style="font-size:12px;">
						   <p><strong>Lets give you a call back to help you buy the most suitable Medical Equipment & Device as per your requirement, We would not only help you choose the most suitable medical device but also help you find the right localised vendor to make your after sale requirements more readily available.</strong></p>
						  <center><img src="assets/images/callback-icon.png" class="img-responsive" style="max-height:175px;" /></center>
						  <p>We at Hospytek hold immense value for every customer or prospective customer and consider it as our utmost duty to provide a solution to all the queries raised by the customers.</p>

<p>We thank you for filling the form and assure you that one of our sales executives would get back to you very soon to understand your problem and provide the most reasonable solution.</p>

<p><strong>We again thank you for your patience and hope you are your experience on our website.</strong></p>
						  </div>
						  
                          </div>
						  </form>
                      </div>
                  </div>
				  </div>
              </div>

 
  
  <!-- jQuery library -->
  <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script> -->
  <script src="https://www.hospytek.com/assets/sellers/tpl0024/js/jquery.min.js"></script>   
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.js"></script>   
  <!-- slick slider -->
  <script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0024/js/slick.js"></script>
  <!-- Price picker slider -->
  <script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0024/js/nouislider.js"></script>
   <!-- mixit slider -->
  <script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0024/js/jquery.mixitup.js"></script>
  <!-- Add fancyBox -->        
  <script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0024/js/jquery.fancybox.pack.js"></script>
  <!-- Custom js -->
  <script src="https://www.hospytek.com/assets/sellers/tpl0024/js/custom.js"></script> 
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>

  </body>
</html>