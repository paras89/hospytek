<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 	
 * 	@author : Hospytek
 * 	20th Sep, 2016
 * 	IT Team
 * 	www.hospytek.com
 * 	http://institution.hospytek.com
 */

class Api extends CI_Controller {
    function __construct() {
        parent::__construct();
		
        $this->load->model('crud_model');
		$this->load->model('apicall');
        $this->load->database();
        $this->load->library('session');
        /* cache control */
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header("Expires: Mon, 26 Jul 2010 05:00:00 GMT");
    }

    //Default function, redirects to logged in user area
    public function index() {
		$module = $this->input->post('module');
		switch($module){
		    case "search_user":
			header('Content-Type: application/json');
			$row_array = array(); $return = array();
			$row = $this->apicall->search_user();
	            if($row->id>0){$row_array['status'] = 1;}else{ $row_array['status'] = 0;}
				$row_array['info'] = $row->user_company.", ".$row->user_address;			
			echo json_encode($row_array);
			break;
				
		    case "get_vars":
			header('Content-Type: application/json');
			$row_array = array(); $return = array();
			$parent = $this->input->post('parent');
			$result = $this->apicall->variations($parent,0,200);
			foreach ($result as $rw)
			{
	            $row_array['id'] = trim($rw->vr_id);
				$row_array['name'] = trim($rw->vr_name);
				$row_array['label'] = trim($rw->vr_name);			
                array_push($return,$row_array);				
			}			
			echo json_encode($return);
			break;


		    case "key_search":
			header('Content-Type: application/json');
			$row_array = array(); $return = array();
			$result = $this->apicall->key_search(1);
			foreach ($result as $rw)
			{
	            $row_array['id'] = trim($rw->cat_id);
				$row_array['label'] = trim($rw->cat_name);			
				$row_array['name'] = trim($rw->cat_name);
				$row_array['slug'] = trim($rw->catslug).'/'.trim($rw->cat_slug);			
				$row_array['type'] = '2';
                array_push($return,$row_array);				
			}


			$result = $this->apicall->key_search(2);
			foreach ($result as $rw)
			{ $arr = array_unique(explode(' ', trim($rw->vr_name).' '.trim($rw->cat_name)));
			$label=''; foreach($arr as $ar){ if($label!=''){ $label .=' ';} $label .=$ar;}
	            $row_array['id'] = trim($rw->vr_id);
				$row_array['label'] = $label;			
				$row_array['name'] = trim($rw->vr_name);
				$row_array['slug'] = trim($rw->catslug).'/'.trim($rw->cat_slug).'/'.trim($rw->vr_slug);			
				$row_array['type'] = '2';
                array_push($return,$row_array);				
			}

/*			$result = $this->apicall->key_search(3);
			foreach ($result as $rw)
			{
	            $row_array['id'] = trim($rw->id);
				$row_array['label'] = trim($rw->user_company);			
				$row_array['name'] = trim($rw->user_company);							
				$row_array['type'] = '3';
                array_push($return,$row_array);				
			}
*/
			$result = $this->apicall->key_search(4);
			foreach ($result as $rw)
			{
	            $row_array['id'] = trim($rw->p_id);
				$row_array['label'] = trim($rw->p_title).' '.trim($rw->vr_name).' - Brand: '.trim($rw->user_company) .' - Model: '. trim($rw->p_model);			
				$row_array['name'] = trim($rw->p_model);	
				$row_array['slug'] = trim($rw->catslug).'/'.trim($rw->cat_slug).'/'.trim($rw->vr_slug).'/'.trim($rw->p_slug);			
				$row_array['type'] = '4';
                array_push($return,$row_array);				
			}
			
			
			echo json_encode($return);
			break;

			// products listing
		    case "products":
			header('Content-Type: application/json');
			$row_array = array(); $return = array();
			$start = $this->input->post('start');
			if($start<=45){
			$result = $this->apicall->products($start, 15);
			foreach ($result as $rw)
			{
				if($rw->p_image=='' || substr($rw->p_image, 0, 7)=='http' || substr($rw->p_image, 0, 9)=='UserFiles'){$img='no-img.jpg';}else{ $img=$rw->p_image;}
	            $row_array['id'] = trim($rw->p_id);
				$row_array['slug'] = $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug;	
				$row_array['label'] = trim($rw->p_title);			
				$row_array['model'] = trim($rw->p_model);			
				$row_array['image'] = $img;
				$row_array['pdf'] = trim($rw->p_brochure);						
				$row_array['brand'] = trim($rw->user_company);
				$row_array['brandslug'] = trim($rw->user_slug);			
				$row_array['cat'] = trim($rw->cat_name);
				$row_array['varname'] = trim($rw->vr_name);			
				$row_array['price'] = trim($rw->p_price);							
				$row_array['mrp'] = trim($rw->p_mrp);							
                array_push($return,$row_array);				
			}}
			echo json_encode($return);
			break;


			// products listing
		    case "inventory":
			header('Content-Type: application/json');
			$row_array = array(); $return = array();
			$start = $this->input->post('start');
			$result = $this->apicall->inventory($start, 6);
			foreach ($result as $rw)
			{
				if($rw->p_image=='' || substr($rw->p_image, 0, 7)=='http' || substr($rw->p_image, 0, 9)=='UserFiles'){$img='no-img.jpg';}else{ $img=$rw->p_image;}
	            $row_array['vid'] = trim($rw->v_id);
				$row_array['id'] = trim($rw->p_id);
				$row_array['label'] = trim($rw->p_title);			
				$row_array['model'] = trim($rw->p_model);			
				$row_array['image'] = $img;
				$row_array['pdf'] = trim($rw->p_brochure);						
				$row_array['brand'] = trim($rw->user_company);			
				$row_array['cat'] = trim($rw->cat_name);
				$row_array['varname'] = trim($rw->vr_name);			
				$row_array['stock'] = trim($rw->v_stock);
				$row_array['price'] = trim($rw->v_price);						
                array_push($return,$row_array);				
			}
			echo json_encode($return);
			break;

			case "order_items":
				header('Content-Type: application/json');
				$row_array = array(); $return = array();
				$id = $this->input->post('query');
				$result = $this->apicall->order_items($id, 0);
				foreach ($result as $rw)
				{
					$row_array['id'] = trim($rw->p_id);
					$row_array['label'] = trim($rw->p_title);			
					$row_array['model'] = trim($rw->p_model);			
					$row_array['brand'] = trim($rw->user_company);			
					$row_array['varname'] = trim($rw->vr_name);			
					$row_array['qty'] = trim($rw->d_qty);			
					array_push($return,$row_array);				
				}
				echo json_encode($return);			
			break;



			/// json variations
		    case "variations":
			header('Content-Type: application/json');
			$row_array = array(); $return = array();
			$result = $this->apicall->cat_search(2);
			foreach ($result as $rw)
			{
			    $row_array['id'] = $rw->vr_id;
				$row_array['label'] = trim($rw->vr_name);			
                array_push($return,$row_array);
			}							
			echo json_encode($return);			
			break;
			

			// products listing
		    case "cart-items":
			header('Content-Type: application/json');
			$row_array = array(); $return = array();						
			if($this->session->userdata('cart_items')!=''){$cart_items = $this->session->userdata('cart_items');}else{ $cart_items = array();}
			//print_r($cart_items);
			foreach($cart_items as $key=>$val){
	       		$rw = $this->apicall->product_view($key);
			    $row_array['id'] = $key;
				$row_array['label'] = trim($rw->p_title);			
				$row_array['model'] = trim($rw->p_model);			
				$row_array['pdf'] = trim($rw->p_brochure);						
				$row_array['brand'] = trim($rw->user_company);			
				$row_array['cat'] = trim($rw->cat_name);
				$row_array['varname'] = trim($rw->vr_name);	
				$row_array['qty'] = $val;			
                array_push($return,$row_array);
			}							
			echo json_encode($return);			
			break;

		    case "adtocart":
			//$this->session->set_userdata('cart_items', '');
			if($this->session->userdata('cart_items')!=''){$cart_items = $this->session->userdata('cart_items');}else{ $cart_items = array();}
			$pid = $this->input->post('query');
			$qty = $this->input->post('qty');
			
			$update=0;
			foreach($cart_items as $key=>$val){
			if($key==$pid){ $cart_items[$pid] =$qty; $update=1; }
			}
			if($update==0){	$cart_items[$pid]=$qty; }
			$this->session->set_userdata('cart_items', $cart_items);
			if($update==1){echo "2";}else{ echo "1";}
			break;

		    case "cart-remove-item":
			if($this->session->userdata('cart_items')!=''){$cart_items = $this->session->userdata('cart_items');}else{ $cart_items = array();}
			$pid = $this->input->post('query');
			$qty = $this->input->post('qty');
			
			foreach($cart_items as $key=>$val){
			if($key==$pid){ unset($cart_items[$pid]); }
			}
			$this->session->set_userdata('cart_items', $cart_items);
			echo "1";
			break;


/// add quote form data
		    case "quote":
            $this->form_validation->set_rules('name', 'Contact Person', 'required|min_length[3]|max_length[55]'); 
            $this->form_validation->set_rules('company', 'Company Name', 'required|min_length[3]|max_length[100]'); 
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email'); 
            $this->form_validation->set_rules('mobile', 'Mobile No.', 'required|alpha_numeric|max_length[10]'); 
            $this->form_validation->set_rules('location', 'Location', 'required'); 
            $this->form_validation->set_rules('qty', 'Required Qty', 'required'); 
            $this->form_validation->set_rules('message', 'Message', 'required|max_length[2500]'); 
			if ($this->form_validation->run() == FALSE){
				echo "0|".validation_errors();
			}else{

			 $oid = $this->apicall->insert_quote();
			 $query = $this->db->query("select * from orders o, users u where o.o_buyer=u.id and o.o_id='$oid'");
			 $order = $query->row();
						 $qty = $this->input->post('qty');
						$name = $this->input->post('name');
						$pname = $this->input->post('pname');
						$location = $this->input->post('location');
						$mobile = $this->input->post('mobile');
						$email = $this->input->post('email');
						$type = $this->input->post('type'); if($type>0){}else{ $type=1;}
						$pid = $this->input->post('pid');
if($pid>0){
  $prod = $this->apicall->product_view($pid);
  $catid = $prod->cat_parent;
  $catsid = $prod->cat_sid;
  $vrid = $prod->p_vrid;		  
}else{		  
						$catid = $this->input->post('catid');
						$catsid = $this->input->post('catsid');
						$vrid = $this->input->post('vrid');	
}						
						$desc = $this->input->post('message');	  
						$query = $this->db->query("select * from category where cat_id='$catsid'");
						$catrow = $query->row();

						 $query = $this->db->query("select * from variations where vr_id='$vrid'");
						 $vrow = $query->row();
						 $pname = $catrow->cat_name." ".$vrow->vr_name;
						
						$msg="New Quote Request From:".$name." Location: ".$location."  Mobile: ".$mobile." for ".$pname.", logon to send your proposal. Best Wishes www.hospytek.com";						
						$ok=sendsms("9805259651", $msg, 'textart');	

						if($type==1){
						 $query = $this->db->query("select * from users u, vendors v, product p, odetails d where v.v_pid=p.p_id and d.o_id='$oid' and v.v_vendor=u.id and p.cat_sid=d.d_catsid and  p.p_vrid=d.d_vrid group by u.id order by u.id desc");
						 }else{
						 $query = $this->db->query("select * from users u, vendors v, product p, odetails d where v.v_pid=p.p_id and d.o_id='$oid' and v.v_vendor=u.id and p.p_id=d.d_pid group by u.id order by u.id desc");		 
						 }
						$rows = $query->result();
						$today = date("Y-m-d H:i:s");
						foreach($rows as $rw){
						$this->db->query("insert into vendor_leads(vl_oid, vl_uid, vl_date) values('$oid', '$rw->id', '$today')");

//	if($rw->user_email=='msewmanan@gmail.com'){ /// remove in production
			$vurl=base64_encode("vendor/carts/".$oid);
			$vmail = base64_encode($rw->user_email);
			$longUrl= "http://www.hospytek.com/account/signin/".$vurl."/".$vmail;
			$url = $this->hospytek->shorten_url($longUrl);
				$msg="New Inquiry From:".$name." Location: ".$location."  for ".$pname.", logon to $url to send your proposal. Best Wishes www.hospytek.com";												
				$ok=sendsms($rw->user_mobile, $msg, 'textart');	
				$link='<font style="font-family: Arial, Helvatica, sans-serif; font-size: 14px; font-weight: bold"><a href="'.$url.'" target="_blank" style="padding: 5px; background-color: #FF0000; outline: none; color: #FFFFFF; text-decoration: none">Clik Here</a></font>';
				
				$message="Hi <strong>".$rw->user_name.",</strong><br>You have recieved a new inquiry for <strong>".$pname."</strong> - Qty: <strong>".$qty." Unit(s)</strong>.<br><strong>Ref No.:</strong> ".$order->o_invoice."<br><strong>Name:</strong> ".$name."<br><strong>Location:</strong> ".$location."<br><strong>Phone:</strong> ".substr($mobile, 0,3)."*********<br><strong>Email:</strong> ".substr($email, 0, 3)."***********<br><br> <strong>Message:</strong><br> ".nl2br(strip_tags($desc))."  <br><br>".$link." to send your proposal.<br><br><strong>Best Wishes</strong><br>www.hospytek.com";
				//$this->hospytek->send_email($rw->user_email, "New inquiry $order->o_invoice for $pname", $message);														
				$this->apicall->insert_mail($rw->user_email, "New inquiry $order->o_invoice for $pname", $message);
				
//}				
$i++;
$vendors .= '<font style="font-family:Arial, Helvetica, sans-serif; font-size:18px; color:#004b8d; line-height:32px;">'.$i.'. '.$rw->user_company.'</font><br>'.ucwords($rw->user_city).' '.$rw->user_state."<br><strong>Phone:</strong> ".$rw->user_phone." &nbsp; <strong>Mobile:</strong> ".$rw->user_mobile."<br><br>";

						}

/// send to customer
				$message="Hi <strong>".$name.",</strong><br>Thank you for posting your medical device requirement <strong>".$order->o_invoice."</strong> on hospytek.com.<br><br> Your inquiry for <strong>".$pname."</strong> - Qty: <strong>".$qty." Unit(s)</strong> has been sent to <strong>".$i." matching vendors</strong> as below:<br><br> ".$vendors." <br><br><strong>Best Wishes</strong><br>www.hospytek.com";
				//$this->hospytek->send_email($email, "Thank you for your inquiry $order->o_invoice for $pname", $message);													
				$this->apicall->insert_mail($email, "Thank you for your inquiry $order->o_invoice for $pname", $message);


						$msg="Dear ".$name.", Thank you for contacting hospytek.com, we have sent your quote request to all vendors near to you. You will get all competitive quotes in next couple hrs. Best Wishes www.hospytek.com";
						$ok=sendsms($mobile, $msg, 'textart');

//// end of customer alert
						
				echo "1";
			}	

			break;

		    case "callback":
            $this->form_validation->set_rules('name', 'Contact Person', 'required|min_length[3]|max_length[55]'); 
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email'); 
            $this->form_validation->set_rules('mobile', 'Mobile No.', 'required|alpha_numeric|max_length[10]'); 
            $this->form_validation->set_rules('message', 'Message', 'required|max_length[2500]'); 
			if ($this->form_validation->run() == FALSE){
				echo "0|".validation_errors();
			}else{
			$this->apicall->insert_callback();
						$name = $this->input->post('name'); $email = $this->input->post('email');
						$mobile = $this->input->post('mobile');
						$msg="Dear ".$name.", Thank you for contacting hospytek.com, one of our representative will contact you very shortly. Best Wishes www.hospytek.com";
						$ok=sendsms($mobile, $msg, 'textart');
						$msg="New Callback Request From:".$name." Email: ".$email."  Mobile: ".$mobile.". Support Team www.hospytek.com";
						$ok=sendsms("9805259651", $msg, 'textart');						
						
				echo "1";
			}	
			break;

		    case "signup":
            $this->form_validation->set_rules('name', 'Contact Person', 'required|min_length[3]|max_length[55]'); 
			$this->form_validation->set_rules('company', 'Company Name', 'required|min_length[3]|max_length[75]'); 
            $this->form_validation->set_rules('email', 'Email', 'required|valid_email'); 
            $this->form_validation->set_rules('mobile', 'Mobile No.', 'required|alpha_numeric|max_length[10]'); 
            $this->form_validation->set_rules('address', 'Address', 'required|max_length[100]'); 
            $this->form_validation->set_rules('city', 'City', 'required|max_length[100]'); 
            $this->form_validation->set_rules('state', 'State', 'required|max_length[100]'); 
            $this->form_validation->set_rules('country', 'Country', 'required|max_length[100]'); 
			
			if ($this->form_validation->run() == FALSE){
				echo "0|".validation_errors();
			}else{
				
				 $name = $this->input->post('name');
				 $company = $this->input->post('company');
				 $email = $this->input->post('email');
				 $mobile = $this->input->post('mobile');
				 $address = $this->input->post('address');
				 $city = $this->input->post('city');
				 $country = $this->input->post('country');
				 $state = $this->input->post('state');
				 $zip = $this->input->post('zip');
				 $type = $this->input->post('type');
				$query = $this->db->query("select id from users where user_email='$email' or user_mobile='$mobile'");
				 if($query->num_rows()==0){ 		 
					$this->apicall->insert_signup();
						$name = $this->input->post('name'); $email = $this->input->post('email'); $mobile = $this->input->post('mobile');
						$msg="Dear ".$name.", Thank you for joining hospytek.com, Please login to your account using your email address. Best Wishes www.hospytek.com";
						$ok=sendsms($mobile, $msg, 'textart');
						$msg="New Signup From:".$name." Email: ".$email."  Mobile: ".$mobile.". Support Team www.hospytek.com";
						$ok=sendsms("9805259651", $msg, 'textart');						
						$tparr= array('', 'Vendor', 'Institution', 'Brand');	
				$message="Dear <strong>".$name."</strong>,<br>
We are proud to have <strong>".$company."</strong> onboard Hospytek!<br><br>
Before getting started we would love to tell you about the benefits of signing in as ".$tparr[$type].".<br><br>";
if($type==1){

$message .="<strong>Benefits for Vendor:</strong><br>
1) Get your very own Automated Quotation Generator<br>
2) Get authentic leads and reply with a pdf quotation in 3 steps.<br>
3) Get Connected directly with Manufacturers<br>
4) Largest Product Library for Medical Equipment<br>
5) Use our Large Product Library to Build your Virtual Inventory<br>
6) Get connected with vendors in 2 steps<br>
7) Automated Logistic Support (in Premium Package Coming Soon)<br>
8) Automated Service and Installation Support (In Premium Package Coming Soon)<br>
9) Automated Marketing Module (in Premium Package- On Usage Basis) <br>
10) Get online Loan support for Bulk/Small Medical Device Procurement. (in Premium Package Coming Soon)<br>and many more..<br><br>";
}


if($type==2){

$message .="<strong>Benefits for Institution:</strong><br>
1) Largest Product Library for Medical Devices<br>
2) Multiple Options under One Umbrella<br>
3) Directing Bridging to Premium Vendor/Manufacturer<br>
4) Logistics Support and online tracking of shipment (in Premium Package, Coming Soon)<br>
5) You Choose your Product, Hospytek gives you the Top Rated Vendors (in Premium Package, Coming Soon)<br>
6) Generate Indent Manager and Get Transparent Idea of What is required in your Institution and by whom? (in Premium Package, Coming Soon)<br>and many more..<br><br>";
}

if($type==3){

$message .="<strong>Benefits for Brands:</strong><br>
1) Management of Channel Partners through Daily analysis of quotations sent (in Premium Package)<br>
2) Automated Logistic support (in Premium Package)<br>
3) Get your very own Automated Quotation Generator<br>
4) Get authentic leads and reply with a pdf quotation in 3 steps.<br>
5) Get online Loan support for Bulk/Small Medical Device Production. (in Premium Package Coming Soon)<br>
6) Automated Marketing Module (in Premium Package).<br>
7) Product Advertisement to Vendors/End Users (in Premium Package)<br>and many more..<br><br>";
}



if($type==1 || $type==3){
			$vurl=base64_encode("vendor");
			$vmail = base64_encode($email);
			$longUrl= "http://www.hospytek.com/account/signin/".$vurl."/".$vmail;
			$url = $this->hospytek->shorten_url($longUrl);		
$message .="Currently since we want to whole heartidly Thank you for joining us, We would be providing you with 20 free leads as a freemium account. A freemium account would give you access to our Large product Library. You can check your account by <a href='".$url."'>clicking here</a><br><br>You can update your account to a Premium Account anytime by choosing the following package:<br><br>

<center> <img src='http://www.hospytek.com/assets/package.jpg'></center><br><br>
Please update your profile by providing us with soft copy of your products to info@hospytek.com. <br><br>

Enter more products to get more authentic leads. <br>
Get a online rating for your quote.<br><br>";
}

if($type==2){
			$vurl=base64_encode("institution");
			$vmail = base64_encode($email);
			$longUrl= "http://www.hospytek.com/account/signin/".$vurl."/".$vmail;
			$url = $this->hospytek->shorten_url($longUrl);			
$message .="Currently since we want to whole heartidly Thank you for joining us, We would be providing you with a freemium account. A freemium account would give you access to our Large product Library. You can check your account by <a href='".$url."'>clicking here</a><br><br>";
}

$message .="We look forward in hearing from you soon.<br><br>  Regards<br>www.hospytek.com";


				//$this->hospytek->send_email($email, "Welcome to hospytek.com - Medical Equipment Marketplace", $message);													
				$this->apicall->insert_mail($email, "Welcome to hospytek.com - Medical Equipment Marketplace", $message);
						
						
						echo "1";
				}else{ echo "0|Account already exist.";}
			}	
			break;
			
			case "otp":
				$resp = array();
				$email = $this->input->post('email');
				//$email = $this->session->userdata('email');
				$otp = rand(11111,999999); 
				$user = $this->apicall->user_byemail($email);
				if($user->id>0 && $email!=''){	
				$this->session->set_userdata('email', $email);
				$this->session->set_userdata('otp', $otp);			
				$msg="Hi ".$user->user_name.", Your one time password (OTP) to login to your hospytek account is: ".$otp."  valid for next 5 minutes. Best Wishes www.hospytek.com";
				$ok=sendsms($user->user_mobile, $msg, 'textart'); $mg="OTP has been sent to your mobile/email"; $status='success';
				$message="Hi <strong>".$user->user_name.",</strong><br>Your one time password (OTP) to login to your hospytek account is: <strong> ".$otp."</strong>  valid for next 5 minutes. <br><br><strong>Best Wishes</strong><br>www.hospytek.com";
				//$this->hospytek->send_email($email, "Your OTP for Hospytek Account", $message);													
				$this->apicall->insert_mail($email, "Your OTP for Hospytek Account", $message);
				}else{
				$mg="Email Address is not registered."; $status='failed';
				}
				$resp['msg']=$mg;
				$resp['status']=$status;
				echo json_encode($resp); exit;
			break;
			
			
		}	

    }

}
