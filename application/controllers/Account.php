<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 	
 * 	@author : Hospytek
 * 	20th Sep, 2016
 * 	IT Team
 * 	www.hospytek.com
 * 	http://institution.hospytek.com
 */

class Account extends CI_Controller {

    function __construct() {
        parent::__construct();
        $this->load->model('crud_model');
		$this->load->model('apicall');
        $this->load->database();
        $this->load->library('session');
        /* cache control */
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header("Expires: Mon, 26 Jul 2010 05:00:00 GMT");				
		if($this->session->userdata('user_uid')>0){
		$urw = $this->apicall->logged_user();
		}else{
		$uinfo = $this->input->cookie('uinfo');
		if($uinfo>0){	$urw = $this->apicall->get_user($uinfo); }
		}
		$menu = array(); $i=0;
		foreach($this->config->item('topcats') as $key=>$val){
			if($val==''){ $val=0; }
			$row = $this->apicall->master_categories($val);
			$row_array = array(); $return = array();						
			foreach($row as $rw){
			    $row_array['cat_id'] = $rw->cat_id;
				$row_array['cat_name'] = $rw->cat_name;			
				$row_array['cat_slug'] = $rw->cat_slug;			
				$row_array['subcats'] = $this->apicall->categories(1,$rw->cat_id,0,500);
                array_push($return,$row_array);
			}
			$menu[$i] = $return; $i++; 								
		}	

			$this->data = array('urw'=> $urw, 'topcats'=> $menu);
		
    }

    //Default function, redirects to logged in user area
    public function index() {
		if($this->session->userdata('user_utype')==1){
        if ($this->session->userdata('user_type') == 1)
            redirect(base_url(), 'refresh');

        if ($this->session->userdata('user_type') == 2)
            redirect(base_url() . 'user', 'refresh');
		}else{
        $this->load->view('landing/index');
		}
    }

    //Ajax login function 
    function ajax_login(){
        $response = array();
        //Recieving post input of email, password from ajax request
        $email =  $this->session->userdata('email');
        //$password = sha1($_POST["password"]);
		$password = $_POST["password"]; $otp = $_POST["otp"];
        $response['submitted_data'] = $_POST;

        //Validating login
		$response['msg'] = "<span style='color:red;'>Login Failed, Try again.</span>";
        $login_status = $this->validate_login($email, $password, $otp);
        $response['status'] = $login_status;
        if ($login_status == 'success') {
            if($this->session->userdata('user_utype')==1){
			if($this->session->userdata('user_type')==1){ if($this->session->userdata('vurl')==''){$response['redirect_url'] = base_url().'vendor';}else{ base_url().$response['redirect_url'] = base_url().$this->session->userdata('vurl'); }}
			if($this->session->userdata('user_type')==2){ if($this->session->userdata('vurl')==''){$response['redirect_url'] = base_url().'vendor/user';}else{   $response['redirect_url']= base_url().'vendor/user/'.str_replace('vendor/', '', $this->session->userdata('vurl')); }}
			}else{
			
			if($this->session->userdata('user_type')==1){ if($this->session->userdata('vurl')==''){$response['redirect_url'] = base_url().'institution';}else{ $response['redirect_url'] = base_url().$this->session->userdata('vurl'); }}
			if($this->session->userdata('user_type')==2){ if($this->session->userdata('vurl')==''){$response['redirect_url'] = base_url().'institution/indent';}else{   $response['redirect_url']= base_url().'institution/indent/'.str_replace('institution/', '', $this->session->userdata('vurl')); }}						
			
			}
			
						$response['redirect_url'] = urlencode($response['redirect_url']);
						$response['msg'] = "<span style='color:green;'>Login successful...redirecting.</span>";
        }

        //Replying ajax request with validation response
        echo json_encode($response);
    }

    //Validating login from ajax request
    function validate_login($email = '', $password = '', $otp = '') {
        $credential = array('user_id' => $email, 'user_pass' => $password, 'user_status' => 'Active');
        // Checking login credential for users
		$sotp = $this->session->userdata('otp');
		if($otp!='' && $this->session->userdata('otp')!=''){
		 $email=$this->session->userdata('email');
         $query = $this->db->query("select * from users where user_id='$email' and user_status='Active' and $otp=$sotp and (user_type<=3 or id in (select id from users where user_type=6 and user_owner in (select id from users where user_type<=3 and user_status='Active')))");		
		}else{
         $query = $this->db->query("select * from users where user_id='$email' and user_pass='$password' and user_status='Active' and (user_type<=3 or id in (select id from users where user_type=6 and user_owner in (select id from users where user_type<=3 and user_status='Active')))");		
		}
        if ($query->num_rows() > 0) {
            $row = $query->row();
            $this->session->set_userdata('user_login', '1');
			if($row->user_owner>0){
			$orow = $this->apicall->get_user($row->user_owner);
			$this->session->set_userdata('user_utype', $orow->user_type); // fixed for Vendors
			}else{
			$this->session->set_userdata('user_utype', $row->user_type); // fixed for Vendors
			}
            $this->session->set_userdata('user_uid', $row->id);
            $this->session->set_userdata('user_id', $row->user_id);
			$this->session->set_userdata('user_owner', $row->user_owner);
            $this->session->set_userdata('user_company', $row->user_company);
            $this->session->set_userdata('user_name', $row->user_name);
            if($row->user_owner==0){$this->session->set_userdata('user_type', '1'); }else{ $this->session->set_userdata('user_type', '2');}
            return 'success';
        }
        return 'invalid';
    }

    /*     * *DEFAULT NOR FOUND PAGE**** */

    function four_zero_four() {
        $this->load->view('four_zero_four');
    }

    // PASSWORD RESET BY EMAIL
    function forgot_password()
    {
        $this->load->view('backend/forgot_password');
    }

    function ajax_forgot_password()
    {
        $resp                   = array();
        $resp['status']         = 'false';
        $email                  = $_POST["email"];
        $reset_account_type     = '';
        //resetting user password here
        $new_password           =   substr( md5( rand(100000000,20000000000) ) , 0,7);

        // Checking credential for admin
        $query = $this->db->get_where('admin' , array('email' => $email));
        if ($query->num_rows() > 0) 
        {
            $reset_account_type     =   'admin';
            $this->db->where('email' , $email);
            $this->db->update('admin' , array('password' => sha1($new_password)));
            $resp['status']         = 'true';
        }
        // Checking credential for student
        $query = $this->db->get_where('student' , array('email' => $email));
        if ($query->num_rows() > 0) 
        {
            $reset_account_type     =   'student';
            $this->db->where('email' , $email);
            $this->db->update('student' , array('password' => sha1($new_password)));
            $resp['status']         = 'true';
        }
        // Checking credential for teacher
        $query = $this->db->get_where('teacher' , array('email' => $email));
        if ($query->num_rows() > 0) 
        {
            $reset_account_type     =   'teacher';
            $this->db->where('email' , $email);
            $this->db->update('teacher' , array('password' => sha1($new_password)));
            $resp['status']         = 'true';
        }
        // Checking credential for parent
        $query = $this->db->get_where('parent' , array('email' => $email));
        if ($query->num_rows() > 0) 
        {
            $reset_account_type     =   'parent';
            $this->db->where('email' , $email);
            $this->db->update('parent' , array('password' => sha1($new_password)));
            $resp['status']         = 'true';
        }

        // send new password to user email  
        $this->email_model->password_reset_email($new_password , $reset_account_type , $email);

        $resp['submitted_data'] = $_POST;

        echo json_encode($resp);
    }


    function signin(){
	    $data = $this->data;
		//echo "URL = ".$vurl." - Email = ".$email;		

		if($this->uri->segment(3)!='' && $this->uri->segment(4)!=''){ 
						$vurl = base64_decode($this->uri->segment(3));
						$email = base64_decode($this->uri->segment(4));
						$user = $this->apicall->user_byemail($email);		
						$this->session->set_userdata('vurl', $vurl); $this->session->set_userdata('email', $email);
						redirect(base_url() . 'account/signin', 'refresh');
		}else{
						$vurl = $this->session->userdata('vurl');
						$email = $this->session->userdata('email');
						$user = $this->apicall->user_byemail($email);
		}
		
		
		$data['vurl'] = $vurl; $data['email'] = $email; $data['otp'] = $this->session->userdata('otp');
		$data['meta_title'] = "Login to hospytek account";	
		$data['meta_desc'] = "Login to hospytek account to manage your medical equipment & device leads or quotes in india";	
		$this->load->vars($data);
		$this->load->view('signin');
    }



    /*     * *****LOGOUT FUNCTION ****** */

    function signout() {
		delete_cookie('uinfo'); 	
        $this->session->sess_destroy();
        $this->session->set_flashdata('logout_notification', 'logged_out');
        redirect(base_url().'account/signin', 'refresh');
    }

}
