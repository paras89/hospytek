<?php defined('BASEPATH') OR exit('No direct script access allowed');
class Hospytek
{
	public function __construct()
    {
        $this->CI =& get_instance();
    }


    public function send_email($to, $subject, $message){
$body = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Hospytek</title>
</head>
<body>
<table class="devicewidth" align="center" cellspacing="0" cellpadding="0" border="0" bgcolor="#ffffff" width="598">
  <tbody>
  </tr><tr>
    <td style="border-left:1px solid #dcdddf; border-right:1px solid #dcdddf;border-top:1px solid #dcdddf; border-bottom:1px solid #dcdddf;" class="devicewidth2" align="center" width="100%"><table st-sortable="devicewidth" cellspacing="0" cellpadding="0" border="0">
        <tbody>
          <tr>
            <td bgcolor="#dcdddf" width="100%"><table class="devicewidth" cellspacing="0" cellpadding="0" border="0" width="598">
                <tbody>
                  <tr>
                    <td style="max-width:598px; display:block;" align="left" bgcolor="#dcdddf" valign="top"><table class="devicewidth" cellspacing="0" cellpadding="0" border="0" width="598">
                        <tbody>
                          <tr>
                            <td style=" padding-top:0px; padding-bottom:0px; padding-left:20px; line-height:0px;" height="59" align="left" width="137" valign="middle"><a href="http://www.hospytek.com" target="_blank" style="outline:none; text-decoration:none;"><img src="http://www.hospytek.com/mailer/logo-hospytek.png" alt="Hospytek" style=" border:none; outline:none; text-decoration:none;" border="0" width="123"></a></td>
							<td style="text-align:right; padding-right:10px;"><span style="font-family:Arial, Helvetica, sans-serif; font-size:20px; color:#FF0000; line-height:24px;">www.hospytek.com</span><br /><span style="font-family:Arial, Helvetica, sans-serif; font-size:12px; line-height:24px; color:#666666">Medical Equipment Marketplace</span></td>
                          </tr>
                        </tbody>
                      </table></td>
                  </tr>
                </tbody>
              </table></td>
          </tr>
                   <tr>
  <td style=" line-height:0px;" align="center" valign="top" ><img src="http://www.hospytek.com/mailer/mailer-banner.jpg" alt="" class="banner" style="width:100%; min-width:100%;" border="0" width="598"></td>
  </tr>
				  <tr>
                    <td align="left" valign="top" class="float" style="padding-top:20px; padding-bottom:0px; padding-left:20px; padding-right:20px; font-family:Arial, Helvetica, sans-serif; font-size:12px; color:#666666;">'.$message.'</td>
                  </tr>
 <tr>
                    <td align="left" valign="top" class="float" style="padding-top:20px; padding-bottom:0px; padding-left:20px; padding-right:20px; font-family:Arial, Helvetica, sans-serif; font-size:14px; color:#231f20;">&nbsp;</td>
                  </tr>				  
</table>
</td>
                                                </tr>
                                               </tbody>
          <tr>
    <td style="padding-left:20px; padding-right:20px;"><table width="100%" cellpadding="0" cellspacing="0" border="0" align="left">
        <tbody>
        <tr>
            <td align="left" style="line-height:0px !important;padding-left:0px; padding-top:5px;"><table width="100%" border="0" cellspacing="0" cellpadding="0">
  <tbody>
    <tr>
      <td width="13%" align="left" valign="top" style="line-height:0px;"><img src="http://www.hospytek.com/mailer/logo-hospytek.png" width="120" alt=""/></td>
      <td width="87%" align="left" valign="top" style="padding-left:45px; padding-top:10px; text-align:right"><font style="font-family: Arial, sans-serif; font-size:11px; line-height:14px !important; color:#000;">SCO 10-11, Above ICICI Bank, Ambala-Chd. Highway, Zirakpur, Punjab, India<br /><strong>Phone:</strong> +91 (1762) 530604, 
+91 (1762) 530605  <strong>Email:</strong> info@hospytek.com </font></td>
    </tr>
  </tbody>
</table>
</td>
  </tr>
       <tr>
            <td align="left" style="line-height:0px !important;padding-left:5px; padding-right:5px; padding-top:35px; padding-bottom:0px; "><font style="font-family:Arial, Helvetica, sans-serif; font-size:11px; line-height:16px !important; "><a href="http://t.mailer.hdfcbank.net/r/?id=h9f9807e9,828ba157,82ab0934" target="_blank" style="outline: none; text-decoration: none; color:#666666;"><strong>*T&C </strong> apply :</a></font></td>
          </tr>  <tr>
            <td align="left" style="font-family:Arial; font-size:11px; line-height:16px; padding-left:5px; padding-right:5px; padding-bottom:4px; color:#666666;">All the information mentioned in proposal is sent by vendors and hospytek does not give any guarantee of its correctness. Please correlate accordingly with participants.</td>
          </tr>
         
        </tbody>
      </table></td>
  </tr>
      </table></td>
  </tr>
   
  
    </tbody>
  </table>
</body></html>';

/*	$config = Array(
		'mailtype' => 'html',
        'protocol' => 'smtp',
        'smtp_host' => 'ssl://smtp.mailgun.org',
        'smtp_port' => 465,
        'smtp_user' => 'postmaster@hospytek.com',
        'smtp_pass' => '4c863b10c413381026bfec9af82c6e50',
        );

     $this->CI->load->library('email', $config); 
     $this->CI->email->from("postmaster@hospytek.com", "Hospytek Support");
     $this->CI->email->reply_to("support@hospytek.com", "Hospytek Support");
     $this->CI->email->to($to);
     $this->CI->email->subject($subject);
     $this->CI->email->message($body);
     $this->CI->email->set_newline("\r\n"); // require this, otherwise sending via gmail times out
     $this->CI->email->send();
*/


/*	$config = Array(
		'mailtype' => 'html',
        'protocol' => 'smtp',
        'smtp_host' => 'ssl://smtp.mailgun.org',
        'smtp_port' => 465,
        'smtp_user' => 'postmaster@hospytek.com',
        'smtp_pass' => '4c863b10c413381026bfec9af82c6e50',
        );

     $this->CI->load->library('email', $config); 
     $this->CI->email->from("postmaster@hospytek.com", "Hospytek Support");
     $this->CI->email->reply_to("support@hospytek.com", "Hospytek Support");
     $this->CI->email->to($to);
     $this->CI->email->subject($subject);
     $this->CI->email->message($body);
     $this->CI->email->set_newline("\r\n"); // require this, otherwise sending via gmail times out
     $this->CI->email->send();
*/
////*********NEW
$config = Array(
		'mailtype' => 'html',
        'protocol' => 'smtp',
        'smtp_host' => 'ssl://email-smtp.eu-west-1.amazonaws.com',
        'smtp_port' => 465,
        'smtp_user' => 'AKIAIVDVSWMD4MTHL7BQ',
        'smtp_pass' => 'Ah6DJM0DPJIK9O3qGbtCc3iSH45rq6qOKCe/GpcMt2Mf',
        );

     $this->CI->load->library('email', $config); 
     $this->CI->email->from("webmaster@hospytek.com", "Hospytek Support");
     $this->CI->email->reply_to("support@hospytek.com", "Hospytek Support");
     $this->CI->email->to($to);
     $this->CI->email->subject($subject);
     $this->CI->email->message($body);
     $this->CI->email->set_newline("\r\n"); // require this, otherwise sending via gmail times out
     $this->CI->email->send();
////*/


/*$mail = new PHPMailer();
$mail->IsSMTP(true);
$mail->SMTPAuth = true;
$mail->Mailer = "smtp";
$mail->SMTPSecure = 'ssl';
$mail->SMTPDebug  = false; 
$mail->Port = 465;
$mail->Host= "email-smtp.eu-west-1.amazonaws.com";
$mail->Username = "AKIAIVDVSWMD4MTHL7BQ";  // SMTP username (Amazon Access Key)
$mail->Password = "Ah6DJM0DPJIK9O3qGbtCc3iSH45rq6qOKCe/GpcMt2Mf";  // SMTP Password (Amazon Secret Key)


$mail->setFrom('webmaster@hospytek.com', 'Hospytek Support');
$mail->addAddress($to, 'Hospytek User');     // Add a recipient
$mail->addReplyTo('support@hospytek.com', 'Hospytek Support');
$mail->isHTML(true);                                  // Set email format to HTML
$mail->Subject = $subject;
$mail->Body    = $body;

if(!$mail->send()) {
    echo 'Message could not be sent.';
    echo 'Mailer Error: ' . $mail->ErrorInfo;
} else {
    echo "Verification code has been sent to your email address "; 
}
*/
	 
}
public function shorten_url($longUrl){
			$apiKey = 'AIzaSyBEplmAj-47lcT_HeyNBt0hVYDoIoYjwDE';
			$postData = array('longUrl' => $longUrl);
			$jsonData = json_encode($postData);
			$curlObj = curl_init( 'https://www.googleapis.com/urlshortener/v1/url?key=AIzaSyBEplmAj-47lcT_HeyNBt0hVYDoIoYjwDE' );
			curl_setopt($curlObj, CURLOPT_URL, 'https://www.googleapis.com/urlshortener/v1/url?key=AIzaSyBEplmAj-47lcT_HeyNBt0hVYDoIoYjwDE');
			curl_setopt($curlObj, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($curlObj, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($curlObj, CURLOPT_HEADER, 0);
			curl_setopt($curlObj, CURLOPT_HTTPHEADER, array('Content-type:application/json'));
			curl_setopt($curlObj, CURLOPT_POST, 1);
			curl_setopt($curlObj, CURLOPT_POSTFIELDS, $jsonData);
			$response = curl_exec($curlObj);
			// Change the response json string to object
			$json = json_decode($response);
			curl_close($curlObj);
			return $json->id;
			
}

}