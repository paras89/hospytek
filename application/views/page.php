<?php $this->view('common/header'); ?>
<!-- ============================================== HEADER : END ============================================== -->
<div class="breadcrumb">
	<div class="container">
		<div class="breadcrumb-inner">
			<ul class="list-inline list-unstyled">
  <li><a href="<?php echo base_url();?>">Home</a>  
  		<i class="fa fa-angle-double-right"></i> <?=trim($item->p_title)?>
</li>
			</ul>
		</div><!-- /.breadcrumb-inner -->
	</div><!-- /.container -->
</div><!-- /.breadcrumb -->

<div class="body-content">
	<div class="container">
	  <?=trim($item->p_desc)?>
</div>
</div>
<div style="clear:both">&nbsp;</div>
<?php $this->view('common/footer'); ?>