<?php
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 	
 * 	@author : Hospytek
 * 	20th Sep, 2016
 * 	IT Team
 * 	www.hospytek.com
 * 	http://vendor.hospytek.com
 */

class Products extends CI_Controller {
    function __construct() {
        parent::__construct();
		
        $this->load->model('crud_model');
		$this->load->model('apicall');
        $this->load->database();
        $this->load->library('session');
        /* cache control */
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header("Expires: Mon, 26 Jul 2010 05:00:00 GMT");				
		if($this->session->userdata('user_uid')>0){
		$urw = $this->apicall->logged_user();
		}
		$menu = array(); $i=0;
		foreach($this->config->item('topcats') as $key=>$val){
			if($val==''){ $val=0; }
//			echo $val."KUMAR";
			$row = $this->apicall->master_categories($val);
			$row_array = array(); $return = array();						
			foreach($row as $rw){
			    $row_array['cat_id'] = $rw->cat_id;
				$row_array['cat_name'] = $rw->cat_name;			
				$row_array['cat_slug'] = $rw->cat_slug;			
				$row_array['subcats'] = $this->apicall->categories(1,$rw->cat_id,0,500);
                array_push($return,$row_array);
			}
			$menu[$i] = $return; $i++; 								
		}	

			$this->data = array('urw'=> $urw, 'topcats'=> $menu);
		
    }

    //Default function, redirects to logged in user area
    public function index() {
        echo $uri1 = $this->uri->segment(1);
        $data = $this->data;
		$newitems = array();
		foreach($this->config->item('newcats') as $key=>$val){ 
		$newitems[$key]=$this->apicall->homepage_products('new', $val, 8);
		}
		$data['newitems'] = $newitems;
		$data['new']=$this->apicall->homepage_products('new',0, 8);
		$data['featured'] = $this->apicall->homepage_products('featured', 0, 8);
		$data['onsale'] = $this->apicall->homepage_products('onsale', 0, 8);
		$this->load->vars($data);
        $this->load->view('home');
    }
}