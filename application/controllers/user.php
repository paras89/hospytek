<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

/* 	
 * 	@author : Hospytek
 * 	20th Sep, 2016
 * 	IT Team
 * 	www.hospytek.com
 * 	http://vendor.hospytek.com
 */

class User extends CI_Controller {
    function __construct() {
        parent::__construct();
		
        $this->load->model('crud_model');
		$this->load->model('apicall');
        $this->load->database();
        $this->load->library('session');
        /* cache control */
        $this->output->set_header('Last-Modified: ' . gmdate("D, d M Y H:i:s") . ' GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0');
        $this->output->set_header('Pragma: no-cache');
        $this->output->set_header("Expires: Mon, 26 Jul 2010 05:00:00 GMT");
				
		if($this->session->userdata('user_utype')==1){
		$urw = $this->apicall->logged_user();
		$qrcount = $this->apicall->count_order_byuser('7', $urw->id);
		$prcount = $this->apicall->count_order_byuser('5', $urw->id);		
		$this->data = array('urw'=> $urw, 'qrcount'=> $qrcount, 'prcount'=> $prcount);
        if ($this->session->userdata('user_type') == 1)
            redirect(base_url(), 'refresh');		
			
		}else{
			redirect(base_url().'login', 'refresh');
		}
		
    }

    //Default function, redirects to logged in user area
    public function index() {

        if ($this->session->userdata('user_type') != 2)
            redirect(base_url().'login', 'refresh');

        $data = $this->data;
		$this->load->vars($data);
        $this->load->view('index');
    }
/****************** ACCOUNT MODIFICATIONS ******/

    function account(){
	    $data = $this->data;
		$this->load->vars($data);	
		$this->load->view('user/account');
    }

    function change_profile(){

            $this->form_validation->set_rules('name', 'Contact Person', 'required|min_length[5]|max_length[25]'); 
            $this->form_validation->set_rules('company', 'Company Name', 'required|min_length[5]|max_length[100]'); 
//            $this->form_validation->set_rules('email', 'Email', 'required|valid_email'); 
//            $this->form_validation->set_rules('mobile', 'Mobile No.', 'required|alpha_numeric|max_length[10]'); 
            $this->form_validation->set_rules('taxno', 'TAX/VAT No.', 'required'); 
            $this->form_validation->set_rules('licno', 'Licence No.', 'required'); 
            $this->form_validation->set_rules('address', 'Address', 'required|max_length[100]'); 
            $this->form_validation->set_rules('city', 'City', 'required|max_length[50]'); 
            $this->form_validation->set_rules('state', 'State', 'required|max_length[50]'); 
            $this->form_validation->set_rules('country', 'Country', 'required|max_length[70]'); 
            $this->form_validation->set_rules('zip', 'Zip Code', 'required|max_length[6]'); 
			
			if ($this->form_validation->run() == FALSE){
				echo "0|".validation_errors();
							
			}else{
			$this->apicall->change_profile();
				echo "1";
			}	
    }





    function settings(){
		$this->session->set_userdata('user_otp', '');
	    $data = $this->data;
		$this->load->vars($data);	
		$this->load->view('user/settings');
    }

    function change_alert_settings(){
			$mobile = $this->input->post('mobile');
			$email = $this->input->post('email');
			if(strlen(trim($mobile))==10 && strlen(trim($email))>5){
			$this->apicall->change_alert_settings();
			echo "1";
			}else{
			echo "0|There is some issue with your posted data.";
			}	
    }


    function changepass(){
			
			$pass = $this->input->post('password');
			$otp = $this->input->post('otp'); if($otp==''){ $this->session->set_userdata('user_otp', '');}
			$cpass = $this->input->post('cpassword');
			if(strlen(trim($pass))>5 && trim($pass)==trim($cpass)){
				if($this->session->userdata('user_otp')>0){
					if($this->session->userdata('user_otp')==$otp && trim($otp)!=''){
							$this->apicall->change_password();
							$this->session->set_userdata('user_otp', '');
					echo "1";
					}else{
							$this->session->set_userdata('user_otp', '');
							echo "0|OTP did not match!";
					}
				}else{
				
						$credential = array('id' => $this->session->userdata('user_uid'));
						$query = $this->db->get_where('users', $credential);
						$urw = $query->row();
	
				
						$otp = rand(11111, 99999); $this->session->set_userdata('user_otp', $otp);
						$message="Your one time password(OTP) ".$otp." applicable to change your password on hospytek will be applicable for next 5 minutes. Thanks www.hospytek.com";
						$ok=sendsms($urw->user_mobile, $message, 'textart');
						echo "1";
				}
			}else{
			echo "0|There is some issue with your posted data.";
			}	
    }


/*******************REQUESTS MANAGEMENT ******************/
    function requests(){
	    	$data = $this->data;
			$row = $this->apicall->orders(9, 0, $this->session->userdata('user_uid'));
			$row_array = array(); $return = array();						
			foreach($row as $rw){
				$itemcount = $this->apicall->order_items_count($rw->o_id);
				$vendorscount = $this->apicall->order_vendors($rw->o_id);
				$offerscount = $this->apicall->order_offers_count($rw->o_id);
				$prods = $this->apicall->order_items($rw->o_id, 3);	  $items='';
				foreach($prods as $prod){ if($items!=''){ $items .="<br>";} $items .=$prod->p_title." ".$prod->vr_name." - ".$prod->p_model;}
			    $row_array['id'] = $rw->o_id;
				$row_array['odate'] = $rw->o_date;			
				$row_array['desc'] = $rw->o_desc;			
				$row_array['invoice'] = $rw->o_invoice;			
				$row_array['itemscount'] = $itemcount;						
				$row_array['items'] = $items;			
				$row_array['vendorscount'] = $vendorscount;
				$row_array['offerscount'] = $offerscount;
				$row_array['status'] = $rw->o_status;				
                array_push($return,$row_array);
			}							
	    $data['orders'] = $return;
		$this->load->vars($data);
		$this->load->view('user/requests');
    }	
}
