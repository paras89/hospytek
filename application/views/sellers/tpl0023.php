<!DOCTYPE html>
<html>
    <head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<meta name="description" content="<?php echo $meta_desc;?>">
		<meta name="author" content="Hospytek">
		<meta name="keywords" content="<?php echo $meta_key;?>">
		<meta name="robots" content="all">
		<title><?php echo $meta_title;?></title>
		<!-- Bootstrap Core CSS -->
		<base href="<?php echo base_url();?>">
        <link href="https://www.hospytek.com/assets/sellers/tpl0023/css/bootstrap.css" rel="stylesheet" type="text/css" media="all" />
        <!--jQuery(necessary for Bootstrap's JavaScript plugins)-->
        <script src="https://www.hospytek.com/assets/sellers/tpl0023/js/jquery-1.11.0.min.js"></script>
        <!--Custom-Theme-files-->
        <!--theme-style-->
        <link href="https://www.hospytek.com/assets/sellers/tpl0023/css/style.css" rel="stylesheet" type="text/css" media="all" />
        <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
        <!--//theme-style-->
        <script type="https://www.hospytek.com/assets/sellers/tpl0023/application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!--start-menu-->
        <script src="https://www.hospytek.com/assets/sellers/tpl0023/js/simpleCart.min.js"> </script>
        <link href="https://www.hospytek.com/assets/sellers/tpl0023/css/memenu.css" rel="stylesheet" type="text/css" media="all" />
        <script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0023/js/memenu.js"></script>
        <script>$(document).ready(function(){$(".memenu").memenu();});</script>	
        <!--dropdown-->
        <script src="https://www.hospytek.com/assets/sellers/tpl0023/js/jquery.easydropdown.js"></script>			
    </head>
    <body>
        <!--start-logo-->
        <!--start-logo-->
        <!--bottom-header-->
        <div class="header navbar-fixed-top" style="background-color:#00bcd4">
            <div class="container col-md-12">
                <div class="header-left">
                    <div class="logo col-md-4">
                          <div class="logo">
                                <a href="<?php echo $brand->user_slug;?>">  
                                    <?  if($brand->user_image=='') 
                                        { ?>
                                            <a href="<?php echo $brand->user_slug;?>"><?php echo $brand->user_company?></a><br/>
                                        <? } else if($brand->user_image=='no.gif') { ?>
                                    <a href="<?php echo $brand->user_slug;?>" style="text-decoration:none; color:#0b8aef;"><?php echo $brand->user_company;?></a><br/>
                                    <? }
                                        else
                                        { $imgurl= 'http://www.hospytek.com/newcrm/UserFiles/Image/'.$brand->user_image;?>
                                    <a href="<?php echo $brand->user_slug;?>"><img src="<?=$imgurl?>" alt="<?=$brand->user_company?>" style="height:70px;margin-top:5px;"/></a><br/>
                                    <? } 
                                ?>
                                   </a>
                                &nbsp;&nbsp;<span><?php echo $brand->user_city;?>, <?php echo $brand->user_state;?> <?php echo $brand->user_country;?></span></h2>
                        </div>
                    </div>
                </div>
                <div class="col-md-8 header-right pull-right">
                    <div class="top-nav ">
                        <ul class="memenu skyblue" style="font-size: 45pt;padding-top:5px">
                            <li class="active" style="padding-left:15% "><a href="<?php echo $brand->user_slug;?>">Home</a></li>
                            <li class="grid"><a href="<?php echo $brand->user_slug;?>#about">About Us</a>
                            </li>
                            <li class="grid"><a href="<?php echo $brand->user_slug;?>#product">Products</a>
                            </li>
                            <li class="grid"><a href="<?php echo $brand->user_slug;?>#contact">Contact</a>
                            </li>
			<li><a data-toggle="modal" href="#modal-callback" style="position:relative;"><button class="btn-default btn-sm" >Become A Dealer</button></a></li>
                        </ul>
                    </div>
                    <div class="clearfix"> </div>
                </div>
            </div>
        </div>
        <!--bottom-header-->
        <!--banner-starts-->
        <div class="bnr" id="home">
            <br><br><br><br><br>
            
	<section id="slider"><!--slider-->
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
				
					<div id="slider-carousel" class="carousel slide" data-ride="carousel">
						<ol class="carousel-indicators">
							<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
							<li data-target="#slider-carousel" data-slide-to="1"></li>
							<li data-target="#slider-carousel" data-slide-to="2"></li>
						</ol>
						
						<div class="carousel-inner">
						
							
						
						
						 <?php $i=0; foreach ($items as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;} ;
						  if($i>6){ break;}
						  ?>				
						  <?php if ($i==0) { $i++; ?>
        			  <div class="item active"> 
								<div class="col-sm-6" style="margin-top:120px">
									<h1><?php echo $rw->p_title." ".$rw->vr_name;?></h1>
									<h3 ><strong>Model:</strong> <?php echo $rw->p_model;?></h3>
									<p><?php echo $rw->p_sdesc;?></p>
									
									<a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online"><button type="button" class="btn btn-default get">View Detail</button></a>
									<a href="<?php echo '../pdf/'.$rw->p_slug.'.pdf'?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Brochure Manual Download"><button type="button" class="btn btn-default get">View Brochure</button></a>
								</div>
								<div class="col-sm-6">
									<img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="girl img-responsive" alt="" />
									
								</div>
							</div>
							<?php } else  {  ?>
							
							<div class="item"> 
								<div class="col-sm-6" style="margin-top:120px">
									<h1><?php echo $rw->p_title." ".$rw->vr_name;?></h1>
									<h3><strong>Model:</strong> <?php echo $rw->p_model;?></h3>
									<p><?php echo $rw->p_sdesc;?></p>
									
									<a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online"><button type="button" class="btn btn-default get">View Detail</button></a>
									<a href="<?php echo '../pdf/'.$rw->p_slug.'.pdf'?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Brochure Manual Download"><button type="button" class="btn btn-default get">View Brochure</button></a>
								</div>
								<div class="col-sm-6">
									<img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="girl img-responsive" alt="" />
									
								</div>
							</div>
							<?php  } ?>
							
							
							
							<?php  } ?>
							
							
							
						</div>
						
						<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
							<i class="fa fa-angle-left"></i>
						</a>
						<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
							<i class="fa fa-angle-right"></i>
						</a>
					</div>
					
				</div>
			</div>
		</div>
	</section><!--/slider-->
            <div class="clearfix"> </div>
        </div>
        <!--banner-ends--> 
        <!--Slider-Starts-Here-->
        <script src="assets/sellers/tpl0023/js/responsiveslides.min.js"></script>
        <script>
            // You can also use "$(window).load(function() {"
            $(function () {
              // Slideshow 4
              $("#slider4").responsiveSlides({
                auto: true,
                pager: true,
                nav: true,
                speed: 500,
                namespace: "callbacks",
                before: function () {
                  $('.events').append("<li>before event fired.</li>");
                },
                after: function () {
                  $('.events').append("<li>after event fired.</li>");
                }
              });
            
            });
        </script>
        <!--End-slider-script-->
        <!--about-starts-->
        <div class="about" id="about">
            <br><br>
            <div class="container">
                <div class="about-top grid-1">
                    <div class="col-md-12 about-left">
                        <h2 style="text-align:center">WELCOME TO <?=$brand->user_company?></h2>
                        <p style="font-size:16px;color:#929292;"><?=$brand->user_about?></p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!--about-end-->
        <!--product-starts-->
        <div class="product" id="product">
            <div class="container">
                <div class="contact-top heading">
                    <h2>Our Products</h2>
                    <br><br><br>
                </div>
                <div class="product-top">
                    <div class="product-one">
					<?php $i=0; foreach ($items as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;}  $i++;?>
				<?php if($i==1){echo '<div class="row">';}?>	
                        <div class="col-md-4 product-left">
                            <div class="product-main simpleCart_shelfItem">
                                <a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" class="mask"><img class="img-responsive zoom-img" src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" /></a>
                                <div class="product-bottom" style="height:100">
                                    <h3 style="text-align:center"><?php echo $rw->p_title." ".$rw->vr_name;?></h3>
                                    <p style="text-align:center">Model <span><?php echo $rw->p_model;?></span></p>
                                    <p style="text-align:center"><a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>"><button class="btn-sm btn-primary">View Details</button></a></p>
                                </div>
                              
                            </div>
                        </div>
						<?php if($i==3){ $i=0; echo '</div><div class="clearfix">&nbsp; </div>';}?>
					<? }?>
						 <div class="clearfix"></div>
                    </div>
                   </div>
            </div>
        </div>
        <!--product-end-->
        <!--Contact area-starts-->
        <div class="contact" id="contact" style="padding-top: none">
            <div class="container">
                <br>
                <div class="contact-top heading">
                    <h2>Connect with <?=$brand->user_company?></h2>
                </div>
                <div class="contact-text">
                    <div class="col-md-3 contact-left">
                        <div class="address">
                            <h5>Address</h5>
                        </div>
                        <div class="address">
                            <p><?=$brand->user_company?> 
                                <span><?=$brand->user_address?></span>
                            </p>
                            <p><?=$brand->user_city?>, <?=$brand->user_state?></p>
							<p><?=$brand->user_country?>  <?=$brand->user_postcode?></p>
							<p>+91-<?=$brand->user_mobile?>   <br/>  <?=$brand->user_phone?></p>
                        </div>
                    </div>
                    <div class="col-md-9 contact-right">
                        <form>
                            <input type="text" placeholder="Name">
                            <input type="text" placeholder="Phone">
                            <input type="text"  placeholder="Email">
                            <textarea placeholder="Message" required=""></textarea>
                            <div class="submit-btn">
                                <input type="submit" value="SUBMIT">
                            </div>
                        </form>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!--information-end-->
        
        
        <!-- Modal Area Starts-->
          <div class="modal fade" id="modal-callback" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h2 class="modal-title">Please Fill the form & we will call you back</h2>
                          </div>
                          <div class="modal-body">
						  <form method="post" action="<?php echo base_url()."api";?>" class="form-ajax">
						  <input type="hidden" name="module" value="callback" />
						  <input type="hidden" name="type" value="0" />
						  <div class="row">
						  <div class="col-md-6">
						  	<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Name <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input" placeholder="" name="name" required>
							</div>

							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Phone <span>*</span></label>
		    <input type="phone" class="form-control unicase-form-control text-input"  placeholder=""  name="mobile" required>
							</div>
							
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Email <span>*</span></label>
		    <input type="email" class="form-control unicase-form-control text-input"  placeholder=""  name="email" required>
						   </div>	
							
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Best time to call you <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input"  placeholder="" name="timetocall" required>
						   </div>	


							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Message <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input"  placeholder=""  name="message" required>
						   </div>	


	<div class="form-group">
		<button type="submit" class="btn-upper btn btn-primary checkout-page-button">Submit Request</button>
	</div>
			
						  </div>

						  <div class="col-md-6" style="font-size:12px;">
						   <p><strong>Lets give you a call back to help you buy the most suitable Medical Equipment & Device as per your requirement, We would not only help you choose the most suitable medical device but also help you find the right localised vendor to make your after sale requirements more readily available.</strong></p>
						  <center><img src="assets/images/callback-icon.png" class="img-responsive" style="max-height:175px;" /></center>
						  <p>We at Hospytek hold immense value for every customer or prospective customer and consider it as our utmost duty to provide a solution to all the queries raised by the customers.</p>

<p>We thank you for filling the form and assure you that one of our sales executives would get back to you very soon to understand your problem and provide the most reasonable solution.</p>

<p><strong>We again thank you for your patience and hope you are your experience on our website.</strong></p>
						  </div>
						  
                          </div>
						  </form>
                      </div>
                  </div>
				  </div>
              </div>
        <!-- End of Modal Area -->
        
        
        <!--footer-starts-->
        <div class="footer" style="background:black;">
            <div class="container">
                <div class="footer-top">
                    <div class="col-md-12 footer-right">
                        <p>Powered by<br><a href="https://www.hospytek.com/" target="blank"><img src="assets/sellers/tpl0023/images/logo.png" /></a></p>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <!--footer-end-->	
        
        
        
    </body>
</html>

