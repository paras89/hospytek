/*  Table of Contents 
01. MENU ACTIVATION
02. GALLERY JAVASCRIPT
03. FITVIDES RESPONSIVE VIDEOS
04. MOBILE MENU BAR
05. SHOW/HIDE MOBILE MENU
06. prettyPhoto JS
07. Form Validation
08. Light Shortcodes
09. SCROLL TO TOP
10. Backstretch
11. STICKY MENU JS
*/





jQuery(document).ready(function($) {
    'use strict';
    
/*
=============================================== 01. MENU ACTIVATION  ===============================================
*/

	jQuery("ul.sf-menu").supersubs({ 
	        minWidth:    2,   // minimum width of sub-menus in em units 
	        maxWidth:    20,   // maximum width of sub-menus in em units 
		extraWidth:  0     // extra width can ensure lines don't sometimes turn over 
	                           // due to slight rounding differences and font-family 
	    }).superfish({ 
			animation:  {opacity:'show'},
			animationOut:  {opacity:'hide'},
			speed:         200,           // speed of the opening animation. Equivalent to second parameter of jQuery’s .animate() method
			speedOut:      'normal',
			autoArrows:    false,               // if true, arrow mark-up generated automatically = cleaner source code at expense of initialisation performance 
			dropShadows:   false,               // completely disable drop shadows by setting this to false 
			delay:     400               // 1.2 second delay on mouseout 
		});

    
    
    
    
/*
=============================================== 02. GALLERY JAVASCRIPT  ===============================================
*/
    $('.gallery-progression').flexslider({
		animation: "fade",      
		slideDirection: "horizontal", 
		slideshow: false,         
		slideshowSpeed: 7000,  
		animationDuration: 200,        
		directionNav: true,             
		controlNav: true,
        prevText: "",
        nextText: ""
    });

/*
=============================================== 03. FITVIDES RESPONSIVE VIDEOS  ===============================================
*/
	 
	 $(".width-container").fitVids();
    
/*
=============================================== 04. MOBILE MENU  ===============================================
*/
    $('ul.mobile-menu-pro').slimmenu({
        resizeWidth: '1200',
        collapserTitle: 'Menu',
        easingEffect:null,
        animSpeed:'easeInOutQuint',
        indentChildren: false,
        childrenIndenter: '- '
    }); 


	
    
/*
=============================================== 05. SHOW/HIDE MOBILE MENU  ===============================================
*/	

	var clickOrTouch = (('ontouchend' in window)) ? 'touchend' : 'click';
	
	$(".mobile-menu-icon-pro").on(clickOrTouch, function(e) {
	   $(".site-header-pro").toggleClass("active-menu-icon-pro");
	});	
    
/*
=============================================== 06. prettyPhoto JS  ===============================================
*/

    $(".type-portfolio  a[data-rel^='prettyPhoto'], .featured-blog-pro a[data-rel^='prettyPhoto'],.images a[data-rel^='prettyPhoto'], .progression-featured-portfolio-single a[data-rel^='prettyPhoto'], .choose-quark-pro a[data-rel^='prettyPhoto']").prettyPhoto({
        hook: 'data-rel',
        animation_speed: 'fast', /* fast/slow/normal */
        slideshow: 5000, /* false OR interval time in ms */
        show_title: false, /* true/false */
        deeplinking: false, /* Allow prettyPhoto to update the url to enable deeplinking. */
        overlay_gallery: false, /* If set to true, a gallery will overlay the fullscreen image on mouse over */
        custom_markup: '',
        default_width: 900,
        default_height: 506,
        social_tools: '' /* html or false to disable */
    });


/*
=============================================== 07. Form Validation  ===============================================
*/


	$("#CommentForm").validate();
		
	
/*
=============================================== 08. Light Shortcodes  ===============================================
*/
	
	
	// Accordion
	$(".ls-sc-accordion").accordion({heightStyle: "content"});
	
	// Toggle
	$(".ls-sc-toggle-trigger").click(function(){$(this).toggleClass("active").next().slideToggle("fast");return false; });
	
	// Tabs
	$( ".ls-sc-tabs" ).tabs();
 
/*
=============================================== 09. SCROLL TO TOP MENU JS  ===============================================
*/
  	// browser window scroll (in pixels) after which the "back to top" link is shown
  	var offset = 150,
  	
	//browser window scroll (in pixels) after which the "back to top" link opacity is reduced
  	offset_opacity = 1200,
  	
	//duration of the top scrolling animation (in ms)
  	scroll_top_duration = 700,
  	
	//grab the "back to top" link
  	$back_to_top = $('#pro-scroll-top');

	//hide or show the "back to top" link
	$(window).scroll(function(){
  		( $(this).scrollTop() > offset ) ? $back_to_top.addClass('cd-is-visible') : $back_to_top.removeClass('cd-is-visible cd-fade-out');
  		if( $(this).scrollTop() > offset_opacity ) { 
  			$back_to_top.addClass('cd-fade-out');
  		}
  	});

	//smooth scroll to top
	$back_to_top.on('click', function(event){
		event.preventDefault();
		$('body,html').animate({
			scrollTop: 0 ,
		}, scroll_top_duration
	);
	});
/*
=============================================== 10. Background Images  ===============================================
*/
    $("body.home .performance-feat-pro").backstretch([ "images/demo/blue-bg.jpg" ],{ fade: 750, centeredY:true }); 
    $("body.home .home-base-feat-pro").backstretch([ "images/demo/home-base.jpg" ],{ fade: 750, centeredY:true });
    $("body.tour .tour-page-head-pro").backstretch([ "images/demo/tour-bg.jpg" ],{ fade: 750, centeredY:true });
    $("body.tour .pro-tour-screenshots-img").backstretch([ "images/demo/second-tour.jpg" ],{ fade: 750, centeredY:true });
    $("body.tour .pro-tour-eye-catching").backstretch([ "images/demo/tour-bg-bottom.jpg" ],{ fade: 750, centeredY:true });
    $("body.blog .blog-page-head-pro").backstretch([ "images/demo/page-title.jpg" ],{ fade: 750, centeredY:true });
    $("body.pricing .pricing-page-head-pro").backstretch([ "images/demo/page-title.jpg" ],{ fade: 750, centeredY:true });
    $("body.shop .shop-page-head-pro").backstretch([ "images/demo/page-title.jpg" ],{ fade: 750, centeredY:true });
    $("body.contact .contact-page-head-pro").backstretch([ "images/demo/page-title.jpg" ],{ fade: 750, centeredY:true });
    $("body.choose-quark .choose-quark-page-head-pro").backstretch([ "images/demo/page-title.jpg" ],{ fade: 750, centeredY:true });
    
    
	$("body.page header").backstretch(["images/demo/page-title.jpg"],{fade:750,});
	$("#page-title-pro").backstretch(["images/demo/page-title.jpg"],{fade:750,});
	 
/*
=============================================== 11. STICKY MENU JS  ===============================================
*/	
	if ($(this).width() > 944) {
    $('#sticky-header-pro').scrollToFixed();
	}
	
    var header = $(".menu-default-pro");
       $(window).on('scroll load', function() {
		   
		   if ($(this).width() > 944) {
			   
	           var scroll = $(window).scrollTop();

	           if (scroll >= 50) {
	               header.removeClass('menu-default-pro').addClass("menu-resized-pro");
				   
	           } else {
	               header.removeClass("menu-resized-pro").addClass('menu-default-pro');
	           }
		   
		   } 
		   
       });
    
// END
});

