<!doctype html>
<html lang="en-gb" class="no-js">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
        <meta name="description" content="<?php echo $meta_desc;?>">
        <meta name="author" content="Hospytek">
        <meta name="keywords" content="<?php echo $meta_key;?>">
        <meta name="robots" content="all">
        <title><?php echo $meta_title;?></title>
        <!-- Bootstrap Core CSS -->
        <base href="<?php echo base_url();?>">
        <link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0006/css/bootstrap.min.css" />
        <link rel="stylesheet" type="text/css" href="https://www.hospytek.com/assets/sellers/tpl0006/isotope.css" media="screen" />
        <link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0006/js/fancybox/jquery.fancybox.css" type="text/css" media="screen" />
        <link rel="stylesheet" type="text/css" href="https://www.hospytek.com/assets/sellers/tpl0006/css/da-slider.css" />
        <link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0006/css/styles.css"/>
        <!-- Font Awesome -->
        <link href="https://www.hospytek.com/assets/sellers/tpl0006/css/font-awesome.min.css" rel="stylesheet">
    </head>
    <body style="background-color: #f2f2f2e6;">
        <header class="header" style="background-color:#66ccff;">
            <div class="navbar-header col-md-6">
            
                <button type="button" id="nav-toggle" class="navbar-toggle" data-toggle="collapse" data-target="#main-nav">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                </button>
              <div class="logo">
                                <a href="<?php echo $brand->user_slug;?>">  
                                    <?  if($brand->user_image=='') 
                                        { ?>
                                            <a href="<?php echo $brand->user_slug;?>"><?php echo $brand->user_company?></a><br/>
                                        <? } else if($brand->user_image=='no.gif') { ?>
                                    <a href="<?php echo $brand->user_slug;?>" style="text-decoration:none; color:#0b8aef;"><?php echo $brand->user_company;?></a><br/>
                                    <? }
                                        else
                                        { $imgurl= 'http://www.hospytek.com/newcrm/UserFiles/Image/'.$brand->user_image;?>
                                    <a href="<?php echo $brand->user_slug;?>"><img src="<?=$imgurl?>" alt="<?=$brand->user_company?>" style="height:70px;margin-top:5px;"/></a><br/>
                                    <? } 
                                ?>
                                   </a>
                                &nbsp;&nbsp;<span><?php echo $brand->user_city;?>, <?php echo $brand->user_state;?> <?php echo $brand->user_country;?></span></h2>
                            </div>
            </div>
            <!--/.navbar-header-->
            <div class="collapse navbar-collapse col-md-6">
                <ul class="nav navbar-nav" id="mainNav">
                    <li class="active"><a href="<?php echo $brand->user_slug;?>" class="scroll-link">Home</a></li>
                    <li><a href="<?php echo $brand->user_slug;?>#aboutUs" class="scroll-link">About Us</a></li>
                    <li><a href="<?php echo $brand->user_slug;?>#product" class="scroll-link">Product</a></li>
                    <li><a href="<?php echo $brand->user_slug;?>#contactUs" class="scroll-link">Contact Us</a></li>
                    <li>
                        <div style="display:inline;float:left;margin-top:20px;padding-left:20px"><a data-toggle="modal" href="#modal-callback" class="cart-sellers"><button class="btn btn-primary">Become a Dealer</button></a></div>
                    </li>
                </ul>
            </div>
           
        </header>
        <!--/.header-->
       
        <section id="main-slider">
            <!--slider-->
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <div id="slider-carousel" class="carousel slide" data-ride="carousel">
                            <ol class="carousel-indicators">
                                <li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
                                <li data-target="#slider-carousel" data-slide-to="1"></li>
                                <li data-target="#slider-carousel" data-slide-to="2"></li>
                            </ol>
                            <div class="carousel-inner">
                                <?php $i=0; foreach ($items as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;} ;
                                    if($i>6){ break;}
                                    ?>				
                                <?php if ($i==0) { $i++; ?>
                                <div class="item active">
                                    <div class="col-sm-6" style="margin-top:150px">
                                        <h1><?php echo $rw->p_title." ".$rw->vr_name;?></h1>
                                        <h3 ><strong>Model:</strong> <?php echo $rw->p_model;?></h3>
                                        <p><?php echo $rw->p_sdesc;?></p>
                                        <a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online"><button type="button" class="btn btn-info ">View Detail</button></a>
                                        <a href="<?php echo '../pdf/'.$rw->p_slug.'.pdf'?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Brochure Manual Download"><button type="button" class="btn btn-info ">View Brochure</span></button></a>
                                    </div>
                                    <div class="col-sm-6">
                                        <img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="girl img-responsive" alt="" />
                                    </div>
                                </div>
                                <?php } else  {  ?>
                                <div class="item">
                                    <div class="col-sm-6" style="margin-top:150px">
                                        <h1><?php echo $rw->p_title." ".$rw->vr_name;?></h1>
                                        <h3><strong>Model:</strong> <?php echo $rw->p_model;?></h3>
                                        <p><?php echo $rw->p_sdesc;?></p>
                                        <button type="button" class="btn btn-info"><a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online">View Detail</a></button>
                                        <button type="button" class="btn btn-info "><a href="<?php echo '../pdf/'.$rw->p_slug.'.pdf'?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Brochure Manual Download">View Brochure</a></span></button>
                                    </div>
                                    <div class="col-sm-6">
                                        <img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="girl img-responsive" alt="" />
                                    </div>
                                </div>
                                <?php  } ?>
                                <?php  } ?>
                            </div>
                            <a href="#slider-carousel" class="left control-carousel pull-left" data-slide="prev">
                            <i class="fa fa-angle-left" style="color:grey;font-size:50px;"></i>
                            </a>
                            <a href="#slider-carousel" class="right control-carousel pull-right" data-slide="next">
                            <i class="fa fa-angle-right" style="color:grey;font-size:50px;"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <!--/slider-->
     
        <section id="aboutUs" class="page-section pDark pdingBtm30">
            <div class="container">
                <div class="heading text-center">
                    <!-- Heading -->
                    <h2>WELCOME TO <?=$brand->user_company?></h2>
                    <p><?=$brand->user_about?></p>
                </div>
            </div>
            <!--/.container-->
        </section>
        <section id="product" class="page-section section appear clearfix">
            <div class="container">
            <div class="heading text-center">
                <!-- Heading -->
                <h2>Our Products</h2>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <div class="row">
                        <div class="product-items isotopeWrapper clearfix" id="3">
                            <?php $i=0; foreach ($items as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;}  $i++;?>
                            <?php if($i==1){echo '<div class="row">';}?>	
                            <article class="col-md-4 col-sm-4 isotopeItem webdesign" >
                                <div class="product-item"  style="border: solid .1px #cddbe9; height:455px;">
                                    <img src="<?php echo $this->config->item('img_url').$img;?>" />
                                    <p><a href="#"><?php echo $rw->p_title." ".$rw->vr_name;?></a></p>
                                    <h6><a href="#">Model <span><?php echo $rw->p_model;?></span></a></h6>
                                </div>
                                <div>
                                    <a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online"><button type="button" class="btn btn-primary">View Detail</button></a>
                                </div>
                            </article>
                            <?php if($i==3){ $i=0; echo '</div><div class="clearfix">&nbsp; </div>';}?>
                            <?php }?>
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <footer class="page-section darkBg" id="contactUs">
            <div class="container">
                <div class="row">
                    <div class="heading text-center">
                        <!-- Heading -->
                        <h2>Contact Us</h2>
                    </div>
                </div>
                <div class="row mrgn30">
                    <div class="col-sm-5"> 
                        <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d1944302.5352489671!2d77.4535942421874!3d17.863656040793934!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sen!2sin!4v1398658474236" width="100%" height="300" frameborder="0" style="border:0"></iframe>
                    </div>
                    <div class="col-sm-3">
                        <h4>Address:</h4>
                        <address>
                            <p><?=$brand->user_address?></p>
                            <p><?=$brand->user_city?>, <?=$brand->user_state?></p>
                            <p><?=$brand->user_country?>  <?=$brand->user_postcode?></p>
                        </address>
                        <h4>Contact Info:</h4>
                        <ul>
                            <li>
                                <p>+91-<?=$brand->user_mobile?>  <br>
                                    <?=$brand->user_phone?>
                                </p>
                            </li>
                        </ul>
                    </div>
                    <form method="post" action="#" id="contactfrm" role="form">
                        <div class="col-sm-4">
                            <div class="form-group">
                                <label for="name">Name</label>
                                <input type="text" class="form-control" name="name" id="name" placeholder="Enter name" title="Please enter your name (at least 2 characters)">
                            </div>
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" class="form-control" name="email" id="email" placeholder="Enter email" title="Please enter a valid email address">
                            </div>
                            <div class="form-group">
                                <label for="comments">Comments</label>
                                <textarea name="comment" class="form-control" id="comments" cols="3" rows="3" placeholder="Enter your message…" title="Please enter your message (at least 10 characters)"></textarea>
                            </div>
                            <button name="submit" type="submit" class="btn btn-lg btn-primary" id="submit"> Submit</button>		
                            <div class="result"></div>
                        </div>
                    </form>
                </div>
            </div>
            <!--/.container-->	
        </footer>
        <!--/.page-section-->
        <section class="copyright">
            <div class="container">
                <div class="row">
                    <div class="col-sm-12">
                        <ul class="socialIcons">
                            <li><a href="#" class="fbIcon" target="_blank"><i class="fa fa-facebook-square fa-lg"></i></a></li>
                            <li><a href="#" class="twitterIcon" target="_blank"><i class="fa fa-twitter-square fa-lg"></i></a></li>
                            <li><a href="#" class="googleIcon" target="_blank"><i class="fa fa-google-plus-square fa-lg"></i></a></li>
                            <li><a href="#" class="pinterest" target="_blank"><i class="fa fa-pinterest-square fa-lg"></i></a></li>
                        </ul>
                        <div class="pull-right webThemez"> 
                            Powered by
                            <a href="https://www.hospytek.com/" ><img src="<?php echo base_url();?>/assets/images/logo.png" alt="logo"></a> 
                        </div>
                    </div>
                </div>
            </div>
        </section>
        <a href="#top" class="topHome"><i class="fa fa-chevron-up fa-2x"></i></a>
        <script src="https://www.hospytek.com/assets/sellers/tpl0006/js/modernizr-latest.js"></script>
        <script src="https://www.hospytek.com/assets/sellers/tpl0006/js/jquery-1.8.2.min.js" type="text/javascript"></script>    
        <script src="https://www.hospytek.com/assets/sellers/tpl0006/js/bootstrap.min.js" type="text/javascript"></script>	 
        <script src="https://www.hospytek.com/assets/sellers/tpl0006/jquery.isotope.min.js" type="text/javascript"></script>
        <script src="https://www.hospytek.com/assets/sellers/tpl0006/fancybox/jquery.fancybox.pack.js" type="text/javascript"></script> 
        <script src="https://www.hospytek.com/assets/sellers/tpl0006/jquery.nav.js" type="text/javascript"></script>
        <script src="https://www.hospytek.com/assets/sellers/tpl0006/js/jquery.cslider.js" type="text/javascript"></script>
        <script src="https://www.hospytek.com/assets/sellers/tpl0006/js/custom.js" type="text/javascript"></script>
        <script type="text/javascript">
            $('.typeahead').typeahead({
            	  items:100,	
               matcher: function(item) {
                  return true;
            			},
                    source: function (query, process) {
             	objects = [];
                  	map = {};
                      $.ajax({
                        url: '<?php echo base_url()."api";?>',
                        type: 'POST',
            	  data: {key : query, module: 'key_search'},
                        dataType: 'JSON',
                        success: function(data) {
            		objects = [];
                  		$.each(data, function(i, object) {
                       map[object.label] = object;
              	        objects.push(object.label);
                  		});
                    process(objects);
                        },
            	   beforeSend: function(){ $('.searchbox-icon').html('<img src="<?php echo base_url();?>assets/images/loader.gif">'); },
                         complete: function(){   $('.searchbox-icon').html('<span class="zmdi zmdi-search search-icon"></span>'); }
                      });
                    },
            updater: function(item) {
            	$('#key').val(map[item].name);
            	if(map[item].slug!=''){ window.location ="<?php echo base_url();?>" + map[item].slug; }else{
            	$('.searchform').submit();		
            	}
               }		  
                  });
            
            
            $(function() {
            $(".scrolling").mCustomScrollbar({
              theme: "dark",
              scrollInertia: 0,
              mouseWheel:{
                preventDefault: true
              }
            });
            });		
            
            
            $(document).on('submit', '.form-ajax', function (e) {
            var elem = $(this);
            		$.ajax({
            				type : "POST",
            				url : elem.attr("action"),
            				dataType : "html",
            				data : elem.serialize(),
            				beforeSend : function() {
            					pleasewait('');
            				},
            				success : function(response) {	
            if(Number(response)>0){ swal("Message Sent!", "Your message has been sent to concerned department. You will get response within next couple hrs.", "success");}else{ var res=response.split("|");  swal("Error", res[1], "error");}
            				}
            		});			  
            return false;		  
            });    
            
            function pleasewait(msg){
            	swal({
                          title: '<img src="assets/images/loading.gif">',
                          text: "Please wait while we process the operation.",
                          showConfirmButton: false
                      });
            }
            
        </script>	
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
            
            ga('create', 'UA-88935475-1', 'auto');
            ga('send', 'pageview');
            
        </script>
        <script type="text/javascript">stLight.options({publisher: "b64324f3-14d8-4cea-a6b6-9550367d02a0", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
        <script>
            var options={ "publisher": "b64324f3-14d8-4cea-a6b6-9550367d02a0", "position": "left", "ad": { "visible": false, "openDelay": 5, "closeDelay": 0}, "chicklets": { "items": ["facebook", "twitter", "googleplus", "whatsapp", "linkedin", "pinterest", "email", "sharethis"]}};
            var st_hover_widget = new sharethis.widgets.hoverbuttons(options);
        </script>
        <div class="modal fade" id="modal-callback" tabindex="-1" role="dialog" aria-hidden="true">
            <div class="modal-dialog modal-lg">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h2 class="modal-title">Please Fill the form & we will call you back</h2>
                    </div>
                    <div class="modal-body">
                        <form method="post" action="<?php echo base_url()."api";?>" class="form-ajax">
                            <input type="hidden" name="module" value="callback" />
                            <input type="hidden" name="type" value="0" />
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="info-title" for="exampleInputName">Your Name <span>*</span></label>
                                        <input type="text" class="form-control unicase-form-control text-input" placeholder="" name="name" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="info-title" for="exampleInputName">Your Phone <span>*</span></label>
                                        <input type="phone" class="form-control unicase-form-control text-input"  placeholder=""  name="mobile" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="info-title" for="exampleInputName">Your Email <span>*</span></label>
                                        <input type="email" class="form-control unicase-form-control text-input"  placeholder=""  name="email" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="info-title" for="exampleInputName">Best time to call you <span>*</span></label>
                                        <input type="text" class="form-control unicase-form-control text-input"  placeholder="" name="timetocall" required>
                                    </div>
                                    <div class="form-group">
                                        <label class="info-title" for="exampleInputName">Your Message <span>*</span></label>
                                        <input type="text" class="form-control unicase-form-control text-input"  placeholder=""  name="message" required>
                                    </div>
                                    <div class="form-group">
                                        <button type="submit" class="btn-upper btn btn-primary checkout-page-button">Submit Request</button>
                                    </div>
                                </div>
                                <div class="col-md-6" style="font-size:12px;">
                                    <p><strong>Lets give you a call back to help you buy the most suitable Medical Equipment & Device as per your requirement, We would not only help you choose the most suitable medical device but also help you find the right localised vendor to make your after sale requirements more readily available.</strong></p>
                                    <center><img src="assets/images/callback-icon.png" class="img-responsive" style="max-height:175px;" /></center>
                                    <p>We at Hospytek hold immense value for every customer or prospective customer and consider it as our utmost duty to provide a solution to all the queries raised by the customers.</p>
                                    <p>We thank you for filling the form and assure you that one of our sales executives would get back to you very soon to understand your problem and provide the most reasonable solution.</p>
                                    <p><strong>We again thank you for your patience and hope you are your experience on our website.</strong></p>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>
</html>

