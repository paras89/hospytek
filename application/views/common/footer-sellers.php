<!-- ============================================================= FOOTER ============================================================= -->
<footer id="footer" class="footer color-bg">
  <div class="copyright-bar">
    <div class="container">
      <div class="col-xs-12 col-sm-5 no-padding social">
        <ul class="link">
          <li class="fb pull-left"><a target="_blank"  href="https://www.facebook.com/Hospytek/" title="Facebook"></a></li>
          <li class="tw pull-left"><a target="_blank"  href="https://twitter.com/hospytek" title="Twitter"></a></li>
          <li class="googleplus pull-left"><a target="_blank"  href="https://plus.google.com/113015133774959781756" title="GooglePlus"></a></li>
          <li class="linkedin pull-left"><a target="_blank"  href="https://www.linkedin.com/company/hospytek" title="Linkedin"></a></li>
          <li class="youtube pull-left"><a target="_blank" href="https://www.youtube.com/channel/UCFBX1N-VcfJTQKINqTKC5jg" title="Youtube"></a></li>
        </ul>
      </div>
      <div class="col-xs-12 col-sm-7 no-padding text-right" style="color:#FFFFFF"> 
	  Copyrights &copy; <?=date("Y")?>. Hospytek (A Unit of Sigma Enterprises). All rights reserved
	  <br /><span style="font-size:10px;"><strong>Disclaimer:</strong> All product's images & logos used in website are properties of respective brands or manufacturers.</span>  
        <!-- /.payment-methods --> 
      </div>
    </div>
  </div>
</footer>
<!-- ============================================================= FOOTER : END============================================================= --> 

	 		  <div class="modal fade" id="modal-callback" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h2 class="modal-title">Please Fill the form & we will call you back</h2>
                          </div>
                          <div class="modal-body">
						  <form method="post" action="<?php echo base_url()."api";?>" class="form-ajax">
						  <input type="hidden" name="module" value="callback" />
						  <input type="hidden" name="type" value="0" />
						  <div class="row">
						  <div class="col-md-6">
						  	<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Name <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input" placeholder="" name="name" required>
							</div>

							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Phone <span>*</span></label>
		    <input type="phone" class="form-control unicase-form-control text-input"  placeholder=""  name="mobile" required>
							</div>
							
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Email <span>*</span></label>
		    <input type="email" class="form-control unicase-form-control text-input"  placeholder=""  name="email" required>
						   </div>	
							
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Best time to call you <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input"  placeholder="" name="timetocall" required>
						   </div>	


							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Message <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input"  placeholder=""  name="message" required>
						   </div>	


	<div class="form-group">
		<button type="submit" class="btn-upper btn btn-primary checkout-page-button">Submit Request</button>
	</div>
			
						  </div>

						  <div class="col-md-6" style="font-size:12px;">
						   <p><strong>Lets give you a call back to help you buy the most suitable Medical Equipment & Device as per your requirement, We would not only help you choose the most suitable medical device but also help you find the right localised vendor to make your after sale requirements more readily available.</strong></p>
						  <center><img src="assets/images/callback-icon.png" class="img-responsive" style="max-height:175px;" /></center>
						  <p>We at Hospytek hold immense value for every customer or prospective customer and consider it as our utmost duty to provide a solution to all the queries raised by the customers.</p>

<p>We thank you for filling the form and assure you that one of our sales executives would get back to you very soon to understand your problem and provide the most reasonable solution.</p>

<p><strong>We again thank you for your patience and hope you are your experience on our website.</strong></p>
						  </div>
						  
                          </div>
						  </form>
                      </div>
                  </div>
				  </div>
              </div>



	 		  <div class="modal fade" id="modal-quote" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h2 class="modal-title" style="font-size:18px;">Request Quotation for <strong><?php echo ucwords(strtolower($item->p_title." ".$item->vr_name));?></strong></h2>
                          </div>
                          <div class="modal-body">
						  <form method="post" action="<?php echo base_url()."api";?>" class="form-ajax">
						   <input type="hidden" name="module" value="quote" />
						   <input type="hidden" name="pid" value="<?php echo $item->p_id;?>" />
						    <input type="hidden" name="pname" value="<?php echo ucwords(strtolower($item->p_title." ".$item->vr_name));?>" />
						   <input type="hidden" name="type" value="1" />
						  <div class="row">
						  <div class="col-md-8">
						  
						 <div class="row">
						  <div class="col-md-6">
						  	<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Name <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input" placeholder="" name="name" required>
							</div>
							</div>
							
							<div class="col-md-6">
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Company Name<span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input"   name="company" required>
						   </div>	
						   </div>
						   </div>
						   

							
							
						<div class="row">
						  <div class="col-md-6">
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Phone <span>*</span></label>
		    <input type="phone" class="form-control unicase-form-control text-input"  placeholder=""  name="mobile" required>
							</div>
						  </div>
						
						  <div class="col-md-6">
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Email <span>*</span></label>
		    <input type="email" class="form-control unicase-form-control text-input"  placeholder=""  name="email" required>
						   </div>	
						   </div>
						   </div>	

						<div class="row">
						  <div class="col-md-6">							
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Quantity<span>*</span></label>
		    <input type="number" class="form-control unicase-form-control text-input" value="1" name="qty" required>
						   </div>
						   </div>	

						 <div class="col-md-6">
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Location<span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input" name="location" required>
						   </div>	
						   </div>
						   </div>


						<div class="row">
						  <div class="col-md-12">							
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Message <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input"  placeholder=""  name="message">
						   </div>	
						</div>
						</div>   


	<div class="form-group">
		<button type="submit" class="btn-upper btn btn-primary checkout-page-button">Submit Request</button>
	</div>
			
						  </div>
<?php if($item->p_image=='' || is_null($item->p_image)){$pimg='no-img.jpg';}else{ $pimg=$item->p_image;}?>
						  <div class="col-md-4" style="font-size:12px;">
						 <img class="img-responsive" alt="" src="<?php echo $this->config->item('img_url').$pimg;?>"/>
						 <p><strong>Model:</strong> <span class="value"><?php echo $item->p_model;?></span><br />
						 <strong>Brand:</strong> <span class="value"><a href="<?php echo $item->user_slug;?>"><?php echo $item->user_company;?></a></span>
						 </p>
						  </div>
						  
                          </div>
						  </form>
                      </div>
                  </div>
				  </div>
              </div>

<!-- JavaScripts placed at the end of the document so the pages load faster --> 
<script src="assets/js/bootstrap.min.js"></script> 
<script src="assets/js/bootstrap-hover-dropdown.min.js"></script> 
<script src="assets/js/owl.carousel.min.js"></script> 
<script src="assets/js/echo.min.js"></script> 
<script src="assets/js/jquery.easing-1.3.min.js"></script> 
<script src="assets/js/bootstrap-slider.min.js"></script> 
<script src="assets/js/jquery.rateit.min.js"></script> 
<script type="text/javascript" src="assets/js/lightbox.min.js"></script> 
<script src="assets/js/bootstrap-select.min.js"></script> 
<script type="text/javascript" src="assets/js/lity.min.js"></script>
<script src="assets/js/wow.min.js"></script> 
<script src="assets/js/sweetalert/sweet-alert.js"></script>
<script src="assets/js/scripts.js"></script>
<script src="assets/js/malihu-custom-scrollbar-plugin/jquery.mCustomScrollbar.js"></script>
<script src="assets/js/bootstrap3-typeahead.min.js"></script>
	
<script type="text/javascript">
	 $('.typeahead').typeahead({
	 	  items:100,	
		   matcher: function(item) {
		      return true;
  			},
          source: function (query, process) {
		 	objects = [];
        	map = {};
            $.ajax({
              url: '<?php echo base_url()."api";?>',
              type: 'POST',
			  data: {key : query, module: 'key_search'},
              dataType: 'JSON',
              success: function(data) {
				objects = [];
        		$.each(data, function(i, object) {
	            map[object.label] = object;
    	        objects.push(object.label);
        		});
		        process(objects);
              },
			   beforeSend: function(){ $('.searchbox-icon').html('<img src="<?php echo base_url();?>assets/images/loader.gif">'); },
               complete: function(){   $('.searchbox-icon').html('<span class="zmdi zmdi-search search-icon"></span>'); }
            });
          },
		updater: function(item) {
			$('#key').val(map[item].name);
			if(map[item].slug!=''){ window.location ="<?php echo base_url();?>" + map[item].slug; }else{
			$('.searchform').submit();		
			}
	    }		  
        });


$(function() {
  $(".scrolling").mCustomScrollbar({
    theme: "dark",
    scrollInertia: 0,
    mouseWheel:{
      preventDefault: true
    }
  });
});		


$(document).on('submit', '.form-ajax', function (e) {
	var elem = $(this);
				$.ajax({
						type : "POST",
						url : elem.attr("action"),
						dataType : "html",
						data : elem.serialize(),
						beforeSend : function() {
							pleasewait('');
						},
						success : function(response) {	
if(Number(response)>0){ swal("Message Sent!", "Your message has been sent to concerned department. You will get response within next couple hrs.", "success");}else{ var res=response.split("|");  swal("Error", res[1], "error");}
						}
				});			  
	return false;		  
});    

function pleasewait(msg){
			swal({
                title: '<img src="assets/images/loading.gif">',
                text: "Please wait while we process the operation.",
                showConfirmButton: false
            });
}

</script>	
<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/58532e52fccdfa3ec85f19f2/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-88935475-1', 'auto');
  ga('send', 'pageview');

</script>
<script type="text/javascript">stLight.options({publisher: "b64324f3-14d8-4cea-a6b6-9550367d02a0", doNotHash: false, doNotCopy: false, hashAddressBar: false});</script>
<script>
var options={ "publisher": "b64324f3-14d8-4cea-a6b6-9550367d02a0", "position": "left", "ad": { "visible": false, "openDelay": 5, "closeDelay": 0}, "chicklets": { "items": ["facebook", "twitter", "googleplus", "whatsapp", "linkedin", "pinterest", "email", "sharethis"]}};
var st_hover_widget = new sharethis.widgets.hoverbuttons(options);
</script>
</body>
</html>