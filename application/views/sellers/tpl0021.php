<!DOCTYPE html>
<html>
    <head>
        <!-- Meta -->
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
		<meta name="description" content="<?php echo $meta_desc;?>">
		<meta name="author" content="Hospytek">
		<meta name="keywords" content="<?php echo $meta_key;?>">
		<meta name="robots" content="all">
		<title><?php echo $meta_title;?></title>
		<!-- Bootstrap Core CSS -->
		<base href="<?php echo base_url();?>">
        <link href="https://www.hospytek.com/assets/sellers/tpl0021/css/bootstrap.css" rel='stylesheet' type='text/css' />
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <script src="https://www.hospytek.com/assets/sellers/tpl0021/js/jquery.min.js"></script>
        <!-- Custom Theme files -->
        <link href="https://www.hospytek.com/assets/sellers/tpl0021/css/style.css" rel="stylesheet" type="text/css" media="all" />
        <!-- Custom Theme files -->
        
        <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
        <!--webfont-->
        <!-- for bootstrap working -->
        <script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0021/js/bootstrap-3.1.1.min.js"></script>
        <!-- //for bootstrap working -->
        <!-- cart -->
        <script src="https://www.hospytek.com/assets/sellers/tpl0021/js/simpleCart.min.js"> </script>
        <!-- cart -->
        <link rel="stylesheet" href="https://www.hospytek.com/assets/sellers/tpl0021/css/flexslider.css" type="text/css" media="screen" />
    </head>
    <body>
        <div class="banner-top"  style="background-color: #bfc4c6;">
            <div class="container">
                <nav class="navbar navbar-default col-md-12" role="navigation">
                    <div class="navbar-header col-md-12">
                   
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        </button>
                    </div>
                    
                   <div class="collapse navbar-collapse  navbar-right " id="bs-example-navbar-collapse-1">
                    
                    
                        <ul class="nav navbar-nav navbar-inverse navbar-fixed-top" style="background-color:#bfc4c6">
                       
                          <li class="col-md-6" style="margin-top:20px;padding-left:70px;">
                               
                                   <div class="logo">
                                <a href="<?php echo $brand->user_slug;?>">  
                                    <?  if($brand->user_image=='') 
                                        { ?>
                                            <a href="<?php echo $brand->user_slug;?>"><?php echo $brand->user_company?></a><br/>
                                        <? } else if($brand->user_image=='no.gif') { ?>
                                    <a href="<?php echo $brand->user_slug;?>" style="text-decoration:none; color:#0b8aef;"><?php echo $brand->user_company;?></a><br/>
                                    <? }
                                        else
                                        { $imgurl= 'http://www.hospytek.com/newcrm/UserFiles/Image/'.$brand->user_image;?>
                                    <a href="<?php echo $brand->user_slug;?>"><img src="<?=$imgurl?>" alt="<?=$brand->user_company?>" style="height:70px;margin-top:5px;"/></a><br/>
                                    <? } 
                                ?>
                                   </a>
                                &nbsp;&nbsp;<span><?php echo $brand->user_city;?>, <?php echo $brand->user_state;?> <?php echo $brand->user_country;?></span></h2>
                        </div>
                              
                            </li>
                           
                            &nbsp;&nbsp;
                            
                            <li style="padding-top:10px" ><a href="<?php echo $brand->user_slug;?>" >Home</a></li>
                            <li style="padding-top:10px"><a href="<?php echo $brand->user_slug;?>#about">About Us</a></li>
                            <li style="padding-top:10px"><a href="<?php echo $brand->user_slug;?>#product">Products</a></li>
                            <li style="padding-top:10px"><a href="<?php echo $brand->user_slug;?>#contact">CONTACT</a></li>
                            <li style="padding-top:10px"><a data-toggle="modal" href="#modal-callback" class="btn-sm">Become a Dealer</a></li>
                        
                        </ul>
                 </div>
                    <!--/.navbar-header-->
                    <!--/.navbar-collapse-->
                </nav>
                <!--/.navbar-->
            </div>
        </div>
        <!-- Area for Slider -->
        <section id="slider" style="margin-top:none"><!--slider-->
				<div class="container">
					<div class="row">
						<div class="col-sm-12">
						
							<div id="slider-carousel" class="carousel slide" data-ride="carousel">
								<ol class="carousel-indicators">
									<li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
									<li data-target="#slider-carousel" data-slide-to="1"></li>
									<li data-target="#slider-carousel" data-slide-to="2"></li>
								</ol>
								
								<div class="carousel-inner">
								
									
								
								
								 <?php $i=0; foreach ($items as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;} ;
								  if($i>6){ break;}
								  ?>				
								  <?php if ($i==0) { $i++; ?>
							  <div class="item active"> 
										<div class="col-sm-6" style="margin-top:150px;">
											<h1><?php echo $rw->p_title." ".$rw->vr_name;?></h1>
											<h3 ><strong>Model:</strong> <?php echo $rw->p_model;?></h3>
											<p><?php echo $rw->p_sdesc;?></p>
											
											<button type="button" class="btn btn-default get"><a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online">View Detail</a></button>
											<button type="button" class="btn btn-default get"><a href="<?php echo '../pdf/'.$rw->p_slug.'.pdf'?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Brochure Manual Download">View Brochure</a></span></button>
										</div>
										<div class="col-sm-6">
											<img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="girl img-responsive" alt="" />
											
										</div>
									</div>
									<?php } else  {  ?>
									
									<div class="item"> 
										<div class="col-sm-6" style="margin-top:150px;">
											<h1><?php echo $rw->p_title." ".$rw->vr_name;?></h1>
											<h3><strong>Model:</strong> <?php echo $rw->p_model;?></h3>
											<p><?php echo $rw->p_sdesc;?></p>
											
											<button type="button" class="btn btn-default get"><a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online">View Detail</a></button>
											<button type="button" class="btn btn-default get"><a href="<?php echo '../pdf/'.$rw->p_slug.'.pdf'?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Brochure Manual Download">View Brochure</a></span></button>
										</div>
										<div class="col-sm-6">
											<img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="girl img-responsive" alt="" />
											
										</div>
									</div>
									<?php  } ?>
									
									
									
									<?php  } ?>
									
									
									
								</div>
								
								<a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
									<i class="fa fa-angle-left"></i>
								</a>
								<a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
									<i class="fa fa-angle-right"></i>
								</a>
							</div>
							
						</div>
					</div>
				</div>
			</section><!--/slider-->
				
        <!-- Area for Slider -->
        
        <!-- Area for about us -->
        <div class="content-top"  id="about">
            <div class="container ">
                <br><br>
                <div class="spec ">
                    <h3 class="head text-center">WELCOME TO <?=$brand->user_company?></h3>
                </div>
                <div class="information-blocks">
                    <div class="row">
                        <div class="col-md-12 information-entry">
                            <div class="information-blocks">
                                <div class="row">
                                    <div class="col-md-12 information-entry">
                                        <div class="article-container">
                                            <p><?=$brand->user_about?></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- content-section-starts-here -->
        <div class="container" id="product">
            <div class="main-content">
                <div class="products-grid">
                    <br>
                    <header>
					
                        <h3 class="head text-center">Our Products</h3>
                    </header>
					<?php $i=0; foreach ($items as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;}  $i++;?>
					<?php if($i==1){echo '<div class="row">';}?>
                    <div class="col-md-4 product simpleCart_shelfItem text-center">
                        <a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>"><img src="<?php echo $this->config->item('img_url').$img;?>" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" /></a>
                        <div class="mask">
                            <a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>"><button class="btn-primary">Quick View</button></a>
                        </div>
                        <a class="product_name" href="#"><?php echo $rw->p_title." ".$rw->vr_name;?></a>
                        <p><span class="item_price">Model <span><?php echo $rw->p_model;?></span></span></p>
                    </div>
					<?php if($i==3){ $i=0; echo '</div><div class="clearfix">&nbsp; </div>';}?>
					<? }?>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
        <div class="other-products" id="contact">
            <div class="container">
                <h3 class="like text-center"><?=$brand->user_company?></h3>
                <!-- Area for Contact us -->
                <br>          
                <div>
                    <div class="container" id="contact">
                        <form action="#">
                            <div class="row">
                                <div class="col-sm-6">
                                    <div class="form-group">
                                        <label>Full Name</label>
                                        <input type="text" class="form-control" name="name" placeholder="Full Name">
                                    </div>
                                    <div class="form-group">
                                        <label>Email address</label>
                                        <input type="email" class="form-control" name="email" placeholder="Email">
                                    </div>
                                    <div class="form-group">
                                        <label>Message</label>
                                        <textarea class="form-control" name="message" placeholder="Your message here"></textarea>
                                    </div>
                                </div>
                                <div class="col-sm-6">
                                    <p><?=$brand->user_address?></p>
									<p><?=$brand->user_city?>, <?=$brand->user_state?></p>
									<p><?=$brand->user_country?>  <?=$brand->user_postcode?></p>
									<p>+91-<?=$brand->user_mobile?>     <?=$brand->user_phone?></p>
                                    <div><img src="" />Map Area</div>
                                </div>
                            </div>
                            <br>
                            <div class="row">
                                <div class="col-sm-6">
                                    <button type="submit" class="btn btn-primary hvr-sweep-to-right" style="width:100%;">Submit</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
                <script type="text/javascript" src="https://www.hospytek.com/assets/sellers/tpl0021/js/jquery.flexisel.js"></script>
            </div>
        </div>
        <!-- content-section-ends-here -->
        <div class="footer" style="background-color:black;">
        <div class="container">
                <div class="row" style="padding-left:45%">
                    <p style="color:white">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Powered by<br></p><p><a href="http://www.hospytek.com"><img src="<?php echo base_url();?>/assets/images/logo.png" alt="logo" /></a></p>
                    
                </div>
        </div>
		</div>
		
		  <div class="modal fade" id="modal-callback" tabindex="-1" role="dialog" aria-hidden="true">
                  <div class="modal-dialog modal-lg">
                      <div class="modal-content">
                          <div class="modal-header">
                              <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                              <h2 class="modal-title">Please Fill the form & we will call you back</h2>
                          </div>
                          <div class="modal-body">
						  <form method="post" action="<?php echo base_url()."api";?>" class="form-ajax">
						  <input type="hidden" name="module" value="callback" />
						  <input type="hidden" name="type" value="0" />
						  <div class="row">
						  <div class="col-md-6">
						  	<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Name <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input" placeholder="" name="name" required>
							</div>

							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Phone <span>*</span></label>
		    <input type="phone" class="form-control unicase-form-control text-input"  placeholder=""  name="mobile" required>
							</div>
							
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Email <span>*</span></label>
		    <input type="email" class="form-control unicase-form-control text-input"  placeholder=""  name="email" required>
						   </div>	
							
							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Best time to call you <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input"  placeholder="" name="timetocall" required>
						   </div>	


							<div class="form-group">
						   <label class="info-title" for="exampleInputName">Your Message <span>*</span></label>
		    <input type="text" class="form-control unicase-form-control text-input"  placeholder=""  name="message" required>
						   </div>	


	<div class="form-group">
		<button type="submit" class="btn-upper btn btn-primary checkout-page-button">Submit Request</button>
	</div>
			
						  </div>

						  <div class="col-md-6" style="font-size:12px;">
						   <p><strong>Lets give you a call back to help you buy the most suitable Medical Equipment & Device as per your requirement, We would not only help you choose the most suitable medical device but also help you find the right localised vendor to make your after sale requirements more readily available.</strong></p>
						  <center><img src="assets/images/callback-icon.png" class="img-responsive" style="max-height:175px;" /></center>
						  <p>We at Hospytek hold immense value for every customer or prospective customer and consider it as our utmost duty to provide a solution to all the queries raised by the customers.</p>

<p>We thank you for filling the form and assure you that one of our sales executives would get back to you very soon to understand your problem and provide the most reasonable solution.</p>

<p><strong>We again thank you for your patience and hope you are your experience on our website.</strong></p>
						  </div>
						  
                          </div>
						  </form>
                      </div>
                  </div>
				  </div>
              </div>
    </body>
</html>

