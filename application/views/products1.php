<?php $this->view('common/header'); ?>
<!-- ============================================== HEADER : END ============================================== -->
<div class="breadcrumb">
  <div class="container">
    <div class="breadcrumb-inner">
      <ul class="list-inline list-unstyled">
        <li><a href="<?php echo base_url();?>">Home</a>         <? if($category->cat_id>0){?> <i class="fa fa-angle-double-right"></i> <a href="<?=$category->cat_slug?>"><?=ucwords(strtolower(trim($category->cat_name)))?></a><? }?>
		<? if($scategory->cat_id>0){?> <i class="fa fa-angle-double-right"></i> <a href="<?=$category->cat_slug.'/'.$scategory->cat_slug?>"><?=ucwords(strtolower(trim($scategory->cat_name)))?> Medical Equipment & Devices</a><? }?>
		<? if($variation->vr_id>0){?> <i class="fa fa-angle-double-right"></i> <a href="<?=$category->cat_slug.'/'.$scategory->cat_slug.'/'.$variation->vr_slug?>"><?=ucwords(strtolower(trim($variation->vr_name)))?> Medical Equipment & Devices</a><? }?>
</li>
      </ul>
    </div>
    <!-- /.breadcrumb-inner --> 
  </div>
  <!-- /.container --> 
</div>
<!-- /.breadcrumb -->
<div class="body-content outer-top-xs">
  <div class='container'>
    <div class='row'>
      <div class='col-md-3 sidebar'> 
        <!-- ================================== TOP NAVIGATION : END ================================== -->
        <div class="sidebar-module-container">
          <div class="sidebar-filter"> 
            <!-- ============================================== SIDEBAR CATEGORY ============================================== -->
            <div class="sidebar-widget sidebar-widget-listing wow fadeInUp">
            <? if($scategory->cat_id==''){?>
               <div class="widget-header"><h3 class="section-title">Filter by Category</h3></div>
			  <div class="scrolling">
              <div class="sidebar-widget-body">
                <div class="accordion">
                  <?php  foreach($cats as $rw){?>				
				  <div class="accordion-group">
                    <div class="accordion-heading"><a href="#acord<?=$rw['cat_id']?>" data-toggle="collapse" class="accordion-toggle collapsed"><?=$rw['cat_name']?></a></div>
                    <!-- /.accordion-heading -->
                    <div class="accordion-body collapse" id="acord<?=$rw['cat_id']?>" style="height: 0px;">
                      <div class="accordion-inner">
                        <ul>
                          <?php foreach ($rw['vars'] as $vrw){?>	
						  <li><a href="<?php echo base_url().$vrw->catslug."/".$vrw->cat_slug."/".$vrw->vr_slug;?>"><?=$vrw->vr_name?></a> <input type="checkbox" value="<?php echo $vrw->vr_id?>" name="vars[]" class="pull-right"></li>
						  <? }?>
                        </ul>
                      </div>
                      <!-- /.accordion-inner --> 
                    </div>
                    <!-- /.accordion-body --> 
                  </div>
                  <!-- /.accordion-group -->
				  <?php }?>
                </div>
                <!-- /.accordion --> 
              </div>
			  </div>
              <!-- /.sidebar-widget-body --> 
			<? }else{?>  
               <div class="widget-header"><h3 class="section-title">Filter by Variations</h3></div>
  			  <div class="scrolling">
              <div class="sidebar-widget-body"> 
			  		<ul class="list">
                  <?php  foreach($vars as $rw){?>				
                          <li><a href="<?php echo base_url().$rw->catslug."/".$rw->cat_slug."/".$rw->vr_slug;?>"><?=$rw->vr_name?></a> <input type="checkbox" value="<?php echo $rw->vr_id?>" name="vars[]" class="pull-right"></li>
				  <?php }?>
				  </ul>
              </div>
			  </div>
              <!-- /.sidebar-widget-body --> 			
			<? }?>
            </div>           
          </div>
          <!-- /.sidebar-filter --> 
		  <div class="clearfix ">&nbsp;</div>
		
		<div class="sidebar-filter"> 
            <div class="sidebar-widget sidebar-widget-listing wow fadeInUp">
              <div class="widget-header">
			  <h3 class="section-title">Filter by Brands</h3>
              </div>
  			  <div class="scrolling scrolling-x">
              <div class="sidebar-widget-body">
                <ul class="list">
                  <?php foreach ($brands as $rw){?>				
				  <li><a href="<?php echo base_url()?><?php echo $rw->user_slug."_".$category->cat_slug; if($scategory->cat_id>0){ echo "/".$rw->user_slug."_".$scategory->cat_slug;} if($variation->vr_id>0){ echo "/".$variation->vr_slug;}?>"><?php echo $rw->user_company?></a> <input type="checkbox" value="<?php echo $rw->id?>" name="brand[]" class="pull-right"></li>
				  <?php } ?>
                </ul>
              </div>
			  </div>
              <!-- /.sidebar-widget-body --> 
            </div>
            <!-- /.sidebar-widget --> 
            <!-- ============================================== COLOR: END ============================================== --> 
            <!-- /.sidebar-widget --> 
          </div>		  
        </div>
        <!-- /.sidebar-module-container --> 
      </div>
      <!-- /.sidebar -->
      <div class='col-md-9'> 
        <!-- ========================================== SECTION – HERO ========================================= -->
        
        <div id="category" class="category-carousel hidden-xs">
          <div class="item">
            <div class="image"><img src="assets/images/banners/cat-banner-1.jpg" alt="" class="img-responsive"> </div>
            <div class="container-fluid">
              <div class="caption vertical-top text-left">
                <div class="big-text"> Big Sale </div>
                <div class="excerpt hidden-sm hidden-md"> SAVE UPTO 49%</div>
                <div class="excerpt-normal hidden-sm hidden-md"> on all medical equipments in this festive seasons.</div>
              </div>
              <!-- /.caption --> 
            </div>
            <!-- /.container-fluid --> 
          </div>
        </div>
        
     
        <div class="clearfix filters-container m-t-10" style="padding-bottom:10px;">
          <div class="row">
            <div class="col col-sm-6 col-md-2">
              <div class="filter-tabs">
                <ul id="filter-tabs" class="nav nav-tabs nav-tab-box nav-tab-fa-icon">
                  <li class="active"> <a data-toggle="tab" href="#grid-container"><i class="icon fa fa-th-large"></i>Grid</a> </li>
                  <li><a data-toggle="tab" href="#list-container"><i class="icon fa fa-th-list"></i>List</a></li>
                </ul>
              </div>
              <!-- /.filter-tabs --> 
            </div>
            <!-- /.col -->
            <div class="col col-sm-12 col-md-6">
              <div class="col col-sm-3 col-md-6 no-padding">
                <!-- /.lbl-cnt --> 
              </div>
              <!-- /.col -->
              <div class="col col-sm-3 col-md-6 no-padding">
                
                <!-- /.lbl-cnt --> 
              </div>
              <!-- /.col --> 
            </div>
            <!-- /.col -->
            <div class="col col-sm-6 col-md-4 text-right">
                <div class="lbl-cnt"> <span class="lbl">Sort by</span>
                  <div class="fld inline">
                    <div class="dropdown dropdown-small dropdown-med dropdown-white inline">
                      <button data-toggle="dropdown" type="button" class="btn dropdown-toggle"> Position <span class="caret"></span> </button>
                      <ul role="menu" class="dropdown-menu">
                        <li role="presentation"><a href="#">position</a></li>
                        <li role="presentation"><a href="#">Price:Lowest first</a></li>
                        <li role="presentation"><a href="#">Price:HIghest first</a></li>
                        <li role="presentation"><a href="#">Product Name:A to Z</a></li>
                      </ul>
                    </div>
                  </div>
                  <!-- /.fld --> 
                </div>              
            </div>
            <!-- /.col --> 
          </div>
          <!-- /.row --> 
        </div>
        <div class="search-result-container ">
          <div id="myTabContent" class="tab-content category-list">
            <div class="tab-pane active " id="grid-container">
              <div class="category-product">
                <div class="row" id="results">
				  <?php foreach ($items as $rw){ if($rw->p_image=='' || $rw->p_image==null){$img='no-img.jpg';}else{ $img=$rw->p_image;}?>				
                  <div class="col-sm-6 col-md-4 wow fadeInUp prod-col">
                    <div class="products">
                      <div class="product">
                        <div class="product-image">
                          <div class="image"> <a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>"><img  src="<?php echo $this->config->item('img_url').$img;?>&w=250" alt="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>"></a> </div>
                          <!-- /.image -->
                          
                          <div class="tag hot"><span>hot</span></div>
                        </div>
                        <!-- /.product-image -->
                        
                        <div class="product-info text-center">
                          <h3 class="name"><a href="<?php echo $rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="<?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?> Buy Online"><?php echo $rw->p_title." ".$rw->vr_name;?></a></h3>
<div style="text-align:center"><? /* if($rw->p_price>0){ echo '<span style="text-decoration:line-through; color:#999999"><i class="fa fa-inr"></i>'.$rw->p_mrp.'</span> &nbsp; &nbsp; <i class="fa fa-inr"></i>'.$rw->p_price; }else{ */ echo 'Price: <b>On Request</b>';//}?></div>
                          <div class="description"><?php echo $rw->p_model;?></div>
                          <div class="product-price"><a href="<?php echo $rw->user_slug;?>" title="<?php echo $rw->user_company;?> - <?php echo $rw->user_city;?> <?php echo $rw->user_state;?> Medical Equipment  Device Manufacturer"><?php echo $rw->user_company;?></a></div>
                          <!-- /.product-price --> 
                          
                        </div>
                        <!-- /.product-info -->

						<div class="row btnrow">
							<div class="col-md-12 text-center">                          
							 <a href="<?php echo base_url().$rw->catslug.'/'.$rw->cat_slug.'/'.$rw->vr_slug.'/'.$rw->p_slug?>" title="view details of <?php echo $rw->user_company." ".$rw->p_title." ".$rw->vr_name." ".$rw->p_model;?>" class="btn btn-sm btn-details" >View Details</a>				   <a href="javascript:;" class="btn btn-quote" pid="<?=$rw->p_id?>" pname="<?php echo $rw->p_title." ".$rw->vr_name;?>">Inquire Now</a>						  
							</div>
						</div> 
						
                        <!-- /.cart --> 
                      </div>
                      <!-- /.product --> 
                      
                    </div>
                    <!-- /.products --> 
				 
                    
                    <!-- /.products --> 
                  </div>
                  <!-- /.item -->
                   <?php }?>
				   
                </div>
				<div id="loadmore"></div>
                <!-- /.row --> 
              </div>
              <!-- /.category-product --> 
              
            </div>
            <!-- /.tab-pane -->
          </div>
          <!-- /.tab-content -->
        </div>
        <!-- /.search-result-container --> 
        
      </div>
      <!-- /.col --> 
    </div>
    <!-- /.row --> 
    <!-- ============================================== BRANDS CAROUSEL ============================================== -->
    <div id="brands-carousel" class="logo-slider wow fadeInUp">
      <div class="logo-slider-inner">
        <div id="brand-slider" class="owl-carousel brand-slider custom-carousel owl-theme">
          <div class="item m-t-15"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand1.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item m-t-10"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand2.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand3.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand4.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand5.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand6.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand2.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand4.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand1.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item-->
          
          <div class="item"> <a href="#" class="image"> <img data-echo="assets/images/brands/brand5.png" src="assets/images/blank.gif" alt=""> </a> </div>
          <!--/.item--> 
        </div>
        <!-- /.owl-carousel #logo-slider --> 
      </div>
      <!-- /.logo-slider-inner --> 
      
    </div>
    <!-- /.logo-slider --> 
    <!-- ============================================== BRANDS CAROUSEL : END ============================================== --> </div>
  <!-- /.container --> 
  
</div>
<!-- /.body-content --> 
<script>
var loaded_items='';
$(document).on('click', 'input[name="brand[]"], input[name="vars[]"]', function (e) {
			load_products(0);  
});

function load_products(paging){
var k=0; var p=0;

		var  cat = <?=(int)$category->cat_id?>;
		var  subcat = <?=(int)$scategory->cat_id?>;
		var  listvrid = <?=(int)$variation->vr_id?>;
		var  listbrand = <?=(int)$brand->id?>;

	 	var brand = [];
        $('input:checkbox[name="brand[]"]:checked').each(function(i){
          brand[i] = $(this).val(); p = p+1;
        });
		if(brand.length==0){ brand =listbrand;}

	 	var vrid = [];
        $('input:checkbox[name="vars[]"]:checked').each(function(i){
          vrid[i] = $(this).val(); p=p+1;
        });
		if(vrid.length==0){ vrid =listvrid;}
		if(paging>0){var items = $('div.prod-col').size(); }else{ var items =0; }
		console.log('p='+p);
		if(p>0 || paging>0){
		$.ajax({
				type : "POST",
				url : '<?php echo base_url()."api";?>',
				dataType : "html",
				data : {key : $('#key').val(), cat : cat, subcat : subcat, brand : brand, vrid : vrid, module : 'products', start: items},
				beforeSend : function() {
					if(paging>0){	
					$("#loadmore").html('<div class="loading"><img src="assets/images/loading.gif"><br /><strong>Please wait!</strong> While we load more products.</div>');
					}else{
					$("#loadmore").html('');
	$("#results").html('<div class="loading"><img src="assets/images/loading.gif"><br /><strong>Please wait!</strong> While we load products relavant to your search terms.</div>');						
					}
				},
				success : function(response) {
					if(paging!=1){$("#results").html('');}
					var data = $.parseJSON(response);
					$.each(data, function(i, field){ k = k+1; 					
					/*if(field.price>0){ var price = '<span style="text-decoration:line-through; color:#999999"><i class="fa fa-inr"></i>'+field.mrp+'</span> &nbsp; &nbsp; <i class="fa fa-inr"></i>'+field.price; }else{*/ var price = 'Price: <b>On Request</b>'; //}
            		$("#results").append('<div class="col-sm-6 col-md-4 wow fadeInUp prod-col"><div class="products"><div class="product"><div class="product-image"><div class="image"><a href="'+ field.slug + '"><img  src="<?php echo $this->config->item('img_url');?>'+ field.image +'&w=250" alt="'+ field.brand + ' ' +field.label+ ' '  +field.varname+ ' ' +field.model+ ' Suppliers Importers Dealers Vendors Manufacturers in India"></a></div><div class="tag hot"><span>hot</span></div></div><div class="product-info text-center"><h3 class="name"><a href="'+ field.slug + '" title="'+ field.brand + ' ' +field.label+ ' '  +field.varname+ ' ' +field.model+ ' Suppliers Importers Dealers Vendors Manufacturers in India">'+ field.label + ' ' +field.varname+ '</a></h3><div style="text-align:center">'+price+'</div><div class="description">'+ field.model + '</div><div class="product-price"><a href="' +field.brandslug+ '">'+ field.brand + '</a></div></div><div class="row btnrow"><div class="col-md-12 text-center"><a href="'+ field.slug + '" class="btn btn-sm btn-details" >View Details</a> &nbsp; <a href="javascript:;" class="btn btn-quote" pid="'+ field.brand + '" pname="'+ field.label + ' ' +field.varname+ '">Inquire Now</a></div></div></div></div></div>');
					$("#loadmore").html('');
        			});
					 jQuery('.rating').rateit({max: 5, step: 1, value : 4, resetable : false , readonly : true});
					if(p==0){ loaded_items = $("#results").html(); }		
					if(k==0){
						if(paging>0){					
						$("#loadmore").html('<div class="loading">There is no more items found!</div>');
						}else{
						$("#loadmore").html('');
						$("#results").html('<div class="loading">There is no more items found!</div>');						
						}	
					}
					
				}
		});
		}else{ $("#results").html(loaded_items); }
		
}		

$(window).scroll(function() {
	if($('#footer').offset().top < $(this).height() + $(this).scrollTop() && $("#loadmore").html()!='<div class="loading">There is no more items found!</div>' && $("#result").html()!='<div class="loading">There is no more items found!</div>' && $("#result").html()!='<div class="loading"><img src="assets/images/loading.gif"><br /><strong>Please wait!</strong> While we load products relavant to your search terms.</div>'){
//	alert('heelo');
	load_products(1);
	}
});	

    $(window).load(function(){
	 setTimeout(function(){	
	    $('.quote-title').html('Are you looking for <b> <? if($scategory->cat_name==''){ echo trim($category->cat_name); }?>  - <?=trim($scategory->cat_name)?>  <?=trim($variation->vr_name)?>?</b>');  $('.mtype').hide(); $('.vpname').show(); $('#pid').val('0');
		$('#modal-quote').modal('show'); 
	 }, 3000);	
    });	
</script>
<?php $this->view('common/footer'); ?>